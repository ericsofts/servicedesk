<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
        ],

        'http_client' => [
            'driver' => 'single',
            'path' => storage_path('logs/http_client.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'after-request' => [
            'driver' => 'single',
            'path' => storage_path('logs/requests.log'),
            'level' => 'debug',
        ],

        'payment_invoice' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoice.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'api3_chatex_lbc_checker' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_chatex_lbc_checker.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'chatex_lbc_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/chatex_lbc_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'api3_chatex_lbc_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/api3_chatex_lbc_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'payment_invoice_server_response' => [
            'driver' => 'single',
            'path' => storage_path('logs/payment_invoice_server_response.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'callback_chatex' => [
            'driver' => 'single',
            'path' => storage_path('logs/callback_chatex.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'balance_queue' => [
            'driver' => 'single',
            'path' => storage_path('logs/balance_queue.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],
        'sd_chatex_lbc_requests' => [
            'driver' => 'single',
            'path' => storage_path('logs/sd_chatex_lbc_requests.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],
    ],

];
