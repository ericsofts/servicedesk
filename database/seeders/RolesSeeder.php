<?php

namespace Database\Seeders;

use App\Models\SdPermission;
use App\Models\SdRole;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions['dashboard'] = SdPermission::firstOrCreate(['slug' => 'dashboard', 'name' => 'dashboard']);
        $permissions['settings'] = SdPermission::firstOrCreate(['slug' => 'settings', 'name' => 'settings']);
        $permissions['sd_user.profile'] = SdPermission::firstOrCreate(['slug' => 'sd_user.profile', 'name' => 'sd_user profile']);
        $permissions['sd_user.profile.update'] = SdPermission::firstOrCreate(['slug' => 'sd_user.profile.update', 'name' => 'sd_user profile update']);

        $permissions['users.index'] = SdPermission::firstOrCreate(['slug' => 'user.index', 'name' => 'User index']);
        $permissions['users.view'] = SdPermission::firstOrCreate(['slug' => 'user.view', 'name' => 'User view']);
        $permissions['user.withdrawal.index'] = SdPermission::firstOrCreate(['slug' => 'user.withdrawal.index', 'name' => 'User Withdrawal Index']);

        $permissions['merchant.index'] = SdPermission::firstOrCreate(['slug' => 'merchant.index', 'name' => 'Merchant index']);
        $permissions['merchant.view'] = SdPermission::firstOrCreate(['slug' => 'merchant.view', 'name' => 'Merchant view']);
        $permissions['merchant.new'] = SdPermission::firstOrCreate(['slug' => 'merchant.new', 'name' => 'Merchant new']);
        $permissions['merchant.edit'] = SdPermission::firstOrCreate(['slug' => 'merchant.edit', 'name' => 'Merchant edit']);

        $permissions['agent.index'] = SdPermission::firstOrCreate(['slug' => 'agent.index', 'name' => 'agent.index']);
        $permissions['agent.view'] = SdPermission::firstOrCreate(['slug' => 'agent.view', 'name' => 'agent.view']);
        $permissions['agent.new'] = SdPermission::firstOrCreate(['slug' => 'agent.new', 'name' => 'agent.new']);
        $permissions['agent.edit'] = SdPermission::firstOrCreate(['slug' => 'agent.edit', 'name' => 'agent.edit']);
        $permissions['merchant.ads_selector'] = SdPermission::firstOrCreate(['slug' => 'merchant.ads_selector', 'name' => 'Merchant Ads Selector']);


        $permissions['trader.index'] = SdPermission::firstOrCreate(['slug' => 'trader.index', 'name' => 'Trader index']);
        $permissions['trader.view'] = SdPermission::firstOrCreate(['slug' => 'trader.view', 'name' => 'Trader view']);
        $permissions['trader.new'] = SdPermission::firstOrCreate(['slug' => 'trader.new', 'name' => 'Trader new']);
        $permissions['trader.edit'] = SdPermission::firstOrCreate(['slug' => 'trader.edit', 'name' => 'Trader edit']);
        $permissions['trader.ads'] = SdPermission::firstOrCreate(['slug' => 'trader.ads', 'name' => 'Trader Ads']);
        $permissions['trader.deal'] = SdPermission::firstOrCreate(['slug' => 'trader.deal', 'name' => 'Trader Deal']);
        $permissions['traders'] = SdPermission::firstOrCreate(['slug' => 'traders', 'name' => 'Trader Section']);

        $permissions['trader_currency.index'] = SdPermission::firstOrCreate(['slug' => 'trader_currency.index', 'name' => 'Trader currencies']);
        $permissions['trader_currency.new'] = SdPermission::firstOrCreate(['slug' => 'trader_currency.new', 'name' => 'Trader currency new']);
        $permissions['trader_currency.view'] = SdPermission::firstOrCreate(['slug' => 'trader_currency.view', 'name' => 'Trader currency view']);
        $permissions['trader_currency.edit'] = SdPermission::firstOrCreate(['slug' => 'trader_currency.edit', 'name' => 'Trader currency edit']);

        $permissions['trader_fiat.index'] = SdPermission::firstOrCreate(['slug' => 'trader_fiat.index', 'name' => 'Trader fiats']);
        $permissions['trader_fiat.new'] = SdPermission::firstOrCreate(['slug' => 'trader_fiat.new', 'name' => 'Trader fiats new']);
        $permissions['trader_fiat.view'] = SdPermission::firstOrCreate(['slug' => 'trader_fiat.view', 'name' => 'Trader fiat view']);
        $permissions['trader_fiat.edit'] = SdPermission::firstOrCreate(['slug' => 'trader_fiat.edit', 'name' => 'Trader fiat edit']);

        $permissions['trader_payment_system'] = SdPermission::firstOrCreate(['slug' => 'trader_payment_system.index', 'name' => 'Trader payment systems']);
        $permissions['trader_payment_system.new'] = SdPermission::firstOrCreate(['slug' => 'trader_payment_system.new', 'name' => 'Trader payment system new']);
        $permissions['trader_payment_system.view'] = SdPermission::firstOrCreate(['slug' => 'trader_payment_system.view', 'name' => 'Trader payment system view']);
        $permissions['trader_payment_system.edit'] = SdPermission::firstOrCreate(['slug' => 'trader_payment_system.edit', 'name' => 'Trader payment system edit']);

        $permissions['input_invoice.index'] = SdPermission::firstOrCreate(['slug' => 'input_invoice.index', 'name' => 'input_invoice index']);
        $permissions['input_invoice.view'] = SdPermission::firstOrCreate(['slug' => 'input_invoice.view', 'name' => 'input_invoice view']);
        $permissions['payment_invoice.index'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.index', 'name' => 'payment_invoice index']);
        $permissions['payment_invoice.view'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.view', 'name' => 'payment_invoice view']);
        $permissions['payment_invoice.set_status_payed'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.set_status_payed', 'name' => 'payment_invoice.set_status_payed']);
        $permissions['payment_invoice.set_status_revert'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.set_status_revert', 'name' => 'payment_invoice.set_status_revert']);
        $permissions['payment_invoice.resend_callback'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.resend_callback', 'name' => 'payment_invoice resend_callback']);
        $permissions['payment_invoice.change_amount'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.change_amount', 'name' => 'payment_invoice change_amount']);
        $permissions['balance_history.index'] = SdPermission::firstOrCreate(['slug' => 'balance_history.index', 'name' => 'balance_history index']);
        $permissions['balance_history.view'] = SdPermission::firstOrCreate(['slug' => 'balance_history.view', 'name' => 'balance_history view']);

        $permissions['withdrawal.index'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.index', 'name' => 'withdrawal.index']);
        $permissions['withdrawal.view'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.view', 'name' => 'withdrawal.view']);
        $permissions['withdrawal.set_status_payed'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.set_status_payed', 'name' => 'withdrawal.set_status_payed']);
        $permissions['withdrawal.resend_callback'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.resend_callback', 'name' => 'withdrawal resend_callback']);
        $permissions['withdrawal.change_amount'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.change_amount', 'name' => 'withdrawal change_amount']);
        $permissions['withdrawal.attachment'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.attachment', 'name' => 'withdrawal.attachment']);

        $permissions['chatex'] = SdPermission::firstOrCreate(['slug' => 'chatex', 'name' => 'chatex']);
        $permissions['chatex.send_message'] = SdPermission::firstOrCreate(['slug' => 'chatex.send_message', 'name' => 'chatex send_message']);
        $permissions['chatex.send_dispute'] = SdPermission::firstOrCreate(['slug' => 'chatex.send_dispute', 'name' => 'chatex send_dispute']);
        $permissions['chatex.dashboard'] = SdPermission::firstOrCreate(['slug' => 'chatex.dashboard', 'name' => 'chatex dashboard']);
        $permissions['chatex.contact'] = SdPermission::firstOrCreate(['slug' => 'chatex.contact', 'name' => 'chatex contact']);
        $permissions['chatex.recent_messages'] = SdPermission::firstOrCreate(['slug' => 'chatex.recent_messages', 'name' => 'chatex recent_messages']);
        $permissions['chatex.image'] = SdPermission::firstOrCreate(['slug' => 'chatex.image', 'name' => 'chatex image']);
        $permissions['chatex.ads'] = SdPermission::firstOrCreate(['slug' => 'chatex.ads', 'name' => 'chatex ads']);
        $permissions['chatex.ads_buy'] = SdPermission::firstOrCreate(['slug' => 'chatex.ads_buy', 'name' => 'chatex ads buy']);

        $permissions_sd['chatex.wallet.history_24'] = SdPermission::firstOrCreate(['slug' => 'chatex.wallet.history_24', 'name' => 'chatex wallet history_24']);

        $role_support = SdRole::firstOrCreate([
            'slug' => 'support1l',
            'name' => 'Support1l'
        ]);

        $role_support->permissions()->detach();
        $attach = [];

        $permissions_sd = array_merge($permissions_sd, $permissions);
        foreach ($permissions_sd as $permission){
            $attach[] = $permission->id;
        }
        $role_support->permissions()->attach($attach);

        $role_support1 = SdRole::firstOrCreate([
            'slug' => 'support2l',
            'name' => 'Support2l'
        ]);

        $role_support1->permissions()->detach();
        $attach = [];

        $permissions['agent.withdrawal.index'] = SdPermission::firstOrCreate(['slug' => 'agent.withdrawal.index', 'name' => 'agent.withdrawal.index']);
        $permissions['agent.withdrawal_invoice.create'] = SdPermission::firstOrCreate(['slug' => 'agent.withdrawal_invoice.create', 'name' => 'agent.withdrawal_invoice.create']);
        $permissions['merchant.input_invoice.create'] = SdPermission::firstOrCreate(['slug' => 'merchant.input_invoice.create', 'name' => 'merchant.input_invoice.create']);
        $permissions['merchant.withdrawal_invoice.create'] = SdPermission::firstOrCreate(['slug' => 'merchant.withdrawal_invoice.create', 'name' => 'merchant.withdrawal_invoice.create']);
        $permissions['system.balance.withdrawal_invoice.create'] = SdPermission::firstOrCreate(['slug' => 'system.balance.withdrawal_invoice.create', 'name' => 'system.balance.withdrawal_invoice.create']);
        $permissions['system.balance.withdrawal_invoice.index'] = SdPermission::firstOrCreate(['slug' => 'system.balance.withdrawal_invoice.index', 'name' => 'system.balance.withdrawal_invoice.index']);
        $permissions['service.provider.withdrawal_invoice.create'] = SdPermission::firstOrCreate(['slug' => 'service.provider.withdrawal_invoice.create', 'name' => 'system.balance.withdrawal_invoice.create']);
        $permissions['service.provider.withdrawal_invoice.index'] = SdPermission::firstOrCreate(['slug' => 'service.provider.withdrawal_invoice.index', 'name' => 'system.balance.withdrawal_invoice.index']);
        $permissions['service.new'] = SdPermission::firstOrCreate(['slug' => 'service.new', 'name' => 'Service new']);
        $permissions['service.edit'] = SdPermission::firstOrCreate(['slug' => 'service.edit', 'name' => 'Service edit']);
        $permissions['service.index'] = SdPermission::firstOrCreate(['slug' => 'service.index', 'name' => 'Service index']);
        $permissions['service.view'] = SdPermission::firstOrCreate(['slug' => 'service.view', 'name' => 'Service view']);
        $permissions['trader.balance_operation'] = SdPermission::firstOrCreate(['slug' => 'trader.balance_operation', 'name' => 'Trader balance opearation']);

        $permissions_sd = array_merge($permissions_sd, $permissions);
        foreach ($permissions_sd as $permission){
            $attach[] = $permission->id;
        }
        $role_support1->permissions()->attach($attach);

        $permissions['chatex.wallet'] = SdPermission::firstOrCreate(['slug' => 'chatex.wallet', 'name' => 'chatex wallet']);
        $permissions['chatex.wallet.history'] = SdPermission::firstOrCreate(['slug' => 'chatex.wallet.history', 'name' => 'chatex wallet history']);
        $permissions['system.balances'] = SdPermission::firstOrCreate(['slug' => 'system.balances', 'name' => 'system balances']);
        $permissions['payment_invoice.commission'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.commission', 'name' => 'payment_invoice commission']);
        $permissions['payment_invoice.commission_amount'] = SdPermission::firstOrCreate(['slug' => 'payment_invoice.commission_amount', 'name' => 'payment_invoice commission_amount']);
        $permissions['withdrawal.commission'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.commission', 'name' => 'withdrawal commission']);
        $permissions['withdrawal.commission_amount'] = SdPermission::firstOrCreate(['slug' => 'withdrawal.commission_amount', 'name' => 'withdrawal commission_amount']);
        $permissions['transactions.all_invoices'] = SdPermission::firstOrCreate(['slug' => 'transactions.all_invoices', 'name' => 'all_invoices invoices - admin']);

        $role_admin = SdRole::firstOrCreate([
            'slug' => 'admin',
            'name' => 'Admin'
        ]);
        $role_admin->permissions()->detach();
        $attach = [];
        foreach ($permissions as $permission){
            $attach[] = $permission->id;
        }
        $role_admin->permissions()->attach($attach);

        $permissions1['dashboard'] = SdPermission::firstOrCreate(['slug' => 'dashboard', 'name' => 'dashboard']);
        $permissions1['chatex.balance'] = SdPermission::firstOrCreate(['slug' => 'chatex.balance', 'name' => 'chatex.balance']);
        $permissions1['chatex.transactions.all_invoices'] = SdPermission::firstOrCreate(['slug' => 'chatex.transactions.all_invoices', 'name' => 'chatex.transactions.all_invoices']);
        $permissions1['chatex.agent.index'] = SdPermission::firstOrCreate(['slug' => 'chatex.agent.index', 'name' => 'chatex.agent.index']);
        $permissions1['chatex.merchant.index'] = SdPermission::firstOrCreate(['slug' => 'chatex.merchant.index', 'name' => 'chatex.merchant.index']);
        $permissions1['chatex.merchant.view'] = SdPermission::firstOrCreate(['slug' => 'chatex.merchant.view', 'name' => 'chatex.merchant.view']);

        $role_admin_chatex = SdRole::firstOrCreate([
            'slug' => 'admin-chatex',
            'name' => 'Admin chatex'
        ]);
        $role_admin_chatex->permissions()->detach();
        $attach = [];
        foreach ($permissions1 as $permission){
            $attach[] = $permission->id;
        }
        $role_admin_chatex->permissions()->attach($attach);
    }
}
