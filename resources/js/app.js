import {Modal} from 'bootstrap';
import $ from 'jquery';
import 'daterangepicker'
import moment from 'moment';
import Swal from 'sweetalert2'
import Jqui from './jquery-ui.min'

window.toastr = require('toastr');
window.$ = window.jQuery = $;
window.Swal = Swal;
require('select2')

$(function () {
    $('.btn-reset').on('click', function(){
        $('.select2 option:selected').removeAttr('selected');
        $('.select2').val('').trigger('change');
    })

    $('.select2').select2({
        width: 'resolve',
        theme: 'bootstrap-5',
    })

    var dp_options = {
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker:true,
        timePicker24Hour:true,
        minYear: 1901,
        maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),
        autoUpdateInput: false,
        applyButtonClasses: 'applyBtn btn btn-sm btn-grow',
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss',
        }
    };
    $('.datepicker').each(function(){
        if($(this).val()){
            dp_options.startDate = undefined;
        }else{
            if($(this).attr('name') === 'to_date'){
                dp_options.startDate = moment().endOf('day')
            }else{
                dp_options.startDate = moment().startOf('day')
            }
        }
        $(this).daterangepicker(dp_options, function (start, end, label) {});
    })

    $('.datepicker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss'));
    });
    $('.datepicker').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        // if($(this).attr('name') === 'to_date'){
        //     picker.setStartDate(moment().endOf('day'))
        // }else{
        //     picker.setStartDate(moment().startOf('day'))
        // }

    });

    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            container: 'swal2-grow',
            content: 'text-danger fw-bold',
            confirmButton: 'btn btn-grow',
            cancelButton: 'btn btn-link'
        },
        buttonsStyling: false
    })
    $(document).on("click", ".btn-confirm-default", function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var html = 'If you want to continue write "CONFIRM"?';
        swalWithBootstrapButtons.fire({
            html: html,
            input: 'text',
            inputPlaceholder: 'CONFIRM',
            inputValidator: (value) => {
                if (value !== 'CONFIRM') {
                    return 'You need to write "CONFIRM"!'
                }
            },
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        })
        return false;
    });

    $(document).on("click", ".btn-change-amount", function (e) {
        e.preventDefault();
        var modal = new Modal(document.getElementById('modal-change-amount'))
        modal.show()
        return false;
    });

    $(document).on('submit', 'form#change-amount', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                var myModalEl = document.getElementById('modal-change-amount')
                var modal = Modal.getInstance(myModalEl)
                modal.hide()
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });

    $('body').on('submit', '.form-ajax', function () {
        var xform = $(this);

        var fields = {};
        xform.find('input, select, textarea').each(function (i, el) {
            fields[$(el).attr('name')] = $(el).val();
        });

        $.ajax({
            url: xform.attr('action'),
            type: fields._method ? fields._method : xform.attr('method'),
            data: fields,
            success:function(response) {
                switch(response['alert-type']){
                    case 'info':
                        toastr.info(response.message);
                        break;

                    case 'warning':
                        toastr.warning(response.message);
                        break;

                    case 'success':
                        toastr.success(response.message);
                        break;

                    case 'error':
                        toastr.error(response.message);
                        break;
                }
            }
        });
        return false;
    });

    function availableProvidersSync() {
        var arr = {};
        $('#available_providers').find('[data-id]').each(function (i, el) {
            arr[el.getAttribute('data-id')] = {
                priority: i
            };
        });
        $('[name="available_providers_ids"]').val(JSON.stringify(arr));
    }

    $( "#maybe_providers, #available_providers" ).sortable({
        connectWith: ".connectedSortable",
        stop: function () {
            availableProvidersSync();
        }
    }).disableSelection();

    availableProvidersSync();

    //
    var pbtInp = $('#priority_banks_traders');
    var bpCont = $('.bank-priority-container');
    var bpAddCont = $('.bank-priority-added-container');

    function pbtRender() {
        var pbt = JSON.parse(pbtInp.val());

        console.log(pbt);

        bpAddCont.html('');
        for (var i in pbt) {
            var row = $('#bank_priority_sample').clone(false);
            row.show();
            row.find('[name="bank"]').val(i);
            row.find('[name="traders"]').val(pbt[i]);
            row.find('[data-key]').attr('data-key', i);
            row.removeAttr('id');
            bpAddCont.append(row);
        }
    }

    pbtInp.each(function () {
        pbtRender();
    });

    bpCont.find('[data-action="add"]').on('click', function () {
        var pbt = JSON.parse(pbtInp.val());

        var key = bpCont.find('[name="bank"]').val();
        var value = bpCont.find('[name="traders"]').val().split(',');

        pbt[key] = value;
        pbtInp.val(JSON.stringify(pbt))

        pbtRender();
    });


    bpAddCont.on('click', '[data-action="del"]', function () {
        var pbt = JSON.parse(pbtInp.val());

        var key = $(this).attr('data-key');
        delete pbt[key];
        pbtInp.val(JSON.stringify(pbt))

        pbtRender();
    });

    bpAddCont.on('change', 'input', function () {
        var pbt = {};

        bpAddCont.find('> .row').each(function () {
            var key = $(this).find('[name="bank"]').val();
            var value = $(this).find('[name="traders"]').val().split(',');

            pbt[key] = value;
        });

        pbtInp.val(JSON.stringify(pbt))
    });

    function limitByMerchant(){
        var limitJson = $('#limit_banks_traders');
        var limitCont = $('.bank-limit-container');
        var limitAddCont = $('.bank-limit-added-container');
        var defaultValue = '__DEF__';
        var currentProvider;
        var currentCurrency;
        var isDefaultExist;
        var avalableCurrency;
        var providerAvalable;

        function deleteByVal(obj, val) {
            for (var key in obj) {
                if (obj[key] == val) delete obj[key];
            }
            return obj;
        }

        function initJson(){
            var limitData = JSON.parse(limitJson.val());
            providerAvalable = JSON.parse($('#provider_avalable').val());
            $.each(limitData, function (provider, currencies){
                currentProvider = provider;
                avalableCurrency = JSON.parse($('#provider_currencies_'+provider.replace(" ", "_")).val());
                $.each(currencies, function (currency, banks){
                    currentCurrency = currency;
                    isDefaultExist = false;
                    $.each(banks, function (bank, invoice){
                        if(bank === defaultValue){
                            isDefaultExist = true;
                        }
                    })
                    if(!isDefaultExist){
                        var record = {[currentProvider]: {[currentCurrency]: {[defaultValue]:{'payment_invoice':{'max':'', 'min':''},'withdrawal_invoice':{'max':'', 'min':''}}}}}
                        limitData = MergeRecursive(limitData, record);
                    }
                    avalableCurrency = deleteByVal(avalableCurrency, currency);
                })
                $.each(avalableCurrency, function (index, value){
                    var record = {[currentProvider]: {[value]: {[defaultValue]:{'payment_invoice':{'max':'', 'min':''},'withdrawal_invoice':{'max':'', 'min':''}}}}}
                    limitData = MergeRecursive(limitData, record);
                })
                providerAvalable = deleteByVal(providerAvalable, provider);
            })
            $.each(providerAvalable, function (index, provider){
                currentProvider = provider;
                avalableCurrency = JSON.parse($('#provider_currencies_'+provider.replace(" ", "_")).val());
                $.each(avalableCurrency, function (index, currency) {
                    var record = {
                        [currentProvider]: {
                            [currency]: {
                                [defaultValue]: {
                                    'payment_invoice': {
                                        'max': '',
                                        'min': ''
                                    }, 'withdrawal_invoice': {'max': '', 'min': ''}
                                }
                            }
                        }
                    }
                    limitData = MergeRecursive(limitData, record);
                })
            })
            limitJson.val(JSON.stringify(limitData))
        }

        function MergeRecursive(obj1, obj2) {

            for (var p in obj2) {
                try {
                    // Property in destination object set; update its value.
                    if ( obj2[p].constructor==Object ) {
                        obj1[p] = MergeRecursive(obj1[p], obj2[p]);

                    } else {
                        obj1[p] = obj2[p];

                    }

                } catch(e) {
                    // Property in destination object not set; create it and set its value.
                    obj1[p] = obj2[p];

                }
            }

            return obj1;
        }

        var limitRender = function (){
            var limitData = JSON.parse(limitJson.val());
            limitAddCont.html('');
            $.each(limitData, function (provider, currencies){
                currentProvider = provider;
                $.each(currencies, function (currency, banks){
                    currentCurrency = currency;
                    $.each(banks, function (bank, invoice){
                        var sample = $('#bank_limit_sample_'+currentProvider.replace(" ", "_")+'_'+currentCurrency);
                        var row = sample.clone(false);
                        row.show();
                        var inputBank = row.find('[name="bank"]').val(bank);
                        if(bank === defaultValue){
                            inputBank.prop('readonly', true);
                            row.find('button').closest('.row').remove()
                        }

                        invoice.payment_invoice = invoice.payment_invoice ?? {};
                        invoice.withdrawal_invoice = invoice.withdrawal_invoice ?? {};

                        row.find('[name="min_payment_invoice_amount"]').val(invoice.payment_invoice.min ?? '');
                        row.find('[name="max_payment_invoice_amount"]').val(invoice.payment_invoice.max ?? '');
                        row.find('[name="min_withdrawal_invoice_amount"]').val(invoice.withdrawal_invoice.min ?? '');
                        row.find('[name="max_withdrawal_invoice_amount"]').val(invoice.withdrawal_invoice.max ?? '');
                        sample.closest('.bank-limit-wrapper').find('.bank-limit-added-container').append(row)
                    })
                })
            })



        }

        limitCont.find('[data-action="add"]').on('click', function () {
            var limitData = JSON.parse(limitJson.val());

            var currency = $(this).closest('.bank-limit-container').attr('data-currency');
            var provider = $(this).closest('.bank-limit-container').attr('data-provider');
            var bank = $(this).closest('.bank-limit-container').find('[name="bank"]').val();
            var minPaymentInvoiceAmount = $(this).closest('.bank-limit-container').find('[name="min_payment_invoice_amount"]').val();
            var maxPaymentInvoiceAmount = $(this).closest('.bank-limit-container').find('[name="max_payment_invoice_amount"]').val();
            var minWithdrawalInvoiceAmount = $(this).closest('.bank-limit-container').find('[name="min_withdrawal_invoice_amount"]').val();
            var maxWithdrawalInvoiceAmount = $(this).closest('.bank-limit-container').find('[name="max_withdrawal_invoice_amount"]').val();

            var record = {[provider]: {[currency]: {[bank]:{'payment_invoice':{'max':'', 'min':''},'withdrawal_invoice':{'max':'', 'min':''}}}}}

            limitData = MergeRecursive(limitData, record);

            limitData[provider][currency][bank]['payment_invoice']['min'] = minPaymentInvoiceAmount;
            limitData[provider][currency][bank]['payment_invoice']['max'] = maxPaymentInvoiceAmount;
            limitData[provider][currency][bank]['withdrawal_invoice']['min'] = minWithdrawalInvoiceAmount;
            limitData[provider][currency][bank]['withdrawal_invoice']['max'] = maxWithdrawalInvoiceAmount;

            limitJson.val(JSON.stringify(limitData))

            limitRender();
        });

        limitAddCont.on('focusin', 'input', function(){
            if($(this).attr('name') == 'bank') {
                $(this).data('val', $(this).val());
            }
        }).on('change', 'input', function (event) {
            var limitData = JSON.parse(limitJson.val());

            var currency = $(this).closest('.bank-limit-added-container').attr('data-currency');
            var provider = $(this).closest('.bank-limit-added-container').attr('data-provider');
            var bank = $(this).closest('.limit-added').find('[name="bank"]').val();
            var minPaymentInvoiceAmount = $(this).closest('.limit-added').find('[name="min_payment_invoice_amount"]').val();
            var maxPaymentInvoiceAmount = $(this).closest('.limit-added').find('[name="max_payment_invoice_amount"]').val();
            var minWithdrawalInvoiceAmount = $(this).closest('.limit-added').find('[name="min_withdrawal_invoice_amount"]').val();
            var maxWithdrawalInvoiceAmount = $(this).closest('.limit-added').find('[name="max_withdrawal_invoice_amount"]').val();

            if($(this).attr('name') == 'bank'){
                var record = {[provider]: {[currency]: {[bank]:{'payment_invoice':{'max':'', 'min':''},'withdrawal_invoice':{'max':'', 'min':''}}}}}
                var prev = $(this).data('val');
                delete limitData[provider][currency][prev];
                limitData = MergeRecursive(limitData, record);
            }


            limitData[provider][currency][bank]['payment_invoice']['min'] = minPaymentInvoiceAmount;
            limitData[provider][currency][bank]['payment_invoice']['max'] = maxPaymentInvoiceAmount;
            limitData[provider][currency][bank]['withdrawal_invoice']['min'] = minWithdrawalInvoiceAmount;
            limitData[provider][currency][bank]['withdrawal_invoice']['max'] = maxWithdrawalInvoiceAmount;

            limitJson.val(JSON.stringify(limitData))
        });

        limitAddCont.on('click', '[data-action="del"]', function () {
            var limitData = JSON.parse(limitJson.val());

            var currency = $(this).closest('.bank-limit-added-container').attr('data-currency');
            var provider = $(this).closest('.bank-limit-added-container').attr('data-provider');
            var bank = $(this).closest('.limit-added').find('[name="bank"]').val();

            delete limitData[provider][currency][bank];

            limitJson.val(JSON.stringify(limitData))

            limitRender();
        });

        initJson();
        limitRender();
    }

    if($('#limit_banks_traders').length){
        limitByMerchant();
    }

});
