@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Trader currencies')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader currencies') }}
        </h1>
    </div>

    @can('trader_currency.new')
        <div class="col-md-3">
            <a class="btn btn-grow" href="{{ route('trader_currency.new') }}" role="button">{{__('Add New')}}</a>
        </div>
    @endcan

    @if($trader_currencies)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Asset')}}</th>
                <th scope="col">{{__('Crypto Asset')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Precision')}}</th>
                <th scope="col">{{__('Can get Address')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($trader_currencies as $i => $unit)
                <tr>
                    <td>{{$unit->id}}</td>
                    <td>{{$unit->asset}}</td>
                    <td>{{$unit->crypto_asset}}</td>
                    <td>{{$unit->name}}</td>
                    <td>{{$unit->precision}}</td>
                    <td>{{$unit->can_get_address}}</td>
                    <td>{{__(['InActive', 'Active'][$unit->status])}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('trader_currency.view', $unit->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('trader_currency.edit')
                            <a href="{{route('trader_currency.edit', $unit->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-arrow-clockwise"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endif
@endsection