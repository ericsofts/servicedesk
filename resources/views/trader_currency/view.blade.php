@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_currency.index')}}">{{__('Trader currencies')}}</a></li>
            <li class="breadcrumb-item active">{{$trader_currency->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader currency details') }}
        </h1>
    </div>
    @if($trader_currency)
        <div class="p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-6">
                    <p class="fw-bold">{{__('ID')}} {{$trader_currency->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Asset')}}</dt>
                        <dd class="col-sm-8">{{$trader_currency->asset}}</dd>
                        <dt class="col-sm-4">{{__('Crypto Asset')}}</dt>
                        <dd class="col-sm-8">{{$trader_currency->crypto_asset}}</dd>
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$trader_currency->name}}</dd>
                        <dt class="col-sm-4">{{__('Precision')}}</dt>
                        <dd class="col-sm-8">{{$trader_currency->precision}}</dd>
                        <dt class="col-sm-4">{{__('Can get Address')}}</dt>
                        <dd class="col-sm-8">{{$trader_currency->can_get_address}}</dd>
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_currency->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_currency->updated_at)}}</dd>
                    </dl>
                </div>
            </div>

        </div>
    @endif
@endsection
