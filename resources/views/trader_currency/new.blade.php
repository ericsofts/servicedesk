@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_currency.index')}}">{{__('Trader currencies')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader currency New') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader_currency.create') }}" class="row g-3 mb-3">
        @csrf
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Asset')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('asset') is-invalid @enderror"
                           value="{{old('asset')}}"
                           name="asset" id="asset">

                    @error('asset')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           value="{{old('name')}}"
                           name="name" id="name">

                    @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Precision')}}:</label>
                <div class="col-sm-12">
                    <input type="number" class="form-control @error('precision') is-invalid @enderror"
                           value="{{old('precision')}}"
                           name="precision" id="precision">

                    @error('precision')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Crypto Asset')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('crypto_asset') is-invalid @enderror"
                           value="{{old('crypto_asset')}}"
                           name="crypto_asset" id="crypto_asset">

                    @error('crypto_asset')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Can get Address')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="can_get_address">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('can_get_address')) selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('can_get_address')) selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('status') == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status') == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection