@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Chatex recent messages')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Chatex recent messages') }}
        </h1>
    </div>
    @if(!empty($error_code))
        <div class="alert alert-danger" role="alert">
            {{$message}} ({{$error_code}})
        </div>
    @else
        <p class="fw-bold">
            {{__('Messages')}}
            <span class="badge bg-secondary">{{$message_count}}</span>
        </p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('Contact ID')}}</th>
                <th scope="col">{{__('Sender')}}</th>
                <th scope="col">{{__('Created')}}</th>
                <th scope="col">{{__('Message')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($message_list as $i => $item)
                <tr>
                    <td>{{$item['contact_id']}}</td>
                    <td>{{$item['sender']['name']}}</td>
                    <td>{{datetimeFormat($item['created_at'])}}</td>
                    <td>
                        @if(!empty($item['attachment_url']) && str_contains($item['attachment_type'], 'image'))
                            <a href="{{$item['attachment_url']}}" target="_blank">
{{--                                <img src="{{$item['attachment_url']}}" class="img-thumbnail w-25">--}}
                                <img src="{{route('chatex.image', base64_encode($item['attachment_url']))}}" class="img-thumbnail w-25">
                            </a>
                        @else

                            <p>{!! nl2br(e($item['msg'])) !!}</p>
                        @endif
                    </td>
                    <td>
                        @if(!empty($inputs[$item['contact_id']]))
                            <a href="{{route('input_invoice.view', $inputs[$item['contact_id']]->id)}}">{{__('Input invoice')}}{{$inputs[$item['contact_id']]->id}}</a>
                            @if(!empty($inputs[$item['contact_id']]->user))
                                <br>
                                <a href="{{route('user.view', $inputs[$item['contact_id']]->user->id)}}">{{$inputs[$item['contact_id']]->user->name}}</a>
                            @endif
                        @endif
                        @if(!empty($payments[$item['contact_id']]))
                            <a href="{{route('payment_invoice.view', $payments[$item['contact_id']]->id)}}">{{__('Payment invoice')}}{{$payments[$item['contact_id']]->id}}</a>
                            @if(!empty($payments[$item['contact_id']]->user))
                                <br>
                                <a href="{{route('user.view', $payments[$item['data']['contact_id']]->user->id)}}">{{$payments[$item['data']['contact_id']]->user->name}}</a>
                            @endif
                            @if(!empty($payments[$item['contact_id']]->merchant))
                                <br>
                                <a href="{{route('merchant.view', $payments[$item['contact_id']]->merchant->id)}}">{{$payments[$item['contact_id']]->merchant->name}}</a>
                            @endif
                        @endif
                    </td>
                    <td>
                        @if($item['contact_id'])
                            <a href="{{route('chatex.contact', $item['contact_id'])}}">
                                <i class="bi bi-info-circle"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
