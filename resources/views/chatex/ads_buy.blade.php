@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Chatex ads buy')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Chatex ads buy') }}
        </h1>
    </div>
    @if(!empty($error_code))
        <div class="alert alert-danger" role="alert">
            {{$message}} ({{$error_code}})
        </div>
    @else
        <dl class="row">
            @foreach($rates as $rate)
            <dt class="col-sm-3">{{$rate['coin']}} to {{$rate['symbol']}}</dt>
            <dd class="col-sm-9">{{number_format($rate['coin_rate'],13)}}</dd>
            @endforeach
        </dl>
        <form method="GET" action="{{ route('chatex.ads_buy') }}" class="row g-3 mb-3">
            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="type">
                            <option value="">{{__('All')}}</option>
                            <option value="usdt"
                                    @if($type === "usdt") selected @endif>{{__('USDT')}}</option>
                            <option value="usdt_trc20"
                                    @if($type === "usdt_trc20") selected @endif>{{__('USDT_TRC20')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-6 col-form-label">{{__('Currency')}}:</label>
                    <div class="col-sm-6">
                        <select class="form-select" name="currency">
                            <option value="">{{__('All')}}</option>
                            @foreach($currencies as $currency)
                            <option value="{{$currency}}"
                                    @if($currency == request('currency')) selected @endif>{{$currency}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-3 col-form-label">{{__('Amount')}}:</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-text">$</span>
                            <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount"
                                   name="amount" placeholder="{{__('Amount')}}"
                                   value="{{request('amount')}}"
                            >
                            @error('amount')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="cc" name="cc" value="1" @if(request('cc')) checked @endif>
                    <label class="form-check-label" for="cc">{{__('CC')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="accept_cc" name="accept_cc" value="1" @if(request('accept_cc')) checked @endif>
                    <label class="form-check-label" for="accept_cc">{{__('Accept CC')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="priority" name="priority" value="1" @if(request('priority')) checked @endif>
                    <label class="form-check-label" for="priority">{{__('Priority')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="blocked" name="blocked" value="1" @if(request('blocked')) checked @endif>
                    <label class="form-check-label" for="blocked">{{__('Blocked')}}</label>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <p class="fw-bold">
            {{__('Ads')}}
            <span class="badge bg-secondary">{{count($ad_list)}}</span>
        </p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('Ad ID')}}</th>
                <th scope="col">{{__('Coin')}}</th>
                <th scope="col">{{__('Rate')}}</th>
                <th scope="col">{{__('Min amount')}}</th>
                <th scope="col">{{__('Max amount')}}</th>
                <th scope="col">{{__('Max amount available')}}</th>
                @if(isset($ad_list[0]['data']['gps_temp_amount']))
                    <th scope="col">{{__('Amount')}}</th>
                @endif
                <th scope="col">{{__('Currency')}}</th>
                <th scope="col">{{__('Bank')}}</th>
                <th scope="col">{{__('Seller')}}</th>
                <th scope="col">{{__('Accept CC')}}</th>
                <th scope="col">{{__('CC')}}</th>
                <th scope="col">{{__('Info')}}</th>
                <th scope="col">{{__('Msg')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ad_list as $item)
                <tr class="@if($item['data']['gps_priority']) bg-success @endif @if($item['data']['gps_blocked']) bg-secondary @endif">
                    <td>{{$item['data']['ad_id']}}</td>
                    <td>{{$item['data']['coin']}}</td>
                    <td>{{$item['data']['temp_price']}}</td>
                    <td>{{$item['data']['min_amount']}}</td>
                    <td>{{$item['data']['max_amount']}}</td>
                    <td>{{$item['data']['max_amount_available']}}</td>
                    @if(isset($ad_list[0]['data']['gps_temp_amount']))
                        <td>{{$item['data']['gps_temp_amount']}}</td>
                    @endif
                    <td>{{$item['data']['currency']}}</td>
                    <td>{{$item['data']['bank_name']}}</td>
                    <td>{{$item['data']['profile']['name']}}</td>
                    <td>{{$item['data']['profile']['is_accept_visa_direct']}}</td>
                    <td>{{$item['data']['clean_card_number']}}</td>
                    <td>{{nl2br($item['data']['account_info'])}}</td>
                    <td>{{nl2br($item['data']['msg'])}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
