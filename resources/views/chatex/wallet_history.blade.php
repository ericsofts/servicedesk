@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Wallet history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Wallet history') }}
        </h1>
    </div>
    @if(!empty($data['errors']))
        <div class="alert alert-danger" role="alert">
            {{$data['message']}}
            <ul class="list-unstyled">
                @foreach($data['errors'] as $k => $error)
                    <li>{{$k}} - {{$error['message']}} - {{$error['code']}}</li>
                @endforeach
            </ul>
        </div>
    @else
        @if(request()->routeIs('chatex.wallet.history'))
        <form method="GET" action="{{ route('chatex.wallet.history') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="type">
                            <option value="">{{__('Choose')}}</option>
                            <option value="FIAT_TRADE" @if($type === "FIAT_TRADE") selected @endif>{{__('FIAT_TRADE')}}</option>
                            <option value="REWARD" @if($type === "REWARD") selected @endif>{{__('REWARD')}}</option>
                            <option value="INVOICE_PAYMENT" @if($type === "INVOICE_PAYMENT") selected @endif>{{__('INVOICE_PAYMENT')}}</option>
                            <option value="REFERRAL_EARNINGS" @if($type === "REFERRAL_EARNINGS") selected @endif>{{__('REFERRAL_EARNINGS')}}</option>
                            <option value="WITHDRAWAL" @if($type === "WITHDRAWAL") selected @endif>{{__('WITHDRAWAL')}}</option>
                            <option value="CRYPTO_TRADE" @if($type === "CRYPTO_TRADE") selected @endif>{{__('CRYPTO_TRADE')}}</option>
                            <option value="DEPOSIT" @if($type === "DEPOSIT") selected @endif>{{__('DEPOSIT')}}</option>
                            <option value="VOUCHER" @if($type === "VOUCHER") selected @endif>{{__('VOUCHER')}}</option>
                            <option value="TRANSFER" @if($type === "TRANSFER") selected @endif>{{__('TRANSFER')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        @endif
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('trade_id')}}</th>
                <th scope="col">{{__('Pair')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Fee')}}</th>
                <th scope="col">{{__('Fiat_amount')}}</th>
                <th scope="col">{{__('Coin')}}</th>
                <th scope="col">{{__('Merchant')}}</th>
                <th scope="col">{{__('User')}}</th>
                <th scope="col">{{__('Invoice_id')}}</th>
                <th scope="col">{{__('Details')}}</th>
                <th scope="col">{{__('Created')}}</th>
                <th scope="col">{{__('Updated')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $i => $item)
                <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['type']}}</td>
                    <td>{{$item['trade_id'] ?? null}}</td>
                    <td>{{$item['pair'] ?? null}}</td>
                    <td>{{$item['amount'] ?? null}}</td>
                    <td>{{$item['fee'] ?? null}}</td>
                    <td>{{$item['fiat_amount'] ?? null}}</td>
                    <td>{{$item['coin'] ?? null}}</td>
                    <td>{{$item['merchant'] ?? null}}</td>
                    <td>{{$item['user'] ?? null}}</td>
                    <td>{{$item['invoice_id'] ?? null}}</td>
                    <td>{{$item['details'] ?? null}}</td>
                    <td>{{datetimeFormat($item['created_at'])}}</td>
                    <td>{{datetimeFormat($item['updated_at'])}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="mb-3">
            @if(request()->routeIs('chatex.wallet.history_24'))
                @if(request('page', 1) > 1)
                    <a href="{{ route('chatex.wallet.history_24', ['page' => request('page', 1) - 1]) }}" class="btn btn-grow me-3">{{__('Previous')}}</a>
                @endif
                @if(!empty($data))
                    <a href="{{ route('chatex.wallet.history_24', ['page' => request('page', 1) + 1]) }}" class="btn btn-grow">{{__('Next')}}</a>
                @endif
            @else
                @if(request('page', 1) > 1)
                    <a href="{{ route('chatex.wallet.history', ['page' => request('page', 1) - 1]) }}" class="btn btn-grow me-3">{{__('Previous')}}</a>
                @endif
                @if(!empty($data))
                    <a href="{{ route('chatex.wallet.history', ['page' => request('page', 1) + 1]) }}" class="btn btn-grow">{{__('Next')}}</a>
                @endif
            @endif
        </div>
    @endif
@endsection
