@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Chatex dashboard')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Chatex dashboard') }}
        </h1>
    </div>
    @if(!empty($error_code))
        <div class="alert alert-danger" role="alert">
            {{$message}} ({{$error_code}})
        </div>
    @else
        <form method="GET" action="{{ route('chatex.dashboard') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="type">
                            <option value="">{{__('Active')}}</option>
                            <option value="canceled"
                                    @if($type === "canceled") selected @endif>{{__('Canceled')}}</option>
                            <option value="released"
                                    @if($type === "released") selected @endif>{{__('Released')}}</option>
                            <option value="closed" @if($type === "closed") selected @endif>{{__('Closed')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <p class="fw-bold">
            {{__('Contacts')}}
            <span class="badge bg-secondary">{{$contact_count}}</span>
        </p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('Contact ID')}}</th>
                <th scope="col">{{__('Ad ID')}}</th>
                <th scope="col">{{__('Payment method')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Currency')}}</th>
                <th scope="col">{{__('Amount C')}}</th>
                <th scope="col">{{__('Seller')}}</th>
                <th scope="col">{{__('Created')}}</th>
                <th scope="col">{{__('Funded')}}</th>
                <th scope="col">{{__('Payment completed')}}</th>
                <th scope="col">{{__('Released')}}</th>
                <th scope="col">{{__('Canceled')}}</th>
                <th scope="col">{{__('Disputed')}}</th>
                <th scope="col">{{__('Closed')}}</th>
                <th scope="col">{{__('Invoice')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($contact_list as $i => $item)
                <tr>
                    <td>{{$item['data']['contact_id']}}</td>
                    <td>{{$item['data']['advertisement']['id']}}</td>
                    <td>{{$item['data']['advertisement']['payment_method']}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item['data']['amount']), $item['data']['currency'])}}</td>
                    <td>{{$item['data']['currency']}}</td>
                    <td>{{$item['data']['amount_btc']}}</td>
                    <td>{{$item['data']['seller']['name']}}</td>
                    <td>{{datetimeFormat($item['data']['created_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['funded_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['payment_completed_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['released_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['canceled_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['disputed_at'])}}</td>
                    <td>{{datetimeFormat($item['data']['closed_at'])}}</td>
                    <td>
                        @if(!empty($inputs[$item['data']['contact_id']]))
                            <a href="{{route('input_invoice.view', $inputs[$item['data']['contact_id']]->id)}}">{{__('Input invoice')}} {{$inputs[$item['data']['contact_id']]->id}}</a>
                            @if(!empty($inputs[$item['data']['contact_id']]->user))
                                <br>
                                <a href="{{route('user.view', $inputs[$item['data']['contact_id']]->user->id)}}">{{$inputs[$item['data']['contact_id']]->user->name}}</a>
                            @endif
                        @endif
                        @if(!empty($payments[$item['data']['contact_id']]))
                            <a href="{{route('payment_invoice.view', $payments[$item['data']['contact_id']]->id)}}">{{__('Payment invoice')}} {{$payments[$item['data']['contact_id']]->id}}</a>
                            @if(!empty($payments[$item['data']['contact_id']]->user))
                                <br>
                                <a href="{{route('user.view', $payments[$item['data']['contact_id']]->user->id)}}">{{$payments[$item['data']['contact_id']]->user->name}}</a>
                            @endif
                            @if(!empty($payments[$item['data']['contact_id']]->merchant))
                                <br>
                                <a href="{{route('merchant.view', $payments[$item['data']['contact_id']]->merchant->id)}}">{{$payments[$item['data']['contact_id']]->merchant->name}}</a>
                            @endif
                        @endif
                            @if(!empty($withdrawals[$item['data']['contact_id']]))
                                <a href="{{route('withdrawal.view', $withdrawals[$item['data']['contact_id']]->id)}}">{{__('Withdrawal invoice')}} {{$withdrawals[$item['data']['contact_id']]->id}}</a>
                            @endif
                    </td>
                    <td>
                        <a href="{{route('chatex.contact', $item['data']['contact_id'])}}">
                            <i class="bi bi-info-circle"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection
