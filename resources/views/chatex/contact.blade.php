@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('chatex.dashboard')}}">{{__('Chatex dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Contact details')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Contact details') }}
        </h1>
    </div>
    @if($contact_res)
        <div class="invoice p-3 mb-3">
            <div class="row">
                @if(!empty($contact_res['data']))
                    <div class="col-md-6 col-lg-4">
                        @if(!empty($ad))
                            <p class="fw-bold">{{__('Ad')}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('ad_id')}}</dt>
                                <dd class="col-sm-8">{{$ad['ad_id']}}</dd>
                                <dt class="col-sm-4">{{__('account_info')}}</dt>
                                <dd class="col-sm-8">{{$ad['account_info']}}</dd>
                                <dt class="col-sm-4">{{__('msg')}}</dt>
                                <dd class="col-sm-8">{{$ad['msg']}}</dd>
                                <dt class="col-sm-4">{{__('temp_price')}}</dt>
                                <dd class="col-sm-8">{{$ad['temp_price']}}</dd>
                                <dt class="col-sm-4">{{__('bank_name')}}</dt>
                                <dd class="col-sm-8">{{$ad['bank_name']}}</dd>
                                <dt class="col-sm-4">{{__('card_number')}}</dt>
                                <dd class="col-sm-8">{{$ad['clean_card_number']}}</dd>
                                <dt class="col-sm-4">{{__('countrycode')}}</dt>
                                <dd class="col-sm-8">{{$ad['countrycode']}}</dd>
                                <dt class="col-sm-4">{{__('min_amount')}}</dt>
                                <dd class="col-sm-8">{{$ad['min_amount']}}</dd>
                                <dt class="col-sm-4">{{__('max_amount')}}</dt>
                                <dd class="col-sm-8">{{$ad['max_amount']}}</dd>
                                <dt class="col-sm-5">{{__('max_amount_available')}}</dt>
                                <dd class="col-sm-7">{{$ad['max_amount_available']}}</dd>
                            </dl>
                        @endif
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <p class="fw-bold">{{__('Contact Info')}}</p>
                        <dl class="row">
                            <dt class="col-sm-4">{{__('contact_id')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['contact_id']}}</dd>
                            <dt class="col-sm-4">{{__('currency')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['currency']}}</dd>
                            <dt class="col-sm-4">{{__('amount')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['amount']}}</dd>
                            @if(!empty($contact_res['data']['amount_btc']))
                                <dt class="col-sm-4">{{__('amount_btc')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['amount_btc']}}</dd>
                            @endif
                        </dl>
                        <p class="fw-bold">{{__('Seller')}}</p>
                        <dl class="row">
                            <dt class="col-sm-4">{{__('username')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['seller']['username']}}</dd>
                            <dt class="col-sm-4">{{__('name')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['seller']['name']}}</dd>
                        </dl>
                        <p class="fw-bold">{{__('Buyer')}}</p>
                        <dl class="row">
                            <dt class="col-sm-4">{{__('username')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['buyer']['username']}}</dd>
                            <dt class="col-sm-4">{{__('name')}}</dt>
                            <dd class="col-sm-8">{{$contact_res['data']['buyer']['name']}}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <p class="fw-bold">{{__('Status')}}</p>
                        <dl class="row">
                            <dt class="col-sm-5">{{__('created_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['created_at'])}}</dd>
                            <dt class="col-sm-5">{{__('funded_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['funded_at'])}}</dd>
                            <dt class="col-sm-5">{{__('payment_completed_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['payment_completed_at'])}}</dd>
                            <dt class="col-sm-5">{{__('released_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['released_at'])}}</dd>
                            <dt class="col-sm-5">{{__('closed_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['closed_at'])}}</dd>
                            <dt class="col-sm-5">{{__('canceled_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['canceled_at'])}}</dd>
                            <dt class="col-sm-5">{{__('disputed_at')}}</dt>
                            <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['disputed_at'])}}</dd>
                        </dl>
                    </div>
                    @if(!empty($paymentInvoice))
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Payment Invoice #')}} {{$paymentInvoice->id}}</p>
                            <dl class="row">
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ID')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->id}}</dd>
                                    <dt class="col-sm-4">{{__('Number')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->invoice_number}}</dd>
                                    <dt class="col-sm-4">{{__('Amount')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->amount}}</dd>
                                    <dt class="col-sm-4">{{__('Amount to pay')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->amount2pay}}</dd>
                                    <dt class="col-sm-4">{{__('Status')}}</dt>
                                    <dd class="col-sm-8">{{payment_invoice_status($paymentInvoice->status)}}</dd>
                                    @if($paymentInvoice->status == 1 && $paymentInvoice->payed)
                                        <dt class="col-sm-4">{{__('Paid')}}</dt>
                                        <dd class="col-sm-8">{{datetimeFormat($paymentInvoice->payed)}}</dd>
                                    @endif
                                    <dt class="col-sm-4">{{__('Created')}}</dt>
                                    <dd class="col-sm-8">{{datetimeFormat($paymentInvoice->created_at)}}</dd>
                                    <dt class="col-sm-4">{{__('Updated')}}</dt>
                                    <dd class="col-sm-8">{{datetimeFormat($paymentInvoice->updated_at)}}</dd>
                                    <dt class="col-sm-4">{{__('Api')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->api_type}}</dd>
                                </dl>
                            </dl>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Merchant')}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('ID')}}</dt>
                                <dd class="col-sm-8">{{$paymentInvoice->merchant->id}}</dd>
                                <dt class="col-sm-4">{{__('Name')}}</dt>
                                <dd class="col-sm-8">{{$paymentInvoice->merchant->name}}</dd>
                                <dt class="col-sm-4">{{__('Email')}}</dt>
                                <dd class="col-sm-8">{{$paymentInvoice->merchant->email}}</dd>
                            </dl>
                        </div>
                        @if(!empty($paymentInvoice->user))
                            <div class="col-md-6 col-lg-4">
                                <p class="fw-bold">{{__('User')}}</p>
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ID')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->user->id}}</dd>
                                    <dt class="col-sm-4">{{__('Account ID')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->user->account_id}}</dd>
                                    <dt class="col-sm-4">{{__('Name')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->user->name}}</dd>
                                    <dt class="col-sm-4">{{__('Email')}}</dt>
                                    <dd class="col-sm-8">{{$paymentInvoice->user->email}}</dd>
                                </dl>
                            </div>
                        @endif
                    @endif
                    @if(!empty($inputInvoice))
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Input Invoice #')}} {{$inputInvoice->id}}</p>
                            <dl class="row">
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ID')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->id}}</dd>
                                    <dt class="col-sm-4">{{__('Amount')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->amount}}</dd>
                                    <dt class="col-sm-4">{{__('Status')}}</dt>
                                    <dd class="col-sm-8">{{input_invoice_status($inputInvoice->status)}}</dd>
                                    @if($inputInvoice->status == 1 && $inputInvoice->payed)
                                        <dt class="col-sm-4">{{__('Paid')}}</dt>
                                        <dd class="col-sm-8">{{datetimeFormat($inputInvoice->payed)}}</dd>
                                    @endif
                                    <dt class="col-sm-4">{{__('Created')}}</dt>
                                    <dd class="col-sm-8">{{datetimeFormat($inputInvoice->created_at)}}</dd>
                                    <dt class="col-sm-4">{{__('Updated')}}</dt>
                                    <dd class="col-sm-8">{{datetimeFormat($inputInvoice->updated_at)}}</dd>
                                </dl>
                            </dl>
                        </div>
                        @if(!empty($inputInvoice->user))
                            <div class="col-md-6 col-lg-4">
                                <p class="fw-bold">{{__('User')}}</p>
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ID')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->user->id}}</dd>
                                    <dt class="col-sm-4">{{__('Account ID')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->user->account_id}}</dd>
                                    <dt class="col-sm-4">{{__('Name')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->user->name}}</dd>
                                    <dt class="col-sm-4">{{__('Email')}}</dt>
                                    <dd class="col-sm-8">{{$inputInvoice->user->email}}</dd>
                                </dl>
                            </div>
                        @endif
                    @endif
                @else
                    <div class="alert alert-danger" role="alert">
                        {{$contact_res['message']}} ({{$contact_res['error_code']}})
                    </div>
                @endif
            </div>
            <div>
                <form method="post" action="{{ route('chatex.send_dispute') }}">
                    @method('POST')
                    @csrf
                    <input type="hidden" name="payment_id" value="{{$id}}">
                    <button type="submit" class="btn btn-grow btn-confirm-default">{{__('Dispute')}}</button>
                </form>
            </div>
            <hr>
            <p class="fw-bold">
                {{__('Messages')}}
                @if(!empty($messages['data']['message_count']))
                    <span class="badge bg-secondary">{{$messages['data']['message_count']}}</span>
                @endif
            </p>
            @if(!empty($messages['data']['message_list']))
                <div class="row">
                    <div class="col-12 messages">
                        @foreach($messages['data']['message_list'] as $message)
                            <div class="message">
                                <div class="user-block">
                                    <span class="username">{{$message['sender']['name']}}</span>
                                    <span class="created">{{datetimeFormat($message['created_at'])}}</span>
                                </div>
                                @if(!empty($message['attachment_url']) && str_contains($message['attachment_type'], 'image'))
                                    <a href="{{$message['attachment_url']}}" target="_blank">
                                        <img src="{{route('chatex.image', base64_encode($message['attachment_url']))}}" class="img-thumbnail w-25">
                                    </a>
                                @else

                                    <p>{!! nl2br(e($message['msg'])) !!}</p>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            @elseif(!empty($messages['message']))
                <div class="alert alert-danger" role="alert">
                    {{$contact_res['message']}} ({{$contact_res['error_code']}})
                </div>
            @endif
            <div class="row">
                <div class="col-12 form-message">
                    <form method="post" action="{{ route('chatex.send_message') }}" enctype="multipart/form-data">
                        @method('POST')
                        @csrf
                        <input type="hidden" name="payment_id" value="{{$id}}">
                        <div class="mb-3">
                            <label for="message" class="form-label">{{__('Message')}}</label>
                            <textarea class="form-control @error('message') is-invalid @enderror" id="message"
                                      name="message" rows="3"></textarea>
                            @error('message')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="document" class="form-label">{{__('Document')}}</label>
                            <input class="form-control" type="file" id="document" name="document">
                        </div>
                        <div class="p-3">
                            <button type="submit" class="btn btn-grow">{{__('Send')}}</button>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
@endsection
