@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Merchants')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Merchants') }}
        </h1>
    </div>

    <form method="GET" action="{{ route('merchant.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Active')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('InActive')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Agent')}}:</label>
                    <div class="col-sm-12">
                        <select name="agents[]" class="form-control select2" multiple="multiple">
                            @foreach($agents as $k => $v)
                                <option
                                    value="{{$k}}"
                                    @if(in_array($k, request('agents') ?? [])) selected @endif>{{$v}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Search')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" name="search" class="form-control" value="{{request('search') }}"/>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-12 col-form-label">&nbsp</label>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                        <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
                    </div>
                </div>
            </div>
            @can('merchant.new')
                <div class="col-md-1">
                    <div class="row">
                        <label class="col-sm-12 col-form-label">&nbsp</label>
                        <div class="col-sm-12">
                            <a class="btn btn-grow" href="{{ route('merchant.new') }}" role="button">{{__('Add New')}}</a>
                        </div>
                    </div>
                </div>

            @endcan
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Email')}}</th>
                <th scope="col">{{__('Balance')}}</th>
                <th scope="col">{{__('Coin')}}</th>
                <th scope="col">{{__('Open withdrawals')}}</th>
                <th scope="col">{{__('Created at')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Agent')}}</th>
                <th scope="col">{{__('Partner')}}</th>
                <th scope="col">{{__('Chat Name')}}/{{__('ID')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($merchants as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{($item->balance->amount ?? 0) - ($open_withdrawals[$item->id] ?? 0)}}</td>
                    <td>
                        @forelse($item->merchant_property as $property)
                            {{ show_merchant_property($property->value, config('app.available_merchant_coins'))}}
                        @empty
                            {{ show_merchant_property(config('app.available_merchant_coins'))}}
                        @endforelse
                    </td>
                    <td>
                        {{$open_withdrawals[$item->id] ?? 0}}
                    </td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{user_status($item->status)}}</td>
                    <td>
                        @if(!empty($item->agent))
                        <a href="{{route('agent.view', $item->agent->id)}}">
                            {{$item->agent->name}}
                        </a>
                            @endif
                    </td>
                    <td>
                        @if(!empty($item->agent->partner))
                            {{$item->agent->partner->name}}
                        @endif
                    </td>
                    <td>{{$item->chat->chat_name ?? ' --- '}}/{{$item->chat->chat_id ?? ' --- '}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('merchant.input_invoice.create', ['merchant_id' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-plus"></i>
                            </a>
                            <a href="{{route('merchant.view', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('merchant.edit')
                            <a href="{{route('merchant.edit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-pencil-square"></i>
                            </a>
                            @endcan
                            <a href="{{route('transactions.all_invoices', ['merchants[]' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-list-ul"></i>
                            </a>
                            <a href="{{route('merchant.withdrawal_invoice.create', ['merchant_id' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-dash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $merchants->links() }}</div>
@endsection
