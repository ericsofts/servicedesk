@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('merchant.index')}}">{{__('Merchants')}}</a></li>
            <li class="breadcrumb-item active">{{$merchant->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Merchant details') }}
        </h1>
    </div>
    @if($merchant)
        <div class="p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('ID')}} {{$merchant->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$merchant->name}}</dd>
                        <dt class="col-sm-4">{{__('Email')}}</dt>
                        <dd class="col-sm-8">{{$merchant->email}}</dd>
                        <dt class="col-sm-4">{{__('Chat Name')}}</dt>
                        <dd class="col-sm-8">{{$merchant->chat?->chat_name}}</dd>
                        <dt class="col-sm-4">{{__('Chat ID')}}</dt>
                        <dd class="col-sm-8">{{$merchant->chat?->chat_id}}</dd>
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($merchant->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($merchant->updated_at)}}</dd>
                        <dt class="col-sm-4">{{__('Email verified at')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($merchant->email_verified_at)}}</dd>
                    </dl>
                </div>
                <div class="col-sm-4">
                    <p>{{__('Agent')}}: <span class="fw-bold">{{optional($agent)->name}}</span></p>
                    <p>{{__('Balance')}}: <span class="fw-bold">{{$merchant->balance_total()}}</span></p>
                </div>
            </div>
            <hr>
            <p>{{__('Commissions')}}</p>
            <form method="GET" action="{{ route('merchant.view', ['id' => $merchant->id]) }}" class="row g-3 mb-3">
                <div class="row mt-2">
                    <div class="col">
                        <div class="row">
                            <label class="col-sm-4 col-form-label">{{__('Api type')}}:</label>
                            <div class="col-sm-8">
                                <select name="api_type[]" class="form-control select2" multiple="multiple">
                                    @foreach(\App\Models\MerchantCommission::AVAILABLE_API as $k => $v)
                                        <option
                                            value="{{$v}}"
                                            @if(in_array($v, request('api_type') ?? [])) selected @endif>{{$v}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                        <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{__('Api type')}}</th>
                        <th scope="col">{{__('Type')}}</th>
                        <th scope="col">{{__('Name')}}</th>
                        <th scope="col">{{__('Amount')}}</th>
                        <th scope="col">{{__('Type')}}</th>
                        <th scope="col">{{__('Value')}}</th>
                        <th scope="col">{{__('Updated')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($commissions as $commission)
                        @foreach($commission->value as $k => $v)
                            <tr>
                                <td>{{$commission->api_type}}</td>
                                <td>{{$commission->property_type == 1 ? __('Input') : __('Output')}}</td>
                                <td>{{$commission->property}}</td>
                                <td>{{$k}}</td>
                                <td>{{$v['type']}}</td>
                                <td>{{$v['value']}}</td>
                                <td>{{$commission->updated_at}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    @endif
@endsection
