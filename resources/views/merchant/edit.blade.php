@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('merchant.index')}}">{{__('Merchants')}}</a></li>
            <li class="breadcrumb-item active">{{__('Edit')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Merchant Edit') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('merchant.update', $id) }}" class="row g-3 mb-3">
        @method('PATCH')
        @csrf

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           name="name" id="name" value="{{old('name',$name)}}">

                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Email')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                           name="email" id="email" value="{{old('email',$email)}}">

                @error('email')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Password')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control"
                           name="password" id="password">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Agent')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="agent_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($agents as $key => $value)
                        <option value="{{$key}}" @if(old('agent_id', $agent_id) == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="status">
                        <option value="1" @if(old('status', $status) == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status', $status) == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Enable 2FA')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="enable_2fa">
                        <option value="1" @if(old('enable_2fa', $enable_2fa) == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('enable_2fa', $enable_2fa) == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4 d-none">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Token')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('token') is-invalid @enderror"
                        name="token" id="token" value="{{old('token',$token)}}">

                    @error('token')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4 d-none">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Payment Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('payment_key') is-invalid @enderror"
                        name="payment_key" id="payment_key" value="{{old('payment_key',$payment_key)}}">

                    @error('payment_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('API3 Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('api3_key') is-invalid @enderror"
                        name="api3_key" id="api3_key" value="{{old('api3_key',$api3_key)}}">

                    @error('api3_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('API5 Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('api5_key') is-invalid @enderror"
                        name="api5_key" id="api5_key" value="{{old('api5_key',$api5_key)}}">

                    @error('api5_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Merchant Type')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="type">
                        <option value="0" @if(old('type', $type) == '0') selected @endif>{{__('0')}}</option>
                        <option value="1" @if(old('type', $type) == '1') selected @endif>{{__('1')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            @include('merchant.avaiprovcomp')
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Permitted ip list')}}:</label>
                <div class="col-sm-12">
                    <textarea name="property-permitted_ip_list" id="property-permitted_ip_list" class="form-control">{{ old('permitted_ip_list', $permitted_ip_list) }}</textarea>

                    @error('permitted_ip_list')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Enable ip list')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="property-enable_ip_list" id="property-enable_ip_list">
                        <option value="">{{__('Choose')}}</option>
                        <option value="0" @if(0 == old('enable_ip_list', $enable_ip_list)) selected @endif>{{__('InActive')}}</option>
                        <option value="1" @if(1 == old('enable_ip_list', $enable_ip_list)) selected @endif>{{__('Active')}}</option>
                    </select>

                    @error('enable_ip_list')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Currency 2 Currency')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="property-currency2currency">
                        <option value="0" @if(old('currency2currency', $currency2currency) == '0') selected @endif>{{__('0')}}</option>
                        <option value="1" @if(old('currency2currency', $currency2currency) == '1') selected @endif>{{__('1')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Excluded Сurrencies')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('property-excluded_currencies') is-invalid @enderror"
                           name="property-excluded_currencies" id="property-excluded_currencies" value="{{old('property-excluded_currencies',$excluded_currencies)}}">

                    @error('property-excluded_currencies')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Disable show merchant name on API page')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="property-disable_show_name" id="property-disable_show_name">
                        <option value="">{{__('Choose')}}</option>
                        <option value="0" @if(0 == old('property-disable_show_name', $disableName)) selected @endif>{{__('InActive')}}</option>
                        <option value="1" @if(1 == old('property-disable_show_name', $disableName)) selected @endif>{{__('Active')}}</option>
                    </select>

                    @error('property-disable_show_name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Exact Currency')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="property-exact_currency" id="property-exact_currency">
                        <option value="">{{__('Choose')}}</option>
                        <option value="0" @if(0 == old('property-exact_currency', $exact_currency)) selected @endif>{{__('InActive')}}</option>
                        <option value="1" @if(1 == old('property-exact_currency', $exact_currency)) selected @endif>{{__('Active')}}</option>
                    </select>

                    @error('property-exact_currency')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="mt-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>

    <div class="page-header">
        <h1>
            {{ __('Merchant Commission Edit') }}
        </h1>
    </div>

    <ul class="nav nav-tabs" role="tablist">
        @foreach ($service_providers as$i =>  $provider)
        <li class="nav-item" role="presentation">
            <button class="nav-link {{$i ? '' : 'active'}}" data-bs-toggle="tab" data-bs-target="#sp_{{$provider->id}}" type="button" role="tab">{{$provider->name}}</button>
        </li>
        @endforeach
    </ul>

    <div class="tab-content pt-3">
    @foreach ($service_providers as $i => $provider)
    <div class="tab-pane fade show {{$i ? '' : 'active'}}" id="sp_{{$provider->id}}">
        <form action="{{ route('merchant.update-commission', $id) }}" method="post" class="form-ajax row g-3 mb-3">
            @csrf
            @method('PATCH')

            <input type="hidden" name="service_provider_id" value="{{ $provider->id }}">
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Invoice Amount')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property-min_payment_invoice_amount') is-invalid @enderror"
                            name="property-min_payment_invoice_amount" id="property-min_payment_invoice_amount" value="{{old('property-min_payment_invoice_amount',$provider->min_payment_invoice_amount)}}">

                        @error('property-min_payment_invoice_amount')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Invoice API3 Commission')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('commission-api3_min_commission') is-invalid @enderror"
                            name="commission-api3_min_commission" id="commission-api3_min_commission" value="{{old('commission-api3_min_commission',$provider->api3_min_commission)}}">

                        @error('commission-api3_min_commission')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Invoice API5 Commission')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('commission-api5_min_commission') is-invalid @enderror"
                            name="commission-api5_min_commission" id="commission-api5_min_commission" value="{{old('commission-api5_min_commission',$provider->api5_min_commission)}}">

                        @error('commission-api5_min_commission')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Withdrawal Amount')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property-min_merchant_withdrawal_amount') is-invalid @enderror"
                            name="property-min_merchant_withdrawal_amount" id="property-min_merchant_withdrawal_amount" value="{{old('property-min_merchant_withdrawal_amount',$provider->min_merchant_withdrawal_amount)}}">

                        @error('property-min_merchant_withdrawal_amount')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Withdrawal API3 Commission')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('commission-api3_type2_min_commission') is-invalid @enderror"
                            name="commission-api3_type2_min_commission" id="commission-api3_type2_min_commission" value="{{old('commission-api3_type2_min_commission',$provider->api3_type2_min_commission)}}">

                        @error('commission-api3_type2_min_commission')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Withdrawal API5 Commission')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('commission-api5_type2_min_commission') is-invalid @enderror"
                            name="commission-api5_type2_min_commission" id="commission-api5_type2_min_commission" value="{{old('commission-api5_type2_min_commission',$provider->api5_type2_min_commission)}}">

                        @error('commission-api5_type2_min_commission')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="agent-commission" class="form-label text-center">{{__('Agent Invoice API3 Commission')}}</label>
                    @foreach($provider->agent_api3_invoice_commission as $key => $commission)
                        <div class="row pb-2 " data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="agent-api3-commission" name="agent-api3-invoice-commission[{{$count}}]" value="{{old('agent-api3-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="form-control type-commission" id="agent-api3-commission-type" name="agent-api3-invoice-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('agent-api3-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('agent-api3-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('agent-api3-invoice-commission-value['.$count.']') is-invalid @enderror" id="agent-api3-commission-value" name="agent-api3-invoice-commission-value[{{$count}}]" value="{{old('agent-api3-invoice-commission-value['.$count.']',$commission['value'])}}">
                                        @error('agent-api3-invoice-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->agent_api3_invoice_commission)) == intval($count))
                                <div class="col-1 control-group">
                                        <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}" >{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->agent_api3_invoice_commission)) == intval($count) && (count($provider->agent_api3_invoice_commission)>1))
                                <div class="col-1 control-group">
                                        <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2 " data-count="{{$count = 0}}">
                    <label for="grow-commission" class="form-label text-center">{{__('Grow Invoice API3 Commission')}}</label>
                    @foreach($provider->grow_api3_invoice_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="grow-api3-commission" name="grow-api3-invoice-commission[{{$count}}]" value="{{old('grow-api3-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="form-control type-commission" id="grow-api3-commission-type" name="grow-api3-invoice-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('grow-api3-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('grow-api3-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('grow-api3-invoice-commission-value['.$count.']') is-invalid @enderror" id="grow-api3-commission-value" name="grow-api3-invoice-commission-value[{{$count}}]" value="{{old('grow-api3-invoice-commission-value['.$count.']',$commission['value'])}}">
                                        @error('grow-api3-invoice-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->grow_api3_invoice_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->grow_api3_invoice_commission)) == intval($count) && (count($provider->grow_api3_invoice_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif

                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2 "  data-count="{{$count = 0}}">
                    <label for="service_commission" class="form-label text-center">{{__('Service Invoice API3 Commission')}}</label>
                    @foreach($provider->service_api3_invoice_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                    <div class="col-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control base-commission" id="service_api3-commission" name="service_api3-invoice-commission[{{$count}}]" value="{{old('service_api3-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <select type="text" class="type-commission form-control" id="service_api3-commission-type" name="service_api3-invoice-commission-type[{{$count}}]">
                                    <option value="percentage" @if(old('service_api3-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                    <option value="fixed" @if(old('service_api3-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="value-commission form-control @error('service_api3-invoice-commission-value['.$count.']') is-invalid @enderror" id="service_api3-commission-value" name="service_api3-invoice-commission-value[{{$count}}]" value="{{old('service_api3-invoice-commission-value['.$count.']',$commission['value'])}}">
                                @error('service_api3-invoice-commission-value['.$count.']')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @if((count($provider->service_api3_invoice_commission)) == intval($count))
                        <div class="col-1 control-group">
                            <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                        </div>
                    @endif
                    @if((count($provider->service_api3_invoice_commission)) == intval($count) && (count($provider->service_api3_invoice_commission)>1))
                        <div class="col-1 control-group">
                            <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                        </div>
                    @endif

                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2"  data-count="{{$count = 0}}">
                    <label for="agent-commission" class="form-label text-center">{{__('Agent Invoice API5 Commission')}}</label>
                    @foreach($provider->agent_api5_invoice_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="agent-api5-commission" name="agent-api5-invoice-commission[{{$count}}]" value="{{old('agent-api5-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="agent-api5-commission-type" name="agent-api5-invoice-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('agent-api5-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('agent-api5-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('agent-api5-invoice-commission-value['.$count.']') is-invalid @enderror" id="agent-api5-commission-value" name="agent-api5-invoice-commission-value[{{$count}}]" value="{{old('agent-api5-invoice-commission-value['.$count.']',$commission['value'])}}">
                                        @error('agent-api5-invoice-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->agent_api5_invoice_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->agent_api5_invoice_commission)) == intval($count) && (count($provider->agent_api5_invoice_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2 " data-count="{{$count = 0}}">
                    <label for="grow-commission" class="form-label text-center">{{__('Grow Invoice API5 Commission')}}</label>
                    @foreach($provider->grow_api5_invoice_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="grow-api5-commission" name="grow-api5-invoice-commission[{{$count}}]" value="{{old('grow-api5-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="grow-api5-commission-type" name="grow-api5-invoice-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('grow-api5-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('grow-api5-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('grow-api5-invoice-commission-value['.$count.']') is-invalid @enderror" id="grow-api5-commission-value" name="grow-api5-invoice-commission-value[{{$count}}]" value="{{old('grow-api5-invoice-commission-value['.$count.']',$commission['value'])}}">
                                        @error('grow-api5-invoice-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->grow_api5_invoice_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->grow_api5_invoice_commission)) == intval($count) && (count($provider->grow_api5_invoice_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="service_commission" class="form-label text-center">{{__('Service Invoice API5 Commission')}}</label>
                    @foreach($provider->service_api5_invoice_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="service_api5-commission" name="service_api5-invoice-commission[{{$count}}]" value="{{old('service_api5-invoice-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="service_api5-commission-type" name="service_api5-invoice-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('service_api5-invoice-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('service_api5-invoice-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('service_api5-invoice-commission-value['.$count.']') is-invalid @enderror" id="service_api5-commission-value" name="service_api5-invoice-commission-value[{{$count}}]" value="{{old('service_api5-invoice-commission-value['.$count.']',$commission['value'])}}">
                                        @error('service_api5-invoice-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->agent_api5_invoice_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->agent_api5_invoice_commission)) == intval($count) && (count($provider->agent_api5_invoice_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="agent-commission" class="form-label text-center">{{__('Agent Withdrawal API3 Commission')}}</label>
                    @foreach($provider->agent_api3_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="agent-api3-withdrawal-commission" name="agent-api3-withdrawal-commission[{{$count}}]" value="{{old('agent-api3-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="agent-api3-withdrawal-commission-type" name="agent-api3-withdrawal-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('agent-api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('agent-api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('agent-api3-withdrawal-commission-value['.$count.']') is-invalid @enderror" id="agent-withdrawal-commission-value" name="agent-api3-withdrawal-commission-value[{{$count}}]" value="{{old('agent-api3-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                        @error('agent-api3-withdrawal-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->agent_api3_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->agent_api3_withdrawal_commission)) == intval($count) && (count($provider->agent_api3_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="grow-commission" class="form-label text-center">{{__('Grow Withdrawal API3 Commission')}}</label>
                    @foreach($provider->grow_api3_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="grow-api3-withdrawal-commission" name="grow-api3-withdrawal-commission[{{$count}}]" value="{{old('grow-api3-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="grow-api3-withdrawal-commission-type" name="grow-api3-withdrawal-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('grow-api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('grow-api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('grow-api3-withdrawal-commission-value['.$count.']') is-invalid @enderror" id="grow-api3-withdrawal-commission-value" name="grow-api3-withdrawal-commission-value[{{$count}}]" value="{{old('grow-api3-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                        @error('grow-api3-withdrawal-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->grow_api3_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->grow_api3_withdrawal_commission)) == intval($count) && (count($provider->grow_api3_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="service_commission" class="form-label text-center">{{__('Service Withdrawal API3 Commission')}}</label>
                    @foreach($provider->service_api3_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="service_api3-withdrawal-commission" name="service_api3-withdrawal-commission[{{$count}}]" value="{{old('service_api3-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="service_api3-withdrawal-commission-type" name="service_api3-withdrawal-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('service_api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('service_api3-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('service_api3-withdrawal-commission-value['.$count.']') is-invalid  @enderror" id="service_api3-withdrawal-commission-value" name="service_api3-withdrawal-commission-value[{{$count}}]" value="{{old('service_api3-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                        @error('service_api3-withdrawal-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->service_api3_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->service_api3_withdrawal_commission)) == intval($count) && (count($provider->service_api3_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2"  data-count="{{$count = 0}}">
                    <label for="agent-commission" class="form-label text-center">{{__('Agent Withdrawal API5 Commission')}}</label>
                    @foreach($provider->agent_api5_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="agent-api5-withdrawal-commission" name="agent-api5-withdrawal-commission[{{$count}}]" value="{{old('agent-api5-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="agent-api5-withdrawal-commission-type" name="agent-api5-withdrawal-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('agent-api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('agent-api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('agent-api5-withdrawal-commission-value['.$count.']') is-invalid @enderror" id="agent-api5-withdrawal-commission-value['.$count.']" name="agent-api5-withdrawal-commission-value[{{$count}}]" value="{{old('agent-api5-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                        @error('agent-api5-withdrawal-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->agent_api5_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->agent_api5_withdrawal_commission)) == intval($count) && (count($provider->agent_api5_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2" data-count="{{$count = 0}}">
                    <label for="grow-commission" class="form-label text-center">{{__('Grow Withdrawal API5 Commission')}}</label>
                    @foreach($provider->grow_api5_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control base-commission" id="grow-api5-withdrawal-commission" name="grow-api5-withdrawal-commission[{{$count}}]" value="{{old('grow-api5-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <select type="text" class="type-commission form-control" id="grow-api5-withdrawal-commission-type" name="grow-api5-withdrawal-commission-type[{{$count}}]">
                                            <option value="percentage" @if(old('grow-api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                            <option value="fixed" @if(old('grow-api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="value-commission form-control @error('grow-api5-withdrawal-commission-value['.$count.']') is-invalid @enderror" id="grow-api5-withdrawal-commission-value" name="grow-api5-withdrawal-commission-value[{{$count}}]" value="{{old('grow-api5-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                        @error('grow-api5-withdrawal-commission-value['.$count.']')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @if((count($provider->grow_api5_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->grow_api5_withdrawal_commission)) == intval($count) && (count($provider->grow_api5_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 border">
                <div class="row p-2"  data-count="{{$count = 0}}">
                    <label for="service_commission" class="form-label text-center">{{__('Service Withdrawal API5 Commission')}}</label>
                    @foreach($provider->service_api5_withdrawal_commission as $key => $commission)
                        <div class="row pb-2" data-count="{{$count++}}">
                    <div class="col-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control base-commission" id="service_api5-withdrawal-commission" name="service_api5-withdrawal-commission[{{$count}}]" value="{{old('service_api5-withdrawal-commission['.$count.']', $key)}}" @if($count == 1) readonly @endif>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <select type="text" class="type-commission form-control" id="service_api5-withdrawal-commission-type" name="service_api5-withdrawal-commission-type[{{$count}}]">
                                    <option value="percentage" @if(old('service_api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'percentage') selected @endif>{{__('Percentage')}}</option>
                                    <option value="fixed" @if(old('service_api5-withdrawal-commission-type['.$count.']', $commission['type']) == 'fixed') selected @endif>{{__('Fixed')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" class="value-commission form-control @error('service_api5-withdrawal-commission-value['.$count.']') is-invalid  @enderror" id="service_api5-withdrawal-commission-value" name="service_api5-withdrawal-commission-value[{{$count}}]" value="{{old('service_api5-withdrawal-commission-value['.$count.']',$commission['value'])}}">
                                @error('service_api5-withdrawal-commission-value['.$count.']')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                            @if((count($provider->service_api5_withdrawal_commission)) == intval($count))
                                <div class="col-1 control-group">
                                    <span class="input-group-text add-control" id="basic-addon3" data-key="{{$count}}">{{'+'}}</span>
                                </div>
                            @endif
                            @if((count($provider->service_api5_withdrawal_commission)) == intval($count) && (count($provider->service_api5_withdrawal_commission)>1))
                                <div class="col-1 control-group">
                                    <span class="input-group-text rem-control" id="basic-addon3" data-key="{{intval($count)-1}}">{{'-'}}</span>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Available Coin')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" multiple disabled>
                            @foreach($provider->available_coins as $coin)
                                <option value="{{$coin}}" selected>{{$coin}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Commission Invoice API3 Type')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="commission_api3_invoice_min_commission_type" id="commission_api3_invoice_min_commission_type">
                            <option value="0" @if(old('commission_api3_invoice_min_commission_type' ,$provider->commission_api3_invoice_min_commission_type) == '0') selected @endif>{{__('0')}}</option>
                            <option value="1" @if(old('commission_api3_invoice_min_commission_type' ,$provider->commission_api3_invoice_min_commission_type) == '1') selected @endif>{{__('1')}}</option>
                            <option value="2" @if(old('commission_api3_invoice_min_commission_type' ,$provider->commission_api3_invoice_min_commission_type) == '2') selected @endif>{{__('2')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Commission Invoice API5 Type')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="commission_api5_invoice_min_commission_type" id="commission_api5_invoice_min_commission_type">
                            <option value="0" @if(old('commission_api5_invoice_min_commission_type', $provider->commission_api5_invoice_min_commission_type) == '0') selected @endif>{{__('0')}}</option>
                            <option value="1" @if(old('commission_api5_invoice_min_commission_type', $provider->commission_api5_invoice_min_commission_type) == '1') selected @endif>{{__('1')}}</option>
                            <option value="2" @if(old('commission_api5_invoice_min_commission_type', $provider->commission_api5_invoice_min_commission_type) == '2') selected @endif>{{__('2')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Commission Withdrawal API3 Type')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="commission_api3_withdrawal_min_commission_type" id="commission_api3_withdrawal_min_commission_type">
                            <option value="0" @if(old('commission_api3_withdrawal_min_commission_type', $provider->commission_api3_withdrawal_min_commission_type) == '0') selected @endif>{{__('0')}}</option>
                            <option value="1" @if(old('commission_api3_withdrawal_min_commission_type', $provider->commission_api3_withdrawal_min_commission_type) == '1') selected @endif>{{__('1')}}</option>
                            <option value="2" @if(old('commission_api3_withdrawal_min_commission_type', $provider->commission_api3_withdrawal_min_commission_type) == '2') selected @endif>{{__('2')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Min Commission Withdrawal API5 Type')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="commission_api5_withdrawal_min_commission_type" id="commission_api5_withdrawal_min_commission_type">
                            <option value="0" @if(old('commission_api5_withdrawal_min_commission_type', $provider->commission_api5_withdrawal_min_commission_type) == '0') selected @endif>{{__('0')}}</option>
                            <option value="1" @if(old('commission_api5_withdrawal_min_commission_type', $provider->commission_api5_withdrawal_min_commission_type) == '1') selected @endif>{{__('1')}}</option>
                            <option value="2" @if(old('commission_api5_withdrawal_min_commission_type', $provider->commission_api5_withdrawal_min_commission_type) == '2') selected @endif>{{__('2')}}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-1-1">{{__('Min Sum Invoice API3 Agent')}}:</label>
                    <div class="col-sm-12 min-type-3-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api3_min_sum_agent') is-invalid @enderror"
                            name="commission_invoice_api3_min_sum_agent" id="commission_invoice_api3_min_sum_agent" value="{{old('commission_invoice_api3_min_sum_agent',$provider->invoice_api3_min_sum_agent)}}">

                        @error('commission_invoice_api3_min_sum_agent')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-1-1">{{__('Min Sum Invoice API3 Grow')}}:</label>
                    <div class="col-sm-12 min-type-3-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api3_min_sum_grow') is-invalid @enderror"
                            name="commission_invoice_api3_min_sum_grow" id="commission_invoice_api3_min_sum_grow" value="{{old('commission_invoice_api3_min_sum_grow',$provider->invoice_api3_min_sum_grow)}}">

                        @error('commission_invoice_api3_min_sum_grow')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-1-1">{{__('Min Sum Invoice API3 Service')}}:</label>
                    <div class="col-sm-12 min-type-3-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api3_min_sum_service') is-invalid @enderror"
                            name="commission_invoice_api3_min_sum_service" id="commission_invoice_api3_min_sum_service" value="{{old('commission_invoice_api3_min_sum_service',$provider->invoice_api3_min_sum_service)}}">

                        @error('commission_invoice_api3_min_sum_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-1-2">{{__('Invoice Min Commission API3 Service')}}:</label>
                    <div class="col-sm-12 min-type-3-1-2">
                        <input type="text" class="form-control @error('commission_invoice_api3_min_commission_service') is-invalid @enderror"
                            name="commission_invoice_api3_min_commission_service" id="commission_invoice_api3_min_commission_service" value="{{old('commission_invoice_api3_min_commission_service',$provider->invoice_api3_min_commission_service)}}">

                        @error('commission_invoice_api3_min_commission_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-1-1">{{__('Min Sum Invoice API5 Agent')}}:</label>
                    <div class="col-sm-12 min-type-5-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api5_min_sum_agent') is-invalid @enderror"
                            name="commission_invoice_api5_min_sum_agent" id="commission_invoice_api5_min_sum_agent" value="{{old('commission_invoice_api5_min_sum_agent',$provider->invoice_api5_min_sum_agent)}}">

                        @error('commission_invoice_api5_min_sum_agent')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-1-1">{{__('Min Sum Invoice API5 Grow')}}:</label>
                    <div class="col-sm-12 min-type-5-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api5_min_sum_grow') is-invalid @enderror"
                            name="commission_invoice_api5_min_sum_grow" id="commission_invoice_api5_min_sum_grow" value="{{old('commission_invoice_api5_min_sum_grow',$provider->invoice_api5_min_sum_grow)}}">

                        @error('commission_invoice_api5_min_sum_grow')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-1-1">{{__('Min Sum Invoice API5 Service')}}:</label>
                    <div class="col-sm-12 min-type-5-1-1">
                        <input type="text" class="form-control @error('commission_invoice_api5_min_sum_service') is-invalid @enderror"
                            name="commission_invoice_api5_min_sum_service" id="commission_invoice_api5_min_sum_service" value="{{old('commission_invoice_api5_min_sum_service',$provider->invoice_api5_min_sum_service)}}">

                        @error('commission_invoice_api5_min_sum_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-1-2">{{__('Invoice Min Commission API5 Service')}}:</label>
                    <div class="col-sm-12 min-type-5-1-2">
                        <input type="text" class="form-control @error('commission_invoice_api5_min_commission_service') is-invalid @enderror"
                            name="commission_invoice_api5_min_commission_service" id="commission_invoice_api5_min_commission_service" value="{{old('commission_invoice_api5_min_commission_service',$provider->invoice_api5_min_commission_service)}}">

                        @error('commission_invoice_api5_min_commission_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-2-1">{{__('Min Sum withdrawal API3 Agent')}}:</label>
                    <div class="col-sm-12 min-type-3-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api3_min_sum_agent') is-invalid @enderror"
                            name="commission_withdrawal_api3_min_sum_agent" id="commission_withdrawal_api3_min_sum_agent" value="{{old('commission_withdrawal_api3_min_sum_agent',$provider->withdrawal_api3_min_sum_agent)}}">

                        @error('commission_withdrawal_api3_min_sum_agent')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-2-1">{{__('Min Sum withdrawal API3 Grow')}}:</label>
                    <div class="col-sm-12 min-type-3-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api3_min_sum_grow') is-invalid @enderror"
                            name="commission_withdrawal_api3_min_sum_grow" id="commission_withdrawal_api3_min_sum_grow" value="{{old('commission_withdrawal_api3_min_sum_grow',$provider->withdrawal_api3_min_sum_grow)}}">

                        @error('commission_withdrawal_api3_min_sum_grow')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-2-1">{{__('Min Sum withdrawal API3 Service')}}:</label>
                    <div class="col-sm-12 min-type-3-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api3_min_sum_service') is-invalid @enderror"
                            name="commission_withdrawal_api3_min_sum_service" id="commission_withdrawal_api3_min_sum_service" value="{{old('commission_withdrawal_api3_min_sum_service',$provider->withdrawal_api3_min_sum_service)}}">

                        @error('commission_withdrawal_api3_min_sum_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-3-2-2">{{__('Withdrawal Min Commission API3 Service')}}:</label>
                    <div class="col-sm-12 min-type-3-2-2">
                        <input type="text" class="form-control @error('commission_withdrawal_api3_min_commission_service') is-invalid @enderror"
                            name="commission_withdrawal_api3_commission_service" id="commission_withdrawal_api3_min_commission_service" value="{{old('commission_withdrawal_api3_commission_service',$provider->withdrawal_api3_min_commission_service)}}">

                        @error('commission_withdrawal_api3_min_commission_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-2-1">{{__('Min Sum withdrawal API5 Agent')}}:</label>
                    <div class="col-sm-12 min-type-5-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api5_min_sum_agent') is-invalid @enderror"
                            name="commission_withdrawal_api5_min_sum_agent" id="commission_withdrawal_api5_min_sum_agent" value="{{old('commission_withdrawal_api5_min_sum_agent',$provider->withdrawal_api5_min_sum_agent)}}">

                        @error('commission_withdrawal_api5_min_sum_agent')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-2-1">{{__('Min Sum withdrawal API5 Grow')}}:</label>
                    <div class="col-sm-12 min-type-5-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api5_min_sum_grow') is-invalid @enderror"
                            name="commission_withdrawal_api5_min_sum_grow" id="commission_withdrawal_api5_min_sum_grow" value="{{old('commission_withdrawal_api5_min_sum_grow',$provider->withdrawal_api5_min_sum_grow)}}">

                        @error('commission_withdrawal_api5_min_sum_grow')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-2-1">{{__('Min Sum withdrawal API5 Service')}}:</label>
                    <div class="col-sm-12 min-type-5-2-1">
                        <input type="text" class="form-control @error('commission_withdrawal_api5_min_sum_service') is-invalid @enderror"
                            name="commission_withdrawal_api5_min_sum_service" id="commission_withdrawal_api5_min_sum_service" value="{{old('commission_withdrawal_api5_min_sum_service',$provider->withdrawal_api5_min_sum_service)}}">

                        @error('commission_withdrawal_api5_min_sum_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label min-type-5-2-2">{{__('Withdrawal Min Commission API5 Service')}}:</label>
                    <div class="col-sm-12 min-type-5-2-2">
                        <input type="text" class="form-control @error('commission_withdrawal_api5_min_commission_service') is-invalid @enderror"
                            name="commission_withdrawal_api5_commission_service" id="commission_withdrawal_api5_min_commission_service" value="{{old('commission_withdrawal_api5_commission_service',$provider->withdrawal_api5_min_commission_service)}}">

                        @error('commission_withdrawal_api5_min_commission_service')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="mt-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
    </div>
    @endforeach
    </div>

    <div class="page-header">
        <h1>
            {{ __('Merchant Provider Limit') }}
        </h1>
    </div>

    <ul class="nav nav-tabs" role="tablist">
        @foreach ($service_providers as $i => $provider)
            <li class="nav-item" role="presentation">
                <button class="nav-link {{$i ? '' : 'active'}}" data-bs-toggle="tab" data-bs-target="#spl_{{$provider->id}}" type="button" role="tab">{{$provider->name}}</button>
            </li>
        @endforeach
    </ul>


        <form action="{{ route('merchant.update-limit', $id) }}" method="post" class="tab-content form-ajax row g-3 mb-3">
            @csrf
            @method('PATCH')
            @foreach ($service_providers as $i => $provider)
            <div class="col-md-12 tab-pane fade show {{$i ? '' : 'active'}}" id="spl_{{$provider->id}}">
                <div class="row">
                    @foreach($provider->available_currencies as $currency)
                    <div class="bank-limit-wrapper col-md-12 border-bottom">
                        <div class="row">
                            <div class="bank-limit-container col-md-12" data-currency="{{$currency}}"
                                 data-provider="{{$provider->name}}">
                                <div class="row">
                                    <label class="col-sm-12 col-form-label">{{__('Currency')}}: <strong>{{$currency}}</strong></label>
                                </div>
                                <div class="row g-3 mb-2">
                                    <div class="col-md-2">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Bank Name')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Min Payment Invoice Amount')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Max Payment Invoice Amount')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Min Withdrawal Invoice Amount')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Max Withdrawal Invoice Amount')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">{{__('Action')}}:</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row g-3 mb-2">
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="bank">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="min_payment_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="max_payment_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="min_withdrawal_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="max_withdrawal_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="row">
                                            <button type="button"
                                                    data-action="add"
                                                    class="btn btn-outline-secondary"
                                            >{{__('Add')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label class="col-sm-12 col-form-label">{{__('Bank Limit Traders Added')}}:</label>
                            <div class="col-md-12 pb-2 limit-added" id="bank_limit_sample_{{str_replace(' ','_',$provider->name)}}_{{$currency}}" data-currency="{{$currency}}"
                                 data-provider="{{$provider->name}}" style="display: none;">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="bank">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="min_payment_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="max_payment_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="min_withdrawal_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" name="max_withdrawal_invoice_amount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="row">
                                            <button type="button"
                                                    data-key=""
                                                    data-action="del"
                                                    class="btn btn-outline-secondary"
                                            >{{__('Delete')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bank-limit-added-container col-md-12" data-currency="{{$currency}}"
                                 data-provider="{{$provider->name}}"></div>
                            <input type="hidden" class="form-control" name="provider_currencies_{{str_replace(' ','_',$provider->name)}}" id="provider_currencies_{{str_replace(' ','_',$provider->name)}}" value="{{old('provider_currencies_'.str_replace(' ','_',$provider->name), json_encode($provider->available_currencies, JSON_FORCE_OBJECT))}}">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
            <input type="hidden" name="provider_limit" id="limit_banks_traders" value="">
            @php $v = old('provider_limit', $provider_limit ?? '{}'); @endphp
            <script>limit_banks_traders.value = JSON.stringify({!!$v!!})</script>
            <input type="hidden" class="form-control" name="provider_avalable" id="provider_avalable" value="{{old('provider_avalable', json_encode($avalable_service_providers, JSON_FORCE_OBJECT))}}">
            <div class="mt-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <div class="page-header">
            <h1>
                {{ __('Merchant Chat') }}
            </h1>
        </div>
    <form method="POST" action="{{ route('merchant.update-chat', $id) }}" class="row g-3 mb-3">
        @method('POST')
        @csrf
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Chat Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('chat_name') is-invalid @enderror"
                           name="chat_name" id="chat_name" value="{{old('chat_name', $merchant->chat?->chat_name)}}">

                    @error('chat_name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Chat ID')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('chat_id') is-invalid @enderror"
                           name="chat_id" id="chat_id" value="{{old('chat_id', $merchant->chat?->chat_id)}}">

                    @error('chat_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="mt-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection
@section('script-bottom')
    <script>
        window.onload = function () {
            $(function () {
                var checkType3Invoice = function (){
                    var type3invoice = $('#commission_api3_invoice_min_commission_type').val();
                    if(type3invoice == '0'){
                        $('.min-type-3-1-1').hide();
                        $('.min-type-3-1-2').hide();
                    }else if(type3invoice == '1'){
                        $('.min-type-3-1-1').show();
                        $('.min-type-3-1-2').hide();
                    }else if(type3invoice == '2'){
                        $('.min-type-3-1-1').show();
                        $('.min-type-3-1-2').show();
                    }
                }
                var checkType5Invoice = function (){
                    var type5invoice = $('#commission_api5_invoice_min_commission_type').val();
                    if(type5invoice == '0'){
                        $('.min-type-5-1-1').hide();
                        $('.min-type-5-1-2').hide();
                    }else if(type5invoice == '1'){
                        $('.min-type-5-1-1').show();
                        $('.min-type-5-1-2').hide();
                    }else if(type5invoice == '2'){
                        $('.min-type-5-1-1').show();
                        $('.min-type-5-1-2').show();
                    }
                }

                var checkType3Withdrawal = function (){
                    var type3withdrawal = $('#commission_api3_withdrawal_min_commission_type').val();
                    if(type3withdrawal == '0'){
                        $('.min-type-3-2-1').hide();
                        $('.min-type-3-2-2').hide();
                    }else if(type3withdrawal == '1'){
                        $('.min-type-3-2-1').show();
                        $('.min-type-3-2-2').hide();
                    }else if(type3withdrawal == '2'){
                        $('.min-type-3-2-1').show();
                        $('.min-type-3-2-2').show();
                    }
                }

                var checkType5Withdrawal = function (){
                    var type5withdrawal = $('#commission_api5_withdrawal_min_commission_type').val();
                    if(type5withdrawal == '0'){
                        $('.min-type-5-2-1').hide();
                        $('.min-type-5-2-2').hide();
                    }else if(type5withdrawal == '1'){
                        $('.min-type-5-2-1').show();
                        $('.min-type-5-2-2').hide();
                    }else if(type5withdrawal == '2'){
                        $('.min-type-5-2-1').show();
                        $('.min-type-5-2-2').show();
                    }
                }

                $(document).on("change", "#commission_api3_invoice_min_commission_type", function (e) {
                    checkType3Invoice();
                    return false;
                });

                $(document).on("change", "#commission_api5_invoice_min_commission_type", function (e) {
                    checkType5Invoice();
                    return false;
                });

                $(document).on("change", "#commission_api3_withdrawal_min_commission_type", function (e) {
                    checkType3Withdrawal();
                    return false;
                });

                $(document).on("change", "#commission_api5_withdrawal_min_commission_type", function (e) {
                    checkType5Withdrawal();
                    return false;
                });

                checkType3Invoice();
                checkType5Invoice();
                checkType3Withdrawal();
                checkType5Withdrawal();
            });
            $(document).on('click', '.add-control', function (event){
                var key = parseInt($(this).attr('data-key'));
                var newKey = key+1;
                var re = /(\S+)\[(\d)\]/

                var currentRow = $(this).closest('.row');
                var currentHtml = currentRow.clone();

                var base = currentHtml.find('input.base-commission');
                var baseName = re.exec(base.attr('name'));
                base.attr('name',baseName[1]+'['+newKey+']');
                base.prop( "readonly", false );

                var fee = currentHtml.find('input.value-commission');
                var feeName = re.exec(fee.attr('name'));
                fee.attr('name',feeName[1]+'['+newKey+']');

                var type = currentHtml.find('select.type-commission');
                var typeName = re.exec(type.attr('name'));
                type.attr('name',typeName[1]+'['+newKey+']');

                var currentControl = currentHtml.find('.add-control');

                if(key == 1){
                    var conteiner = currentControl.closest('.control-group');
                    var newControl = conteiner.clone();
                    newControl.find('.add-control').removeClass('add-control').addClass('rem-control').html('-').attr('data-key', 1)
                    currentControl.attr('data-key', newKey)
                    conteiner.after(newControl)
                }else{

                    currentControl.attr('data-key', newKey)
                    currentHtml.find('.rem-control').attr('data-key', key)
                }
                currentRow.after(currentHtml);
                currentRow.find('.control-group').remove()
            })
            $(document).on('click', '.rem-control', function (event){

                var key = parseInt($(this).attr('data-key'));
                var newKey = key-1;
                var currentRow = $(this).closest('.row');
                var currentControl = currentRow.find('.control-group');
                var newControl = currentControl.clone();
                newControl.find('.add-control').attr('data-key', key)
                if(key == 1){
                    delete newControl[1];
                }else{
                    newControl.find('.rem-control').attr('data-key', newKey)
                }
                var oldRow = currentRow.prev();
                currentRow.remove();
                oldRow.append(newControl);
            })
        }
    </script>
@endsection
