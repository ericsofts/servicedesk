<div class="row g-0">
    <div class="col-md-6 pe-md-1">
        <label class="col-sm-12 col-form-label">{{__('Available Provider')}}:</label>

        <div class="form-control p-2 connectedSortable" id="maybe_providers" style="min-height: 78px;">
            @foreach ($maybe_providers as $unit)
                @if (!in_array($unit->id, $available_providers_ids))
                    <div class="ui-state-default px-2 py-0 mb-1 form-control" data-id="{{$unit->id}}">{{$unit->name}}</div>
                @endif
            @endforeach
        </div>
    </div>

    <div class="col-md-6">
        <label class="col-sm-12 col-form-label">{{__('Selected Provider')}}:</label>

        <div class="form-control p-2 connectedSortable" id="available_providers" style="min-height: 78px;">
            @foreach ($available_providers as $unit)
                <div class="ui-state-default px-2 py-0 mb-1 form-control" data-id="{{$unit->service_provider->id}}">{{$unit->service_provider->name}}</div>
            @endforeach
        </div>
    </div>

    <input type="hidden" name="available_providers_ids" id="available_providers_ids" value="">
</div>

<style>
    #available_providers .ui-state-default {
        color: #0f5132;
        background-color: #d1e7dd;
        border-color: #badbcc;
    }
</style>