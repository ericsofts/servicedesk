@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('merchant.index')}}">{{__('Merchants')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Merchants New') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('merchant.create') }}" class="row g-3 mb-3">
        @csrf
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           value="{{old('name')}}"
                           name="name" id="name">

                    @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Email')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                           value="{{old('email')}}"
                           name="email" id="email">

                    @error('email')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Password')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control"
                           name="password" id="password" value="{{old('password', $password)}}">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Agent')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="agent_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($agents as $key => $value)
                            <option value="{{$key}}" @if(old('agent_id') == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('status') == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status') == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Enable 2FA')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="enable_2fa">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('enable_2fa') == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('enable_2fa') == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4 d-none">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Token')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('token') is-invalid @enderror"
                           name="token" id="token" value="{{old('token',$token)}}">

                    @error('token')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4 d-none">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Payment Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('payment_key') is-invalid @enderror"
                           name="payment_key" id="payment_key" value="{{old('payment_key',$payment_key)}}">

                    @error('payment_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('API3 Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('api3_key') is-invalid @enderror"
                           name="api3_key" id="api3_key" value="{{old('api3_key',$api3_key)}}">

                    @error('api3_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('API5 Key')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('api5_key') is-invalid @enderror"
                           name="api5_key" id="api5_key" value="{{old('api5_key',$api5_key)}}">

                    @error('api5_key')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Merchant Type')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="type">
                        <option value="0" @if(old('type') == '0') selected @endif>{{__('0')}}</option>
                        <option value="1" @if(old('type') == '1') selected @endif>{{__('1')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-4">           
            @include('merchant.avaiprovcomp')
        </div>

        <div class="mt-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection
@section('script-bottom')
    <script>
        window.onload = function () {
            $(function () {
                var checkType = function () {
                    var typeValue = $('#commission-min_commission_type').val();
                    if (typeValue == '0') {
                        $('.min-type-1').hide();
                        $('.min-type-2').hide();
                    } else if (typeValue == '1') {
                        $('.min-type-1').show();
                        $('.min-type-2').hide();
                    } else if (typeValue == '2') {
                        $('.min-type-1').show();
                        $('.min-type-2').show();
                    }
                }
                $(document).on("change", "#commission-min_commission_type", function (e) {
                    checkType();
                    return false;
                });
                checkType();
            });
        }
    </script>
@endsection
