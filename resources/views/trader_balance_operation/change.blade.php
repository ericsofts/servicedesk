@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.index')}}">{{__('Traders')}}</a></li>
            <li class="breadcrumb-item active">{{__($title)}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{__($title)}}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader.balance-change', $id) }}" class="row g-3 mb-3">
        @method('PATCH')
        @csrf
        <div class="col-md-12">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Balance Type')}}:</label>
                <div class="col-sm-12">
                    <select class="form-control" name="type" id="type">
                        <option value="0">Normal</option>>
                    </select>
                </div>

                <label class="col-sm-12 col-form-label">{{__('Currency')}}: ( {{__('Available Amount')}} <span id="currency_amount">0</span>)</label>
                <div class="col-sm-12">
                    <select class="form-control" name="currency_id" id="currency_id">
                        @foreach($currencies as $currency)
                            <option value="{{$currency->id}}">{{$currency->name}}</option>>
                        @endforeach
                    </select>
                </div>

                <label class="col-sm-12 col-form-label">{{__('Amount')}}:</label>
                <div class="col-sm-12">
                    <input type="number" step="0.00000001" class="form-control @error('amount') is-invalid @enderror"
                           name="amount" id="balance-amount" required>
                    @error('balance-amount')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>

                <label class="col-sm-12 col-form-label">{{__('Comment')}}:</label>
                <div class="col-sm-12">
                    <textarea name="comment" class="form-control"></textarea>
                    <input type="hidden" name="invoice_type" value="{{$invoice_type}}">
                </div>
            </div>

            <div class="mt-3">
                <button type="submit" class="btn btn-grow btn-confirm-default">{{__('Submit')}}</button>
            </div>
        </div>
    </form>

    <script>
        $(document).on('change', '#currency_id', function(){
            var currency_id = $(this).val(),
                trader_id = "{{ $id }}",
                type = $('#type').val(),
                _token = $('input[name="_token"]').val();
            var action = "{{ route('trader.show-balance') }}";
            var data = {
                "currency_id":currency_id,
                "trader_id":trader_id,
                "_token":_token,
                "type": type
            }
            $.ajax({
                url: action,
                data: data,
                method: "POST",
                success: function(response){
                    console.log(response)
                    $('#currency_amount').text(response.answer);
                }
            })
        });
        $('#currency_id').trigger('change');
    </script>
@endsection
