<?php
use App\Models\TraderBalanceQueue;
?>

@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.index')}}">{{__('Traders')}}</a></li>
            <li class="breadcrumb-item active">{{__('Trader Balance Operations')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader Balance Operations') }}
        </h1>
    </div>

    <form method="GET" action="{{ route('trader.balance_operation') }}" class="row g-3 mb-3">
        <div class="col-3">
            <div class="row">
                <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                        <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}"
                                value="{{request('from_date')}}" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="row">
                <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                <div class="col-sm-10">
                    <div class="input-group">
                        <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                        <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}"
                                value="{{request('to_date')}}" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <label class="col-sm-4 col-form-label">{{__('Trader')}}:</label>
                <div class="col-sm-8">
                    <select name="traders[]" class="form-control select2" multiple="multiple">
                        @foreach($traders as $k => $v)
                            <option
                                value="{{$k}}"
                                @if(in_array($k, request('traders') ?? [])) selected @endif>{{$v}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <label class="col-sm-3 col-form-label">{{__('Type')}}:</label>
                <div class="col-sm-9">
                    <select class="form-select" name="type">
                        <option value="">{{__('Choose')}}</option>
                        <option value="{{TraderBalanceQueue::TYPE_CREDIT}}" @if($type === (string) TraderBalanceQueue::TYPE_CREDIT) selected @endif>{{__(TraderBalanceQueue::typeTitles()[TraderBalanceQueue::TYPE_CREDIT])}}</option>
                        <option value="{{TraderBalanceQueue::TYPE_DEBIT}}" @if($type === (string) TraderBalanceQueue::TYPE_DEBIT) selected @endif>{{__(TraderBalanceQueue::typeTitles()[TraderBalanceQueue::TYPE_DEBIT])}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-3">
            <div class="row">
                <label class="col-sm-4 col-form-label">{{__('Search')}}:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="q" placeholder="{{__('Comment')}}" value="{{ request()->input('q') }}">
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">{{__('ID')}}</th>
            <th scope="col">{{__('Date')}}</th>
            <th scope="col">{{__('Trader')}}</th>
            <th scope="col">{{__('Type')}}</th>
            <th scope="col">{{__('Amount')}}</th>
            <th scope="col">{{__('SD User')}}</th>
            <th scope="col">{{__('Comment')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($operations as $i => $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{datetimeFormat($item->created_at)}}</td>
                <td><a href="{{route('trader.edit', ['id' => $item->trader_id])}}">{{$item->trader->name}}</a></td>
                <td>{{__(TraderBalanceQueue::typeTitles()[$item->type])}}</td>
                <td>{{$item->amount}}</td>
                <td>{{$item->sd_user->name}}</td>
                <td>{{$item->comment}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-end">{{ $operations->links() }}</div>
@endsection
