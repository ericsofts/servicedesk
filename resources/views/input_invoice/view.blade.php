@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('input_invoice.index')}}">{{__('Input invoices')}}</a></li>
            <li class="breadcrumb-item active">{{__('Input invoice details')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Input invoice details') }}
        </h1>
    </div>
    @if($invoice)
        <div class="invoice p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-md-6 col-lg-4">
                    <p class="fw-bold">{{__('Invoice #')}} {{$invoice->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('ID')}}</dt>
                        <dd class="col-sm-8">{{$invoice->id}}</dd>
                        <dt class="col-sm-4">{{__('Amount')}}</dt>
                        <dd class="col-sm-8">{{$invoice->amount}}</dd>
                        <dt class="col-sm-4">{{__('Status')}}</dt>
                        <dd class="col-sm-8">{{input_invoice_status($invoice->status)}}</dd>
                        @if($invoice->status == 1 && $invoice->payed)
                            <dt class="col-sm-4">{{__('Paid')}}</dt>
                            <dd class="col-sm-8">{{datetimeFormat($invoice->payed)}}</dd>
                        @endif
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($invoice->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($invoice->updated_at)}}</dd>
                    </dl>
                </div>
                <div class="col-md-6 col-lg-4">
                    <p class="fw-bold">{{__('User')}}</p>
                    @if(!empty($invoice->user))
                        <dl class="row">
                            <dt class="col-sm-4">{{__('ID')}}</dt>
                            <dd class="col-sm-8">{{$invoice->user->id}}</dd>
                            <dt class="col-sm-4">{{__('Account ID')}}</dt>
                            <dd class="col-sm-8">{{$invoice->user->account_id}}</dd>
                            <dt class="col-sm-4">{{__('Name')}}</dt>
                            <dd class="col-sm-8">{{$invoice->user->name}}</dd>
                            <dt class="col-sm-4">{{__('Email')}}</dt>
                            <dd class="col-sm-8">{{$invoice->user->email}}</dd>
                        </dl>
                    @endif
                </div>
            </div>
            @if(!empty($invoice->payment_id))
                <div>
                    <form method="post" action="{{ route('chatex.send_dispute') }}">
                        @method('POST')
                        @csrf
                        <input type="hidden" name="payment_id" value="{{$invoice->payment_id}}">
                        <button type="submit" class="btn btn-grow btn-confirm-default">{{__('Dispute')}}</button>
                    </form>
                </div>
            @endif
            @if(!empty($invoice->payment_system == 2 && $contact_res))
                <hr>
                <div class="row">
                    @if(!empty($contact_res['data']))
                        @if(!empty($addition_info['ad']))
                            <div class="col-md-6 col-lg-4">
                                <p class="fw-bold">{{__('Ad')}}</p>
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ad_id')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['id'] ?? $addition_info['ad']['ad_id']}}</dd>
                                    <dt class="col-sm-4">{{__('coin')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['coin'] ?? null}}</dd>

                                    @if ($addition_info['ad']['account_info'] ?? '')
                                        <dt class="col-sm-4">{{__('account_info')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['account_info']}}</dd>
                                    @endif

                                    @if ($addition_info['ad']['msg'] ?? '')
                                        <dt class="col-sm-4">{{__('msg')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['msg']}}</dd>
                                    @endif

                                    <dt class="col-sm-4">{{__('temp_price')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['temp_price']}}</dd>
                                    <dt class="col-sm-4">{{__('bank_name')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['bank_name']}}</dd>
                                    <dt class="col-sm-4">{{__('card_number')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['card_number'] ?? $addition_info['ad']['clean_card_number']}}</dd>

                                    @if ($addition_info['ad']['min_amount'] ?? '')
                                        <dt class="col-sm-4">{{__('min_amount')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['min_amount']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount'] ?? '')
                                        <dt class="col-sm-4">{{__('max_amount')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['max_amount']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount_available'] ?? '')
                                        <dt class="col-sm-5">{{__('max_amount_available')}}</dt>
                                        <dd class="col-sm-7">{{$addition_info['ad']['max_amount_available']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount_available'] ?? '')
                                        <dt class="col-sm-5">{{__('max_amount_available')}}</dt>
                                        <dd class="col-sm-7">{{$addition_info['ad']['max_amount_available']}}</dd>
                                    @endif
                                </dl>
                            </div>
                        @endif
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Contact Info')}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('contact_id')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['contact_id']}}</dd>
                                <dt class="col-sm-4">{{__('currency')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['currency']}}</dd>
                                <dt class="col-sm-4">{{__('amount')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['amount']}}</dd>
                                @if(!empty($contact_res['data']['amount_btc']))
                                    <dt class="col-sm-4">{{__('amount_btc')}}</dt>
                                    <dd class="col-sm-8">{{$contact_res['data']['amount_btc']}}</dd>
                                @endif
                            </dl>
                            <p class="fw-bold">{{__('Seller')}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('username')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['seller']['username']}}</dd>
                                <dt class="col-sm-4">{{__('name')}}</dt>
                                <dd class="col-sm-8">{{$contact_res['data']['seller']['name']}}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Status')}}</p>
                            <dl class="row">
                                <dt class="col-sm-5">{{__('created_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['created_at'])}}</dd>
                                <dt class="col-sm-5">{{__('funded_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['funded_at'])}}</dd>
                                <dt class="col-sm-5">{{__('payment_completed_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['payment_completed_at'])}}</dd>
                                <dt class="col-sm-5">{{__('released_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['released_at'])}}</dd>
                                <dt class="col-sm-5">{{__('closed_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['closed_at'])}}</dd>
                                <dt class="col-sm-5">{{__('canceled_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['canceled_at'])}}</dd>
                                <dt class="col-sm-5">{{__('disputed_at')}}</dt>
                                <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['disputed_at'])}}</dd>
                            </dl>
                        </div>
                    @else
                        <div class="alert alert-danger" role="alert">
                            {{$contact_res['message']}} ({{$contact_res['error_code']}})
                        </div>
                    @endif
                </div>
                <hr>
                <p class="fw-bold">
                    {{__('Messages')}}
                    @if(!empty($messages['data']['message_count']))
                        <span class="badge bg-secondary">{{$messages['data']['message_count']}}</span>
                    @endif
                </p>
                @if(!empty($messages['data']['message_list']))
                    <div class="row">
                        <div class="col-12 messages">
                            @foreach($messages['data']['message_list'] as $message)
                                <div class="message">
                                    <div class="user-block">
                                        <span class="username">{{$message['sender']['name']}}</span>
                                        <span class="created">{{datetimeFormat($message['created_at'])}}</span>
                                    </div>
                                    @if(!empty($message['attachment_url']) && str_contains($message['attachment_type'], 'image'))
                                        <a href="{{$message['attachment_url']}}" target="_blank">
                                            <img
                                                src="{{route('chatex.image', base64_encode($message['attachment_url']))}}"
                                                class="img-thumbnail w-25">
                                        </a>
                                    @else

                                        <p>{!! nl2br(e($message['msg'])) !!}</p>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @elseif(!empty($messages['message']))
                    <div class="alert alert-danger" role="alert">
                        {{$contact_res['message']}} ({{$contact_res['error_code']}})
                    </div>
                @endif
                <div class="row">
                    <div class="col-12 form-message">
                        <form method="post" action="{{ route('chatex.send_message') }}" enctype="multipart/form-data">
                            @method('POST')
                            @csrf
                            <input type="hidden" name="payment_id" value="{{$invoice->payment_id}}">
                            <div class="mb-3">
                                <label for="message" class="form-label">{{__('Message')}}</label>
                                <textarea class="form-control @error('message') is-invalid @enderror" id="message"
                                          name="message" rows="3"></textarea>
                                @error('message')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="document" class="form-label">{{__('Document')}}</label>
                                <input class="form-control" type="file" id="document" name="document">
                            </div>
                            <div class="p-3">
                                <button type="submit" class="btn btn-grow">{{__('Send')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    @endif
@endsection
