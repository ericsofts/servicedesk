@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Input invoices')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Input invoices') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('input_invoice.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value="2" @if($status === "2") selected @endif>{{__('Confirmed')}}</option>
                            <option value="5" @if($status === "5") selected @endif>{{__('User Selected')}}</option>
                            <option value="3" @if($status === "3") selected @endif>{{__('Trader Confirm')}}</option>
                            <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Payment system')}}</th>
                <th scope="col">{{__('Payment ID')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('User')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{payment_system($item->payment_system)}}</td>
                    <td>{{$item->payment_id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>
                        @if(!empty($item->user))
                            <a href="{{route('user.view', $item->user->id)}}">{{$item->user->name}}</a>
                        @endif
                    </td>
                    <td>{{input_invoice_status($item->status)}}</td>
                    <td>
                        <a href="{{route('input_invoice.view', $item->id)}}">
                            <i class="bi bi-info-circle"></i>
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
