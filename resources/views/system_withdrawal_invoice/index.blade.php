@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('System Withdrawal history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('System Withdrawal history') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('system.balance.withdrawal_invoice.index') }}" class="row g-3 mb-3">
            <div class="col">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <label class="col-sm-4 col-form-label">{{__('System balance')}}:</label>
                    <div class="col-sm-8">
                        <select name="system_balances[]" class="form-control select2" multiple="multiple">
                            @foreach($system_balances as $k => $v)
                                <option
                                    value="{{$k}}"
                                    @if(in_array($k, request('system_balances') ?? [])) selected @endif>{{$v}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('System balance')}}</th>
                <th scope="col">{{__('SD user')}}</th>
                <th scope="col">{{__('Comment')}}</th>
                <th scope="col">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="2">{{__('Total')}}</td>
                <td class="fw-bold">{{price_format($total->total_amount)}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach($invoices as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>

                    <td>{{price_format($item->amount)}}</td>
                    <td>{{$item->system_balance->name ?? 'CHATEX'}}</td>
                    <td>{{$item->sd_user->name}}</td>
                    <td>{!! nl2br($item->comment) !!}</td>
                    <td>{{payment_invoice_status($item->status)}}</td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
