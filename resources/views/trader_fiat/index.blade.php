@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Trader fiats')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader fiats') }}
        </h1>
    </div>

    @can('trader_fiat.new')
        <div class="col-md-3">
            <a class="btn btn-grow" href="{{ route('trader_fiat.new') }}" role="button">{{__('Add New')}}</a>
        </div>
    @endcan

    @if($trader_fiats)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Asset')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Precision')}}</th>
                <th scope="col">{{__('Payment Systems')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($trader_fiats as $i => $fiat)
                <tr>
                    <td>{{$fiat->id}}</td>
                    <td>{{$fiat->asset}}</td>
                    <td>{{$fiat->name}}</td>
                    <td>{{$fiat->precision}}</td>
                    <td>
                        @foreach ($fiat->payment_systems as $unit)
                            <div class="badge bg-black">{{$unit->name}}</div>
                        @endforeach
                    </td>
                    <td>{{__(['InActive', 'Active'][$fiat->status])}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('trader_fiat.view', $fiat->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('trader_fiat.edit')
                            <a href="{{route('trader_fiat.edit', $fiat->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-arrow-clockwise"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endif
@endsection
