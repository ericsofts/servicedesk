@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_fiat.index')}}">{{__('Trader fiats')}}</a></li>
            <li class="breadcrumb-item active">{{$trader_fiat->fiat_id}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader fiat details') }}
        </h1>
    </div>
    @if($trader_fiat)
        <div class="p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('ID')}} {{$trader_fiat->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Asset')}}</dt>
                        <dd class="col-sm-8">{{$trader_fiat->asset}}</dd>
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$trader_fiat->name}}</dd>
                        <dt class="col-sm-4">{{__('Precision')}}</dt>
                        <dd class="col-sm-8">{{$trader_fiat->precision}}</dd>

                        <dt class="col-sm-4">{{__('Fiats')}}</dt>
                        <dd class="col-sm-8">
                            @foreach ($trader_fiat->payment_systems as $item)
                                <div class="badge bg-black">{{$item->name}}</div>
                            @endforeach
                        </dd>

                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_fiat->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_fiat->updated_at)}}</dd>
                    </dl>
                </div>
            </div>

        </div>
    @endif
@endsection
