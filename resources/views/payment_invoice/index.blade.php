@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Payment invoices')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Payment invoices') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('payment_invoice.index') }}" class="row g-3 mb-3">
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}"
                                   value="{{request('from_date')}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}"
                                   value="{{request('to_date')}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-3 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-9">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value="2" @if($status === "2") selected @endif>{{__('Confirmed')}}</option>
                            <option value="5" @if($status === "5") selected @endif>{{__('User Selected')}}</option>
                            <option value="3" @if($status === "3") selected @endif>{{__('Trader accepted')}}</option>
                            <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                            <option value="97" @if($status === "97") selected @endif>{{__('Reverted')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-4 col-form-label">{{__('Merchant')}}:</label>
                    <div class="col-sm-8">
                        <select name="merchants[]" class="form-control select2" multiple="multiple">
                            @foreach($merchants as $k => $v)
                                <option
                                    value="{{$k}}"
                                    @if(in_array($k, request('merchants') ?? [])) selected @endif>{{$v}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-4 col-form-label">{{__('Search')}}:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="q" value="{{ request()->input('q') }}">
                    </div>
                </div>
            </div>
            <div class="col">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">{{__('ID')}}</th>
                    <th scope="col">{{__('Order ID')}}</th>
                    <th scope="col">{{__('Payment ID')}}</th>
                    <th scope="col">{{__('Date')}}</th>
                    <th scope="col">{{__('Amount')}}</th>
                    <th scope="col">{{__('Amount to pay')}}</th>
                    @can('payment_invoice.commission_amount')
                        <th scope="col">{{__('Grow')}}</th>
                        <th scope="col">{{__('Service')}}</th>
                        <th scope="col">{{__('Agent')}}</th>
                    @endcan
                    <th scope="col">{{__('Fiat currency')}}</th>
                    <th scope="col">{{__('Fiat amount')}}</th>
                    <th scope="col">{{__('Coin')}}</th>
                    <th scope="col">{{__('Merchant')}}</th>
                    <th scope="col">{{__('User')}}</th>
                    <th scope="col">{{__('Api')}}</th>
                    <th scope="col">{{__('Service Provider')}}</th>
                    <th scope="col">{{__('Status')}}</th>
                    <th scope="col">{{__('Action')}}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="4">{{__('Total')}}</td>
                    <td class="fw-bold">{{price_format($total->total_amount)}}</td>
                    <td class="fw-bold">{{price_format($total->total_amount2pay)}}</td>
                    @can('payment_invoice.commission_amount')
                        <td class="fw-bold">{{price_format($total->total_amount2grow)}}</td>
                        <td class="fw-bold">{{price_format($total->total_amount2service)}}</td>
                        <td class="fw-bold">{{price_format($total->total_amount2agent)}}</td>
                    @endcan
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">{{__('Total page')}}</td>
                    <td class="fw-bold">{{price_format($total_page['amount'])}}</td>
                    <td class="fw-bold">{{price_format($total_page['amount2pay'])}}</td>
                    @can('payment_invoice.commission_amount')
                        <td class="fw-bold">{{price_format($total_page['amount2grow'])}}</td>
                        <td class="fw-bold">{{price_format($total_page['amount2service'])}}</td>
                        <td class="fw-bold">{{price_format($total_page['amount2agent'])}}</td>
                    @endcan
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($invoices as $i => $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td class="text-break" style="max-width: 200px">
                            {{$item->merchant_order_id}}
                            @if($item->merchant_order_desc)
                                <br>
                                <span>{{$item->merchant_order_desc}}</span>
                            @endif
                        </td>
                        <td>{{$item->payment_id}}</td>
                        <td>{{datetimeFormat($item->created_at)}}</td>
                        <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                        <td>{{$fmt->formatCurrency(price_format($item->amount2pay), "USD")}}</td>
                        @can('payment_invoice.commission_amount')
                            <td>{{price_format($item->amount2grow)}}</td>
                            <td>{{price_format($item->amount2service)}}</td>
                            <td>{{price_format($item->amount2agent)}}</td>
                        @endcan
                        <td>{{$item->fiat_currency}}</td>
                        <td>{{price_format($item->fiat_amount)}}</td>
                        <td>{{$item->ps_currency}}</td>
                        <td>
                            <a href="{{route('merchant.view', $item->merchant->id)}}">{{$item->merchant->name}}</a>
                        </td>
                        <td>
                            @if(!empty($item->user))
                                <a href="{{route('user.view', $item->user->id)}}">{{$item->user->name}}</a>
                            @endif
                        </td>
                        <td>{{$item->api_type}}</td>
                        <td>{{$item->service_provider ? $item->service_provider->name : ''}}</td>
                        <td>{{payment_invoice_status($item->status)}}</td>
                        <td>
                            <div class="d-flex mb-3">
                                <a href="{{route('payment_invoice.view', $item->id)}}" class="btn btn-outline-secondary me-1" title="{{__('Info')}}">
                                    <i class="bi bi-info-circle"></i>
                                </a>
                                @if(!empty($item->merchant_server_url) && in_array($item->status, [1, 99, 98]))
                                    <form method="POST" action="{{ route('payment_invoice.resend_callback', $item->id) }}">
                                    @csrf
                                        <button type="button" class="btn btn-outline-secondary btn-resend-callback me-1" title="{{__('Re-send callback')}}">
                                            <i class="bi bi-reply"></i>
                                        </button>
                                    </form>
                                @endif
                                @if($item->status != 1)
                                    <form method="POST" action="{{ route('payment_invoice.set_status_payed', $item->id) }}">
                                        @csrf
                                        <button type="button" class="btn btn-outline-secondary btn-status-paid" title="{{__('Set status payed')}}">
                                            <i class="bi bi-arrow-up-circle"></i>
                                        </button>
                                    </form>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection

@section('script-bottom')
    <script>
        $(function () {
            var swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    container: 'swal2-grow',
                    content: 'text-danger fw-bold',
                    confirmButton: 'btn btn-grow',
                    cancelButton: 'btn btn-link'
                },
                buttonsStyling: false
            })
            $(document).on("click", ".btn-resend-callback", function (e) {
                e.preventDefault();
                var form = $(this).closest('form');
                var html = "{{__('Re-send callback to merchant server')}}<br>{{__('If you want to continue write "CONFIRM"?')}}";
                swalWithBootstrapButtons.fire({
                    html: html,
                    input: 'text',
                    inputPlaceholder: 'CONFIRM',
                    inputValidator: (value) => {
                        if (value !== 'CONFIRM') {
                            return 'You need to write "CONFIRM"!'
                        }
                    },
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{__('Submit')}}',
                    cancelButtonText: '{{__('Cancel')}}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
                return false;
            });
            $(document).on("click", ".btn-status-paid", function (e) {
                e.preventDefault();
                var form = $(this).closest('form');
                var html = "{{__('Change invoice status to paid')}}<br>{{__('If you want to continue write "YES"?')}}";
                swalWithBootstrapButtons.fire({
                    html: html,
                    input: 'text',
                    inputPlaceholder: 'YES',
                    inputValidator: (value) => {
                        if (value !== 'YES') {
                            return 'You need to write "YES"!'
                        }
                    },
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{__('Submit')}}',
                    cancelButtonText: '{{__('Cancel')}}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
                return false;
            });
            $(document).on("click", ".btn-status-revert", function (e) {
                e.preventDefault();
                var form = $(this).closest('form');
                var html = "{{__('Change invoice status to revert')}}<br>{{__('If you want to continue write "YES"?')}}";
                swalWithBootstrapButtons.fire({
                    html: html,
                    input: 'text',
                    inputPlaceholder: 'YES',
                    inputValidator: (value) => {
                        if (value !== 'YES') {
                            return 'You need to write "YES"!'
                        }
                    },
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{__('Submit')}}',
                    cancelButtonText: '{{__('Cancel')}}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
                return false;
            });
        });
    </script>
@endsection
