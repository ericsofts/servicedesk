@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('payment_invoice.index')}}">{{__('Payment invoices')}}</a></li>
            <li class="breadcrumb-item active">{{__('Payment invoice details')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Payment invoice details') }}
        </h1>
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="invoice-tab" data-bs-toggle="tab" href="#invoice" role="tab" aria-controls="invoice" aria-selected="true">Invoice</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="history-tab" data-bs-toggle="tab" href="#history" role="tab" aria-controls="history" aria-selected="false">History</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="invoice" role="tabpanel" aria-labelledby="home-tab">
            @if($invoice)
                <div class="invoice p-3 mb-3">
                    <div class="row invoice-info">
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Invoice #')}} {{$invoice->id}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('ID')}}</dt>
                                <dd class="col-sm-8">{{$invoice->id}}</dd>
                                <dt class="col-sm-4">{{__('Service Provider')}}</dt>
                                <dd class="col-sm-8">{{$invoice->service_provider ? $invoice->service_provider->name : ''}}</dd>
                                <dt class="col-sm-4">{{__('Number')}}</dt>
                                <dd class="col-sm-8">{{$invoice->invoice_number}}</dd>
                                <dt class="col-sm-4">{{__('Amount')}}</dt>
                                <dd class="col-sm-8">{{$invoice->amount}}</dd>
                                <dt class="col-sm-4">{{__('Amount to pay')}}</dt>
                                <dd class="col-sm-8">{{$invoice->amount2pay}}</dd>
                                <dt class="col-sm-4">{{__('Status')}}</dt>
                                <dd class="col-sm-8">{{payment_invoice_status($invoice->status)}}</dd>
                                @if($invoice->status == 1 && $invoice->payed)
                                    <dt class="col-sm-4">{{__('Paid')}}</dt>
                                    <dd class="col-sm-8">{{datetimeFormat($invoice->payed)}}</dd>
                                @endif
                                <dt class="col-sm-4">{{__('Created')}}</dt>
                                <dd class="col-sm-8">{{datetimeFormat($invoice->created_at)}}</dd>
                                <dt class="col-sm-4">{{__('Updated')}}</dt>
                                <dd class="col-sm-8">{{datetimeFormat($invoice->updated_at)}}</dd>
                                <dt class="col-sm-4">{{__('Api')}}</dt>
                                <dd class="col-sm-8">{{$invoice->api_type}}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('Merchant')}}</p>
                            <dl class="row">
                                <dt class="col-sm-4">{{__('ID')}}</dt>
                                <dd class="col-sm-8">{{$invoice->merchant->id}}</dd>
                                <dt class="col-sm-4">{{__('Name')}}</dt>
                                <dd class="col-sm-8">{{$invoice->merchant->name}}</dd>
                                <dt class="col-sm-4">{{__('Email')}}</dt>
                                <dd class="col-sm-8">{{$invoice->merchant->email}}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <p class="fw-bold">{{__('User')}}</p>
                            @if(!empty($invoice->user))
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ID')}}</dt>
                                    <dd class="col-sm-8">{{$invoice->user->id}}</dd>
                                    <dt class="col-sm-4">{{__('Account ID')}}</dt>
                                    <dd class="col-sm-8">{{$invoice->user->account_id}}</dd>
                                    <dt class="col-sm-4">{{__('Name')}}</dt>
                                    <dd class="col-sm-8">{{$invoice->user->name}}</dd>
                                    <dt class="col-sm-4">{{__('Email')}}</dt>
                                    <dd class="col-sm-8">{{$invoice->user->email}}</dd>
                                </dl>
                            @endif
                        </div>
                    </div>

                    @if(!empty($invoice->payment_id) && $is_chatex)
                        <div class="mb-2">
                            <form method="post" action="{{ route('chatex.send_dispute') }}">
                                @method('POST')
                                @csrf
                                <input type="hidden" name="payment_id" value="{{$invoice->payment_id}}">
                                <button type="submit" class="btn btn-grow btn-confirm-default">{{__('Dispute')}}</button>
                            </form>
                        </div>
                    @endif
                    @can('payment_invoice.change_amount')
                        @if(\App\Models\PaymentInvoice::isCancelStatus($invoice->status))
                            <a href="#" class="btn btn-grow btn-change-amount">{{__('Change Amount')}}</a>
                        @endif
                    @endcan
                    @can('payment_invoice.set_status_revert')
                        @if($invoice->status == 1)
                            <form method="POST" action="{{ route('payment_invoice.set_status_revert', $invoice->id) }}">
                                @csrf
                                <button type="submit" class="btn btn-outline-secondary btn-status-revert" title="{{__('Set status revert')}}">
                                    <i class="bi bi-arrow-counterclockwise"></i> {{__('Set status revert')}}
                                </button>
                            </form>
                        @endif
                    @endcan
                    @if(!empty($invoice->payment_id))
                        <hr>
                        <div class="row">
                            @if(!empty($addition_info['ad']))
                            <div class="col-md-6 col-lg-4">
                                <p class="fw-bold">{{__('Ad')}}</p>
                                <dl class="row">
                                    <dt class="col-sm-4">{{__('ad_id')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['id'] ?? $addition_info['ad']['ad_id']}}</dd>
                                    <dt class="col-sm-4">{{__('coin')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['coin'] ?? null}}</dd>

                                    @if ($addition_info['ad']['account_info'] ?? '')
                                        <dt class="col-sm-4">{{__('account_info')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['account_info']}}</dd>
                                    @endif

                                    @if ($addition_info['ad']['msg'] ?? '')
                                        <dt class="col-sm-4">{{__('msg')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['msg']}}</dd>
                                    @endif

                                    <dt class="col-sm-4">{{__('temp_price')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['temp_price']}}</dd>
                                    <dt class="col-sm-4">{{__('bank_name')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['bank_name']}}</dd>
                                    <dt class="col-sm-4">{{__('card_number')}}</dt>
                                    <dd class="col-sm-8">{{$addition_info['ad']['card_number'] ?? $addition_info['ad']['clean_card_number']}}</dd>

                                    @if ($addition_info['ad']['min_amount'] ?? '')
                                        <dt class="col-sm-4">{{__('min_amount')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['min_amount']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount'] ?? '')
                                        <dt class="col-sm-4">{{__('max_amount')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['max_amount']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount_available'] ?? '')
                                        <dt class="col-sm-5">{{__('max_amount_available')}}</dt>
                                        <dd class="col-sm-7">{{$addition_info['ad']['max_amount_available']}}</dd>
                                    @endif
                                    @if ($addition_info['ad']['max_amount_available'] ?? '')
                                        <dt class="col-sm-5">{{__('max_amount_available')}}</dt>
                                        <dd class="col-sm-7">{{$addition_info['ad']['max_amount_available']}}</dd>
                                    @endif
                                </dl>
                            </div>
                            @endif

                            @if($is_trader)
                                <div class="col-md-6 col-lg-4">
                                    <p class="fw-bold">{{__('Contact Info')}}</p>
                                    <dl class="row">
                                        <dt class="col-sm-4">{{__('deal_id')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['id']}}</dd>
                                        <dt class="col-sm-4">{{__('amount_currency')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['amount_btc']}}</dd>
                                        <dt class="col-sm-4">{{__('currency')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['currency']}}</dd>
                                        <dt class="col-sm-4">{{__('amount_fiat')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['amount']}}</dd>
                                        <dt class="col-sm-4">{{__('fiat')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['ad']['currency']}}</dd>
                                        <dt class="col-sm-4">{{__('status')}}</dt>
                                        <dd class="col-sm-8">{{__('status_' . $addition_info['contact']['status'])}}</dd>
                                        <dt class="col-sm-4">{{__('funded_at')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['funded_at']}}</dd>
                                        <dt class="col-sm-4">{{__('account_info')}}</dt>
                                        <dd class="col-sm-8">{{$addition_info['contact']['account_info']}}</dd>
                                    </dl>
                                </div>
                            @endif

                            @if($is_crypto)
                                <div class="col-md-6 col-lg-4">
                                    <p class="fw-bold">{{__('Contact Info')}}</p>
                                    <dl class="row">
                                        @if($cryptoInvoice->address)
                                            <dt class="col-sm-5">{{__('address')}}</dt>
                                            <dd class="col-sm-7">{{$cryptoInvoice->address}}</dd>
                                            @if($cryptoInvoice->currency->scan_url && str_contains($cryptoInvoice->currency->scan_url,'{address}'))
                                                <dt class="col-sm-5">{{__('scan_url')}}</dt>
                                                <dd class="col-sm-7">
                                                    <a target="_blank" href="{{str_replace('{address}',$cryptoInvoice->address, $cryptoInvoice->currency->scan_url)}}">
                                                        {{str_replace('{address}',$cryptoInvoice->address, $cryptoInvoice->currency->scan_url)}}
                                                    </a>
                                                </dd>
                                            @endif
                                        @endif
                                        <dt class="col-sm-5">{{__('amount')}}</dt>
                                        <dd class="col-sm-7">{{$cryptoInvoice->amount}}</dd>
                                        <dt class="col-sm-5">{{__('address_amount')}}</dt>
                                        <dd class="col-sm-7">{{$cryptoInvoice->address_amount}}</dd>
                                        <dt class="col-sm-5">{{__('status')}}</dt>
                                        <dd class="col-sm-7">{{crypto_payment_invoice_status($cryptoInvoice->status)}}</dd>
                                            @if($cryptoInvoice->status == 99)
                                                <dt class="col-sm-5">{{__('cancelation_reason')}}</dt>
                                                <dd class="col-sm-7">{{crypto_payment_invoice_cancel($cryptoInvoice->cancelation_reason)}}</dd>
                                            @endif
                                        <dt class="col-sm-5">{{__('expired_at')}}</dt>
                                        <dd class="col-sm-7">{{$cryptoInvoice->expired_at}}</dd>
                                        <dt class="col-sm-5">{{__('payed')}}</dt>
                                        <dd class="col-sm-7">{{$cryptoInvoice->payed}}</dd>
                                    </dl>
                                </div>
                            @endif

                            @if(!empty($contact_res['data']) && $is_chatex)
                                <div class="col-md-6 col-lg-4">
                                    <p class="fw-bold">{{__('Contact Info')}}</p>
                                    <dl class="row">
                                        <dt class="col-sm-4">{{__('contact_id')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['contact_id']}}</dd>
                                        <dt class="col-sm-4">{{__('currency')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['currency']}}</dd>
                                        <dt class="col-sm-4">{{__('amount')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['amount']}}</dd>
                                        @if(!empty($contact_res['data']['amount_btc']))
                                            <dt class="col-sm-4">{{__('amount_btc')}}</dt>
                                            <dd class="col-sm-8">{{$contact_res['data']['amount_btc']}}</dd>
                                        @endif
                                    </dl>
                                    <p class="fw-bold">{{__('Seller')}}</p>
                                    <dl class="row">
                                        <dt class="col-sm-4">{{__('username')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['seller']['username']}}</dd>
                                        <dt class="col-sm-4">{{__('name')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['seller']['name']}}</dd>
                                    </dl>
                                    <p class="fw-bold">{{__('Buyer')}}</p>
                                    <dl class="row">
                                        <dt class="col-sm-4">{{__('username')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['buyer']['username']}}</dd>
                                        <dt class="col-sm-4">{{__('name')}}</dt>
                                        <dd class="col-sm-8">{{$contact_res['data']['buyer']['name']}}</dd>
                                    </dl>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <p class="fw-bold">{{__('Status')}}</p>
                                    <dl class="row">
                                        <dt class="col-sm-5">{{__('created_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['created_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('funded_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['funded_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('payment_completed_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['payment_completed_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('released_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['released_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('closed_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['closed_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('canceled_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['canceled_at'])}}</dd>
                                        <dt class="col-sm-5">{{__('disputed_at')}}</dt>
                                        <dd class="col-sm-7">{{datetimeFormat($contact_res['data']['disputed_at'])}}</dd>
                                    </dl>
                                </div>
                            @elseif ($is_chatex)
                                <div class="alert alert-danger" role="alert">
                                    {{$contact_res['message']}} ({{$contact_res['error_code']}})
                                </div>
                            @endif
                        </div>
                        <hr>
                        <p class="fw-bold">
                            {{__('Messages')}}
                            @if(!empty($messages['data']['message_count']))
                                <span class="badge bg-secondary">{{$messages['data']['message_count']}}</span>
                            @endif
                        </p>

                        @if ($is_trader)
                            <div class="row">
                                <div class="col-12 messages">
                                    @foreach($messages as $message)
                                        <div class="message">
                                            <div class="user-block mt-3 mb-1">
                                            <strong class="usertype">{{__($senderType[$message->sender_type]['label'])}}</strong>
                                                <strong class="username">{{$message->sender_name}}</strong>
                                                <span class="created">{{datetimeFormat($message->created_at)}}</span>
                                            </div>
                                            @if($message->attachment_type)
                                                <a target="_blank" href="{{$message->file}}" target="_blank">
                                                    <img src="{{$message->file}}" class="img-thumbnail w-25">
                                                </a>
                                            @else
                                                <p>{!! nl2br(e($message->msg)) !!}</p>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        @if ($is_chatex)
                            @if(!empty($messages['data']['message_list']))
                                <div class="row">
                                    <div class="col-12 messages">
                                        @foreach($messages['data']['message_list'] as $message)
                                            <div class="message">
                                                <div class="user-block">
                                                    <span class="username">{{$message['sender']['name']}}</span>
                                                    <span class="created">{{datetimeFormat($message['created_at'])}}</span>
                                                </div>
                                                @if(!empty($message['attachment_url']) && str_contains($message['attachment_type'], 'image'))
                                                    <a href="{{$message['attachment_url']}}" target="_blank">
                                                        <img src="{{route('chatex.image', base64_encode($message['attachment_url']))}}" class="img-thumbnail w-25">
                                                    </a>
                                                @else
                                                    <p>{!! nl2br(e($message['msg'])) !!}</p>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @elseif(!empty($messages['message']))
                                <div class="alert alert-danger" role="alert">
                                    {{$contact_res['message']}} ({{$contact_res['error_code']}})
                                </div>
                            @endif
                        @endif

                        <div class="row">
                            <div class="col-12 form-message">
                                <form method="post" action="{{ $is_chatex ? route('chatex.send_message') : route('trader_deal.message.send', $addition_info['contact']['id'])}}" enctype="multipart/form-data">
                                    @method('POST')
                                    @csrf
                                    <input type="hidden" name="payment_id" value="{{$invoice->payment_id}}">
                                    <div class="mb-3">
                                        <label for="message" class="form-label">{{__('Message')}}</label>
                                        <textarea class="form-control @error('message') is-invalid @enderror" id="message"
                                                name="message" rows="3"></textarea>
                                        @error('message')
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="document" class="form-label">{{__('Document')}}</label>
                                        <input class="form-control" type="file" id="document" name="document">
                                    </div>
                                    <div class="p-3">
                                        <button type="submit" class="btn btn-grow">{{__('Send')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
        <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="profile-tab">
            <div class="invoice p-3 mb-3">
                @if(!empty($invoice->invoice_history))
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{__('ID')}}</th>
                                <th scope="col">{{__('Created')}}</th>
                                <th scope="col">{{__('Comment')}}</th>
                                <th scope="col">{{__('Event')}}</th>
                                <th scope="col">{{__('Status')}}</th>
                                <th scope="col">{{__('Source request')}}</th>
                                <th scope="col">{{__('Source response')}}</th>
                                <th scope="col">{{__('Info')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoice->invoice_history as $i => $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{datetimeFormat($item->created_at)}}</td>
                                    <td>{{$item->comment}}</td>
                                    <td>{{$item->event}}</td>
                                    <td>{{$item->status}}</td>
                                    <td>{{$item->source_request}}</td>
                                    <td>{{$item->source_response}}</td>
                                    <td>
                                        <div class="text-break" style="max-height: 200px; overflow-y: scroll;">
                                            {{$item->additional_info}}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-change-amount" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="change-amount" action="{{ route('payment_invoice.change_amount', $invoice->id) }}" class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">{{__('Change Amount')}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="amount" class="">{{ __('The amount transferred by the trader')}}</label>
                            </div>
                            <div class="col-sm-6 mx-auto">
                                <div class="mt-3">
                                    <input id="amount" type="text"
                                           placeholder="{{ __('Amount (USD)') }}"
                                           class="form-control"
                                           name="amount"
                                           required
                                           autofocus
                                    >
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mt-3">
                                <label for="comment" class="">{{ __('Comment')}}</label>
                            </div>
                            <div class="col-sm-12 mx-auto">
                                <div class="mt-3">
                                    <textarea id="comment" rows="5"
                                              placeholder="{{ __('Comment') }}"
                                              class="form-control"
                                              name="comment"
                                              required
                                    >{!! $invoice->comments !!}</textarea>
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">{{__('Cancel')}}</button>
                        <button type="submit" class="btn btn-grow">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
