@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Balance history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Balance history') }}
        </h1>
    </div>
    @if($balance_histories)
        <form method="GET" action="{{ route('balance_history.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="type">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($type === "1") selected @endif>{{__('Credit')}}</option>
                            <option value="0" @if($type === "0") selected @endif>{{__('Debit')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Description')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('User')}}</th>
                <th scope="col">{{__('Merchant')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($balance_histories as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{price_format_currency($item->amount, $item->currency, $fmt)}}</td>
                    <td>{{$item->description}}</td>
                    <td>{{balance_history_type($item->type)}}</td>
                    <td>{{datetimeFormat($item->created_at)}}
                    <td>
                        @if(!empty($item->user))
                            <a href="{{route('user.view', $item->user->id)}}">{{$item->user->name}}</a>
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->merchant))
                            <a href="{{route('merchant.view', $item->merchant->id)}}">{{$item->merchant->name}}</a>
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->input_invoice))
                            <a href="{{route('input_invoice.view', $item->input_invoice->id)}}">Input invoice {{$item->input_invoice->id}}</a>
                            <br>
                        @endif
                        @if(!empty($item->payment_invoice))
                            <a href="{{route('payment_invoice.view', $item->payment_invoice->id)}}">Payment invoice {{$item->payment_invoice->id}}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $balance_histories->links() }}</div>
    @endif
@endsection
