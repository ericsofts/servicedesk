<?php
use App\Models\TraderAd;
use App\Models\Trader;
?>
@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active"> {{ __('Available Ads') }}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Available Ads') }}
        </h1>
    </div>

<div class="row">
    <div class="col-md-6">
    <form action="{{ route('merchant.get-ads') }}" class="row mb-3">
        @method('GET')
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Merchant')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control m_select @error('merchant') is-invalid @enderror" name="merchant">
                            @foreach($merchants as $merchant)
                                <option value="{{$merchant->id}}" @if(request('merchant') == $merchant->id) selected @endif>{{$merchant->name}}</option>
                            @endforeach
                        </select>
                            @error('merchant')
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Amount')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <input class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{request('amount')}}">
                        @error('amount')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Fiat')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control" name="currency">
                            <option value="">{{__('Choose')}}</option>
                            @foreach($currancies as $currancy)
                                <option value="{{$currancy}}" @if(request('currency') == $currancy) selected @endif>{{$currancy}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Type')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control" name="type" id="type">
                            <option value="{{TraderAd::TYPE_SELL}}" @if(request('type') == TraderAd::TYPE_SELL) selected @endif>Sell</option>
                            <option value="{{TraderAd::TYPE_BUY}}" @if(request('type') == TraderAd::TYPE_BUY) selected @endif>Buy</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('API Type')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control" name="api_type">
                            <option value="3" @if(request('api_type') == 3) selected @endif>API v3</option>
                            <option value="5" @if(request('api_type') == 5) selected @endif>API v5</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="expence">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Expence')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control" name="expence">
                            <option value="1" @if(request('expence') == 1) selected @endif>{{ __("Yes") }}</option>
                            <option value="0" @if(request('expence') == 0) selected @endif>{{ __("No") }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Exact Currency')}}:</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <select class="form-control" name="exact_currency">
                            <option value="1" @if(request('exact_currency') == 1) selected @endif>{{ __("Yes") }}</option>
                            <option value="0" @if(request('exact_currency') == 0) selected @endif>{{ __("No") }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <label class="col-sm-12 col-form-label">&nbsp;</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
    <div class="col-md-6" id="resultblock">
        <div class="row">
            <div class="row">
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label">{{__('Merchant')}}:</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label" id="mname">{{$merch['name'] ?? ''}}</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label">{{__('Excluded Сurrencies')}}:</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label" id="mexcurr">
                       {{$merch['ex_curr'] ?? ''}}
                    </label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label">{{__('Currency 2 Currency')}}:</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label" id="mc2c">{{$merch['c2c'] ?? ''}}</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label">{{__('Selected Provider')}}:</label>
                </div>
                <div class="col-md-6">
                    <label class="col-sm-12 col-form-label" id="service_providers">
                        {{ $merch['service_providers'] ?? '' }}
                    </label>
                </div>

            </div>

        </div>
    </div>
</div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">{{__('Currency')}}</th>
            <th scope="col">{{__('Trader')}}</th>
            <th scope="col">{{__('Fiat')}}</th>
            <th scope="col">{{__('Temp Price')}}</th>
            <th scope="col">{{__('Account Info')}}</th>
            <th scope="col">{{__('Service Provider Id')}}</th>
            <th scope="col">{{__('Service Provider Type')}}</th>
            <th scope="col">{{__('Service Provider Name')}}</th>
            <th scope="col">{{__('Payment System')}}</th>
            <th scope="col">{{__('Payment System Type')}}</th>
        </tr>
        </thead>
        <tbody>

        @if(isset($ads))
            @foreach($ads as $key => $value)
                @foreach($value as $ad)

                <tr>
                    <td>{{ $ad['coin'] ?? ''}}</td>
                    <td>
                        @if(isset($ad['profile']))
                            {{ $ad['profile']['username'] }}
                        @elseif(isset($ad['trader_id']))
                            {{ Trader::find($ad['trader_id'])->name }}
                        @endif
                    </td>
                    <td>{{ $ad['currency'] ?? ''}}</td>
                    <td>{{ $ad['gps_temp_amount'] ?? ''}}</td>
                    <td>{{ $ad['account_info'] ?? ''}}</td>
                    <td>{{ $ad['service_provider_id'] ?? ''}}</td>
                    <td>{{ $ad['service_provider_type'] ?? ''}}</td>
                    <td>{{ $ad['service_provider_name'] ?? ''}}</td>
                    <td>{{ $ad['bank_name'] ?? ''}}</td>
                    <td>{{ $ad['payment_system_type'] ?? ''}}</td>

                </tr>
                @endforeach
            @endforeach
        @endif
        </tbody>
    </table>

    <script>
        $(function(){
            $(".m_select").select2();
        })
        const csrf = document
            .querySelector('meta[name="csrf-token"]')
            .getAttribute('content')
        $(document).on('change', '.m_select', function(){
            var id = $(this).val();
            var data = {
                'id': id,
                '_token': csrf
            };

            $.ajax({
                url: '{{route('merchant.get_merchant_props')}}',
                data: data,
                type: "POST",
                success: function(response){
                    $('#mname').text(response.name);
                    $('#mexcurr').text(response.ex_curr);
                    $('#mc2c').text(response.c2c);
                    $('#service_providers').text(response.service_providers)
                }
            })
        })
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#expence').hide()
            } else (
                $('#expence').show()
            )
        })
        $(function(){
            if($('#type').val() == 1){
                $('#expence').hide()
            } else (
                $('#expence').show()
            )
        })
    </script>
@endsection
