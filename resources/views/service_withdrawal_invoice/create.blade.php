@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="#">{{__('Service Provider')}}</a></li>
            <li class="breadcrumb-item active">{{__('Service Provider withdrawal invoice')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Service Provider withdrawal invoice') }}
    </h2>
    <form method="POST" action="{{ route('service.provider.withdrawal_invoice.store') }}">
        @csrf
        <input type="hidden" name="confirm" value="0">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                @error('confirm')
                    <strong>{{ $message }}</strong>
                @enderror
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount"
                       placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="fee" class="form-label">{{__('Fee')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="text" class="form-control @error('fee') is-invalid @enderror" id="fee" name="fee"
                       placeholder="{{__('Fee')}}"
                       value="{{old('fee')}}"
                >
                @error('fee')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="system_balance_id" class="form-label">{{__('Service Provider balance')}}</label>
            <select class="form-select @error('service_provider_id') is-invalid @enderror" id="service_provider_id" name="service_provider_id"
                    required>
                <option value=""></option>
                @foreach($service_providers as $service_provider)
                    <option
                        value="{{$service_provider->id}}"
                        @if($service_provider->id == old('service_provider_id', request('service_provider_id'))) selected @endif>
                        {{$service_provider->name}} - {{$service_provider->id}} Total: {{$service_provider->balance()->getResults()->amount}}
                    </option>
                @endforeach
            </select>
            @error('service_provider_id')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="comment" class="form-label">{{__('Comment')}}</label>
            <textarea class="form-control @error('comment') is-invalid @enderror" id="comment" name="comment"
                      placeholder="{{__('Comment')}}">{{old('comment')}}</textarea>
            @error('comment')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3 float-end">
            <button type="button" class="btn btn-grow btn-confirm">
                {{__('Submit')}}
            </button>
        </div>
    </form>
@endsection

@section('script-bottom')
    <script>
        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var html = "{{__('If you want to continue write "CONFIRM"?')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        input: 'text',
                        inputPlaceholder: 'CONFIRM',
                        inputValidator: (value) => {
                            if (value !== 'CONFIRM') {
                                return 'You need to write "CONFIRM"!'
                            }
                        },
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Submit')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.find('input[name="confirm"]').val(1);
                            form.submit();
                        }
                    })
                    return false;
                });
            });
        }
    </script>
@endsection
