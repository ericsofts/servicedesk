@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Trader payment systems')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader payment systems') }}
        </h1>
    </div>

    @can('trader_payment_system.new')
        <div class="col-md-3">
            <a class="btn btn-grow" href="{{ route('trader_payment_system.new') }}" role="button">{{__('Add New')}}</a>
        </div>
    @endcan

    @if($trader_ps)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Fiats')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($trader_ps as $i => $unit)
                <tr>
                    <td>{{$unit->id}}</td>
                    <td>{{$unit->name}}</td>
                    <td>
                        @foreach ($unit->fiats as $funit)
                            <div class="badge bg-black">{{$funit->name}}</div>
                        @endforeach
                    </td>
                    <td>{{__(['InActive', 'Active'][$unit->status])}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('trader_payment_system.view', $unit->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('trader_payment_system.edit')
                            <a href="{{route('trader_payment_system.edit', $unit->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-arrow-clockwise"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @endif
@endsection