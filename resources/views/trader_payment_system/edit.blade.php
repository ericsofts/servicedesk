@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_payment_system.index')}}">{{__('Trader payment systems')}}</a></li>
            <li class="breadcrumb-item active">{{__('Edit')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader payment system Edit') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader_payment_system.update', $id) }}" class="row g-3 mb-3">
        @csrf
        @method('PATCH')
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           value="{{old('name', $name)}}"
                           name="name" id="name">

                    @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Color')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('color') is-invalid @enderror"
                           value="{{old('color', $color)}}"
                           name="color" id="color">

                    @error('color')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Background Color')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('bg_color') is-invalid @enderror"
                           value="{{old('bg_color', $bg_color)}}"
                           name="bg_color" id="bg_color">

                    @error('bg_color')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('status', $status) == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status', $status) == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection