@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_payment_system.index')}}">{{__('Trader payment systems')}}</a></li>
            <li class="breadcrumb-item active">{{$trader_payment_system->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader payment system details') }}
        </h1>
    </div>
    @if($trader_payment_system)
        <div class="p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('ID')}} {{$trader_payment_system->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">
                            <div class="badge badge-ps" style="
                                @if ($trader_payment_system->color)
                                color: {{$trader_payment_system->color}};
                                @endif
                                @if ($trader_payment_system->bg_color)
                                    background-color: {{$trader_payment_system->bg_color}};
                                @endif
                            ">{{$trader_payment_system->name}}</div>
                        </dd>

                        <dt class="col-sm-4">{{__('Fiats')}}</dt>
                        <dd class="col-sm-8">
                            @foreach ($trader_payment_system->fiats as $item)
                                <div class="badge bg-black">{{$item->name}}</div>
                            @endforeach
                        </dd>

                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_payment_system->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader_payment_system->updated_at)}}</dd>
                    </dl>
                </div>
            </div>

        </div>
    @endif
@endsection
