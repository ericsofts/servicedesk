@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Profile')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Profile') }}
        </h1>
    </div>
    <div class="content-wrapper">
        <form method="post" action="{{ route('sd_user.profile.update') }}">
            @method('PATCH')
            @csrf
            <div class="p-3">
                <label class="form-label">{{__('Name')}}</label>
                <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" required value="{{request()->user()->name}}">
                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="p-3">
                <button type="submit" class="btn btn-grow">{{__('Update')}}</button>
            </div>
            <div class="p-3">
                <label class="form-label">{{__('Email')}}</label>
                <input class="form-control" type="text" disabled value="{{request()->user()->email}}">
            </div>
        </form>
    </div>
@endsection
