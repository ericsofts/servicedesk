@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Merchants')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Merchants') }}
        </h1>
    </div>
    @if($merchants)
        <form method="GET" action="{{ route('merchant.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Active')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('InActive')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-4 col-form-label">{{__('Agent')}}:</label>
                    <div class="col-sm-8">
                        <select name="agents[]" class="form-control select2" multiple="multiple">
                            @foreach($agents as $k => $v)
                                <option
                                    value="{{$k}}"
                                    @if(in_array($k, request('agents') ?? [])) selected @endif>{{$v}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Email')}}</th>
                <th scope="col">{{__('Balance')}}</th>
                <th scope="col">{{__('Open withdrawals')}}</th>
                <th scope="col">{{__('Created at')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Agent')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($merchants as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->balance->amount - ($open_withdrawals[$item->id] ?? 0)}}</td>
                    <td>
                        {{$open_withdrawals[$item->id] ?? 0}}
                    </td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{user_status($item->status)}}</td>
                    <td>
                        @if(!empty($item->agent))
{{--                        <a href="{{route('agent.view', $item->agent->id)}}">--}}
                            {{$item->agent->name}}
{{--                        </a>--}}
                            @endif
                    </td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('chatex.merchant.view', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            <a href="{{route('chatex.transactions.all_invoices', ['merchants[]' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-list-ul"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $merchants->links() }}</div>
    @endif
@endsection
