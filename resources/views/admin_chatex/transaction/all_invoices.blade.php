@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Transactions')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Transactions') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('chatex.transactions.all_invoices') }}" class="row g-3 mb-3">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}"
                                       value="{{$from_date}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}"
                                       value="{{$to_date}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="status">
                                <option value="">{{__('Choose')}}</option>
                                <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                                <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                                <option value="2" @if($status === "2") selected @endif>{{__('Confirmed')}}</option>
                                <option value="3" @if($status === "3") selected @endif>{{__('Trader accepted')}}</option>
                                <option value="5" @if($status === "5") selected @endif>{{__('User selected')}}</option>
                                <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <div class="row">
                        <label class="col-sm-4 col-form-label">{{__('Agent')}}:</label>
                        <div class="col-sm-8">
                            <select name="agents[]" class="form-control select2" multiple="multiple">
                                @foreach($agents as $k => $v)
                                    <option
                                        value="{{$k}}"
                                        @if(in_array($k, request('agents') ?? [])) selected @endif>{{$v}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <label class="col-sm-4 col-form-label">{{__('Merchant')}}:</label>
                        <div class="col-sm-8">
                            <select name="merchants[]" class="form-control select2" multiple="multiple">
                                @foreach($merchants as $k => $v)
                                    <option
                                        value="{{$k}}"
                                        @if(in_array($k, request('merchants') ?? [])) selected @endif>{{$v}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                    <button type="submit" name="btnExport" class="btn btn-grow">{{__('Export')}}</button>
{{--                    <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>--}}
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('Contact ID')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Volume')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Amount to pay')}}</th>
                <th scope="col">{{__('Grow')}}</th>
                <th scope="col">{{__('Service')}}</th>
                <th scope="col">{{__('Agent')}}</th>
                <th scope="col">{{__('Agent Name')}}</th>
                <th scope="col">{{__('Merchant')}}</th>
                <th scope="col">{{__('Fiat')}}</th>
                <th scope="col">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="3" class="fw-bold text-end">{{__('Total')}}:</td>
                <td class="fw-bold">{{price_format($total['volume'])}}</td>
                <td class="fw-bold">{{price_format($total['amount'])}}</td>
                <td class="fw-bold">{{price_format($total['amount2pay'])}}</td>
                <td class="fw-bold">{{price_format($total['amount2grow'])}}</td>
                <td class="fw-bold">{{price_format($total['amount2service'])}}</td>
                <td class="fw-bold">{{price_format($total['amount2agent'])}}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach($invoices as $i => $item)
                <tr>
                    <td>{{$item->payment_id}}</td>
                    <td>{{invoice_type($item->invoice_type)}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format($item->amount)}}</td>
                    <td>
                        @if($item->invoice_type==1)-@endif{{price_format($item->amount)}}
                    </td>
                    <td>
                        @if(!empty($item->merchant->agent->partner->id) && $item->merchant->agent->partner->id == $partner->id)
                            @if($item->invoice_type==1)-@endif{{price_format($item->amount2pay)}}
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->merchant->agent->partner->id) && $item->merchant->agent->partner->id == $partner->id)
                            {{price_format($item->amount2grow)}}
                        @endif
                    </td>
                    <td>{{price_format($item->amount2service)}}</td>
                    <td>
                        @if(!empty($item->merchant->agent->partner->id) && $item->merchant->agent->partner->id == $partner->id)
                            {{price_format($item->amount2agent)}}
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->merchant->agent->partner->id) && $item->merchant->agent->partner->id == $partner->id && !empty($item->merchant->agent))
{{--                            <a href="{{route('chatex.agent.view', $item->merchant->agent->id)}}">--}}
                                {{$item->merchant->agent->name}}
{{--                            </a>--}}
                        @endif
                    </td>
                    <td>
                        @if(!empty($item->merchant->agent->partner->id) && $item->merchant->agent->partner->id == $partner->id && !empty($item->merchant))
                            <a href="{{route('chatex.merchant.view', $item->merchant->id)}}">{{$item->merchant->name}}</a>
                        @endif
                    </td>
                    <td>{{$item->fiat_currency}}</td>
                    <td>{{invoice_status($item->status)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
