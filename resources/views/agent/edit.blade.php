@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('agent.index')}}">{{__('Agents')}}</a></li>
            <li class="breadcrumb-item active">{{__('Edit')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Agent Edit') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('agent.update', $id) }}" class="row g-3 mb-3">
        @method('PATCH')
        @csrf
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           name="name" id="name" value="{{old('name',$name)}}">

                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Email')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                           name="email" id="email" value="{{old('email',$email)}}">

                @error('email')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Password')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"
                           name="password" id="password">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Partner')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="partner_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($partner as $key => $value)
                        <option value="{{$key}}" @if(old('partner_id', $partner_id) == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="status">
                        <option value="1" @if(old('status', $status) == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status', $status) == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Enable 2FA')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="enable_2fa">
                        <option value="1" @if(old('enable_2fa', $enable_2fa) == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('enable_2fa', $enable_2fa) == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection
