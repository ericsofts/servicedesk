@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('agent.index')}}">{{__('Agents')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Agent New') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('agent.create') }}" class="row g-3 mb-3">
        @csrf
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           value="{{old('name')}}"
                           name="name" id="name">

                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Email')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                           value="{{old('email')}}"
                           name="email" id="email">

                @error('email')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Password')}}:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"
                           name="password" id="password" value="{{old('password', $password)}}">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Partner')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="partner_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($partner as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1">{{__('Active')}}</option>
                        <option value="0">{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-10 col-form-label">{{__('Enable 2FA')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="enable_2fa">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1">{{__('Active')}}</option>
                        <option value="0">{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection
