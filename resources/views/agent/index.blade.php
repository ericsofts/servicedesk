@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Agents')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Agents') }}
        </h1>
    </div>
    @if($agents)
        <form method="GET" action="{{ route('agent.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Active')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('InActive')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
            </div>
            @can('agent.new')
            <div class="col-md-3 offset-md-3">
                <a class="btn btn-grow" href="{{ route('agent.new') }}" role="button">{{__('Add New')}}</a>
            </div>
            @endcan
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Email')}}</th>
                <th scope="col">{{__('Balance')}}</th>
                <th scope="col">{{__('Created at')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($agents as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{price_format($item->balance->amount)}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{user_status($item->status)}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            @can('agent.withdrawal_invoice.create')
                            <a href="{{route('agent.withdrawal_invoice.create', ['agent_id' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-dash"></i>
                            </a>
                            @endcan
                            <a href="{{route('agent.view', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('agent.edit')
                            <a href="{{route('agent.edit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-pencil-square"></i>
                            </a>
                            @endcan
                            <a href="{{route('transactions.all_invoices', ['agents[]' => $item->id])}}" class="btn btn-outline-secondary">
                                <i class="bi bi-list-ul"></i>
                            </a>
                            @can('agent.withdrawals')
                            <a href="{{route('agent.withdrawals', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-wallet"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $agents->links() }}</div>
    @endif
@endsection
