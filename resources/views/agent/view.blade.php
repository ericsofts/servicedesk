@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('agent.index')}}">{{__('Agents')}}</a></li>
            <li class="breadcrumb-item active">{{$agent->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Agent details') }}
        </h1>
    </div>
    @if($agent)
        <div class="invoice p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('ID')}} {{$agent->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$agent->name}}</dd>
                        <dt class="col-sm-4">{{__('Email')}}</dt>
                        <dd class="col-sm-8">{{$agent->email}}</dd>
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($agent->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($agent->updated_at)}}</dd>
                        <dt class="col-sm-4">{{__('Email verified at')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($agent->email_verified_at)}}</dd>
                    </dl>
                </div>
            </div>
            <hr>
        </div>
    @endif
@endsection
