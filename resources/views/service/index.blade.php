@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Service Providers')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Service Providers') }}
        </h1>
    </div>
    @if($services)
        <form method="GET" action="{{ route('service.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            @foreach($statuses as $key=>$item)
                            <option value="{{$key}}" @if($status === strval($key)) selected @endif>{{__($item)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
            </div>
            @can('service.new')
            <div class="col-md-3">
                <a class="btn btn-grow" href="{{ route('service.new') }}" role="button">{{__('Add New')}}</a>
            </div>
            @endcan
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Balance')}}</th>
                <th scope="col">{{__('Created at')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($services as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->type}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{price_format($item->balance->amount, 8)}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$statuses[$item->status]}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            @can('service.edit')
                            <a href="{{route('service.edit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-arrow-clockwise"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $services->links() }}</div>
    @endif
@endsection
