@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('service.index')}}">{{__('Service Provider')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Service Provider New') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('service.create') }}" class="row g-3 mb-3">
        @csrf
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                               value="{{old('name')}}"
                               name="name" id="name">

                        @error('name')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Type')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="type">
                            <option value="">{{__('Choose')}}</option>
                            @foreach($types as $item)
                            <option value="{{$item}}" @if(old('type') == strval($item)) selected @endif>{{__($item)}}</option>
                            @endforeach
                        </select>

                        @error('property.type')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            @foreach($statuses as $key=>$item)
                            <option value="{{$key}}" @if(old('status') == strval($key)) selected @endif>{{__($item)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Priority Traders')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.priority_traders') is-invalid @enderror"
                               value="{{old('property[priority_traders]')}}"
                               name="property[priority_traders]" id="priority_traders">

                        @error('property.priority_traders')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Blocked Traders')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.blocked_traders') is-invalid @enderror"
                               value="{{old('property[blocked_traders]')}}"
                               name="property[blocked_traders]" id="blocked_traders">

                        @error('property.blocked_traders')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Available Currencies')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.available_currencies') is-invalid @enderror"
                               value="{{old('property[available_currencies]')}}"
                               name="property[available_currencies]" id="available_currencies">

                        @error('property.available_currencies')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Withdrawal Priority Traders')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.withdrawal_priority_traders') is-invalid @enderror"
                               value="{{old('property[withdrawal_priority_traders]')}}"
                               name="property[withdrawal_priority_traders]" id="withdrawal_priority_traders">

                        @error('property.withdrawal_priority_traders')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Blocked Banks')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.blocked_banks') is-invalid @enderror"
                               value="{{old('property[blocked_banks]')}}"
                               name="property[blocked_banks]" id="blocked_banks">

                        @error('property.blocked_banks')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Percentage Currency Rate')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.percentage_currency_rate') is-invalid @enderror"
                               value="{{old('property[percentage_currency_rate]')}}"
                               name="property[percentage_currency_rate]" id="percentage_currency_rate">

                        @error('property.percentage_currency_rate')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Currencies Without Card Number')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('property.currencies_without_card_number') is-invalid @enderror"
                               value="{{old('property[currencies_without_card_number]')}}"
                               name="property[currencies_without_card_number]" id="currencies_without_card_number">

                        @error('property.currencies_without_card_number')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection

