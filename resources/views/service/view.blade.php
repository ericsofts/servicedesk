@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('service.index')}}">{{__('Service Providers')}}</a></li>
            <li class="breadcrumb-item active">{{$service->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Service Provider Details') }}
        </h1>
    </div>
    @if($service)
        <div class="p-3 mb-3">
            <div class="row invoice-info border-bottom">
                <div class="col-sm-4">
                    <dl class="row">
                        <dt class="col-sm-5">{{__('ID')}}</dt>
                        <dd class="col-sm-7">{{$service->id}}</dd>
                        <dt class="col-sm-5">{{__('Name')}}</dt>
                        <dd class="col-sm-7">{{$service->name}}</dd>
                        <dt class="col-sm-5">{{__('Created')}}</dt>
                        <dd class="col-sm-7">{{datetimeFormat($service->created_at)}}</dd>
                        <dt class="col-sm-5">{{__('Priority')}}</dt>
                        <dd class="col-sm-7">{{$service->priority}}</dd>
                        <dt class="col-sm-5">{{__('Status')}}</dt>
                        <dd class="col-sm-7">{{$statuses[$service->status]}}</dd>
                        <dt class="col-sm-5">{{__('Balance')}}</dt>
                        <dd class="col-sm-7">{{price_format($service->balance->amount, 6)}}</dd>
                    </dl>
                </div>
                <div class="row border-top pt-4">
                    <div class="col-sm-12">
                        <dl class="row">
                            <dt class="col-sm-3"><span class="text-break">{{__('Ad ID Prefix')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['ad_id_prefix'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Coin')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['coin'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Base url')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['base_url'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Callback url')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['callback_url'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Api Token')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['api_token'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Priority Traders')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{implode(',',json_decode($property['priority_traders'] ?? ''))}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Blocked Traders')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{implode(',',json_decode($property['blocked_traders'] ?? ''))}}</span></dd>
                        </dl>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <dl class="row">
                            <dt class="col-sm-3"><span class="text-break">{{__('Available Currencies')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{ implode(',',json_decode($property['available_currencies'] ?? ''))}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Withdrawal Priority Traders')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{implode(',',json_decode($property['withdrawal_priority_traders'] ?? ''))}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Blocked Banks')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{ implode(',',json_decode($property['blocked_banks'] ?? ''))}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Percentage Currency Rate')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{$property['percentage_currency_rate'] ?? ''}}</span></dd>
                            <dt class="col-sm-3"><span class="text-break">{{__('Currencies Without Card Number')}}</span></dt>
                            <dd class="col-sm-9"><span class="text-break">{{implode(',',json_decode($property['currencies_without_card_number'] ?? ''))}}</span></dd>
                        </dl>
                    </div>
                </div>
            </div>

        </div>
    @endif
@endsection
