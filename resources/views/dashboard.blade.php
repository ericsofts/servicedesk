@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1>
            {{ __('Dashboard') }}
        </h1>
    </div>
    <p>{{__('Welcome')}}, {{request()->user()->name}}</p>
    <div class="container">
        <div class="row">
            <div class="col">
                @can('system.balances')
                    <p>Balances</p>
                    <dl class="row">
                        @foreach([$balance_grow] as $balance)
                            <dt class="col-sm-6">{{$balance->name}}</dt>
                            <dd class="col-sm-6">{{$balance->amount}}</dd>
                        @endforeach
                        @foreach($service_providers as $balance)
                            <dt class="col-sm-6">{{$balance->service_provider->name}}</dt>
                            <dd class="col-sm-6">{{$balance->amount}}</dd>
                        @endforeach
                    </dl>
                @endcan
            </div>
            <div class="col">
                @can('chatex.wallet')
                    <p>Wallets in chatex</p>
                    <dl class="row">
                        @foreach($wallets as $wallet)
                            <dt class="col-sm-4">{{$wallet['coin']}}</dt>
                            <dd class="col-sm-8">{{$wallet['amount']}} | {{$wallet['held']}}</dd>
                        @endforeach
                    </dl>
                @endcan
            </div>
        </div>
    </div>
    @can('chatex.balance')
        <div class="container">
            <div class="row">
                <div class="col">
                    <p>Balances</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Total')}}</dt>
                        <dd class="col-sm-8">{{$balance_chatex->amount}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    @endcan

@endsection
