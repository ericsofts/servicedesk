<?php
    use App\Models\TraderAd;
    use App\Models\TraderDeal;
?>

@extends('layouts.app')

@section('msgform')
<div class="row">
    <div class="col-12 form-message">
        <form method="post" action="{{ route('trader_deal.message.send', ['id' => $id]) }}" enctype="multipart/form-data">
            @method('POST')
            @csrf
            <input type="hidden" name="id" value="{{$id}}">
            <div class="mb-3">
                <label for="message" class="form-label">{{__('Message')}}</label>
                <textarea class="form-control @error('message') is-invalid @enderror" id="message"
                            name="message" rows="3"></textarea>
                @error('message')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="document" class="form-label">{{__('Image')}}</label>
                <input class="form-control" accept="image/png,image/jpeg" type="file" id="document" name="document">
            </div>
            <div class="my-3">
                <button type="submit" class="btn btn-grow">{{__('Send')}}</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('details')
<section class="mb-3">
    @php
        $detail = json_decode($deal->ad_json);
    @endphp
    <table class="table">
        <tr><td>{{__('ID')}}<td>{{$deal->id}}
        <tr><td>{{__('Type')}}<td>{{__(TraderAd::getTypes()[$deal->type])}}
        <tr><td>{{__('Currency Amount')}}<td>{{price_format($deal->amount_currency, 8)}} {{$deal->currency->asset}}
        <tr><td>{{__('Fiat Amount')}}<td>{{price_format($deal->amount_fiat, 2)}} {{$deal->fiat->asset}}
        <tr><td>{{__('Rate')}}<td>{{price_format($deal->rate, 2)}} {{$deal->fiat->asset}}
        @if ($deal->type == TraderAd::TYPE_SELL)
        <tr><td>{{__('Card Number')}}<td>{{$deal->card_number}}
        @endif
        <tr><td>{{__('Created At')}}<td>{{datetimeFormat($deal->created_at)}}
        <tr><td>{{__('Status')}}<td>{{__('status_' . $deal->status)}}
        <tr><td>{{__('Comment')}}<td>{!! $deal->comment !!}
    </table>

    <div class="btn-group">
        @if ($deal->type == TraderAd::TYPE_SELL)
            @if ($deal->status == TraderDeal::STATUS_DISPUTED)
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Complete Sell Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_COMPLETED}}">
                    <input type="hidden" name="description" value="SD completed deal">
                </form>
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Cancel Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_CANCELED_SD}}">
                    <input type="hidden" name="description" value="SD canceled deal">
                </form>
            @endif

            @if (TraderDeal::isCancelStatus($deal->status))
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Complete Sell Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_COMPLETED}}">
                    <input type="hidden" name="description" value="SD completed deal">
                </form>
            @endif

            @if ($deal->status != TraderDeal::STATUS_COMPLETED)
            <div>
                <button class="btn btn-grow btn-change-amount">{{__('Change Amount')}}</button>
            </div>
            @endif
        @endif

        @if ($deal->type == TraderAd::TYPE_BUY)
            @if ($deal->status == TraderDeal::STATUS_DISPUTED)
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Complete Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_COMPLETED}}">
                    <input type="hidden" name="description" value="SD completed deal">
                </form>
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Cancel Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_CANCELED_SD}}">
                    <input type="hidden" name="description" value="SD canceled deal">
                </form>
            @endif

            @if (TraderDeal::isCancelStatus($deal->status))
                <form class="me-2" action="{{route('trader_deal.status', ['id' => $id])}}" method="post">
                    @csrf
                    <button class="btn btn-grow btn-confirm-default">{{__('Complete Deal')}}</button>
                    <input type="hidden" name="status" value="{{TraderDeal::STATUS_COMPLETED}}">
                    <input type="hidden" name="description" value="SD completed deal">
                </form>
            @endif
        @endif
    </div>
    <hr>
</section>
@endsection

@section('content')
    <ul class="nav nav-tabs mt-3" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.detail', ['id' => $id])}}" class="nav-link active" role="tab">{{__('Deal Details')}}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.history', ['id' => $id])}}" class="nav-link" role="tab">{{__('Deal History')}}</a>
        </li>
    </ul>

    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.deals')}}">{{__('Trader Deals')}}</a></li>
            <li class="breadcrumb-item active">{{__('Deal Details') . " #$deal->id"}}</li>
        </ol>
    </nav>

    <div class="page-header">
        <h1>
            {{ __('Deal Details') }}
            @if(count($dealMessage))
                <span class="badge bg-secondary">{{count($dealMessage)}}</span>
            @endif
        </h1>
    </div>
    @yield('details')

    <div class="page-header">
        <h1>
            {{ __('Deal Messages') }}
            @if(count($dealMessage))
                <span class="badge bg-secondary">{{count($dealMessage)}}</span>
            @endif
        </h1>
    </div>
    @yield('msgform')

    @if($dealMessage)
        <div class="row">
            <div class="col-12 messages">
                @foreach($dealMessage as $message)
                    <div class="message">
                        <div class="user-block mt-3 mb-1">
                            <strong class="usertype">{{__($senderType[$message->sender_type]['label'])}}</strong>
                            <strong class="username">{{$message->sender_name}}</strong>
                            <span class="created">{{datetimeFormat($message->created_at)}}</span>
                        </div>
                        @if($message->attachment_type)
                            <a target="_blank" href="{{route('trader_deal.message.attachment', ['id' => $message->id])}}">
                                <img src="{{$message->file}}" class="img-thumbnail w-25">
                            </a>
                        @else
                            <p>{!! nl2br(e($message->msg)) !!}</p>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <div class="float-end">{{ $dealMessage->links() }}</div>
    @endif

    <div class="modal fade" id="modal-change-amount" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="change-amount" action="{{ route('trader_deal.change_amount', $deal->id) }}" class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">{{__('Change Amount')}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="amount_currency" class="">{{ __('Currency')}}</label>
                            </div>
                            <div class="col-sm-6 mx-auto">
                                <div class="mt-3">
                                    <input id="amount_currency" type="text"
                                           placeholder="{{ __('Amount') }} (USD)"
                                           class="form-control"
                                           name="amount_currency"
                                           autofocus
                                    >
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="amount_fiat" class="">{{ __('Fiat')}}</label>
                            </div>
                            <div class="col-sm-6 mx-auto">
                                <div class="mt-3">
                                    <input id="amount_fiat" type="text"
                                           placeholder="{{ __('Amount') }} ({{$deal->fiat->asset}})"
                                           class="form-control"
                                           name="amount_fiat"
                                           autofocus
                                    >
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 mt-3">
                                <label for="comment" class="">{{ __('Comment')}}</label>
                            </div>
                            <div class="col-sm-12 mx-auto">
                                <div class="mt-3">
                                    <textarea id="comment" rows="5"
                                              placeholder="{{ __('Comment') }}"
                                              class="form-control"
                                              name="comment"
                                              required
                                    >{!! $deal->comment !!}</textarea>
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">{{__('Cancel')}}</button>
                        <button type="submit" class="btn btn-grow">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
