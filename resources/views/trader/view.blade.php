@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.index')}}">{{__('Trader')}}</a></li>
            <li class="breadcrumb-item active">{{$trader->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader details') }}
        </h1>
    </div>
    @if($trader)
        <div class="p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-6">
                    <p class="fw-bold">{{__('ID')}} {{$trader->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$trader->name}}</dd>
                        <dt class="col-sm-4">{{__('Service Provider')}}</dt>
                        <dd class="col-sm-8">{{$trader->service_provider->name ?? ''}}</dd>
                        <dt class="col-sm-4">{{__('Email')}}</dt>
                        <dd class="col-sm-8">{{$trader->email}}</dd>
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader->updated_at)}}</dd>
                        <dt class="col-sm-4">{{__('Email verified at')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($trader->email_verified_at)}}</dd>
                    </dl>
                </div>
                <div class="col-sm-4">
{{--                    <p>{{__('Balance')}}: <span class="fw-bold">{{$merchant->balance_total()}}</span></p>--}}
                </div>
            </div>

        </div>
    @endif
@endsection
