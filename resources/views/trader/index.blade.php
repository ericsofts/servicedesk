@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Traders')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Traders') }}
        </h1>
    </div>
    @if($traders)
        <form method="GET" action="{{ route('trader.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Active')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('InActive')}}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
            </div>
            @can('trader.new')
            <div class="col-md-3">
                <a class="btn btn-grow" href="{{ route('trader.new') }}" role="button">{{__('Add New')}}</a>
            </div>
            @endcan
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Service Provider')}}</th>
                <th scope="col">{{__('Email')}}</th>
                @foreach ($currency as $unit)
                    <th scope="col">{{__('Balance')}} {{$unit->asset}}</th>
                @endforeach
                <th scope="col">{{__('Created At')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($traders as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->service_provider->name ?? ''}}</td>
                    <td>{{$item->email}}</td>

                    @foreach ($currency as $unit)
                    <td>{{$item->getTotalBalance($unit->id) ?? 0}}</td>
                    @endforeach

                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{user_status($item->status)}}</td>
                    <td>
                        <div class="btn-group mb-3">

                            <a href="{{route('trader.view', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-info-circle"></i>
                            </a>
                            @can('trader.balance_operation')
                            <a href="{{route('trader.credit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-plus"></i>
                            </a>
                            @endcan
                            @can('trader.edit')
                            <a href="{{route('trader.edit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-pencil-square"></i>
                            </a>
                            @endcan
                            @can('trader.balance_operation')
                            <a href="{{route('trader.debit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-dash"></i>
                            </a>
                            @endcan
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $traders->links() }}</div>
    @endif
@endsection
