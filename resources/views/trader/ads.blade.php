@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Trader Ads')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader Ads') }}
        </h1>
    </div>
    @if($adsList)
        <form method="POST" action="{{ route('trader.ads') }}">
            @csrf
            <div class="row g-3 mb-3">
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}"
                                       value="{{request('from_date')}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}"
                                       value="{{request('to_date')}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Trader')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="trader_id">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($traders as $key=>$item)
                                    <option value="{{$key}}" @if($trader === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row g-3 mb-3">

                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="type">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($types as $key=>$item)
                                    <option value="{{$key}}" @if($type === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Currency')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="currency_id">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($currencies as $key=>$item)
                                    <option value="{{$key}}" @if($currency === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Fiat')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="fiat_id">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($fiats as $key=>$item)
                                    <option value="{{$key}}" @if($fiat === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


            </div>

            <div class="row g-3 mb-4">
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Payment System')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="payment_system_id">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($paymentSystems as $key=>$item)
                                    <option value="{{$key}}" @if($paymentSystem === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="status">
                                <option value="">{{__('Choose')}}</option>
                                @foreach($statuses as $key=>$item)
                                    <option value="{{$key}}" @if($status === strval($key)) selected @endif>{{__($item)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                    <button type="reset" class="btn btn-reset">{{__('Reset')}}</button>
                </div>
            </div>

        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Trader Name')}}</th>
                <th scope="col">{{__('Currency')}}</th>
                <th scope="col">{{__('Fiat')}}</th>
                <th scope="col">{{__('Payment System')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Rate')}}</th>
                <th scope="col">{{__('Rate Type')}}</th>
                <th scope="col">{{__('Rate ID')}}</th>
                <th scope="col">{{__('Max Amount')}}</th>
                <th scope="col">{{__('Min Amount')}}</th>
                <th scope="col">{{__('Rate Source')}}</th>
                <th scope="col">{{__('Rate Stop')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Created At')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($adsList as $key => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$traders[$item->trader_id]}}</td>
                    <td>{{$currencies[$item->currency_id]}}</td>
                    <td>{{$fiats[$item->fiat_id]}}</td>
                    <td>{{$paymentSystems[$item->payment_system_id]}}</td>
                    <td>{{$types[$item->type]}}</td>
                    <td>{{price_format($item->rate,2)}}</td>
                    <td>{{$rates[$item->rate_type]}}</td>
                    <td>{{$item->rate_id}}</td>
                    <td>{{price_format($item->max_amount,2)}}</td>
                    <td>{{price_format($item->min_amount,2)}}</td>
                    <td>{{$ratesSource[$item->rate_source_id] ?? 'none'}}</td>
                    <td>{{price_format($item->rate_stop,2)}}</td>
                    <td>{{$statuses[$item->status]}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $adsList->links() }}</div>
    @endif
@endsection
