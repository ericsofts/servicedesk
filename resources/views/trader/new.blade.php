@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.index')}}">{{__('Trader')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader New') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader.create') }}" class="row g-3 mb-3">
        @csrf
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Name')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                           value="{{old('name')}}"
                           name="name" id="name">

                    @error('name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Email')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('email') is-invalid @enderror"
                           value="{{old('email')}}"
                           name="email" id="email">

                    @error('email')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Password')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control"
                           name="password" id="password" value="{{old('password', $password)}}">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Service Provider')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="service_provider_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($service_providers as $k => $item)
                        <option value="{{$item->id}}" @if(old('service_provider_id') == strval($item->id)) selected @endif>{{__($item->name)}}</option>
                        @endforeach
                    </select>

                    @error('property.service_provider_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Agent')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="agent_id">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($agents as $key => $value)
                        <option value="{{$key}}" @if(old('agent_id') == $key) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-12">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="1" @if(old('status') == '1') selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status') == '0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection

