@extends('layouts.app')
@section('content')
    <ul class="nav nav-tabs mt-3" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.detail', ['id' => $id])}}" class="nav-link" role="tab">{{__('Deal Details')}}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.history', ['id' => $id])}}" class="nav-link active" role="tab">{{__('Deal History')}}</a>
        </li>
    </ul>

    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.deals')}}">{{__('Trader Deals')}}</a></li>
            <li class="breadcrumb-item active">{{__('Deal History') . " #$id"}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Trader Deal Change Status') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader_deal.status', ['id' => $id]) }}">
        @csrf
        <input name="id" value="{{$id}}" type="hidden">
        <div class="row g-3 mb-4">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-12">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            @foreach($statusesAvalable as $key=>$item)
                                <option value="{{$item}}" @if($status === $item) selected @endif>{{__($statuses[$item])}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Description')}}:</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control @error('description') is-invalid @enderror"
                               name="description" id="description" value="{{old('description')}}">

                        @error('description')
                        <div class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <label class="col-sm-12 col-form-label">{{__('Action')}}:</label>
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                </div>
            </div>
        </div>

    </form>
    @if($dealHistory)
        <div class="page-header">
            <h1>
                {{ __('Trader Deal History') }}
            </h1>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Description')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Customer Type')}}</th>
                <th scope="col">{{__('Customer Name')}}</th>
                <th scope="col">{{__('Created At')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealHistory as $key => $item)
                <tr>
                    <td>{{$item->deal_id}}</td>
                    <td>{{$item->description}}</td>
                    <td>{{__($statuses[$item->status])}}</td>
                    <td>{{__($customerType[$item->customer_type]['label'] ?? '')}}</td>
                    <td>{{$item->customer_name ?? ''}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $dealHistory->links() }}</div>
    @endif
@endsection
