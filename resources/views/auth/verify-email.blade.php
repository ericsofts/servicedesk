
@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4" src="{{ asset('images/logo.png') }}">
            <p class="mb-3 fw-normal">{{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}</p>
        </div>
        @if (session('status') == 'verification-link-sent')
            <div class="mb-3 fw-normal">
                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
            </div>
        @endif
        <div class="text-center">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf
                <button class="w-100 btn btn-lg btn-secondary fw-bold border-white bg-white" type="submit">{{ __('Resend Verification Email') }}</button>
            </form>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="btn btn-link text-muted" type="submit">{{ __('Log out') }}</button>
            </form>
        </div>
    </main>
@endsection
