@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('User Withdrawal invoices')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('User Withdrawal invoices') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('user.withdrawal.index') }}" class="row g-3 mb-3">
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="submit" name="btnExport" class="btn btn-grow">{{__('Export')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('User')}}</th>
                <th scope="col">{{__('Email')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Amount2Pay')}}</th>
                <th scope="col">{{__('Commission')}}</th>
                <th scope="col">{{__('Amount2Grow')}}</th>
                <th scope="col">{{__('Address')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4">{{__('Total')}}</td>
                <td class="fw-bold">{{price_format($total->total_amount)}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">{{__('Total page')}}</td>
                <td class="fw-bold">{{price_format($total_page['amount'])}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach($invoices as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->user->email}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format($item->amount, 6)}}</td>
                    <td>{{price_format($item->amount2pay, 6)}}</td>
                    <td>{{$item->commission ?? '0'}}%</td>
                    <td>{{price_format($item->amount2commission, 6)}}</td>
                    <td>{{$item->address}}</td>
                    <td>{{invoice_status($item->status)}}</td>
                    <td>
                        <div class="d-flex mb-3">
                            @if(($item->status != 1) && ($item->status != 99))
                                <form method="POST" action="{{ route('user.withdrawal.set_status_payed', $item->id) }}">
                                    @csrf
                                    <button type="button" class="btn btn-outline-secondary btn-status-paid" title="{{__('Set status payed')}}">
                                        <i class="bi bi-cash-stack"></i>
                                    </button>
                                </form>
                            @endif
                            @if(($item->status != 1) && ($item->status != 99))
                                <form method="POST" action="{{ route('user.withdrawal.set_status_cancel', $item->id) }}">
                                    @csrf
                                    <button type="button" class="btn btn-outline-secondary btn-status-cancel" title="{{__('Set status Cancel')}}">
                                        <i class="bi bi-x-circle"></i>
                                    </button>
                                </form>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection

@section('script-bottom')
    <script>
        $(function () {
            var swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    container: 'swal2-grow',
                    content: 'text-danger fw-bold',
                    confirmButton: 'btn btn-grow',
                    cancelButton: 'btn btn-link'
                },
                buttonsStyling: false
            })
            $(document).on("click", ".btn-status-cancel", function (e) {
                e.preventDefault();
                var form = $(this).closest('form');
                var html = "{{__('Change invoice status to paid')}}<br>{{__('If you want to continue write "YES"?')}}";
                swalWithBootstrapButtons.fire({
                    html: html,
                    input: 'text',
                    inputPlaceholder: 'YES',
                    inputValidator: (value) => {
                        if (value !== 'YES') {
                            return 'You need to write "YES"!'
                        }
                    },
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{__('Submit')}}',
                    cancelButtonText: '{{__('Cancel')}}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
                return false;
            });
            $(document).on("click", ".btn-status-paid", function (e) {
                e.preventDefault();
                var form = $(this).closest('form');
                var html = "{{__('Change invoice status to paid')}}<br>{{__('If you want to continue write "YES"?')}}";
                swalWithBootstrapButtons.fire({
                    html: html,
                    input: 'text',
                    inputPlaceholder: 'YES',
                    inputValidator: (value) => {
                        if (value !== 'YES') {
                            return 'You need to write "YES"!'
                        }
                    },
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '{{__('Submit')}}',
                    cancelButtonText: '{{__('Cancel')}}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
                return false;
            });
        });
    </script>
@endsection
