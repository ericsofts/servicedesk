@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Users')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Users') }}
        </h1>
    </div>
    @if($users)
        <form method="GET" action="{{ route('user.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Active')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('InActive')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Name')}}</th>
                <th scope="col">{{__('Email')}}</th>
                <th scope="col">{{__('Created at')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{user_status($item->status)}}</td>
                    <td>
                        <a href="{{route('user.view', $item->id)}}">
                            <i class="bi bi-info-circle"></i>
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $users->links() }}</div>
    @endif
@endsection
