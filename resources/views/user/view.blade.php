@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('user.index')}}">{{__('Users')}}</a></li>
            <li class="breadcrumb-item active">{{$user->name}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('User details') }}
        </h1>
    </div>
    @if($user)
        <div class="invoice p-3 mb-3">
            <div class="row invoice-info">
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('ID')}} {{$user->id}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Account ID')}}</dt>
                        <dd class="col-sm-8">{{$user->account_id}}</dd>
                        <dt class="col-sm-4">{{__('Name')}}</dt>
                        <dd class="col-sm-8">{{$user->name}}</dd>
                        <dt class="col-sm-4">{{__('Email')}}</dt>
                        <dd class="col-sm-8">{{$user->email}}</dd>
                        <dt class="col-sm-4">{{__('Created')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($user->created_at)}}</dd>
                        <dt class="col-sm-4">{{__('Updated')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($user->updated_at)}}</dd>
                        <dt class="col-sm-4">{{__('Email verified at')}}</dt>
                        <dd class="col-sm-8">{{datetimeFormat($user->email_verified_at)}}</dd>
                    </dl>
                </div>
                <div class="col-sm-4">
                    <p class="fw-bold">{{__('Balance')}}</p>
                    <dl class="row">
                        <dt class="col-sm-4">{{__('Total')}}</dt>
                        <dd class="col-sm-8">{{$balanceTotal}}</dd>
                    </dl>
                </div>

            </div>
            <hr>
        </div>
    @endif
@endsection
