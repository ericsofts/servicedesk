<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse sidebar-menu">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mb-1 text-muted">
                    {{ Auth::user()->name }}
                </h6>
            </li>
            @can('dashboard')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('dashboard')) active @endif"
                       href="{{route('dashboard')}}">
                        <i class="bi bi-house"></i>
                        {{__('Dashboard')}}
                    </a>
                </li>
            @endcan
            @can('user.withdrawal.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('user.withdrawal.index')) active @endif"
                       href="{{route('user.withdrawal.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('User Withdrawal invoices')}}
                    </a>
                </li>
            @endcan
            @can('input_invoice.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('input_invoice.index')) active @endif"
                       href="{{route('input_invoice.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Top-Up invoices')}}
                    </a>
                </li>
            @endcan
            @can('balance_history.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('balance_history.index')) active @endif"
                       href="{{route('balance_history.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Balance history')}}
                    </a>
                </li>
            @endcan
            @can('payment_invoice.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('payment_invoice.index')) active @endif"
                       href="{{route('payment_invoice.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Payment invoices')}}
                    </a>
                </li>
            @endcan
            @can('withdrawal.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('withdrawal.index')) active @endif"
                       href="{{route('withdrawal.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Withdrawal invoices')}}
                    </a>
                </li>
            @endcan
            @can('user.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('user.index')) active @endif"
                       href="{{route('user.index')}}">
                       <i class="bi bi-person"></i>
                        {{__('Users')}}
                    </a>
                </li>
            @endcan
            @can('agent.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('agent.index')) active @endif"
                       href="{{route('agent.index')}}">
                       <i class="bi bi-person"></i>
                        {{__('Agents')}}
                    </a>
                </li>
            @endcan
            @can('merchant.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('merchant.index')) active @endif"
                       href="{{route('merchant.index')}}">
                       <i class="bi bi-person"></i>
                        {{__('Merchants')}}
                    </a>
                </li>
            @endcan
            @can('merchant.ads_selector')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('merchant.ads-selector')) active @endif" href="{{route('merchant.ads-selector')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Available Ads')}}
                    </a>
                </li>
            @endcan
            @can('service.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('service.index')) active @endif"
                        href="{{route('service.index')}}">
                        <i class="bi bi-person"></i>
                        {{__('Service Providers')}}
                    </a>
                </li>
            @endcan
            @can('transactions.all_invoices')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('transactions.all_invoices')) active @endif"
                       href="{{route('transactions.all_invoices')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Transactions')}}
                    </a>
                </li>
            @endcan
            @can('chatex.agent.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('chatex.agent.index')) active @endif"
                       href="{{route('chatex.agent.index')}}">
                       <i class="bi bi-person"></i>
                        {{__('Agents')}}
                    </a>
                </li>
            @endcan
            @can('chatex.merchant.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('chatex.merchant.index')) active @endif"
                       href="{{route('chatex.merchant.index')}}">
                       <i class="bi bi-person"></i>
                        {{__('Merchants')}}
                    </a>
                </li>
            @endcan
            @can('chatex.transactions.all_invoices')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('chatex.transactions.all_invoices')) active @endif"
                       href="{{route('chatex.transactions.all_invoices')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Transactions')}}
                    </a>
                </li>
            @endcan
            @can('system.balance.withdrawal_invoice.create')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('system.balance.withdrawal_invoice.create')) active @endif"
                       href="{{route('system.balance.withdrawal_invoice.create')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('System withdrawal')}}
                    </a>
                </li>
            @endcan
            @can('system.balance.withdrawal_invoice.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('system.balance.withdrawal_invoice.index')) active @endif"
                       href="{{route('system.balance.withdrawal_invoice.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('System withdrawal history')}}
                    </a>
                </li>
            @endcan
            @can('service.provider.withdrawal_invoice.create')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('service.provider.withdrawal_invoice.create')) active @endif"
                       href="{{route('service.provider.withdrawal_invoice.create')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Service Provider withdrawal')}}
                    </a>
                </li>
            @endcan
            @can('service.provider.withdrawal_invoice.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('service.provider.withdrawal_invoice.index')) active @endif"
                       href="{{route('service.provider.withdrawal_invoice.index')}}">
                        <i class="bi bi-card-list"></i>
                        {{__('Service Provider withdrawal history')}}
                    </a>
                </li>
            @endcan
        </ul>
        @can('traders')
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>{{__('Trader')}}</span>
                <i class="bi bi-chat"></i>
            </h6>
            <ul class="nav flex-column mb-2">
                @can('trader.index')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('trader.index')) active @endif"
                           href="{{route('trader.index')}}">
                            <i class="bi bi-person"></i>
                            {{__('Traders')}}
                        </a>
                    </li>
                @endcan
                @can('trader.balance_operation')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('trader.balance_operation')) active @endif"
                           href="{{route('trader.balance_operation')}}">
                            <i class="bi bi-list"></i>
                            {{__('Balance Operations')}}
                        </a>
                    </li>
                @endcan
                @can('trader.ads')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('trader.ads')) active @endif"
                           href="{{route('trader.ads')}}">
                            <i class="bi bi-card-list"></i>
                            {{__('Trader Ads')}}
                        </a>
                    </li>
                @endcan
                @can('trader.deal')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('trader.deals')) active @endif"
                           href="{{route('trader.deals')}}">
                            <i class="bi bi-card-list"></i>
                            {{__('Trader Deals')}}
                        </a>
                    </li>
                @endcan
            </ul>
        @endcan
        @can('chatex')
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>{{__('Chatex')}}</span>
                <i class="bi bi-chat"></i>
            </h6>
            <ul class="nav flex-column mb-2">
                @can('chatex.dashboard')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('chatex.dashboard')) active @endif"
                           href="{{route('chatex.dashboard')}}">
                            <i class="bi bi-chat"></i>
                            {{__('Dashboard contacts')}}
                        </a>
                    </li>
                @endcan
                @can('chatex.recent_messages')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('chatex.recent_messages')) active @endif"
                           href="{{route('chatex.recent_messages')}}">
                            <i class="bi bi-chat"></i>
                            {{__('Recent messages')}}
                        </a>
                    </li>
                @endcan
                @can('chatex.ads')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('chatex.ads')) active @endif"
                           href="{{route('chatex.ads')}}">
                            <i class="bi bi-chat"></i>
                            {{__('Ads')}}
                        </a>
                    </li>
                @endcan
                    @can('chatex.ads_buy')
                        <li class="nav-item">
                            <a class="nav-link @if(request()->routeIs('chatex.ads_buy')) active @endif"
                               href="{{route('chatex.ads_buy')}}">
                                <i class="bi bi-chat"></i>
                                {{__('Ads buy')}}
                            </a>
                        </li>
                    @endcan
                @can('chatex.wallet.history')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('chatex.wallet.history')) active @endif"
                           href="{{route('chatex.wallet.history')}}">
                            <i class="bi bi-card-list"></i>
                            {{__('Wallet history')}}
                        </a>
                    </li>
                @endcan
                @can('chatex.wallet.history_24')
                    <li class="nav-item">
                        <a class="nav-link @if(request()->routeIs('chatex.wallet.history_24')) active @endif"
                           href="{{route('chatex.wallet.history_24')}}">
                            <i class="bi bi-card-list"></i>
                            {{__('Wallet history 24')}}
                        </a>
                    </li>
                @endcan
            </ul>
        @endcan
        @can('settings')
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>{{__('Settings')}}</span>
                <i class="bi bi-gear"></i>
            </h6>
            <ul class="nav flex-column mb-2">
                @can('trader_currency.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_currency.index')) active @endif"
                       href="{{route('trader_currency.index')}}">
                        <i class="bi bi-person"></i>
                        {{__('Trader currencies')}}
                    </a>
                </li>
                @endcan
                @can('trader_fiat.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_fiat.index')) active @endif"
                       href="{{route('trader_fiat.index')}}">
                        <i class="bi bi-person"></i>
                        {{__('Trader fiats')}}
                    </a>
                </li>
                @endcan
                @can('trader_payment_system.index')
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_payment_system.index')) active @endif"
                       href="{{route('trader_payment_system.index')}}">
                        <i class="bi bi-person"></i>
                        {{__('Trader payment systems')}}
                    </a>
                </li>
                @endcan
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('sd_user.profile')) active @endif"
                       href="{{route('sd_user.profile')}}">
                        <i class="bi bi-person"></i>
                        {{__('Profile')}}
                    </a>
                </li>
            </ul>
        @endcan
    </div>
</nav>
