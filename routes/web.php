<?php

use App\Http\Controllers\Admin\TransactionController;
use App\Http\Controllers\AgentController;
use App\Http\Controllers\AgentWithdrawalInvoiceController;
use App\Http\Controllers\BalanceHistoryController;
use App\Http\Controllers\Chatex\AgentController as ChatexAgentController;
use App\Http\Controllers\Chatex\MerchantController as ChatexMerchantController;
use App\Http\Controllers\Chatex\TransactionController as ChatexTransactionController;
use App\Http\Controllers\ChatexController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InputInvoiceController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\MerchantInputInvoiceController;
use App\Http\Controllers\MerchantWithdrawalInvoiceController;
use App\Http\Controllers\SdUserController;
use App\Http\Controllers\PaymentInvoiceController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\MerchantAdsSelector;
use App\Http\Controllers\ServiceWithdrawalInvoiceController;
use App\Http\Controllers\SystemWithdrawalInvoiceController;
use App\Http\Controllers\TraderAdController;
use App\Http\Controllers\TraderController;
use App\Http\Controllers\TraderCurrencyController;
use App\Http\Controllers\TraderDealController;
use App\Http\Controllers\TraderFiatController;
use App\Http\Controllers\TraderPaymentSystemController;
use App\Http\Controllers\TraderBalanceOperationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserWithdrawalController;
use App\Http\Controllers\WithdrawalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Locale
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, config()->get('app.locales'))) {
        session()->put('locale', $locale);
    }
    return redirect()->back()->cookie('locale', $locale, 60 * 24 * 360);
});

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')
        ->middleware(['can:dashboard']);
    Route::get('/payment-invoice', [PaymentInvoiceController::class, 'index'])->name('payment_invoice.index')
        ->middleware(['can:payment_invoice.index']);
    Route::get('/payment-invoice/{id}', [PaymentInvoiceController::class, 'view'])->name('payment_invoice.view')
        ->middleware(['can:payment_invoice.view']);

    Route::post('/payment-invoice/{id}/resend-callback', [PaymentInvoiceController::class, 'reSendCallback'])->name('payment_invoice.resend_callback')
        ->middleware(['can:payment_invoice.resend_callback']);
    Route::post('/payment-invoice/{id}/set_status_payed', [PaymentInvoiceController::class, 'set_status_payed'])->name('payment_invoice.set_status_payed')
        ->middleware(['can:payment_invoice.set_status_payed']);
    Route::post('/payment-invoice/{id}/set_status_revert', [PaymentInvoiceController::class, 'set_status_revert'])->name('payment_invoice.set_status_revert')
        ->middleware(['can:payment_invoice.set_status_revert']);
    Route::post('/payment-invoice/{id}/change_amount', [PaymentInvoiceController::class, 'changeAmount'])->name('payment_invoice.change_amount')
        ->middleware(['can:payment_invoice.change_amount']);

    Route::get('/input-invoice', [InputInvoiceController::class, 'index'])->name('input_invoice.index')
        ->middleware(['can:input_invoice.index']);
    Route::get('/input-invoice/{id}', [InputInvoiceController::class, 'view'])->name('input_invoice.view')
        ->middleware(['can:input_invoice.view']);

    Route::get('/transactions', [TransactionController::class, 'allInvoices'])->name('transactions.all_invoices')
        ->middleware(['can:transactions.all_invoices']);

    Route::get('chatex/transactions', [ChatexTransactionController::class, 'allInvoices'])->name('chatex.transactions.all_invoices')
        ->middleware(['can:chatex.transactions.all_invoices']);
    Route::get('chatex/agents', [ChatexAgentController::class, 'index'])->name('chatex.agent.index')
        ->middleware(['can:chatex.agent.index']);
    Route::get('chatex/merchants', [ChatexMerchantController::class, 'index'])->name('chatex.merchant.index')
        ->middleware(['can:chatex.merchant.index']);
    Route::get('chatex/merchant/{id}', [ChatexMerchantController::class, 'view'])->name('chatex.merchant.view')
        ->middleware(['can:chatex.merchant.view']);

    Route::get('/balance-history', [BalanceHistoryController::class, 'index'])->name('balance_history.index')
        ->middleware(['can:balance_history.index']);
    Route::get('/balance-history/{id}', [BalanceHistoryController::class, 'view'])->name('balance_history.view')
        ->middleware(['can:balance_history.view']);

    Route::post('/chatex/send-message', [ChatexController::class, 'send_message'])->name('chatex.send_message')
        ->middleware(['can:chatex.send_message']);
    Route::post('/chatex/send-dispute', [ChatexController::class, 'send_dispute'])->name('chatex.send_dispute')
        ->middleware(['can:chatex.send_dispute']);
    Route::get('/chatex/dashboard', [ChatexController::class, 'dashboard'])->name('chatex.dashboard')
        ->middleware(['can:chatex.dashboard']);
    Route::get('/chatex/contact/{id}', [ChatexController::class, 'contact'])->name('chatex.contact')
        ->middleware(['can:chatex.contact']);
    Route::get('/chatex/recent_messages', [ChatexController::class, 'recent_messages'])->name('chatex.recent_messages')
        ->middleware(['can:chatex.recent_messages']);
    Route::get('/chatex/image/{url}', [ChatexController::class, 'image'])->name('chatex.image')
        ->middleware(['can:chatex.image']);

    Route::get('/chatex/ads', [ChatexController::class, 'ads'])->name('chatex.ads')
        ->middleware(['can:chatex.ads']);
    Route::get('/chatex/ads-buy', [ChatexController::class, 'adsBuy'])->name('chatex.ads_buy')
        ->middleware(['can:chatex.ads_buy']);
    Route::get('/wallet/history', [ChatexController::class, 'wallet_history'])->name('chatex.wallet.history')
        ->middleware(['can:chatex.wallet.history']);
    Route::get('/wallet/history-24', [ChatexController::class, 'wallet_history_24'])->name('chatex.wallet.history_24')
        ->middleware(['can:chatex.wallet.history_24']);
    Route::get('/withdrawal-history', [WithdrawalController::class, 'index'])->name('withdrawal.index')
        ->middleware(['can:withdrawal.index']);

    Route::get('/chatex/dictionaries', [ChatexController::class, 'dictionaries'])->name('chatex.dictionaries')
        ->middleware(['can:chatex.ads']);

    Route::post('/user/withdrawal/set_status_payed/{id}', [UserWithdrawalController::class, 'set_status_payed'])->name('user.withdrawal.set_status_payed')
        ->middleware(['can:user.withdrawal.index']);
    Route::post('/user/withdrawal/set_status_cancel/{id}', [UserWithdrawalController::class, 'set_status_cancel'])->name('user.withdrawal.set_status_cancel')
        ->middleware(['can:user.withdrawal.index']);
    Route::get('/user/withdrawal', [UserWithdrawalController::class, 'index'])->name('user.withdrawal.index')
        ->middleware(['can:user.withdrawal.index']);
    Route::get('/users', [UserController::class, 'index'])->name('user.index')
        ->middleware(['can:user.index']);
    Route::get('/user/{id}', [UserController::class, 'view'])->name('user.view')
        ->middleware(['can:user.view']);

    Route::get('/agents', [AgentController::class, 'index'])->name('agent.index')
        ->middleware(['can:agent.index']);
    Route::get('/agent/new', [AgentController::class, 'new'])->name('agent.new')
        ->middleware(['can:agent.new']);
    Route::get('/agent/edit/{id}', [AgentController::class, 'edit'])->name('agent.edit')
        ->middleware(['can:agent.edit']);
    Route::patch('/agent/update/{id}', [AgentController::class, 'update'])->name('agent.update')
        ->middleware(['can:agent.edit']);
    Route::post('/agent/create', [AgentController::class, 'create'])->name('agent.create')
        ->middleware(['can:agent.new']);
    Route::get('/agent/{id}', [AgentController::class, 'view'])->name('agent.view')
        ->middleware(['can:agent.view']);
    Route::get('/agent/{id}/withdrawals', [AgentController::class, 'withdrawals'])->name('agent.withdrawals')
        ->middleware(['can:agent.withdrawals']);


    Route::get('/merchants', [MerchantController::class, 'index'])->name('merchant.index')
        ->middleware(['can:merchant.index']);
    Route::get('/merchant/new', [MerchantController::class, 'new'])->name('merchant.new')
        ->middleware(['can:merchant.new']);
    Route::post('/merchant/create', [MerchantController::class, 'create'])->name('merchant.create')
        ->middleware(['can:merchant.new']);
    Route::get('/merchant/edit/{id}', [MerchantController::class, 'edit'])->name('merchant.edit')
        ->middleware(['can:merchant.edit']);
    Route::patch('/merchant/update/{id}', [MerchantController::class, 'update'])->name('merchant.update')
        ->middleware(['can:merchant.edit']);
    Route::post('/merchant/update-chat/{id}', [MerchantController::class, 'updateChat'])->name('merchant.update-chat')
        ->middleware(['can:merchant.edit']);
    Route::patch('/merchant/update-commission/{id}', [MerchantController::class, 'updateCommission'])->name('merchant.update-commission')
        ->middleware(['can:merchant.edit']);
    Route::patch('/merchant/update-limit/{id}', [MerchantController::class, 'updateLimit'])->name('merchant.update-limit')
        ->middleware(['can:merchant.edit']);
    Route::get('/merchant/{id}', [MerchantController::class, 'view'])->name('merchant.view')
        ->middleware(['can:merchant.view']);
    Route::get('/merchant/{id}/withdrawals', [MerchantController::class, 'withdrawals'])->name('merchant.withdrawal.index')
        ->middleware(['can:merchant.withdrawal.index']);

    Route::get('/withdrawal/{id}', [WithdrawalController::class, 'view'])->name('withdrawal.view')
        ->middleware(['can:withdrawal.view']);
    Route::get('/withdrawal/attachment/{id}', [WithdrawalController::class, 'attachment'])->name('withdrawal.attachment')
        ->middleware(['can:withdrawal.attachment']);
    Route::post('/withdrawal/{id}/resend-callback', [WithdrawalController::class, 'reSendCallback'])->name('withdrawal.resend_callback')
        ->middleware(['can:withdrawal.resend_callback']);
    Route::post('/withdrawal/set_status_payed/{id}', [WithdrawalController::class, 'set_status_payed'])->name('withdrawal.set_status_payed')
        ->middleware(['can:withdrawal.set_status_payed']);

    Route::post('/withdrawal/{id}/change_amount', [WithdrawalController::class, 'changeAmount'])->name('withdrawal.change_amount')
        ->middleware(['can:withdrawal.change_amount']);

    Route::get('/merchant/input-invoice/create', [MerchantInputInvoiceController::class, 'create'])->name('merchant.input_invoice.create')
        ->middleware(['can:merchant.input_invoice.create']);
    Route::post('/merchant/input-invoice/create', [MerchantInputInvoiceController::class, 'store'])->name('merchant.input_invoice.store')
        ->middleware(['can:merchant.input_invoice.create']);
    Route::get('/merchant/withdrawal-invoice/create', [MerchantWithdrawalInvoiceController::class, 'create'])->name('merchant.withdrawal_invoice.create')
        ->middleware(['can:merchant.withdrawal_invoice.create']);
    Route::post('/merchant/withdrawal-invoice/create', [MerchantWithdrawalInvoiceController::class, 'store'])->name('merchant.withdrawal_invoice.store')
        ->middleware(['can:merchant.withdrawal_invoice.create']);
    Route::get('/merchants/ads-selector', [MerchantAdsSelector::class, 'index'])->name('merchant.ads-selector')
        ->middleware(['can:merchant.ads_selector']);
    Route::get('/merchants/get-ads', [MerchantAdsSelector::class, 'index'])->name('merchant.get-ads')
        ->middleware(['can:merchant.ads_selector']);
    Route::post('/merchants/get_merchant_props', [MerchantAdsSelector::class, 'get_merchant_props'])->name('merchant.get_merchant_props')
        ->middleware(['can:merchant.ads_selector']);

    Route::get('/agent/withdrawal-invoice/create', [AgentWithdrawalInvoiceController::class, 'create'])->name('agent.withdrawal_invoice.create')
        ->middleware(['can:agent.withdrawal_invoice.create']);
    Route::post('/agent/withdrawal-invoice/create', [AgentWithdrawalInvoiceController::class, 'store'])->name('agent.withdrawal_invoice.store')
        ->middleware(['can:agent.withdrawal_invoice.create']);

    Route::get('/system/balance/withdrawal-invoice/create', [SystemWithdrawalInvoiceController::class, 'create'])->name('system.balance.withdrawal_invoice.create')
        ->middleware(['can:system.balance.withdrawal_invoice.create']);
    Route::post('/system/balance/withdrawal-invoice/create', [SystemWithdrawalInvoiceController::class, 'store'])->name('system.balance.withdrawal_invoice.store')
        ->middleware(['can:system.balance.withdrawal_invoice.create']);
    Route::get('/system/balance/withdrawal-invoices', [SystemWithdrawalInvoiceController::class, 'index'])->name('system.balance.withdrawal_invoice.index')
        ->middleware(['can:system.balance.withdrawal_invoice.index']);

    Route::get('/service/provider/withdrawal-invoice/create', [ServiceWithdrawalInvoiceController::class, 'create'])->name('service.provider.withdrawal_invoice.create')
        ->middleware(['can:service.provider.withdrawal_invoice.create']);
    Route::post('/service/provider/withdrawal-invoice/create', [ServiceWithdrawalInvoiceController::class, 'store'])->name('service.provider.withdrawal_invoice.store')
        ->middleware(['can:service.provider.withdrawal_invoice.create']);
    Route::get('/service/provider/withdrawal-invoices', [ServiceWithdrawalInvoiceController::class, 'index'])->name('service.provider.withdrawal_invoice.index')
        ->middleware(['can:service.provider.withdrawal_invoice.index']);

    Route::get('/profile', [SdUserController::class, 'profile'])->name('sd_user.profile')
        ->middleware(['can:sd_user.profile']);
    Route::patch('/profile', [SdUserController::class, 'profile_update'])->name('sd_user.profile.update')
        ->middleware(['can:sd_user.profile.update']);

    Route::get('/trader/currency', [TraderCurrencyController::class, 'index'])->name('trader_currency.index')
        ->middleware(['can:trader_currency.index']);
    Route::get('/trader/currency/new', [TraderCurrencyController::class, 'new'])->name('trader_currency.new')
        ->middleware(['can:trader_currency.new']);
    Route::post('/trader/currency/create', [TraderCurrencyController::class, 'create'])->name('trader_currency.create')
        ->middleware(['can:trader_currency.new']);
    Route::get('/trader/currency/edit/{id}', [TraderCurrencyController::class, 'edit'])->name('trader_currency.edit')
        ->middleware(['can:trader_currency.edit']);
    Route::patch('/trader/currency/update/{id}', [TraderCurrencyController::class, 'update'])->name('trader_currency.update')
        ->middleware(['can:trader_currency.edit']);
    Route::get('/trader/currency/{id}', [TraderCurrencyController::class, 'view'])->name('trader_currency.view')
        ->middleware(['can:trader_currency.view']);

    Route::get('/trader/fiat', [TraderFiatController::class, 'index'])->name('trader_fiat.index')
        ->middleware(['can:trader_fiat.index']);
    Route::get('/trader/fiat/new', [TraderFiatController::class, 'new'])->name('trader_fiat.new')
        ->middleware(['can:trader_fiat.new']);
    Route::post('/trader/fiat/create', [TraderFiatController::class, 'create'])->name('trader_fiat.create')
        ->middleware(['can:trader_fiat.new']);
    Route::get('/trader/fiat/edit/{id}', [TraderFiatController::class, 'edit'])->name('trader_fiat.edit')
        ->middleware(['can:trader_fiat.edit']);
    Route::patch('/trader/fiat/update/{id}', [TraderFiatController::class, 'update'])->name('trader_fiat.update')
        ->middleware(['can:trader_fiat.edit']);
    Route::get('/trader/fiat/{id}', [TraderFiatController::class, 'view'])->name('trader_fiat.view')
        ->middleware(['can:trader_fiat.view']);

    Route::get('/trader/payment_system', [TraderPaymentSystemController::class, 'index'])->name('trader_payment_system.index')
        ->middleware(['can:trader_payment_system.index']);
    Route::get('/trader/payment_system/new', [TraderPaymentSystemController::class, 'new'])->name('trader_payment_system.new')
        ->middleware(['can:trader_payment_system.new']);
    Route::post('/trader/payment_system/create', [TraderPaymentSystemController::class, 'create'])->name('trader_payment_system.create')
        ->middleware(['can:trader_payment_system.new']);
    Route::get('/trader/payment_system/edit/{id}', [TraderPaymentSystemController::class, 'edit'])->name('trader_payment_system.edit')
        ->middleware(['can:trader_payment_system.edit']);
    Route::patch('/trader/payment_system/update/{id}', [TraderPaymentSystemController::class, 'update'])->name('trader_payment_system.update')
        ->middleware(['can:trader_payment_system.edit']);
    Route::get('/trader/payment_system/{id}', [TraderPaymentSystemController::class, 'view'])->name('trader_payment_system.view')
        ->middleware(['can:trader_payment_system.view']);

    Route::get('/trader', [TraderController::class, 'index'])->name('trader.index')
        ->middleware(['can:trader.index']);
    Route::get('/trader/ads', [TraderAdController::class, 'index'])->name('trader.ads')
        ->middleware(['can:trader.ads']);
    Route::post('/trader/ads', [TraderAdController::class, 'index'])->name('trader.ads')
        ->middleware(['can:trader.ads']);
    Route::get('/trader/deal/detail/{id}', [TraderDealController::class, 'detail'])->name('trader_deal.detail')
        ->middleware(['can:trader.deal']);
    Route::post('/trader/deal/status/{id}', [TraderDealController::class, 'status'])->name('trader_deal.status')
        ->middleware(['can:trader.deal']);
    Route::get('/trader/deal/history/{id}', [TraderDealController::class, 'history'])->name('trader_deal.history')
        ->middleware(['can:trader.deal']);
    Route::post('/trader/deal/message/send/{id}', [TraderDealController::class, 'messageSend'])->name('trader_deal.message.send')
        ->middleware(['can:trader.deal']);
    Route::post('/trader/deal/{id}/change_amount', [TraderDealController::class, 'changeAmount'])->name('trader_deal.change_amount')
        ->middleware(['can:trader.deal']);

    Route::get('/trader/deal/message/attachment/{id}', [TraderDealController::class, 'attachment'])->name('trader_deal.message.attachment')
        ->middleware(['can:trader.deal']);

    Route::get('/trader/debit/{id}', [TraderBalanceOperationController::class, 'changeBalanceIndexDebit'])->name('trader.debit')
        ->middleware(['can:trader.balance_operation']);
    Route::get('/trader/credit/{id}', [TraderBalanceOperationController::class, 'changeBalanceIndexCredit'])->name('trader.credit')
        ->middleware(['can:trader.balance_operation']);
    Route::patch('/trader/balance-change/{id}', [TraderBalanceOperationController::class, 'traderBalanceChange'])->name('trader.balance-change')
        ->middleware(['can:trader.balance_operation']);
   Route::post('/trader/show-balance', [TraderBalanceOperationController::class, 'showBalance'])->name('trader.show-balance')
        ->middleware(['can:trader.balance_operation']);
    Route::get('/trader/balance-operations', [TraderBalanceOperationController::class, 'index'])->name('trader.balance_operation')
        ->middleware(['can:trader.balance_operation']);

    Route::get('/trader/deals', [TraderDealController::class, 'index'])->name('trader.deals')
        ->middleware(['can:trader.deal']);
    Route::post('/trader/deals', [TraderDealController::class, 'index'])->name('trader.deals')
        ->middleware(['can:trader.deal']);
    Route::get('/trader/new', [TraderController::class, 'new'])->name('trader.new')
        ->middleware(['can:trader.new']);
    Route::post('/trader/create', [TraderController::class, 'create'])->name('trader.create')
        ->middleware(['can:trader.new']);
    Route::get('/trader/edit/{id}', [TraderController::class, 'edit'])->name('trader.edit')
        ->middleware(['can:trader.edit']);
    Route::patch('/trader/trader/{id}', [TraderController::class, 'update'])->name('trader.update')
        ->middleware(['can:trader.edit']);
    Route::get('/trader/{id}', [TraderController::class, 'view'])->name('trader.view')
        ->middleware(['can:trader.view']);

    Route::get('/service', [ServiceController::class, 'index'])->name('service.index')
        ->middleware(['can:service.index']);
    Route::get('/service/new', [ServiceController::class, 'new'])->name('service.new')
        ->middleware(['can:service.new']);
    Route::post('/service/create', [ServiceController::class, 'create'])->name('service.create')
        ->middleware(['can:service.new']);
    Route::get('/service/edit/{id}', [ServiceController::class, 'edit'])->name('service.edit')
        ->middleware(['can:service.edit']);
    Route::patch('/service/update/{id}', [ServiceController::class, 'update'])->name('service.update')
        ->middleware(['can:service.edit']);
    Route::patch('/service/update_priority_banks_traders/{id}', [ServiceController::class, 'update_priority_banks_traders'])->name('service.update_priority_banks_traders')
        ->middleware(['can:service.edit']);
    Route::get('/service/{id}', [ServiceController::class, 'view'])->name('service.view')
        ->middleware(['can:service.view']);


});

require __DIR__ . '/auth.php';
