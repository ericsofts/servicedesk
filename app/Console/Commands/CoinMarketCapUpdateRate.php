<?php

namespace App\Console\Commands;

use App\Models\Property;
use CoinMarketCap\Features\Tools;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CoinMarketCapUpdateRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CoinMarketCap:update_rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update coin rate from CoinMarketCap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $properties = Property::getProperties('coinmarketcap');
            $coin = $properties['coin'];

            $cmc = new Tools($properties['key']);
            $res = $cmc->priceConversion(['amount' => 1, 'symbol' => $properties['symbol'], 'convert' => $coin]);
            //print_r($res);
            if (!empty($res->data->quote->{$coin}->price)) {
                Property::setProperty('coinmarketcap', 0, 'coin_rate', $res->data->quote->{$coin}->price);
            } else {
                Log::error('Не удалось обновить курс CoinMarketCap.');
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
        return 0;
    }
}
