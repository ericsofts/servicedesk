<?php

namespace App\Traits;


use App\Lib\Chatex\ChatexApi;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use Illuminate\Support\Facades\Log;

trait ChatexAds
{
    use Invoice;

    public function getChatexLBCConnection(){
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        return $chatexLBC;
    }

    private function getChatexAvalableAds(){

        $adsList = $this->getChatexAds();
        if (!empty($adsList)) {
            $ads_coin = $this->getChatexCoins($adsList);
            if (!empty($ads_coin)) {
                $coins_filtered = $this->filterChatexCoin($ads_coin);
                return $coins_filtered;
            }
        }
        return [];
    }

    private function getChatexAds(){
        $ad_list = [];

        $chatexLBC = $this->getChatexLBCConnect();
        $available_coins = $this->userType==self::TYPE_MERCHANT ? $this->getAvailableMerchantCoins($this->merchant->id) : $this->getAvailableUserCoins();
        if($this->type == TraderAd::TYPE_BUY){
            foreach ($available_coins as $coin) {
                $ad_list = array_merge($ad_list, $chatexLBC->adList2buy($coin));
            }
        }elseif ($this->type == TraderAd::TYPE_SELL){
            foreach ($available_coins as $coin) {
                $ad_list = array_merge($ad_list, $chatexLBC->adList($coin));
            }
        }
        return $ad_list;
    }

    private function getChatexCoins($ad_list){
        $ads_coins = [];
        if($this->type == TraderAd::TYPE_BUY || $this->currency2currency) {
            $currencyRate = $this->getCurrencyRate($this->currency);
        }
        if ($this->currency2currency) {
            $percentage_currency_rate = ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'percentage_currency_rate', 0);
            $maxCurrencyRate = round(1/$currencyRate * (1 + $percentage_currency_rate / 100), 2);
            $minCurrencyRate = round(1/$currencyRate * (1 - $percentage_currency_rate / 100), 2);
        }
        $rates = $this->getCoinRates($this->userType==self::TYPE_MERCHANT ? $this->getAvailableMerchantCoins($this->merchant->id) : $this->getAvailableUserCoins());
        if($this->type == TraderAd::TYPE_SELL){
            $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'priority_traders', '[]'), true);
        }
        if($this->type == TraderAd::TYPE_BUY){
            $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'withdrawal_priority_traders', '[]'), true);
        }

        $blocked_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'blocked_traders'), true);
        $excluded_currencies = $this->userType==self::TYPE_MERCHANT ? json_decode(MerchantProperty::getProperty($this->merchant->id, 'excluded_currencies', '[]'), true):[];
        $blocked_banks = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'blocked_banks', '[]'), true);
        $available_currencies = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'available_currencies'), true);
        $priority_banks_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'priority_banks_traders', '[]'), true);
        if($priority_banks_traders){
            foreach ($priority_banks_traders as &$item){
                $item = array_reverse($item);
            }
        }
        if($this->type == TraderAd::TYPE_SELL){
            $currencies_without_card_number = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, $this->currentProvider->id, 'currencies_without_card_number', '[]'), true);
            $rounding_input_amount = $this->userType==self::TYPE_MERCHANT ? MerchantProperty::getProperty($this->merchant->id, 'rounding_input_amount'): false;
        }
        foreach ($ad_list as $item) {
            $data = $item['data'];
            $currency = strtoupper($data['currency']);
            if (!empty($data['profile']['username']) && $blocked_traders && in_array($data['profile']['username'], $blocked_traders)) {
                continue;
            }
            $bank_name = mb_strtolower($data['bank_name']);
            if (!empty($bank_name) && $blocked_banks && in_array($bank_name, $blocked_banks)) {
                continue;
            }
            if (str_starts_with($bank_name, 'cash') || str_starts_with($bank_name, 'нал')) {
                continue;
            }

            if ($available_currencies && !in_array($currency, $available_currencies)) {
                continue;
            }
            if ($excluded_currencies && in_array($currency, $excluded_currencies)) {
                continue;
            }

            if (($this->currency2currency || $this->exact_currency) && $currency != $this->currency) {
                continue;
            }

            if ($this->currency2currency && ($data['temp_price'] < $minCurrencyRate || $data['temp_price'] > $maxCurrencyRate)) {
                continue;
            }

            $data['gps_priority'] = 0;
            if (!empty($data['profile']['username'])){
                if($priority_banks_traders && !empty($priority_banks_traders[$data['bank_name']]) && in_array($data['profile']['username'], $priority_banks_traders[$data['bank_name']])){
                    $data['gps_priority'] = 2 + array_search($data['profile']['username'], $priority_banks_traders[$data['bank_name']]);
                }else if($priority_traders && in_array($data['profile']['username'], $priority_traders)){
                    $data['gps_priority'] = 1;
                }
            }

            $coin = strtolower($data['coin']);
            if (empty($rates[$coin]['coin_rate'])) {
                continue;
            }

            if($this->type == TraderAd::TYPE_SELL) {
                if (config('app.chatex_lbc_is_accept_visa_direct') && !$data['profile']['is_accept_visa_direct']) {
                    continue;
                }
                if (config('app.chatex_lbc_clean_card_number') && empty($data['clean_card_number']) &&
                    (!in_array($currency, $currencies_without_card_number) || (!$priority_traders || !in_array($data['profile']['username'], $priority_traders)))
                ) {
                    continue;
                }
                if ($this->currency2currency) {
                    $c_amount = floatval($this->input_amount);
                } else {
                    $coin_amount = $this->amount * $rates[$coin]['coin_rate'];
                    $c_amount = $coin_amount * $data['temp_price'];
                }

            }elseif ($this->type == TraderAd::TYPE_BUY){
                if ($this->currency2currency) {
                    $amount_usd = floatval($this->input_amount) / $data['temp_price'];
                }else {
                    $amount_usd = round(floatval($this->input_amount) * $currencyRate, 2);
                }
                if ($this->expense) {
                    $amount2pay = $amount_usd;
                } else {
                    $fee = $this->merchant->getFeeCommission(
                        $this->merchant->id,
                        $amount_usd,
                        $this->api,
                        MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                        $this->currentProvider->id
                    );
                    $amount2pay = $amount_usd - $fee['total'];
                }

                $coin_amount = $amount2pay * $rates[$coin]['coin_rate'];
                $c_amount = $coin_amount * $data['temp_price'];
            }

            if(($this->userType==self::TYPE_MERCHANT) && $this->filterAdByLimit($data, $c_amount)){
                continue;
            }

            if ((is_null($data['min_amount']) || $c_amount >= $data['min_amount'])
                && (is_null($data['max_amount']) || $c_amount <= $data['max_amount'])
                && $c_amount <= $data['max_amount_available']
            ) {
                if($this->type == TraderAd::TYPE_SELL){
                    if ($rounding_input_amount) {
                        $data['gps_temp_amount'] = ceil($c_amount);
                    } else {
                        $data['gps_temp_amount'] = round($c_amount, 2);
                    }
                }elseif ($this->type == TraderAd::TYPE_BUY) {
                    $data['gps_temp_amount'] = round($c_amount, 2);
                }
                if($currency == 'INR'){
                    $data['payment_system_type'] = PaymentSystemType::UPI;
                }else{
                    $data['payment_system_type'] = PaymentSystemType::CARD_NUMBER;
                }
                $ads_coins[$data['currency']][] = $data;
            }
        }
        return $ads_coins;
    }

    private function filterChatexCoin($ads_coin){
        foreach ($ads_coin as &$item) {
            if($this->type == TraderAd::TYPE_SELL){
                usort($item, function ($a, $b) {
                    if($a['gps_priority'] && $b['gps_priority']){
                        if ($a['gps_priority'] == $b['gps_priority'] && $a['temp_price'] > $b['temp_price']) {
                            return 1;
                        }else if ($a['gps_priority'] > $b['gps_priority']) {
                            return -1;
                        }else{
                            return 1;
                        }
                    }
                    if (!$a['gps_priority'] && !$b['gps_priority'] && $a['temp_price'] == $b['temp_price']) {
                        return 0;
                    }
                    if (!$a['gps_priority'] && !$b['gps_priority'] && $a['temp_price'] > $b['temp_price']) {
                        return 1;
                    }
                    return $a['gps_priority'] < $b['gps_priority'] ? 1 : -1;
                });

            }elseif($this->type == TraderAd::TYPE_BUY){
                usort($item, function ($a, $b) {
                    if($a['gps_priority'] && $b['gps_priority']){
                        if ($a['gps_priority'] == $b['gps_priority'] && $a['temp_price'] < $b['temp_price']) {
                            return 1;
                        }else if ($a['gps_priority'] > $b['gps_priority']) {
                            return -1;
                        }else{
                            return 1;
                        }
                    }
                    if ($a['gps_priority'] && $b['gps_priority'] && $a['temp_price'] < $b['temp_price']) {
                        return 1;
                    }
                    if (!$a['gps_priority'] && !$b['gps_priority'] && $a['temp_price'] == $b['temp_price']) {
                        return 0;
                    }
                    if (!$a['gps_priority'] && !$b['gps_priority'] && $a['temp_price'] < $b['temp_price']) {
                        return 1;
                    }
                    return $a['gps_priority'] < $b['gps_priority'] ? 1 : -1;
                });
            }

        }
        $coins_filtered = [];
        foreach ($ads_coin as $k => $v) {
            $ads = [];
            $banks = [];
            foreach ($v as $k1 => $ad) {
                if ($ad['gps_priority'] && !in_array(mb_strtolower($ad['bank_name']), $banks)) {
                    $banks[] = mb_strtolower($ad['bank_name']);
                    $ad['bank_name'] .= "*";
                    $ads[] = $ad;
                    unset($v[$k1]);
                }
            }
            foreach ($v as $ad) {
                if (!$ad['gps_priority'] && !in_array(mb_strtolower($ad['bank_name']), $banks)) {
                    $ads[] = $ad;
                    $banks[] = mb_strtolower($ad['bank_name']);
                }
            }
            $coins_filtered[$k] = $ads;
        }
        return $coins_filtered;
    }

    private function getChatexAd($adId, $invoice = false){
        $ad_res = $this->getChatexLBCConnect()->adGet($adId);
        $ad = $ad_res['data']['ad_list'][0]['data'] ?? null;

        if($ad){
            $provider = $this->getFirstProviderByType(ChatexApi::OBJECT_NAME);
            if($ad['currency'] == 'INR'){
                $payment_system_type = PaymentSystemType::UPI;
            }else{
                $payment_system_type = PaymentSystemType::CARD_NUMBER;
            }
            return [
                'id' => $ad['ad_id'],
                'coin' => $ad['coin'],
                'card_number' => $ad['clean_card_number'] ?? null,
                'currency' => $ad['currency'],
                'bank_name' => $ad['bank_name'],
                'name' => $ad['profile']['username'],
                'temp_price' => $ad['temp_price'],
                'account_info' => $ad['account_info'],
                'type' => $this->chatexAdType($ad),
                'service_provider_id' => $provider->id,
                'service_provider_type' => $provider->type,
                'service_provider_name' => $provider->name,
                'payment_system_type' => $payment_system_type,
                'log' => 'chatex_lbc_requests',
                'auto_acceptance' => false,
            ];
        }else if($invoice){
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::getInvoiceTypeByInstance($invoice),
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($ad),
                'Chatex response incorrect'
            );
            Log::channel('chatex_lbc_requests')->info('AD|NOT_FOUND|ERROR|MERCHANT_ID|' . $invoice->merchant_id ?? '', ['ad_res' => $ad_res]);
        }

        return $ad;
    }

    private function chatexAdType($ad)
    {
        return match ($ad['trade_type']) {
            'ONLINE_BUY' => 'BUY',
            'ONLINE_SELL' => 'SELL',
            default => null,
        };
    }

}
