<?php

namespace App\Http\Middleware;

use App\Models\Merchant;
use Closure;

class CheckMerchantToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->input('token');

        if(!$token){
            $token = $request->bearerToken();
        }

        if (!$token) {
            abort(403, 'Unauthorized action.');
        }

        $merchant = Merchant::where([
            'token' => $token,
            'status' => 1
        ])->first();

        if(!$merchant){
            abort(403, 'Unauthorized action.');
        }

        $request->merge(['merchant' => $merchant]);

        return $next($request);
    }
}
