<?php
namespace App\Http\Libs\Api;

use Illuminate\Support\Facades\Storage;

class User extends Base
{
    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.payment_system_host_token');
        parent::__construct($token);
    }

    public function setStatusPayed($params = [])
    {
        $response = $this->httpClient->post(
            config('app.payment_system_host') . '/api/v1',
            $this->prepare_params('uwi@setStatusPayed', $params)
        );
        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result']['success']) && ($result['result']['success'] == true)) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public function setStatusCancel($params = [])
    {
        $response = $this->httpClient->post(
            config('app.payment_system_host') . '/api/v1',
            $this->prepare_params('uwi@setStatusCancel', $params)
        );
        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result']['success']) && ($result['result']['success'] == true)) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }



}
