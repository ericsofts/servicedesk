<?php


namespace App\Http\Libs\Api;


class MerchantWithdrawalInvoiceApi extends Base
{
    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.payment_system_host_token');
        parent::__construct($token);
    }


    public function setStatusPayed($params = [])
    {
        $response = $this->httpClient->post(
            config('app.payment_system_host') . '/api/i/v1',
            $this->prepare_params('mwi@setStatusPayed', $params)
        );
        if ($response->ok()) {
            $result = $response->json();
            if (!empty($result['result']['success'])) {
                return $result['result']['success'];
            }
        } else {
            return false;
        }
        return false;
    }

}
