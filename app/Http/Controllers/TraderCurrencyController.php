<?php

namespace App\Http\Controllers;

use App\Models\TraderCurrency;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TraderCurrencyController extends Controller
{

    public function index(Request $request)
    {
        $trader_currencies = TraderCurrency::orderBy('updated_at', 'desc')->get();

        return view('trader_currency.index', [
            'trader_currencies' => $trader_currencies
        ]);
    }

    public function view(Request $request, $id)
    {
        $trader_currency = TraderCurrency::where(['id' => $id])->firstOrFail();
        return view('trader_currency.view', [
            'trader_currency' => $trader_currency
        ]);
    }

    public function new(Request $request)
    {
        return view('trader_currency.new', [
        ]);
    }

    public function edit(Request $request, $id)
    {
        $trader_currency = TraderCurrency::where(['id' => $id])->firstOrFail();

        return view('trader_currency.edit', [
            'id' => $trader_currency->id,
            'asset' => $trader_currency->asset,
            'name' => $trader_currency->name,
            'precision' => $trader_currency->precision,
            'crypto_asset' => $trader_currency->crypto_asset,
            'can_get_address' => $trader_currency->can_get_address,
            'status' => $trader_currency->status,
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'asset' => 'required|string|unique:trader_currencies,asset',
            'name' => 'required|string|unique:trader_currencies,name',
            'precision' => 'required|numeric',
            'crypto_asset' => 'required|string|unique:trader_currencies,crypto_asset',
            'can_get_address' => 'required|integer',
            'status' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_currency = $this->createTraderCurrency($request);
        if($trader_currency){
            return redirect()->route('trader_currency.index')->with(['alert-type' => 'success', 'message' => __('The Trader currency Created')]);
        }

        return redirect()->route('trader_currency.index')->with(['alert-type' => 'error', 'message' => __('The Trader currency Create Error')]);
    }

    public function update(Request $request, $id){
        $messages = [];

        $rules = [
            'asset' => 'required|string|unique:trader_currencies,asset,' . $id,
            'name' => 'required|string|unique:trader_currencies,name,' . $id,
            'precision' => 'required|numeric',
            'crypto_asset' => 'required|string|unique:trader_currencies,crypto_asset,' . $id,
            'can_get_address' => 'required|integer',
            'status' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_currency = TraderCurrency::where(['id' => $id])->first();

        if($trader_currency){
            $this->updateTraderCurrency($request, $trader_currency);
            return redirect()->route('trader_currency.index')->with(['alert-type' => 'success', 'message' => __('The Trader currency Updated')]);
        }

        return redirect()->route('trader_currency.index')->with(['alert-type' => 'error', 'message' => __('The Trader currency Update Error')]);
    }

    protected function createTraderCurrency($request)
    {
        $trader_currency = TraderCurrency::create([
            'asset' => $request->input('asset'),
            'name' => $request->input('name'),
            'precision' => $request->input('precision'),
            'crypto_asset' => $request->input('crypto_asset'),
            'can_get_address' => $request->input('can_get_address'),
            'status' => $request->input('status') ? $request->input('status') : 0
        ]);
        return $trader_currency;
    }

    protected function updateTraderCurrency($request, $trader_currency)
    {
        $trader_currency->asset = $request->input('asset');
        $trader_currency->name = $request->input('name');
        $trader_currency->precision = $request->input('precision');
        $trader_currency->crypto_asset = $request->input('crypto_asset');
        $trader_currency->can_get_address = $request->input('can_get_address');
        $trader_currency->status = $request->input('status');
        $trader_currency->save();
    }

}
