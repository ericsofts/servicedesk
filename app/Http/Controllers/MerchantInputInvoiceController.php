<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Models\MerchantInputInvoice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MerchantInputInvoiceController extends Controller
{
    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $merchants = Merchant::all()->pluck('name', 'id');

        return view('merchant_input_invoice.create', [
            'merchants' => $merchants
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'merchant_id' => [
                'required',
                'numeric',
                Rule::exists('merchants', 'id')
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'fee' => ['nullable', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'comment' => ['required'],
            'confirm' => ['required', 'accepted']
        ];

        $request->validate($rules);

        $invoice = MerchantInputInvoice::create([
            'merchant_id' => $request->input('merchant_id'),
            'amount' => $request->input('amount'),
            'comment' => $request->input('comment'),
            'payment_system' => MerchantInputInvoice::PAYMENT_SYSTEM_MANUALLY,
            'payed' => now(),
            'status' => MerchantInputInvoice::STATUS_PAYED,
            'sd_user_id' => $request->user()->id
        ]);

        return redirect()->route('merchant.input_invoice.create')->with('status',
            __('The payment is complete')
        );
    }

}
