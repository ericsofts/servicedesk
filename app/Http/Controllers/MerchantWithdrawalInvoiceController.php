<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Models\MerchantWithdrawalInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MerchantWithdrawalInvoiceController extends Controller
{
    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $merchants = Merchant::all();

        return view('merchant_withdrawal_invoice.create', [
            'merchants' => $merchants
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'merchant_id' => [
                'required',
                'numeric',
                Rule::exists('merchants', 'id')
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'fee' => ['nullable', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'comment' => ['required'],
            'confirm' => ['required', 'accepted']
        ];

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $amount = $request->input('amount');
        $fee = $request->input('fee', 0);
        $merchant_id = $request->input('merchant_id');

        $invoice = MerchantWithdrawalInvoice::create([
            'merchant_id' => $merchant_id,
            'amount' => $amount,
            'comment' => $request->input('comment'),
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
            'payed' => now(),
            'status' => MerchantWithdrawalInvoice::STATUS_PAYED,
            'sd_user_id' => $request->user()->id
        ]);

        if ($fee) {
            MerchantWithdrawalInvoice::create([
                'merchant_id' => $merchant_id,
                'amount' => $fee,
                'comment' => 'Invoice fee #' . $invoice->id . '(' . $invoice->amount . ' / ' . $fee . ')',
                'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
                'payed' => now(),
                'status' => MerchantWithdrawalInvoice::STATUS_PAYED,
                'sd_user_id' => $request->user()->id
            ]);
        }
        return redirect()->route('merchant.withdrawal_invoice.create')->with('status',
            __('The payment is complete')
        );
    }
}
