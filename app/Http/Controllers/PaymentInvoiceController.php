<?php

namespace App\Http\Controllers;

use App\Http\Libs\Api\PaymentInvoiceApi;
use App\Jobs\PaymentInvoiceSendResponse;
use App\Lib\Chatex\ChatexApiLBC;
use App\Lib\PaymentApiHelper;
use App\Models\CryptoPaymentInvoice;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\TraderDealMessage;
use App\Models\TraderDeal;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;
use App\Http\Libs\Api\Trader as TraderClient;

class PaymentInvoiceController extends Controller
{
    private $traderClient;

    public function __construct(
        TraderClient $traderClient
    ) {
        $this->traderClient = $traderClient;
    }

    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $q = $request->input('q');
        $merchant_selected = $request->input('merchants', []);

        $invoicesQuery = PaymentInvoice::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }

        if ($merchant_selected) {
            $invoicesQuery->whereIn('merchant_id', $merchant_selected);
        }

        if (!is_null($status) && in_array($status, [
            PaymentInvoice::STATUS_PAYED,
            PaymentInvoice::STATUS_CREATED,
            PaymentInvoice::STATUS_USER_CONFIRMED,
            PaymentInvoice::STATUS_USER_SELECTED,
            PaymentInvoice::STATUS_TRADER_CONFIRMED,
            PaymentInvoice::STATUS_CANCELED
        ])) {
            $invoicesQuery->where('status', '=', $status);
        }

        if (!empty($q)) {
            $invoicesQuery->where(function ($query) use ($q) {
                foreach (PaymentInvoice::searchLikeItems as $item) {
                    $query->OrWhere($item, 'like', "%{$q}%");
                }
            });
        }

        $invoicesQuery->with(['user', 'merchant', 'service_provider']);
        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $totalQuery = PaymentInvoice::where([
            'status' => PaymentInvoice::STATUS_PAYED
        ])
            ->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount2pay) as total_amount2pay'),
                DB::raw('SUM(amount2grow) as total_amount2grow'),
                DB::raw('SUM(amount2service) as total_amount2service'),
                DB::raw('SUM(amount2agent) as total_amount2agent'),
            );

        if ($from_date) {
            $totalQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $totalQuery->where('created_at', '<=', $to_date);
        }
        if ($merchant_selected) {
            $totalQuery->whereIn('merchant_id', $merchant_selected);
        }
        if (!empty($q)) {
            $totalQuery->where(function ($query) use ($q) {
                foreach (PaymentInvoice::searchLikeItems as $item) {
                    $query->OrWhere($item, 'like', "%{$q}%");
                }
            });
        }
        $total = $totalQuery->first();
        $merchants = Merchant::all()->pluck('name', 'id');
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('payment_invoice.index', [
            'merchants' => $merchants,
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'fmt' => $fmt,
            'total' => $total,
            'total_page' => [
                'amount' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount'),
                'amount2pay' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount2pay'),
                'amount2grow' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount2grow'),
                'amount2service' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount2service'),
                'amount2agent' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount2agent')
            ]
        ]);
    }

    public function view(Request $request, $id)
    {
        $invoice = PaymentInvoice::where(['id' => $id])->with(['user', 'merchant', 'service_provider'])->firstOrFail();
        $addition_info = json_decode($invoice['addition_info'], true) ?? null;

        if ($invoice->service_provider) {
            if ($invoice->service_provider->type == ServiceProvider::CHATEX_PROVIDER) {
                if (in_array($invoice->api_type, [PaymentInvoice::API_TYPE_3, PaymentInvoice::API_TYPE_5, PaymentInvoice::API_TYPE_0]) && !empty($invoice['payment_id'])) {
                    $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
                    $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
                    $contact_res = $chatexLBC->contactInfo($invoice['payment_id']);
                    $messages = $chatexLBC->contactMessages($invoice['payment_id']);
                }
            } elseif ($invoice->service_provider->type == ServiceProvider::TRADER_PROVIDER) {
                $messages = TraderDealMessage::where('deal_id', $addition_info['contact']['id'])->get();
                $senderType = $this->traderClient->getCustomerType();

                foreach ($messages as $message) {
                    if ($message->sender_type == TraderDeal::SYSTEM) {
                        $message->sender_name = $senderType[TraderDeal::SYSTEM]['label'];
                    } else {
                        $instance = app($senderType[$message->sender_type]['instance']);
                        $message->sender_name = $instance::where('id', '=', $message->sender_id)->first()->name ?? '';
                    }
                    if ($message->attachment_type) {
                        $content = $this->traderClient->getFile(['name' => $message->msg]);
                        $fh = fopen('php://memory', 'w+b');
                        fwrite($fh, $content);
                        $contentType = mime_content_type($fh);
                        fclose($fh);
                        $message->file = 'data: ' . $contentType . ';base64,' . base64_encode($content);
                    }
                }
            } elseif ($invoice->service_provider->type == ServiceProvider::CRYPTO_PROVIDER){
                $cryptoInvoice = CryptoPaymentInvoice::with(['currency'])->where('id', $invoice['payment_id'])->first();
            }
        }

        return view('payment_invoice.view', [
            'senderType' => $senderType ?? null,
            'is_chatex' => ($invoice->service_provider->type ?? '') == ServiceProvider::CHATEX_PROVIDER,
            'is_trader' => ($invoice->service_provider->type ?? '') == ServiceProvider::TRADER_PROVIDER,
            'is_crypto' => ($invoice->service_provider->type ?? '') == ServiceProvider::CRYPTO_PROVIDER,
            'invoice' => $invoice,
            'contact_res' => $contact_res ?? null,
            'messages' => $messages ?? null,
            'addition_info' => $addition_info,
            'cryptoInvoice' => $cryptoInvoice ?? null
        ]);
    }

    public function reSendCallback(Request $request, $id)
    {
        $invoice = PaymentInvoice::where(['id' => $id])->with(['merchant'])->firstOrFail();
        if (($invoice->status == PaymentInvoice::STATUS_PAYED || PaymentInvoice::isCancelStatus($invoice->status)) && $invoice->merchant_server_url) {
            $status = -1;
            if($invoice->status == PaymentInvoice::STATUS_PAYED){
                $status = 1;
            }
            $response = [
                'invoice_id' => $invoice->id,
                'merchant_id' => $invoice->merchant_id,
                'order_id' => $invoice->merchant_order_id,
                'amount' => round($invoice->amount, 2),
                'amount_currency' => round($invoice->input_amount_value, 2),
                'currency' => $invoice->input_currency,
                'order_desc' => $invoice->merchant_order_desc,
                'merchant_amount' => round($invoice->amount2pay, 2),
                'account_info' => mask_credit_card($invoice->account_info),
                'status' => $status
            ];
            $response['signature'] = PaymentApiHelper::generateSignature($response, $invoice->merchant->api5_key);
            PaymentInvoiceSendResponse::dispatch($invoice, $response);
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('The callback is queued')]);
        }
        $message = __('Whoops! Something went wrong.');
        if ($invoice->status != PaymentInvoice::STATUS_PAYED) {
            $message = __('You cannot send a callback if the status is not paid');
        }
        return redirect()->back()->with(['alert-type' => 'success', 'message' => $message]);
    }

    public function set_status_payed(Request $request, $id)
    {
        $invoice = PaymentInvoice::where(['id' => $id])
            ->firstOrFail();
        if ($invoice->status == PaymentInvoice::STATUS_PAYED) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Payment invoice ":id" has status paid', ['id' => $id])]);
        }
        $result = (new PaymentInvoiceApi())->setStatusPayed(['id' => $id]);
        if($result){
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status changed.')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status has not changed.')]);
    }

    public function set_status_revert(Request $request, $id)
    {
        $invoice = PaymentInvoice::where(['id' => $id])
            ->firstOrFail();
        if ($invoice->status == PaymentInvoice::STATUS_CANCELED_BY_REVERT) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Payment invoice ":id" has status revert', ['id' => $id])]);
        }
        if ($invoice->status != PaymentInvoice::STATUS_PAYED) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Payment invoice ":id" has not status payed', ['id' => $id])]);
        }
        $result = (new PaymentInvoiceApi())->setStatusRevert(['id' => $id]);
        if($result){
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status changed.')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status has not changed.')]);
    }

    public function changeAmount(Request $request, Merchant $merchantModel, $id)
    {
        $invoice = PaymentInvoice::where(['id' => $id])
            ->whereIn('status', PaymentInvoice::getCancelStatuses())
            ->first();

        if(!$invoice){
            return response()->json([
                'status' => false,
                'errors' => null,
                'message' => __('Инвойс не найден или статус некорректный')
            ]);
        }

        $validator = Validator::make($request->input(), [
            'amount' => ['required', 'numeric', function ($attribute, $value, $fail) use ($invoice) {
                $mProperties = MerchantProperty::getProperties($invoice->merchant_id);
                $min_payment_invoice_amount = $mProperties['min_payment_invoice_amount'] ?? config('app.min_payment_invoice_amount');
                if ($value < $min_payment_invoice_amount) {
                    $fail(__('The minimum amount must be greater than or equal to :amount', ['amount' => $min_payment_invoice_amount]));
                }
            },
             'comment' => 'required'
            ]
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
                'message' => __('Amount has not been changed')
            ]);
        }

        $ps_amount = $request->input('amount');
        $amount = round($ps_amount, 2);
        $fee = $merchantModel->getFeeCommission($invoice->merchant_id, $amount, $invoice->api_type,MerchantCommission::PROPERTY_TYPE_COMMISSION ,$invoice->service_provider_id);
        $invoice->amount2pay = $amount - $fee['total'];
        $invoice->amount = $amount;
        $invoice->amount2service = $fee['service']['amount'];
        $invoice->amount2agent = $fee['agent']['amount'];
        $invoice->amount2grow = $fee['grow']['amount'];
        $invoice->commission_grow = $fee['grow']['commission'];
        $invoice->commission_agent = $fee['agent']['commission'];
        $invoice->commission_service = $fee['service']['commission'];
        $invoice->ps_amount = $ps_amount;
        $invoice->comments = $request->input('comment');
        $invoice->save();

        return response()->json([
            'status' => true,
            'message' => __('Amount has been changed')
        ]);
    }

}
