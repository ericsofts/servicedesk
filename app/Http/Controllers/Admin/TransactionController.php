<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TransactionsExport;
use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Merchant;
use App\Models\Partner;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Repositories\TransactionRepository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Exception;

class TransactionController extends Controller
{
    private TransactionRepository $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function allInvoices(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $merchant_selected = $request->input('merchants', []);
        $agent_selected = $request->input('agents', []);
        $partner_selected = $request->input('partners', null);
        $invoice_types = $request->input('invoice_types', []);
        $withdrawal_types = $request->input('withdrawal_types', []);
        $provider_selected = $request->input('provider', []);
        $fiats = $request->input('fiats', []);

//        $fiatsList = json_decode(strtolower(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME,'0','available_currencies','[]')));
        $fiatsList = ServiceProvidersProperty::getAllAvalableCurrencies();
        $merchant_filtered = [];
        $isMerchantFiltered = false;
        if($partner_selected){
            if($partner_selected != 'none'){
                $merchantIds = Merchant::with(['agent', 'agent.partner'])
                    ->whereHas('agent', function (Builder $query) use ($partner_selected) {
                        $query->whereHas('partner', function (Builder $query) use ($partner_selected) {
                            $query->where('id', '=', $partner_selected);
                        });
                    });
            }else{
                $merchantIds = Merchant::with(['agent', 'agent.partner'])
                    ->doesntHave('agent')
                    ->orDoesntHave('agent.partner');
            }
            $resultIds = $merchantIds->get()
                ->pluck('id')
                ->toArray();
            $merchant_filtered = array_merge($merchant_filtered, $resultIds);
            $isMerchantFiltered = true;

        }

        if ($agent_selected) {
            $merchantIds = Merchant::whereIn('agent_id', $agent_selected)->get()->pluck('id')->toArray();
            if($isMerchantFiltered){
                $merchant_filtered = array_intersect($merchant_filtered, $merchantIds);
            }else{
                $merchant_filtered = array_merge($merchant_filtered, $merchantIds);
                $isMerchantFiltered = true;
            }
        }

        if($merchant_selected && $isMerchantFiltered){
            $merchant_selected = array_intersect($merchant_selected, $merchant_filtered);
        }elseif(!$merchant_selected && $isMerchantFiltered){
            $merchant_selected = $merchant_filtered;
        }

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if($request->has('btnExport')){
            return (new TransactionsExport($this->transactionRepository, [
                'invoice_types' => $invoice_types,
                'withdrawal_types' => $withdrawal_types,
                'status' => $status,
                'fiats' => $fiats,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'merchant_selected' => $merchant_selected,
                'provider_selected' => $provider_selected,
                'isMerchantFiltered' => $isMerchantFiltered
            ]))->download('transactions.xlsx');
        }

        $paymentInvoiceQuery = $this->transactionRepository->transactions([
            'invoice_types' => $invoice_types,
            'withdrawal_types' => $withdrawal_types,
            'status' => $status,
            'fiats' => $fiats,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'merchant_selected' => $merchant_selected,
            'provider_selected' => $provider_selected,
            'isMerchantFiltered' => $isMerchantFiltered
        ]);

        if($paymentInvoiceQuery){
            $paymentInvoiceQuery->orderBy('created_at', 'desc')->with(['merchant', 'merchant.agent', 'merchant.agent.partner']);
            if(!$invoice_types || !in_array(2, $invoice_types)){
                $paymentInvoiceQuery->with('service_provider');
            }
            $invoices = $paymentInvoiceQuery->paginate($limit)->withQueryString();
        }else{
            $invoices = false;
        }

        $total = $this->transactionRepository->transactions_total([
            'invoice_types' => $invoice_types,
            'withdrawal_types' => $withdrawal_types,
            'fiats' => $fiats,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'merchant_selected' => $merchant_selected,
            'provider_selected' => $provider_selected,
            'isMerchantFiltered' => $isMerchantFiltered
        ]);
        $provider = ServiceProvider::all()->pluck('name', 'id');
        $merchants = Merchant::all()->pluck('name', 'id');
        $partners = Partner::all()->pluck('name', 'id');
        $agents = Agent::all()->pluck('name', 'id');
        return view('admin.transaction.all_invoices', [
            'merchants' => $merchants,
            'partners' => $partners,
            'provider' => $provider,
            'agents' => $agents,
            'status' => $status,
            'fiats' => $fiats,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'total' => $total,
            'fiatsList' => $fiatsList
        ]);
    }
}
