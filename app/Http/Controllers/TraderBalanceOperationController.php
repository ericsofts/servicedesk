<?php

namespace App\Http\Controllers;

use App\Models\TraderCurrency;
use App\Models\TraderBalance;
use App\Models\TraderBalanceOperation;
use App\Models\TraderBalanceQueue;
use App\Models\Trader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Exception;

class TraderBalanceOperationController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $type = $request->input('type');
        $q = $request->input('q');
        $traders_selected = $request->input('traders', []);

        $operations = TraderBalanceOperation::with('trader')->orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if ($from_date) {
            $operations->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $operations->where('created_at', '<=', $to_date);
        }
        if ($type != '') {
            $operations->where('type', '=', $type);
        }

        if ($traders_selected) {
            $operations->whereIn('trader_id', $traders_selected);
        }

        if (!empty($q)) {
            $operations->where('comment', 'like', "%$q%");
        }

        $operations = $operations->paginate($limit)->withQueryString();
        $traders = Trader::all()->pluck('name', 'id');

        return view('trader_balance_operation.index', [
            'operations' => $operations,
            'traders' => $traders,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'type' => $type,
        ]);
    }

    public function changeBalanceIndexDebit(Request $request, $id)
    {
        $currencies = TraderCurrency::all();

        return view('trader_balance_operation.change', [
            'id' => $id,
            'currencies' => $currencies,
            'invoice_type' => TraderBalanceQueue::TYPE_DEBIT,
            'title' => 'Debit',
        ]);
    }
    public function changeBalanceIndexCredit(Request $request, $id)
    {
        $currencies = TraderCurrency::all();

        return view('trader_balance_operation.change', [
            'id' => $id,
            'currencies' => $currencies,
            'invoice_type' => TraderBalanceQueue::TYPE_CREDIT,
            'title' => 'Credit',
        ]);
    }
    public function traderBalanceChange(Request $request, $id)
    {
        $messages = [];
        $rules = [
            'amount' => 'required|numeric',
            'type' => 'required|digits_between:0,1',
            'invoice_type' => 'required|digits_between:0,1',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $type = (int) $request->input('type');
        $currency_id = (int) $request->input('currency_id');
        $invoice_type = (int) $request->input('invoice_type');

        $currency = TraderCurrency::findOrFail($currency_id);

        $balance = TraderBalance::firstOrCreate(
            [
                'trader_id' => $id,
                'currency' => $currency->asset,
                'currency_id' => $currency->id,
                'type' => $type,
            ],
            [
                'amount' => 0
            ]
        );

        TraderBalanceOperation::create([
            'trader_id' => $id,
            'balance_id' => $balance->id,
            'amount' => (float) $request->input('amount'),
            'type' => $invoice_type,
            'sd_user_id' => Auth::user()->id,
            'comment' => $request->input('comment')
        ]);

        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Saved')]);
    }

    public function showBalance(Request $request)
    {
        $balance = TraderBalance::where([
            'trader_id' => $request->input('trader_id'),
            'currency_id' => $request->input('currency_id'),
            'type' => $request->input('type'),
        ])->first();
        $balans = $balance->amount ?? 0;
        return response()->json(['answer' => $balans]);
    }
}
