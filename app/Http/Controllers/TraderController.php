<?php

namespace App\Http\Controllers;


use App\Models\Agent;
use App\Models\Trader;
use App\Models\TraderCurrency;
use App\Models\ServiceProvider;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TraderController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');

        $query = Trader::orderBy('updated_at', 'desc');

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }

        $traders = $query->paginate($limit)->withQueryString();
        $currency = TraderCurrency::where('status', 1)->get();

        return view('trader.index', [
            'status' => $status,
            'traders' => $traders,
            'currency' => $currency,
        ]);
    }

    public function view(Request $request, $id)
    {
        $trader = Trader::where(['id' => $id])->firstOrFail();
        return view('trader.view', [
            'trader' => $trader
        ]);
    }

    public function new(Request $request)
    {
        $agents = Agent::pluck('name', 'id');
        $service_providers = ServiceProvider::where('status', ServiceProvider::STATUS_ACTIVE)
            ->whereIn('type', [Trader::OBJECT_NAME])
            ->get();
        
        return view('trader.new', [
            'service_providers' => $service_providers,
            'password' => Trader::generatePassword(),
            'agents' => $agents
        ]);
    }

    public function edit(Request $request, $id)
    {
        $trader = Trader::where(['id' => $id])->first();
        $agents = Agent::pluck('name', 'id');
        $service_providers = ServiceProvider::where('status', ServiceProvider::STATUS_ACTIVE)
            ->whereIn('type', [Trader::OBJECT_NAME])
            ->get();
        
        return view('trader.edit', [
            'id' => $trader->id,
            'name' => $trader->name,
            'email' => $trader->email,
            'status' => $trader->status,
            'agent_id' => $trader->agent_id,
            'service_provider_id' => $trader->service_provider_id,
            'telegram' => $trader->telegram,
            'agents' => $agents,
            'service_providers' => $service_providers,
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:traders,name',
            'email' => 'required|string|email|unique:traders,email',
            'password' => 'nullable|string',
            'status' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader = $this->createTrader($request);
        if($trader){
            return redirect()->route('trader.index')->with(['alert-type' => 'success', 'message' => __('The Trader Created')]);
        }

        return redirect()->route('trader.index')->with(['alert-type' => 'error', 'message' => __('The Trader Create Error')]);
    }

    public function update(Request $request, $id){
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:traders,name,'.$id,
            'email' => 'required|string|email|unique:traders,email,'.$id,
            'password' => 'nullable|string',
            'status' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader = Trader::where(['id' => $id])->first();

        if($trader){
            $this->updateTrader($request, $trader);
            return redirect()->route('trader.index')->with(['alert-type' => 'success', 'message' => __('The Trader Updated')]);
        }

        return redirect()->route('trader.index')->with(['alert-type' => 'error', 'message' => __('The Trader Update Error')]);
    }

    protected function createTrader($request){
        $trader = Trader::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'agent_id' => $request->input('agent_id'),
            'service_provider_id' => $request->input('service_provider_id'),
            'password' => Hash::make($request->input('password')),
            'status' => $request->input('status') ? $request->input('status') : 0,
            'email_verified_at' => now()
        ]);
        return $trader;
    }

    protected function updateTrader($request, $trader){
        $trader->name = $request->input('name');
        $trader->email = $request->input('email');
        $trader->status = $request->input('status');
        $trader->agent_id = $request->input('agent_id');
        $trader->service_provider_id = $request->input('service_provider_id');
        $trader->telegram = $request->input('telegram');
        if($request->input('password')){
            $trader->password = Hash::make($request->input('password'));
        }
        $trader->save();
    }

}
