<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\InputInvoice;
use App\Models\Property;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use NumberFormatter;

class InputInvoiceController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');

        $invoicesQuery = InputInvoice::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }
        if (!is_null($status) && in_array($status, [
                InputInvoice::STATUS_PAYED,
                InputInvoice::STATUS_CREATED,
                InputInvoice::STATUS_USER_CONFIRMED,
                InputInvoice::STATUS_CANCELED,
                InputInvoice::STATUS_USER_SELECTED,
                InputInvoice::STATUS_TRADER_CONFIRMED
            ])) {
            $invoicesQuery->where('status', '=', $status);
        }
        $invoicesQuery->with(['user']);
        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('input_invoice.index', [
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'fmt' => $fmt
        ]);
    }

    public function view(Request $request, $id)
    {
        $invoice = InputInvoice::where(['id' => $id])->with(['user'])->firstOrFail();
        if ($invoice->payment_system == InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC && !empty($invoice['payment_id'])) {
            $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
            $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
            $contact_res = $chatexLBC->contactInfo($invoice['payment_id']);
            $messages = $chatexLBC->contactMessages($invoice['payment_id']);
        }
        return view('input_invoice.view', [
            'invoice' => $invoice,
            'contact_res' => $contact_res ?? null,
            'messages' => $messages ?? null,
            'addition_info' => json_decode($invoice['addition_info'], true) ?? null
        ]);
    }
}
