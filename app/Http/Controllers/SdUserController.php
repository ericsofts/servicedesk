<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SdUserController extends Controller
{
    public function profile()
    {
        return view('sd_user.profile');
    }

    public function profile_update(Request $request)
    {
        $postData = $request->only([
            'name'
        ]);
        $request->validate([
            'name' => 'required|max:255'
        ]);
        $request->user()->update($postData);
        return redirect()->route('sd_user.profile')->with(['alert-type' => 'success', 'message' => __('Profile has been successfully saved!')]);
    }
}
