<?php

namespace App\Http\Controllers;


use App\Models\ServiceProvider;
use App\Models\ServiceProvidersBalance;
use App\Models\ServiceProvidersWithdrawalInvoice;
use App\Models\SystemBalance;
use App\Models\SystemBalanceWithdrawalInvoice;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ServiceWithdrawalInvoiceController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $service_provider_selected = $request->input('service_providers', []);

        $invoicesQuery = ServiceProvidersWithdrawalInvoice::with(['service_provider', 'sd_user'])
            ->orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }
        
        if (!is_null($status) && in_array($status, [
                SystemBalanceWithdrawalInvoice::STATUS_PAYED,
                SystemBalanceWithdrawalInvoice::STATUS_CREATED,
                SystemBalanceWithdrawalInvoice::STATUS_CANCELED])
        ) {
            $invoicesQuery->where('status', '=', $status);
        }

        if ($service_provider_selected) {
            $invoicesQuery->whereIn('service_provider_id', $service_provider_selected);
        }

        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $totalQuery = ServiceProvidersWithdrawalInvoice::where([
            'status' => ServiceProvidersWithdrawalInvoice::STATUS_PAYED
        ])
            ->select(
                DB::raw('SUM(amount) as total_amount'),
            );

        if ($from_date) {
            $totalQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $totalQuery->where('created_at', '<=', $to_date);
        }

        if ($service_provider_selected) {
            $totalQuery->whereIn('service_provider_id', $service_provider_selected);
        }
        $total = $totalQuery->first();

        $service_providers = ServiceProvidersBalance::all()->pluck('name', 'id');

        return view('service_withdrawal_invoice.index', [
            'service_providers' => $service_providers,
            'status' => $status,
            'from_date' => $from_date ?? null,
            'to_date' => $to_date ?? null,
            'invoices' => $invoices,
            'total' => $total,
        ]);
    }

    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $service_providers = ServiceProvider::all();

        return view('service_withdrawal_invoice.create', [
            'service_providers' => $service_providers
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'service_provider_id' => [
                'required',
                'numeric',
                Rule::exists('service_providers', 'id')
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'fee' => ['nullable', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'comment' => ['required'],
            'confirm' => ['required', 'accepted']
        ];

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $amount = $request->input('amount');
        $fee = $request->input('fee', 0);
        $service_provider_id = $request->input('service_provider_id');

        $invoice = ServiceProvidersWithdrawalInvoice::create([
            'service_provider_id' => $service_provider_id,
            'amount' => $amount,
            'comment' => $request->input('comment'),
            'payment_system' => ServiceProvidersWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
            'payed' => now(),
            'status' => ServiceProvidersWithdrawalInvoice::STATUS_PAYED,
            'sd_user_id' => $request->user()->id
        ]);

        if ($fee) {
            ServiceProvidersWithdrawalInvoice::create([
                'service_provider_id' => $service_provider_id,
                'amount' => $fee,
                'comment' => 'Invoice fee #' . $invoice->id . '(' . $invoice->amount . ' / ' . $fee . ')',
                'payment_system' => ServiceProvidersWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
                'payed' => now(),
                'status' => ServiceProvidersWithdrawalInvoice::STATUS_PAYED,
                'sd_user_id' => $request->user()->id
            ]);
        }
        return redirect()->route('service.provider.withdrawal_invoice.create')->with('status',
            __('The payment is complete')
        );
    }
}
