<?php

namespace App\Http\Controllers;

use App\Models\TraderFiat;
use App\Models\TraderPaymentSystem;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TraderFiatController extends Controller
{

    public function index(Request $request)
    {
        $trader_fiats = TraderFiat::with('payment_systems')->orderBy('updated_at', 'desc')->get();

        return view('trader_fiat.index', [
            'trader_fiats' => $trader_fiats
        ]);
    }


    public function view(Request $request, $id)
    {
        $trader_fiat = TraderFiat::where(['id' => $id])->with('payment_systems')->firstOrFail();
        $all_payment_systems = TraderPaymentSystem::all();

        return view('trader_fiat.view', [
            'trader_fiat' => $trader_fiat,
            'all_payment_systems' => $all_payment_systems,
        ]);
    }

    public function new(Request $request)
    {
        $all_payment_systems = TraderPaymentSystem::all();

        return view('trader_fiat.new', [
            'all_payment_systems' => $all_payment_systems
        ]);
    }

    public function edit(Request $request, $id)
    {
        $trader_fiat = TraderFiat::where(['id' => $id])->with('payment_systems')->firstOrFail();
        $all_payment_systems = TraderPaymentSystem::all();

        $selected_payment_systems = [];
        if (!empty($trader_fiat) && !empty($trader_fiat->payment_systems)) {
            foreach ($trader_fiat->payment_systems as $unit) {
                $selected_payment_systems[] = $unit->id;
            }
        }

        return view('trader_fiat.edit', [
            'id' => $trader_fiat->id,
            'asset' => $trader_fiat->asset,
            'name' => $trader_fiat->name,
            'precision' => $trader_fiat->precision,
            'status' => $trader_fiat->status,
            'payment_systems' => $trader_fiat->payment_systems,
            'selected_payment_systems' => $selected_payment_systems,
            'all_payment_systems' => $all_payment_systems,
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'asset' => 'required|string|unique:trader_fiats,asset',
            'name' => 'required|string|unique:trader_fiats,name',
            'precision' => 'required|numeric',
            'status' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_fiat = $this->createTraderFiat($request);
        if($trader_fiat){
            return redirect()->route('trader_fiat.index')->with(['alert-type' => 'success', 'message' => __('The Trader fiat Created')]);
        }

        return redirect()->route('trader.index')->with(['alert-type' => 'error', 'message' => __('The Trader fiat Create Error')]);
    }

    public function update(Request $request, $id){
        $messages = [];

        $rules = [
            'asset' => 'required|string|unique:trader_fiats,asset,' . $id,
            'name' => 'required|string|unique:trader_fiats,name,' . $id,
            'precision' => 'required|numeric',
            'status' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_fiat = TraderFiat::where(['id' => $id])->first();

        if($trader_fiat){
            $this->updateTraderFiat($request, $trader_fiat);
            return redirect()->route('trader_fiat.index')->with(['alert-type' => 'success', 'message' => __('The Trader fiat Updated')]);
        }

        return redirect()->route('trader_fiat.index')->with(['alert-type' => 'error', 'message' => __('The Trader fiat Update Error')]);
    }

    protected function createTraderFiat($request)
    {
        $trader_fiat = TraderFiat::create([
            'asset' => $request->input('asset'),
            'name' => $request->input('name'),
            'precision' => $request->input('precision'),
            'status' => $request->input('status') ? $request->input('status') : 0
        ]);

        $trader_fiat->payment_systems()->sync($request->payment_systems);

        return $trader_fiat;
    }

    protected function updateTraderFiat($request, $trader_fiat)
    {
        $trader_fiat->asset = $request->input('asset');
        $trader_fiat->name = $request->input('name');
        $trader_fiat->precision = $request->input('precision');
        $trader_fiat->status = $request->input('status');
        $trader_fiat->save();

        $trader_fiat->payment_systems()->sync($request->payment_systems);
    }

}
