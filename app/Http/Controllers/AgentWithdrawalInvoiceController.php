<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\AgentWithdrawalInvoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AgentWithdrawalInvoiceController extends Controller
{
    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $agents = Agent::all();

        return view('agent_withdrawal_invoice.create', [
            'agents' => $agents
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'agent_id' => [
                'required',
                'numeric',
                Rule::exists('agents', 'id')
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'fee' => ['nullable', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'comment' => ['required'],
            'confirm' => ['required', 'accepted']
        ];

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $amount = $request->input('amount');
        $fee = $request->input('fee', 0);
        $agent_id = $request->input('agent_id');

        $invoice = AgentWithdrawalInvoice::create([
            'agent_id' => $agent_id,
            'amount' => $amount,
            'comment' => $request->input('comment'),
            'payment_system' => AgentWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
            'payed' => now(),
            'status' => AgentWithdrawalInvoice::STATUS_PAYED,
            'sd_user_id' => $request->user()->id
        ]);

        if ($fee) {
            AgentWithdrawalInvoice::create([
                'agent_id' => $agent_id,
                'amount' => $fee,
                'comment' => 'Invoice fee #' . $invoice->id . '(' . $invoice->amount . ' / ' . $fee . ')',
                'payment_system' => AgentWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
                'payed' => now(),
                'status' => AgentWithdrawalInvoice::STATUS_PAYED,
                'sd_user_id' => $request->user()->id
            ]);
        }
        return redirect()->route('agent.withdrawal_invoice.create')->with('status',
            __('The payment is complete')
        );
    }
}
