<?php

namespace App\Http\Controllers\Chatex;

use App\Http\Controllers\Controller;
use App\Lib\Chatex\ChatexApi;
use App\Models\Agent;
use App\Models\Partner;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');
        $query = Agent::orderBy('id', 'desc')
            ->where(['partner_id' => $partner->id]);

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }
        $query->with(['balance']);

        $agents = $query->paginate($limit);

        return view('admin_chatex.agent.index', [
            'status' => $status,
            'agents' => $agents
        ]);
    }

}
