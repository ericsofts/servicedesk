<?php

namespace App\Http\Controllers\Chatex;

use App\Http\Controllers\Controller;
use App\Lib\Chatex\ChatexApi;
use App\Models\Agent;
use App\Models\Merchant;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\Partner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MerchantController extends Controller
{
    public function index(Request $request)
    {
        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $agents = new Collection();
        if ($partner) {
            $agents = Agent::where(['partner_id' => $partner->id])->pluck('name', 'id');
        }
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');
        $agent_selected = $request->input('agents', []);

        $query = Merchant::whereHas('agent', function (Builder $query) use($partner){
            $query->where('partner_id', $partner->id);
        })
            ->orderBy('id', 'desc');

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }

        foreach ($agent_selected as $k => $v) {
            if (!$agents->get($v)) {
                unset($agent_selected[$k]);
            }
        }

        if(!empty($agent_selected)){
            $query->whereIn('agent_id', $agent_selected);
        }
        $query->with(['balance', 'agent']);

        $open_withdrawals = MerchantWithdrawalInvoice::whereNotIn('status', [MerchantWithdrawalInvoice::STATUS_PAYED, MerchantWithdrawalInvoice::STATUS_CANCELED])
            ->select([DB::raw("SUM(amount) as total_amount"), 'merchant_id'])
            ->groupBy('merchant_id')
            ->get()
            ->pluck('total_amount', 'merchant_id');

        $merchants = $query->paginate($limit);

        $agents = Agent::where(['partner_id' => $partner->id])->pluck('name', 'id');

        return view('admin_chatex.merchant.index', [
            'status' => $status,
            'merchants' => $merchants,
            'agents' => $agents,
            'open_withdrawals' => $open_withdrawals
        ]);
    }

    public function view(Request $request, $id)
    {
        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $merchant = Merchant::whereHas('agent', function (Builder $query) use($partner){
            $query->where('partner_id', $partner->id);
        })
        ->where(['id' => $id])->firstOrFail();
        $agent = $merchant->agent()->first();
        $api_type = $request->input('api_type', []);
        $commissionsQuery = $merchant->commissions()
            ->orderBy('api_type', 'desc')
            ->orderBy('property_type');
        if($api_type){
            $commissionsQuery->whereIn('api_type', $api_type);
        }
        $commissions = $commissionsQuery->get();
        return view('admin_chatex.merchant.view', [
            'merchant' => $merchant,
            'agent' => $agent,
            'commissions' => $commissions
        ]);
    }

}
