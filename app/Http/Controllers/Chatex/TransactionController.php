<?php

namespace App\Http\Controllers\Chatex;

use App\Exports\TransactionsChatexExport;
use App\Http\Controllers\Controller;
use App\Lib\Chatex\ChatexApi;
use App\Models\Agent;
use App\Models\Merchant;
use App\Models\Partner;
use App\Models\ServiceProvider;
use App\Repositories\TransactionRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Exception;

class TransactionController extends Controller
{
    private TransactionRepository $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function allInvoices(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $merchant_selected = $request->input('merchants', []);
        $agent_selected = $request->input('agents', []);

        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $merchants = new Collection();
        $agents = new Collection();
        $chatexProvider = ServiceProvider::where('type', 'chatex')->first();

        if ($partner) {
            $agents = Agent::where(['partner_id' => $partner->id])->pluck('name', 'id');
            $merchants = Merchant::whereHas('agent', function($query) use($partner){
                $query->where('partner_id', $partner->id);
            })->get()->pluck('name', 'id');
        }
        $merchants->put(0, __('Other'));

        foreach ($agent_selected as $k => $v) {
            if (!$agents->get($v)) {
                unset($agent_selected[$k]);
            }
        }

        foreach ($merchant_selected as $k => $v) {
            if (!$merchants->get($v)) {
                unset($merchant_selected[$k]);
            }
        }

        if ($agent_selected) {
            $merchantIds = Merchant::whereIn('agent_id', $agent_selected)->get()->pluck('id')->toArray();
            if (!empty($merchantIds)) {
                $merchant_selected = array_merge($merchant_selected, $merchantIds);
            }
        }

        if (in_array(0, $merchant_selected)){
            $merchantsOther = Merchant::whereNotIn('agent_id', $agents->keys())
                ->orWhereNull('agent_id')
                ->pluck('id')->toArray();
            if (($key = array_search(0, $merchant_selected)) !== false) {
                unset($merchant_selected[$key]);
            }
            if($merchantsOther){
                $merchant_selected = array_merge($merchant_selected, $merchantsOther);
            }
        }

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if($request->has('btnExport')){
            return (new TransactionsChatexExport($this->transactionRepository, [
                'status' => $status,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'merchant_selected' => $merchant_selected,
                'service_provider_id' => $chatexProvider->id
            ]))->download('transactions.xlsx');
        }

        $paymentInvoiceQuery = $this->transactionRepository->chatex_transactions([
            'status' => $status,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'merchant_selected' => $merchant_selected,
            'service_provider_id' => $chatexProvider->id
        ]);

        $total = $this->transactionRepository->chatex_transactions_total([
            'from_date' => $from_date,
            'to_date' => $to_date,
            'merchant_selected' => $merchant_selected,
            'service_provider_id' => $chatexProvider->id
        ]);

        $paymentInvoiceQuery->orderBy('created_at', 'desc')->with(['merchant', 'merchant.agent']);

        $invoices = $paymentInvoiceQuery->paginate($limit)->withQueryString();

        return view('admin_chatex.transaction.all_invoices', [
            'partner' => $partner,
            'merchants' => $merchants,
            'agents' => $agents,
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'total' => $total
        ]);
    }

}
