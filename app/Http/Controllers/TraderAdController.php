<?php

namespace App\Http\Controllers;

use App\Models\Trader;
use App\Models\TraderAd;
use App\Models\TraderCurrency;
use App\Models\TraderFiat;
use App\Models\TraderPaymentSystem;
use App\Models\TraderRateSource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class TraderAdController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date', null);
        $to_date = $request->input('to_date', null);
        $currencyId = $request->input('currency_id', null);
        $fiatId = $request->input('fiat_id', null);
        $status = $request->input('status', null);
        $paymentSystemId = $request->input('payment_system_id', null);
        $traderId = $request->input('trader_id', null);
        $type = $request->input('type', null);

        $messages = [];
        $rules = [
            'currency_id' => 'nullable|numeric',
            'fiat_id' => 'nullable|numeric',
            'status' => 'nullable|numeric',
            'payment_system_id' => 'nullable|numeric',
            'trader_id' => 'nullable|numeric',
            'type' => 'nullable|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $adsListQuery = TraderAd::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $adsListQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $adsListQuery->where('created_at', '<=', $to_date);
        }

        if (!is_null($status)) {
            $adsListQuery->where('status', '=', $status);
        }

        if (!is_null($currencyId)) {
            $adsListQuery->where('currency_id', '=', $currencyId);
        }

        if (!is_null($fiatId)) {
            $adsListQuery->where('fiat_id', '=', $fiatId);
        }

        if (!is_null($paymentSystemId)) {
            $adsListQuery->where('payment_system_id', '=', $paymentSystemId);
        }

        if (!is_null($traderId)) {
            $adsListQuery->where('trader_id', '=', $traderId);
        }

        if (!is_null($type)) {
            $adsListQuery->where('type', '=', $type);
        }

        $currencies = TraderCurrency::all()->pluck('name', 'id');
        $fiats = TraderFiat::all()->pluck('name', 'id');
        $paymentSystems = TraderPaymentSystem::all()->pluck('name', 'id');
        $traders = Trader::all()->pluck('name', 'id');
        $types = TraderAd::getTypes();
        $statuses = TraderAd::getStatuses();
        $rates = TraderAd::getRates();
        $ratesSource = TraderRateSource::all()->pluck('name', 'id')->toArray();

        $adsList = $adsListQuery->paginate($limit)->withQueryString();
        return view('trader.ads', [
            'adsList' => $adsList,
            'fiats' => $fiats,
            'paymentSystems' => $paymentSystems,
            'traders' => $traders,
            'types' => $types,
            'statuses' => $statuses,
            'currencies' => $currencies,
            'rates' => $rates,
            'ratesSource' => $ratesSource,

            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'currency' => $currencyId,
            'fiat' => $fiatId,
            'status' => $status,
            'paymentSystem' => $paymentSystemId,
            'trader' => $traderId,
            'type' => $type,

        ]);
    }

}
