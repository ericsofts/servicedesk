<?php

namespace App\Http\Controllers;

use App\Models\TraderPaymentSystem;
use App\Models\TraderFiat;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TraderPaymentSystemController extends Controller
{

    public function index(Request $request)
    {
        $trader_ps = TraderPaymentSystem::with('fiats')->orderBy('updated_at', 'desc')->get();

        return view('trader_payment_system.index', [
            'trader_ps' => $trader_ps
        ]);
    }

    public function view(Request $request, $id)
    {
        $trader_payment_system = TraderPaymentSystem::where(['id' => $id])->with('fiats')->firstOrFail();

        return view('trader_payment_system.view', [
            'trader_payment_system' => $trader_payment_system
        ]);
    }

    public function new(Request $request)
    {
        return view('trader_payment_system.new', [
        ]);
    }

    public function edit(Request $request, $id)
    {
        $trader_payment_system = TraderPaymentSystem::where(['id' => $id])->firstOrFail();
        return view('trader_payment_system.edit', [
            'id' => $trader_payment_system->id,
            'name' => $trader_payment_system->name,
            'status' => $trader_payment_system->status,
            'color' => $trader_payment_system->color,
            'bg_color' => $trader_payment_system->bg_color,
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:traders,name',
            'status' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_payment_system = $this->createTraderPaymentSystem($request);
        if($trader_payment_system){
            return redirect()->route('trader_payment_system.index')->with(['alert-type' => 'success', 'message' => __('The Trader fiat Created')]);
        }

        return redirect()->route('trader.index')->with(['alert-type' => 'error', 'message' => __('The Trader fiat Create Error')]);
    }

    public function update(Request $request, $id){
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:traders,name,'.$id,
            'status' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_payment_system = TraderPaymentSystem::where(['id' => $id])->first();

        if($trader_payment_system){
            $this->updateTraderPaymentSystem($request, $trader_payment_system);
            return redirect()->route('trader_payment_system.index')->with(['alert-type' => 'success', 'message' => __('The Trader fiat Updated')]);
        }

        return redirect()->route('trader_payment_system.index')->with(['alert-type' => 'error', 'message' => __('The Trader fiat Update Error')]);
    }

    protected function createTraderPaymentSystem($request)
    {
        $trader_payment_system = TraderPaymentSystem::create([
            'name' => $request->input('name'),
            'status' => $request->input('status'),
            'color' => $request->input('color'),
            'bg_color' => $request->input('bg_color'),
        ]);
        return $trader_payment_system;
    }

    protected function updateTraderPaymentSystem($request, $trader_payment_system)
    {
        $trader_payment_system->name = $request->input('name');
        $trader_payment_system->status = $request->input('status');
        $trader_payment_system->color = $request->input('color');
        $trader_payment_system->bg_color = $request->input('bg_color');
        $trader_payment_system->save();
    }

}
