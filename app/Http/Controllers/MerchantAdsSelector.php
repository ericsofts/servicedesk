<?php

namespace App\Http\Controllers;

use App\Models\MerchantServiceProvider;
use App\Models\ServiceProvider;
use App\Models\WhiteLabel;
use Illuminate\Http\Request;
use App\Services\AdsSelector;
use App\Models\Merchant;
use App\Models\Trader;
use App\Models\MerchantProperty;
use App\Models\Property;
use Illuminate\Support\Facades\Validator;


class MerchantAdsSelector extends Controller
{

    public function index(Request $request)
    {
        $merchants = Merchant::where([
            'status' => 1
        ])->get();
        $ads = [];
        $merch = [];
        $currancies = json_decode(Property::getProperty('currency', 0, 'available_currency'));

        if($request->input('merchant')){
            $rules = [
                'amount' => 'required|numeric',
            ];
            $validator = Validator::make($request->input(), $rules);
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $merchant = Merchant::find((int) $request->input('merchant'));
            if($merchant){
                $ads = (new AdsSelector($merchant))->getAdsByMerchant($merchant, [
                        'currency' => $request->input('currency') ?? 'USD',
                        'amount' => $request->input('amount'),
                        'type' => (int) $request->input('type'),
                        'api' => (int) $request->input('api_type'),
                        'expense' => (int) $request->input('expense'),
                        'exact_currency' => (int) $request->input('exact_currency'),
                        'currency2currency' => MerchantProperty::getProperty($merchant->id, 'currency2currency')
                    ]) ?? [];
                $merch = $merchant->toArray();
                $merchant_property = MerchantProperty::getProperties($merchant->id);
                $merch['c2c'] = $merchant_property['currency2currency'] ?? 0;
                $merch['ex_curr'] = implode(',', json_decode($merchant_property['excluded_currencies'] ?? '[]', true));
                $available_providers = MerchantServiceProvider::with('service_provider')->where(['merchant_id' => $merchant->id])->orderBy('priority', 'asc')->get();
                $available_providers_ids = [];
                foreach ($available_providers as $aunit) {
                    $available_providers_ids[] = $aunit->service_provider_id;
                }
                $service_providers = ServiceProvider::whereIn('id', $available_providers_ids)->pluck('name')->toArray();
                $merch['service_providers'] = implode(', ', $service_providers);
            }
        }

        return view('available_ads.index',[
            'merchants' => $merchants,
            'currancies' => $currancies,
            'ads' => $ads,
            'merch' => $merch
        ]);
    }
    public function get_merchant_props(Request $request)
    {
        $merchant = Merchant::find((int) $request->input('id'));
        $result = $merchant->toArray();
        $result['c2c'] = MerchantProperty::getProperty($merchant->id, 'currency2currency');
        $result['ex_curr'] = json_decode(MerchantProperty::getProperty($merchant->id, 'excluded_currencies'), JSON_FORCE_OBJECT);
        $available_providers = MerchantServiceProvider::with('service_provider')->where(['merchant_id' => $merchant->id])->orderBy('priority', 'asc')->get();
        $available_providers_ids = [];
        foreach ($available_providers as $aunit) {
            $available_providers_ids[] = $aunit->service_provider_id;
        }
        $service_providers = ServiceProvider::whereIn('id', $available_providers_ids)->pluck('name')->toArray();
        $result['service_providers'] = implode(', ', $service_providers);
        return $result;
    }

}
