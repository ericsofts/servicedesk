<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\AgentWithdrawalInvoice;
use App\Models\SystemBalance;
use App\Models\SystemBalanceWithdrawalInvoice;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SystemWithdrawalInvoiceController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $system_balances_selected = $request->input('system_balances', []);

        $invoicesQuery = SystemBalanceWithdrawalInvoice::with(['system_balance', 'sd_user'])
            ->orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        
        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }

        if (!is_null($status) && in_array($status, [
                SystemBalanceWithdrawalInvoice::STATUS_PAYED,
                SystemBalanceWithdrawalInvoice::STATUS_CREATED,
                SystemBalanceWithdrawalInvoice::STATUS_CANCELED])
        ) {
            $invoicesQuery->where('status', '=', $status);
        }

        if ($system_balances_selected) {
            $invoicesQuery->whereIn('system_balance_id', $system_balances_selected);
        }

        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $totalQuery = SystemBalanceWithdrawalInvoice::where([
            'status' => SystemBalanceWithdrawalInvoice::STATUS_PAYED
        ])
            ->select(
                DB::raw('SUM(amount) as total_amount'),
            );

        if ($from_date) {
            $totalQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $totalQuery->where('created_at', '<=', $to_date);
        }

        if ($system_balances_selected) {
            $totalQuery->whereIn('system_balance_id', $system_balances_selected);
        }
        $total = $totalQuery->first();

        $system_balances = SystemBalance::all()->pluck('name', 'id');

        return view('system_withdrawal_invoice.index', [
            'system_balances' => $system_balances,
            'status' => $status,
            'from_date' => $from_date ?? null,
            'to_date' => $to_date ?? null,
            'invoices' => $invoices,
            'total' => $total,
        ]);
    }

    /**
     * Display the create view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Request $request)
    {
        $system_balances = SystemBalance::all();

        return view('system_withdrawal_invoice.create', [
            'system_balances' => $system_balances
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'system_balance_id' => [
                'required',
                'numeric',
                Rule::exists('system_balances', 'id')
            ],
            'amount' => ['required', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'fee' => ['nullable', 'numeric', 'gt:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'comment' => ['required'],
            'confirm' => ['required', 'accepted']
        ];

        $validator = Validator::make($request->input(), $rules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $amount = $request->input('amount');
        $fee = $request->input('fee', 0);
        $system_balance_id = $request->input('system_balance_id');

        $invoice = SystemBalanceWithdrawalInvoice::create([
            'system_balance_id' => $system_balance_id,
            'amount' => $amount,
            'comment' => $request->input('comment'),
            'payment_system' => SystemBalanceWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
            'payed' => now(),
            'status' => SystemBalanceWithdrawalInvoice::STATUS_PAYED,
            'sd_user_id' => $request->user()->id
        ]);

        if ($fee) {
            SystemBalanceWithdrawalInvoice::create([
                'system_balance_id' => $system_balance_id,
                'amount' => $fee,
                'comment' => 'Invoice fee #' . $invoice->id . '(' . $invoice->amount . ' / ' . $fee . ')',
                'payment_system' => SystemBalanceWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY,
                'payed' => now(),
                'status' => SystemBalanceWithdrawalInvoice::STATUS_PAYED,
                'sd_user_id' => $request->user()->id
            ]);
        }
        return redirect()->route('system.balance.withdrawal_invoice.create')->with('status',
            __('The payment is complete')
        );
    }
}
