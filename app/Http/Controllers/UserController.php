<?php

namespace App\Http\Controllers;

use App\Models\BalanceQueue;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use NumberFormatter;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');

        $query = User::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $query->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $query->where('created_at', '<=', $to_date);
        }
        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }

        $users = $query->paginate($limit)->withQueryString();

        return view('user.index', [
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'users' => $users
        ]);
    }

    public function view(Request $request, $id)
    {
        $user = User::where(['id' => $id])->firstOrFail();

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        $balance = $user->balance->amount ?? 0;
        $balanceDebitQueue = BalanceQueue::where([
            'user_id' => $request->user()->id,
            'type' => BalanceQueue::TYPE_DEBIT,
        ])
            ->where('status', '!=', BalanceQueue::STATUS_DONE)
            ->sum('amount');

        $total = round($balance + $balanceDebitQueue, 6);

        return view('user.view', [
            'user' => $user,
            'balanceTotal' => $total
        ]);
    }
}
