<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');
        $query = Agent::orderBy('id', 'desc');

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }
        $query->with(['balance']);

        $agents = $query->paginate($limit)->withQueryString();

        return view('agent.index', [
            'status' => $status,
            'agents' => $agents
        ]);
    }

    public function view(Request $request, $id)
    {
        $agent = Agent::where(['id' => $id])->firstOrFail();
        return view('agent.view', ['agent' => $agent]);
    }

    public function new(Request $request)
    {
        $password = Agent::generatePassword();
        $partner = Partner::pluck('name', 'id');
        return view('agent.new', [
            'password' => $password,
            'partner' => $partner
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:agents,name',
            'email' => 'required|string|email|unique:agents,email',
            'password' => 'nullable|string',
            'partner_id' => 'nullable|string',
            'status' => 'nullable|string',
            'enable_2fa'  => 'nullable|string'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        Agent::create([
                'name'=> $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'partner_id' => $request->input('partner_id') ? $request->input('status') : NULL,
                'status' => $request->input('status') ? $request->input('status') : 0,
                'enable_2fa'  => $request->input('enable_2fa') ? true : false
            ]);

        return redirect()->route('agent.index')->with(['alert-type' => 'success', 'message' => __('The Agent Created')]);
    }

    public function edit(Request $request, $id)
    {
        $agent = Agent::where(['id' => $id])->first();
        $partner = Partner::pluck('name', 'id');
        return view('agent.edit', [
            'id' => $agent->id,
            'partner' => $partner,
            'name' => $agent->name,
            'email' => $agent->email,
            'status' => $agent->status,
            'partner_id' => $agent->partner_id,
            'enable_2fa' => $agent->enable_2fa
        ]);
    }

    public function update(Request $request, $id)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:agents,name,'.$id,
            'email' => 'required|string|email|unique:agents,email,'.$id,
            'password' => 'nullable|string',
            'partner_id' => 'nullable|string',
            'status' => 'string',
            'enable_2fa'  => 'string'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $agent = Agent::where(['id' => $id])->first();
        $postData = $request->input();
        $updateData = $this->preparePostData($postData);
        $agent->update($updateData);

        return redirect()->route('agent.index')->with(['alert-type' => 'success', 'message' => __('The Agent Save')]);
    }

    protected function preparePostData($postData){
        if($postData['password']){
            $postData['password'] = Hash::make($postData['password']);
        }else{
            unset($postData['password']);
        }
        return $postData;
    }
}
