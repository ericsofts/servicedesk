<?php

namespace App\Http\Controllers;


use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status', null);

        $messages = [];

        $rules = [
            'status' => 'nullable|string|in:0,1',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $query = ServiceProvider::orderBy('updated_at', 'desc')->with(['balance']);

        if (!is_null($status)) {
            $query->where('status', '=', $status);
        }

        $services = $query->paginate($limit)->withQueryString();

        return view('service.index', [
            'status' => $status,
            'statuses' => ServiceProvider::getStatuses(),
            'services' => $services
        ]);
    }

    public function view(Request $request, $id)
    {
        $service = ServiceProvider::where(['id' => $id])->with(['balance'])->firstOrFail();
        $property = ServiceProvidersProperty::getProperties($service->type, $service->id);
        return view('service.view', [
            'service' => $service,
            'statuses' => ServiceProvider::getStatuses(),
            'property' => $property
        ]);
    }

    public function new(Request $request)
    {
        return view('service.new',[
            'types' => ServiceProvider::allowCreateTypes(),
            'statuses' => ServiceProvider::getStatuses(),
        ]);
    }

    public function edit(Request $request, $id)
    {
        $service = ServiceProvider::where(['id' => $id])->first();
        $property = ServiceProvidersProperty::getProperties($service->type, $service->id);
        
        $types = ServiceProvider::allowCreateTypes();
        array_unshift($types, $service->type);
        $types = array_unique(array_filter($types));

        return view('service.edit', [
            'types' => $types,
            'service' => $service,
            'statuses' => ServiceProvider::getStatuses(),
            'property' => $property
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:service_providers,name',
            'status' => 'required|string|in:0,1',
            'type' => 'nullable|string',
            'property.priority_traders' => 'nullable|string',
            'property.blocked_traders' => 'nullable|string',
            'property.available_currencies' => 'nullable|string',
            'property.api_token' => 'nullable|string',
            'property.withdrawal_priority_traders' => 'nullable|string',
            'property.blocked_banks' => 'nullable|string',
            'property.currencies_without_card_number' => 'nullable|string',
            'property.percentage_currency_rate' => 'nullable|numeric|gte:0',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $service = $this->createService($request);
        $property = $this->createProperty($request, $service);
        if($service && $property){
            return redirect()->route('service.index')->with(['alert-type' => 'success', 'message' => __('The Service Provider Created')]);
        }

        return redirect()->route('service.index')->with(['alert-type' => 'error', 'message' => __('The Service Provider Create Error')]);
    }

    public function update(Request $request, $id){
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:service_providers,name,'.$id,
            'status' => 'required|string|in:0,1',
            'type' => 'nullable|string',
            'property.priority_traders' => 'nullable|string',
            'property.blocked_traders' => 'nullable|string',
            'property.available_currencies' => 'nullable|string',
            'property.api_token' => 'nullable|string',
            'property.withdrawal_priority_traders' => 'nullable|string',
            'property.blocked_banks' => 'nullable|string',
            'property.currencies_without_card_number' => 'nullable|string',
            'property.percentage_currency_rate' => 'nullable|numeric|gte:0',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $service = ServiceProvider::where(['id' => $id])->first();

        if($service){
            $this->updateServiceProviderProperty($request, $service);
            $this->updateServiceProvider($request, $service);

            return redirect()->route('service.index')->with(['alert-type' => 'success', 'message' => __('The Service Provider Updated')]);
        }

        return redirect()->route('service.index')->with(['alert-type' => 'error', 'message' => __('The Service Provider Update Error')]);
    }

    public function update_priority_banks_traders(Request $request, $id){
        $messages = [];

        $rules = [
            'property.priority_banks_traders' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $service = ServiceProvider::where(['id' => $id])->first();

        if($service){
            $this->updateServiceProviderProperty($request, $service);
            return redirect()->route('service.index')->with(['alert-type' => 'success', 'message' => __('The Service Provider Updated')]);
        }

        return redirect()->route('service.index')->with(['alert-type' => 'error', 'message' => __('The Service Provider Update Error')]);
    }

    protected function createService($request){
        $service = ServiceProvider::create([
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'status' => $request->input('status') ? $request->input('status') : 0
        ]);
        return $service;
    }

    protected function createProperty($request, $service = null){
        $properties = $request->input('property');
        foreach ($properties as $key=>$property){
            if($properties){
                $property = match ($key) {
                    'priority_traders', 'blocked_traders', 'available_currencies', 'withdrawal_priority_traders', 'blocked_banks', 'currencies_without_card_number' => json_encode(explode(',', $property)),
                    default => $property
                };
                ServiceProvidersProperty::create([
                    'obj_name' => $service->type,
                    'obj_id' => $service->id,
                    'property' => $key,
                    'property_value' => $property
                ]);
            }
        }
        return true;
    }

    protected function updateServiceProvider($request, $service){
        $service->name = $request->input('name');
        $service->status = $request->input('status');
        $service->save();
    }

    protected function updateServiceProviderProperty($request, $service){
        $properties = $request->input('property');

        foreach ($properties as $key=>$property){
            if($property) {
                $property = match ($key) {
                    'priority_traders', 'blocked_traders', 'available_currencies', 'withdrawal_priority_traders', 'blocked_banks', 'currencies_without_card_number' => json_encode(explode(',', $property)),
                    default => $property
                };

                $serviceProvidersProperty = ServiceProvidersProperty::firstOrCreate([
                    'obj_name' => $service->type,
                    'obj_id' => $service->id,
                    'property' => $key,
                ],
                [
                    'property_value' => $property
                ]);
                
                $serviceProvidersProperty->property_value = $property;
                $serviceProvidersProperty->save();
            }else{
                $record = ServiceProvidersProperty::where([
                    'obj_name' => $service->type,
                    'obj_id' => $service->id,
                    'property' => $key
                ])->first();
                $record?->delete();
            }
        }
        return true;
    }
}
