<?php

namespace App\Http\Controllers;

use App\Models\BalanceHistory;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use NumberFormatter;

class BalanceHistoryController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $type = $request->input('type');
        $currency = $request->input('currency');

        $query = BalanceHistory::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $query->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $query->where('created_at', '<=', $to_date);
        }
        if (!is_null($type) && in_array($type, [
                BalanceHistory::TYPE_CREDIT,
                BalanceHistory::TYPE_DEBIT
            ])) {
            $query->where('type', '=', $type);
        }
        $query->with(['user', 'merchant', 'input_invoice', 'payment_invoice']);
        $balance_histories = $query->paginate($limit)->withQueryString();

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('balance_history.index', [
            'type' => $type,
            'currency' => $currency,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'balance_histories' => $balance_histories,
            'fmt' => $fmt
        ]);
    }

    public function view(Request $request, $id)
    {
        $invoice = BalanceHistory::where(['id' => $id])->with(['user', 'merchant'])->firstOrFail();
        if ($invoice->payment_system == InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC && !empty($invoice['payment_id'])) {
            $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
            $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
            $contact_res = $chatexLBC->contactInfo($invoice['payment_id']);
            $messages = $chatexLBC->contactMessages($invoice['payment_id']);
        }
        return view('balance_history.view', [
            'invoice' => $invoice,
            'contact_res' => $contact_res ?? null,
            'messages' => $messages ?? null,
            'addition_info' => json_decode($invoice['addition_info'], true) ?? null
        ]);
    }
}
