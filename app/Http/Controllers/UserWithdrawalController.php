<?php

namespace App\Http\Controllers;

use App\Exports\UserWithdrawalExport;
use App\Models\GrowTokenInvoice;
use App\Models\User;
use App\Repositories\UserWithdrawalRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use NumberFormatter;
use App\Http\Libs\Api\User as UserClient;

class UserWithdrawalController extends Controller
{

    private UserWithdrawalRepository $invoiceRepository;

    private UserClient $userClient;

    public function __construct(
        UserClient $userClient,
        UserWithdrawalRepository $invoiceRepository
    ) {
        $this->userClient = $userClient;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if($request->has('btnExport')){
            return (new UserWithdrawalExport($this->invoiceRepository, $this->userClient, [
                'status' => GrowTokenInvoice::STATUS_CREATED,
                'from_date' => $from_date,
                'to_date' => $to_date,
            ]))->download('userWithdrawalInvoices.csv', \Maatwebsite\Excel\Excel::CSV);
        }

        $invoicesQuery = GrowTokenInvoice::with(['user'])
            ->orderBy('id', 'desc');

        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }

        if (!is_null($status)) {
            $invoicesQuery->where('status', '=', $status);
        }

        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $totalQuery = GrowTokenInvoice::where([
            'status' => GrowTokenInvoice::STATUS_PAYED
        ])
            ->select(
                DB::raw('SUM(amount) as total_amount'),
            );

        if ($from_date) {
            $totalQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $totalQuery->where('created_at', '<=', $to_date);
        }

        $total = $totalQuery->first();

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        $users = User::all()->pluck('name', 'id');

        return view('user.withdrawal.index', [
            'users' => $users,
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'fmt' => $fmt,
            'total' => $total,
            'total_page' => [
                'amount' => $invoices->getCollection()->where('status', '=', GrowTokenInvoice::STATUS_PAYED)->sum('amount'),
            ]
        ]);
    }

    public function set_status_payed(Request $request, $id)
    {
        $invoice = GrowTokenInvoice::where(['id' => $id])
            ->firstOrFail();
        if ($invoice->status == GrowTokenInvoice::STATUS_PAYED) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Withdrawal invoice ":id" has status paid', ['id' => $id])]);
        }
        $result = $this->userClient->setStatusPayed(['id' => $id]);
        if($result){
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status changed.')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status has not changed.')]);
    }

    public function set_status_cancel(Request $request, $id)
    {
        $invoice = GrowTokenInvoice::where(['id' => $id])
            ->firstOrFail();
        if ($invoice->status == GrowTokenInvoice::STATUS_CANCELED) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Withdrawal invoice ":id" has status cancel', ['id' => $id])]);
        }
        $result = $this->userClient->setStatusCancel(['id' => $id]);
        if($result){
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status changed.')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Status has not changed.')]);
    }

}
