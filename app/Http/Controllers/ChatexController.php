<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApi;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\InputInvoice;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Models\ServiceProvidersProperty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use NumberFormatter;

class ChatexController extends Controller
{
    public function send_message(Request $request)
    {
        $request->validate([
            'payment_id' => 'required|integer',
            'message' => 'nullable',
            'document' => 'nullable|image'
        ]);

        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        if ($request->hasFile('document')) {
            $res = $chatexLBC->contactMessagePost(
                $request->input('payment_id'),
                null,
                $request->file('document')
            );
            Log::channel('sd_chatex_lbc_requests')->info('SEND_MESSAGE|SD_USER_ID|' . $request->user()->id, $res);
        }
        if ($request->input('message')) {
            $res = $chatexLBC->contactMessagePost(
                $request->input('payment_id'),
                $request->input('message')
            );
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Message sent.')]);
        } else {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Whoops! Something went wrong.')]);
        }
    }

    public function send_dispute(Request $request)
    {
        $request->validate([
            'payment_id' => 'required|integer'
        ]);

        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $res = $chatexLBC->contactDispute($request->input('payment_id'));
        if (!empty($res['data']['message']) && $res['data']['message'] == 'OK') {
            Log::channel('sd_chatex_lbc_requests')->info('CONTACT_DISPUTE|SD_USER_ID|' . $request->user()->id, [
                'res' => $res,
                'payment_id' => $request->input('payment_id')
            ]);
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('The contact sent to dispute.')]);
        } else {
            Log::channel('sd_chatex_lbc_requests')->info('CONTACT_DISPUTE|ERROR|SD_USER_ID|' . $request->user()->id, [
                'res' => $res,
                'payment_id' => $request->input('payment_id')
            ]);
            return redirect()->back()->with([
                'alert-type' => 'error',
                'message' => __('Whoops! Something went wrong.') . " " . $res['errors']
            ]);
        }
    }

    public function dashboard(Request $request)
    {
        $type = $request->input('type');
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $res = $chatexLBC->dashboard($type);
        $contactIds = [];
        if (!empty($res['data'])) {
            foreach ($res['data']['contact_list'] as $contact) {
                if (!empty($contact['data']['contact_id'])) {
                    $contactIds[] = $contact['data']['contact_id'];
                }
            }
        }
        $inputs = $payments = $withdrawals = [];
        if ($contactIds) {
            $inputInvoices = InputInvoice::whereIn('payment_id', $contactIds)
                ->with(['user'])
                ->get();
            foreach ($inputInvoices as $invoice) {
                $inputs[$invoice->payment_id] = $invoice;
            }
            $paymentInvoices = PaymentInvoice::whereIn('payment_id', $contactIds)
                ->with(['user', 'merchant'])
                ->get();
            foreach ($paymentInvoices as $invoice) {
                $payments[$invoice->payment_id] = $invoice;
            }
            $merchantWithdrawalInvoices = MerchantWithdrawalInvoice::whereIn('payment_id', $contactIds)
                ->with(['merchant'])
                ->get();
            foreach ($merchantWithdrawalInvoices as $invoice) {
                $withdrawals[$invoice->payment_id] = $invoice;
            }
        }
        $contacts = $res['data']['contact_list'] ?? [];
        usort($contacts, function ($a, $b) {
            return $a['data']['created_at'] < $b['data']['created_at'] ? 1 : -1;
        });
        return view('chatex.dashboard', [
            'contact_list' => $contacts,
            'contact_count' => $res['data']['contact_count'] ?? 0,
            'error_code' => $res['error_code'] ?? null,
            'message' => $res['message'] ?? null,
            'type' => $type,
            'inputs' => $inputs,
            'payments' => $payments,
            'withdrawals' => $withdrawals,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY)
        ]);
    }

    public function contact(Request $request, $id)
    {
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $contact_res = $chatexLBC->contactInfo($id);
        if (!empty($contact_res['data']['advertisement']['id'])) {
            $ad_res = $chatexLBC->adGet($contact_res['data']['advertisement']['id']);
        }
        $messages = $chatexLBC->contactMessages($id);

        $inputInvoice = InputInvoice::where('payment_id', $id)
            ->with(['user'])
            ->first();

        $paymentInvoice = PaymentInvoice::where('payment_id', $id)
            ->with(['user', 'merchant'])
            ->first();

        return view('chatex.contact', [
            'contact_res' => $contact_res ?? null,
            'ad' => $ad_res['data']['ad_list'][0]['data'] ?? null,
            'messages' => $messages ?? null,
            'id' => $id,
            'inputInvoice' => $inputInvoice,
            'paymentInvoice' => $paymentInvoice
        ]);
    }

    public function recent_messages()
    {
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $res = $chatexLBC->recent_messages(now()->toRfc3339String());
        $contactIds = [];
        if (!empty($res['data'])) {
            foreach ($res['data']['message_list'] as $item) {
                if (!empty($item['contact_id'])) {
                    $contactIds[] = $item['contact_id'];
                }
            }
        }
        $inputs = $payments = [];
        if ($contactIds) {
            $inputInvoices = InputInvoice::whereIn('payment_id', $contactIds)
                ->with(['user'])
                ->get();
            foreach ($inputInvoices as $invoice) {
                $inputs[$invoice->payment_id] = $invoice;
            }
            $paymentInvoices = PaymentInvoice::whereIn('payment_id', $contactIds)
                ->with(['user', 'merchant'])
                ->get();
            foreach ($paymentInvoices as $invoice) {
                $payments[$invoice->payment_id] = $invoice;
            }
        }

        return view('chatex.recent_messages', [
            'message_list' => $res['data']['message_list'] ?? [],
            'message_count' => $res['data']['message_count'] ?? 0,
            'error_code' => $res['error_code'] ?? null,
            'message' => $res['message'] ?? null,
            'inputs' => $inputs,
            'payments' => $payments,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY)
        ]);
    }

    public function ads(Request $request)
    {
        $type = $request->input('type');
        $rates['btc'] = Property::getProperties('coinmarketcap2');
        $rates['usdt'] = Property::getProperties('coinmarketcap');
        $rates['usdt2usd'] = Property::getProperties('coinmarketcap_usdt2usd');
        asort($rates);
        if ($type) {
            $ad_list = $this->_adList($type);
        } else {
            $ad_list = $this->_adList('usdt');
            $ad_list = array_merge($ad_list, $this->_adList('usdt_trc20'));
            $ad_list = array_merge($ad_list, $this->_adList('btc'));
        }
        $currencies = [];

        $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, 1, 'priority_traders'), true);
        $blocked_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, 1, 'blocked_traders'), true);

        if (!empty($ad_list)) {
            foreach ($ad_list as $item) {
                $currency = strtolower($item['data']['currency']);
                if (!in_array($currency, $currencies)) {
                    $currencies[] = $currency;
                }
            }
            $ad_list_filtered = [];
            foreach ($ad_list as $k => $item) {
                $data = $item['data'];
                if ($request->input('accept_cc') && !$data['profile']['is_accept_visa_direct']) {
                    continue;
                }
                if ($request->input('cc') && empty($data['clean_card_number'])) {
                    continue;
                }
                if ($request->input('currency') && strtolower($request->input('currency')) != strtolower($data['currency'])) {
                    continue;
                }

                if (!empty($data['profile']['username']) && $blocked_traders && in_array($data['profile']['username'], $blocked_traders)) {
                    $item['data']['gps_blocked'] = 1;
                } else {
                    $item['data']['gps_blocked'] = 0;
                }
                if (!empty($data['profile']['username']) && $priority_traders && in_array($data['profile']['username'], $priority_traders)) {
                    $item['data']['gps_priority'] = 1;
                } else {
                    $item['data']['gps_priority'] = 0;
                }

                if ($request->input('priority') && !$item['data']['gps_priority']) {
                    continue;
                }

                if ($request->input('blocked') && !$item['data']['gps_blocked']) {
                    continue;
                }

                if ($request->input('amount')) {
                    $coin = strtolower($data['coin']);
                    $rate = $rates[$coin]['coin_rate'] ?? 1;
                    $coin_amount = (float)$request->input('amount') * $rate;
                    $c_amount = $coin_amount * $data['temp_price'];
                    if ((is_null($data['min_amount']) || $c_amount >= $data['min_amount'])
                        && (is_null($data['max_amount']) || $c_amount <= $data['max_amount'])
                        && $c_amount <= $data['max_amount_available']
                    ) {
                        $item['data']['gps_temp_amount'] = round($c_amount, 2);
                    } else {
                        continue;
                    }
                }
                $ad_list_filtered[] = $item;
            }
            $ad_list = $ad_list_filtered;
        }

        return view('chatex.ads', [
            'type' => $type,
            'ad_list' => $ad_list,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
            'rates' => $rates,
            'currencies' => $currencies
        ]);
    }

    public function adsBuy(Request $request)
    {
        $type = $request->input('type');
        $rates['btc'] = Property::getProperties('coinmarketcap2');
        $rates['usdt'] = Property::getProperties('coinmarketcap');
        $rates['usdt2usd'] = Property::getProperties('coinmarketcap_usdt2usd');
        asort($rates);
        $currencies = [];

        $priority_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, 1, 'withdrawal_priority_traders'), true);
        $blocked_traders = json_decode(ServiceProvidersProperty::getProperty(ChatexApi::OBJECT_NAME, 1, 'blocked_traders'), true);
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        if ($type) {
            $ad_list = $chatexLBC->adList2buy($type);
        } else {
            $ad_list = $chatexLBC->adList2buy('usdt');
            $ad_list = array_merge($ad_list, $chatexLBC->adList2buy('usdt_trc20'));
        }
        if (!empty($ad_list)) {
            foreach ($ad_list as $item) {
                $currency = strtolower($item['data']['currency']);
                if (!in_array($currency, $currencies)) {
                    $currencies[] = $currency;
                }
            }
            $ad_list_filtered = [];
            foreach ($ad_list as $k => $item) {
                $data = $item['data'];
                if ($request->input('accept_cc') && !$data['profile']['is_accept_visa_direct']) {
                    continue;
                }
                if ($request->input('cc') && empty($data['clean_card_number'])) {
                    continue;
                }
                if ($request->input('currency') && strtolower($request->input('currency')) != strtolower($data['currency'])) {
                    continue;
                }

                if (!empty($data['profile']['username']) && $blocked_traders && in_array($data['profile']['username'], $blocked_traders)) {
                    $item['data']['gps_blocked'] = 1;
                } else {
                    $item['data']['gps_blocked'] = 0;
                }
                if (!empty($data['profile']['username']) && $priority_traders && in_array($data['profile']['username'], $priority_traders)) {
                    $item['data']['gps_priority'] = 1;
                } else {
                    $item['data']['gps_priority'] = 0;
                }

                if ($request->input('priority') && !$item['data']['gps_priority']) {
                    continue;
                }

                if ($request->input('blocked') && !$item['data']['gps_blocked']) {
                    continue;
                }

                if ($request->input('amount')) {
                    $coin = strtolower($data['coin']);
                    $rate = $rates[$coin]['coin_rate'] ?? 1;
                    $coin_amount = (float)$request->input('amount') * $rate;
                    $c_amount = $coin_amount * $data['temp_price'];
                    if ((is_null($data['min_amount']) || $c_amount >= $data['min_amount'])
                        && (is_null($data['max_amount']) || $c_amount <= $data['max_amount'])
                        && $c_amount <= $data['max_amount_available']
                    ) {
                        $item['data']['gps_temp_amount'] = round($c_amount, 2);
                    } else {
                        continue;
                    }
                }
                $ad_list_filtered[] = $item;
            }
            $ad_list = $ad_list_filtered;
        }

        return view('chatex.ads_buy', [
            'type' => $type,
            'ad_list' => $ad_list,
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
            'rates' => $rates,
            'currencies' => $currencies
        ]);
    }


    public function wallet_history(Request $request)
    {
        $type = $request->input('type');
        $page = $request->input('page', 1);
        $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
        $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
        $offset = 0;
        $limit = 100;
        $page = $page >= 1 ? $page : 1;

        $data = $chatex->walletHistory($offset + ((int)$page - 1) * $limit, $limit, $type);

        return view('chatex.wallet_history', [
            'data' => $data,
            'type' => $type
        ]);
    }

    public function wallet_history_24(Request $request)
    {
        $page = $request->input('page', 1);
        $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
        $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
        $offset = 0;
        $limit = 100;
        $page = $page >= 1 ? $page : 1;

        $data = $chatex->walletHistory($offset + ((int)$page - 1) * $limit, $limit, "FIAT_TRADE,TRANSFER");
        $date = Carbon::now()->subDay();
        $data = array_filter($data, function ($item) use($date){
            return $date->isBefore(Carbon::parse($item['updated_at']));
        });

        return view('chatex.wallet_history', [
            'data' => $data
        ]);
    }

    private function _adList($type)
    {
        $ad_list = [];
        $hasPage = true;
        $maxPage = 20;
        $page = null;
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        if ($type == 'usdt') {
            $method = 'sellUSDTOnline';
        } elseif ($type == 'usdt_trc20') {
            $method = 'sellUsdtTrc20Online';
        } elseif ($type == 'btc') {
            $method = 'sellBitcoinsOnline';
        } else {
            return [];
        }
        while ($hasPage && $maxPage >= $page) {
            $hasPage = false;
            $res_sell = $chatexLBC->$method(null, null, $page);
            if (!empty($res_sell['data']['ad_list'])) {
                $ad_list = array_merge($ad_list, $res_sell['data']['ad_list']);
            } else {
                Log::channel('chatex_lbc_requests')->info('AD_LIST|ERROR|USER_ID|' . request()->user()->id, ['res_sell' => $res_sell]);
            }
            if (!empty($res_sell['pagination']['next'])) {
                $url_query = parse_url($res_sell['pagination']['next'], PHP_URL_QUERY);
                $query = explode('=', $url_query);
                if (!empty($query[1])) {
                    $page = $query[1];
                    $hasPage = true;
                }
            }
        }
        return $ad_list;
    }

    public function image(Request $request, $url)
    {
        $url = base64_decode($url);
        $ext = pathinfo(
            parse_url($url, PHP_URL_PATH),
            PATHINFO_EXTENSION
        );
        return response(file_get_contents($url), 200)->header('Content-type', 'image/' . $ext);
    }

    public function dictionaries()
    {
        $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
        $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
        $fiats = $chatex->fiats();
        $result = [];
        foreach ($fiats as $item){
            $result['fiats'][$item['name']] = $item['full_name'];
        }
        foreach ($chatex->payment_systems('rus', 'rub') as $item){
            $result['payment_systems']['rub'][$item['id']] = $item['name'];
        }
        foreach ($chatex->payment_systems('ukr', 'uah') as $item){
            $result['payment_systems']['uah'][$item['id']] = $item['name'];
        }
        foreach ($chatex->payment_systems('kaz', 'kzt') as $item){
            $result['payment_systems']['kzt'][$item['id']] = $item['name'];
        }
        foreach ($chatex->payment_systems('blr', 'byn') as $item){
            $result['payment_systems']['byn'][$item['id']] = $item['name'];
        }

        return response()->json($result);

    }

}
