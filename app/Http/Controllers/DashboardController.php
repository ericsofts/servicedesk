<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApi;
use App\Models\Property;
use App\Models\ServiceProvidersProperty;
use App\Models\SystemBalance;
use App\Models\ServiceProvidersBalance;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $data = [];

        if ($request->user()->can('chatex.wallet')) {
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
            $data['wallets'] = $chatex->wallet() ?: [];
        }

        if ($request->user()->can('system.balances')) {
            $data['balance_grow'] = SystemBalance::where('name', 'grow')->first();
            $data['service_providers'] = ServiceProvidersBalance::orderBy('id', 'asc')->with(['service_provider'])->get();
        }

        if ($request->user()->can('chatex.balance')) {
            $data['balance_chatex'] = ServiceProvidersBalance::where('name', 'chatex')->first();
        }

        return view('dashboard', $data);
    }
}
