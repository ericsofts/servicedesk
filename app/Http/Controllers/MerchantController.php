<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Merchant;
use App\Models\MerchantChat;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\MerchantServiceProvider;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MerchantController extends Controller
{
    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');
        $agent_selected = $request->input('agents', []);
        $searchresults = null;
        $query = Merchant::orderBy('id', 'desc');

        if (!is_null($request->input('status'))) {
            $query->where('status', '=', $status);
        }

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }

        if (!empty($agent_selected)) {
            $query->whereIn('agent_id', $agent_selected);
        }

        if (!is_null($request->input('search'))) {
            $s = $request->input('search');
            $query->where(function ($query) use ($s) {
                foreach (Merchant::searchLikeItems as $item) {
                    $query->OrWhere($item, 'like', "%{$s}%");
                }
            })->orWhereHas('chat', function ($query) use ($s) {
                $query->where(function ($q) use ($s) {
                    foreach (MerchantChat::searchLikeItems as $item) {
                        $q->OrWhere($item, 'like', "%{$s}%");
                    }
                });
            });
        }

        $query->with([
            'balance',
            'agent.partner',
            'merchant_property' => function ($q) {
                $q->where([
                    'property' => 'available_coins'
                ]);
            },
            'chat'
        ]);

        $open_withdrawals = MerchantWithdrawalInvoice::whereNotIn('status', [
            MerchantWithdrawalInvoice::STATUS_PAYED,
            MerchantWithdrawalInvoice::STATUS_CANCELED,
            MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT
        ])
            ->select([DB::raw("SUM(amount) as total_amount"), 'merchant_id'])
            ->groupBy('merchant_id')
            ->get()
            ->pluck('total_amount', 'merchant_id');

        $merchants = $query->paginate($limit)->withQueryString();


        $agents = Agent::all()->pluck('name', 'id');

        return view('merchant.index', [
            'status' => $status,
            'merchants' => $merchants,
            'agents' => $agents,
            'open_withdrawals' => $open_withdrawals
        ]);
    }

    public function view(Request $request, $id)
    {
        $merchant = Merchant::where(['id' => $id])->with(['chat'])->firstOrFail();
        $agent = $merchant->agent()->first();
        $api_type = $request->input('api_type', []);
        $commissionsQuery = $merchant->commissions()
            ->orderBy('api_type', 'desc')
            ->orderBy('property_type');
        if ($api_type) {
            $commissionsQuery->whereIn('api_type', $api_type);
        }
        $commissions = $commissionsQuery->get();
        return view('merchant.view', [
            'merchant' => $merchant,
            'agent' => $agent,
            'commissions' => $commissions
        ]);
    }

    public function new(Request $request)
    {
        $agents = Agent::pluck('name', 'id');

        $maybe_providers = ServiceProvider::where('status', ServiceProvider::STATUS_ACTIVE)->get();

        $available_providers = [];
        $available_providers_ids = [];

        return view('merchant.new', [
            'available_providers_ids' => $available_providers_ids,
            'available_providers' => $available_providers,
            'maybe_providers' => $maybe_providers,
            'password' => Merchant::generatePassword(),
            'agents' => $agents,
            'token' => Str::random(128),
            'payment_key' => Merchant::generatePaymentKey(),
            'api3_key' => Merchant::generateApi3Key(),
            'api5_key' => Merchant::generateApi5Key(),
        ]);
    }

    public function edit(Request $request, $id)
    {
        $merchant = Merchant::where(['id' => $id])->with(['chat'])->first();

        $maybe_providers = ServiceProvider::where('status', ServiceProvider::STATUS_ACTIVE)->get();

        $available_providers = MerchantServiceProvider::with('service_provider')->where(['merchant_id' => $merchant->id])->orderBy('priority', 'asc')->get();
        $available_providers_ids = [];
        foreach ($available_providers as $aunit) {
            $available_providers_ids[] = $aunit->service_provider_id;
        }

        $merchant_property = MerchantProperty::getProperties($id);

        $permitted_ip_list = $merchant_property['permitted_ip_list'] ?? '';
        $enable_ip_list = $merchant_property['enable_ip_list'] ?? [];
        $disableName = $merchant_property['disable_show_name'] ?? 0;
        $exact_currency = $merchant_property['exact_currency'] ?? 0;
        $currency2currency = $merchant_property['currency2currency'] ?? 0;
        $excluded_currencies = implode(',', json_decode($merchant_property['excluded_currencies'] ?? '[]', true));
        $provider_limit = MerchantProperty::getProperty($id, 'provider_limit', '{}');
        $service_providers = ServiceProvider::where('status', ServiceProvider::STATUS_ACTIVE)->get();
        $avalable_service_providers = $maybe_providers->pluck('name')->toArray();
        foreach ($service_providers as &$provider) {
            $property = MerchantProperty::getProperties($id, $provider->id);
            $provider->available_currencies = $this->getAvalableCurrencies();
            $provider->min_payment_invoice_amount = $property['min_payment_invoice_amount'] ?? config('app.min_payment_invoice_amount');
            $provider->min_merchant_withdrawal_amount = $property['min_merchant_withdrawal_amount'] ?? config('app.min_merchant_withdrawal_amount');
            $provider->available_coins = json_decode($property['available_coins'] ?? config('app.available_merchant_coins'), true);

            $commissionApi5Invoice = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API5, MerchantCommission::PROPERTY_TYPE_COMMISSION, $provider->id);
            $commissionApi3Invoice = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API3, MerchantCommission::PROPERTY_TYPE_COMMISSION, $provider->id);
            $commissionApi5Withdrawal = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API5, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $provider->id);
            $commissionApi3Withdrawal = MerchantCommission::getProperties($merchant->id, MerchantCommission::API_TYPE_API3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $provider->id);

            $provider->api3_min_commission = $commissionApi3Invoice['min_commission'][0]['value'] ?? config('app.commission.min_commission');
            $provider->api5_min_commission = $commissionApi5Invoice['min_commission'][0]['value'] ?? config('app.commission.min_commission');
            $provider->api3_type2_min_commission = $commissionApi3Withdrawal['min_commission'][0]['value'] ?? config('app.commission_withdrawal.min_commission');
            $provider->api5_type2_min_commission = $commissionApi5Withdrawal['min_commission'][0]['value'] ?? config('app.commission_withdrawal.min_commission');

            $provider->agent_api3_invoice_commission = $commissionApi3Invoice['agent'] ?? json_decode(config('app.commission.agent'), true);
            $provider->agent_api5_invoice_commission = $commissionApi5Invoice['agent'] ?? json_decode(config('app.commission.agent'), true);
            $provider->grow_api3_invoice_commission = $commissionApi3Invoice['grow'] ?? json_decode(config('app.commission.grow'), true);
            $provider->grow_api5_invoice_commission = $commissionApi5Invoice['grow'] ?? json_decode(config('app.commission.grow'), true);
            $provider->service_api3_invoice_commission = $commissionApi3Invoice['service'] ?? json_decode(config('app.commission.service'), true);
            $provider->service_api5_invoice_commission = $commissionApi5Invoice['service'] ?? json_decode(config('app.commission.service'), true);

            $provider->agent_api3_withdrawal_commission = $commissionApi3Withdrawal['agent'] ?? json_decode(config('app.commission_withdrawal.agent'), true);
            $provider->agent_api5_withdrawal_commission = $commissionApi5Withdrawal['agent'] ?? json_decode(config('app.commission_withdrawal.agent'), true);
            $provider->grow_api3_withdrawal_commission = $commissionApi3Withdrawal['grow'] ?? json_decode(config('app.commission_withdrawal.grow'), true);
            $provider->grow_api5_withdrawal_commission = $commissionApi5Withdrawal['grow'] ?? json_decode(config('app.commission_withdrawal.grow'), true);
            $provider->service_api3_withdrawal_commission = $commissionApi3Withdrawal['service'] ?? json_decode(config('app.commission_withdrawal.service'), true);
            $provider->service_api5_withdrawal_commission = $commissionApi5Withdrawal['service'] ?? json_decode(config('app.commission_withdrawal.service'), true);

            $provider->commission_api3_invoice_min_commission_type = $commissionApi3Invoice['min_commission_type'][0]['value'] ?? 0;
            $provider->commission_api5_invoice_min_commission_type = $commissionApi5Invoice['min_commission_type'][0]['value'] ?? 0;
            $provider->commission_api3_withdrawal_min_commission_type = $commissionApi3Withdrawal['min_commission_type'][0]['value'] ?? 0;
            $provider->commission_api5_withdrawal_min_commission_type = $commissionApi5Withdrawal['min_commission_type'][0]['value'] ?? 0;

            $provider->invoice_api3_min_sum_agent = $commissionApi3Invoice['min_sum_agent'][0]['value'] ?? config('app.commission.min_sum_agent');
            $provider->invoice_api3_min_sum_service = $commissionApi3Invoice['min_sum_service'][0]['value'] ?? config('app.commission.min_sum_service');
            $provider->invoice_api3_min_sum_grow = $commissionApi3Invoice['min_sum_grow'][0]['value'] ?? config('app.commission.min_sum_grow');
            $provider->invoice_api3_min_commission_service = $commissionApi3Invoice['min_commission_service'][0]['value'] ?? config('app.commission.min_commission_service');

            $provider->invoice_api5_min_sum_agent = $commissionApi5Invoice['min_sum_agent'][0]['value'] ?? config('app.commission.min_sum_agent');
            $provider->invoice_api5_min_sum_service = $commissionApi5Invoice['min_sum_service'][0]['value'] ?? config('app.commission.min_sum_service');
            $provider->invoice_api5_min_sum_grow = $commissionApi5Invoice['min_sum_grow'][0]['value'] ?? config('app.commission.min_sum_grow');
            $provider->invoice_api5_min_commission_service = $commissionApi5Invoice['min_commission_service'][0]['value'] ?? config('app.commission.min_commission_service');

            $provider->withdrawal_api3_min_sum_agent = $commissionApi3Withdrawal['min_sum_agent'][0]['value'] ?? config('app.commission_withdrawal.min_sum_agent');
            $provider->withdrawal_api3_min_sum_service = $commissionApi3Withdrawal['min_sum_service'][0]['value'] ?? config('app.commission_withdrawal.min_sum_service');
            $provider->withdrawal_api3_min_sum_grow = $commissionApi3Withdrawal['min_sum_grow'][0]['value'] ?? config('app.commission_withdrawal.min_sum_grow');
            $provider->withdrawal_api3_min_commission_service = $commissionApi3Withdrawal['min_commission_service'][0]['value'] ?? config('app.commission_withdrawal.min_commission_service');

            $provider->withdrawal_api5_min_sum_agent = $commissionApi5Withdrawal['min_sum_agent'][0]['value'] ?? config('app.commission_withdrawal.min_sum_agent');
            $provider->withdrawal_api5_min_sum_service = $commissionApi5Withdrawal['min_sum_service'][0]['value'] ?? config('app.commission_withdrawal.min_sum_service');
            $provider->withdrawal_api5_min_sum_grow = $commissionApi5Withdrawal['min_sum_grow'][0]['value'] ?? config('app.commission_withdrawal.min_sum_grow');
            $provider->withdrawal_api5_min_commission_service = $commissionApi5Withdrawal['min_commission_service'][0]['value'] ?? config('app.commission_withdrawal.min_commission_service');
        }

        $agents = Agent::pluck('name', 'id');
        return view('merchant.edit', [
            'merchant' => $merchant,
            'currency2currency' => $currency2currency,
            'available_providers_ids' => $available_providers_ids,
            'available_providers' => $available_providers,
            'maybe_providers' => $maybe_providers,
            'merchant_property' => $merchant_property,
            'service_providers' => $service_providers,
            'agents' => $agents,
            'id' => $merchant->id,
            'token' => $merchant->token,
            'payment_key' => $merchant->payment_key,
            'api3_key' => $merchant->api3_key,
            'api5_key' => $merchant->api5_key,
            'name' => $merchant->name,
            'email' => $merchant->email,
            'agent_id' => $merchant->agent_id,
            'status' => $merchant->status,
            'enable_2fa' => $merchant->enable_2fa,
            'permitted_ip_list' => $permitted_ip_list,
            'enable_ip_list' => $enable_ip_list,
            'disableName' => $disableName,
            'type' => $merchant->type,
            'excluded_currencies' => $excluded_currencies,
            'provider_limit' => $provider_limit,
            'avalable_service_providers' => $avalable_service_providers,
            'exact_currency' => $exact_currency
        ]);
    }

    public function create(Request $request)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:merchants,name',
            'email' => 'required|string|email|unique:merchants,email',
            'password' => 'nullable|string',
            'agent_id' => 'nullable|numeric',
            'status' => 'nullable|string',
            'enable_2fa' => 'nullable|string',
            'type' => 'nullable|string',
            'token' => 'required|string|unique:merchants,token',
            'payment_key' => 'nullable|string|unique:merchants,payment_key',
            'api3_key' => 'nullable|string|unique:merchants,api3_key',
            'api5_key' => 'nullable|string|unique:merchants,api5_key',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $merchant = $this->createMerchant($request);
        if ($merchant) {
            $this->updateMerchantProperty($request, $merchant);
            return redirect()->route('merchant.edit', ['id' => $merchant->id])->with(['alert-type' => 'success', 'message' => __('The Merchant Created')]);
        }

        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Merchant Create Error')]);
    }

    public function updateChat(Request $request, $id)
    {
        $merchant = Merchant::where(['id' => $id])->first();
        if ($merchant) {
            MerchantChat::updateOrCreate(
                [
                    'merchant_id' => $id
                ],
                [
                    'chat_id' => $request->input('chat_id'),
                    'chat_name' => $request->input('chat_name'),
                ]
            );
            return redirect()->route('merchant.index')->with(['alert-type' => 'success', 'message' => __('The Merchant Updated')]);
        }
        return redirect()->route('merchant.index')->with(['alert-type' => 'error', 'message' => __('The Merchant Update Error')]);
    }

    public function update(Request $request, $id)
    {
        $messages = [];

        $rules = [
            'name' => 'required|string|unique:merchants,name,' . $id,
            'email' => 'required|string|email|unique:merchants,email,' . $id,
            'password' => 'nullable|string',
            'agent_id' => 'nullable|numeric',
            'status' => 'required|string',
            'enable_2fa' => 'required|string',
            'type' => 'required|string',
            'token' => 'required|string|unique:merchants,token,' . $id,
            'payment_key' => 'nullable|string|unique:merchants,payment_key,' . $id,
            'api3_key' => 'nullable|string|unique:merchants,api3_key,' . $id,
            'api5_key' => 'nullable|string|unique:merchants,api5_key,' . $id,
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $merchant = Merchant::where(['id' => $id])->first();

        if ($merchant) {
            $this->updateMerchant($request, $merchant);
            $this->updateMerchantProperty($request, $merchant);
            return redirect()->route('merchant.index')->with(['alert-type' => 'success', 'message' => __('The Merchant Updated')]);
        }

        return redirect()->route('merchant.index')->with(['alert-type' => 'error', 'message' => __('The Merchant Update Error')]);
    }

    // AJAX
    public function updateCommission(Request $request, $id)
    {
        $messages = [];

        $rules = [
            'service_provider_id' => 'required|numeric|gte:0',
            'commission-api3_min_commission' => 'required|numeric|gte:0',
            'commission-api5_min_commission' => 'required|numeric|gte:0',

            'commission-api3_type2_min_commission' => 'required|numeric|gte:0',
            'commission-api5_type2_min_commission' => 'required|numeric|gte:0',

            'agent-api3-invoice-commission.*' => 'required|numeric',
            'agent-api3-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'agent-api3-invoice-commission-value.*' => 'required|numeric|gte:0',
            'grow-api3-invoice-commission.*' => 'required|numeric',
            'grow-api3-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'grow-api3-invoice-commission-value.*' => 'required|numeric|gte:0',
            'service_api3-invoice-commission.*' => 'required|numeric',
            'service_api3-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'service_api3-invoice-commission-value.*' => 'required|numeric|gte:0',
            'agent-api5-invoice-commission.*' => 'required|numeric',
            'agent-api5-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'agent-api5-invoice-commission-value.*' => 'required|numeric|gte:0',
            'grow-api5-invoice-commission.*' => 'required|numeric',
            'grow-api5-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'grow-api5-invoice-commission-value.*' => 'required|numeric|gte:0',
            'service_api5-invoice-commission.*' => 'required|numeric',
            'service_api5-invoice-commission-type.*' => 'required|string|in:percentage,fixed',
            'service_api5-invoice-commission-value.*' => 'required|numeric|gte:0',
            'agent-api3-withdrawal-commission.*' => 'required|numeric',
            'agent-api3-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'agent-api3-withdrawal-commission-value.*' => 'required|numeric|gte:0',
            'grow-api3-withdrawal-commission.*' => 'required|numeric',
            'grow-api3-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'grow-api3-withdrawal-commission-value.*' => 'required|numeric|gte:0',
            'service_api3-withdrawal-commission.*' => 'required|numeric',
            'service_api3-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'service_api3-withdrawal-commission-value.*' => 'required|numeric|gte:0',
            'agent-api5-withdrawal-commission.*' => 'required|numeric',
            'agent-api5-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'agent-api5-withdrawal-commission-value.*' => 'required|numeric|gte:0',
            'grow-api5-withdrawal-commission.*' => 'required|numeric',
            'grow-api5-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'grow-api5-withdrawal-commission-value.*' => 'required|numeric|gte:0',
            'service_api5-withdrawal-commission.*' => 'required|numeric',
            'service_api5-withdrawal-commission-type.*' => 'required|string|in:percentage,fixed',
            'service_api5-withdrawal-commission-value.*' => 'required|numeric|gte:0',

            //'property-available_coins' => 'required|array',

            'commission_api3_invoice_min_commission_type' => 'required|numeric',
            'commission_api5_invoice_min_commission_type' => 'required|numeric',
            'commission_api3_withdrawal_min_commission_type' => 'required|numeric',
            'commission_api5_withdrawal_min_commission_type' => 'required|numeric',

            'commission_invoice_api3_min_sum_agent' => 'required_if:commission_api3_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api3_min_sum_grow' => 'required_if:commission_api3_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api3_min_sum_service' => 'required_if:commission_api3_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api3_min_commission_service' => 'required_if:commission_api3_invoice_min_commission_type,==,2|numeric|gte:0',

            'commission_invoice_api5_min_sum_agent' => 'required_if:commission_api5_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api5_min_sum_grow' => 'required_if:commission_api5_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api5_min_sum_service' => 'required_if:commission_api5_invoice_min_commission_type,==,1|numeric|gte:0',
            'commission_invoice_api5_min_commission_service' => 'required_if:commission_api5_invoice_min_commission_type,==,2|numeric|gte:0',

            'commission_withdrawal_api3_min_sum_agent' => 'required_if:commission_api3_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api3_min_sum_grow' => 'required_if:commission_api3_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api3_min_sum_service' => 'required_if:commission_api3_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api3_commission_service' => 'required_if:commission_api3_withdrawal_min_commission_type,==,2|numeric|gte:0',

            'commission_withdrawal_api5_min_sum_agent' => 'required_if:commission_api5_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api5_min_sum_grow' => 'required_if:commission_api5_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api5_min_sum_service' => 'required_if:commission_api5_withdrawal_min_commission_type,==,1|numeric|gte:0',
            'commission_withdrawal_api5_commission_service' => 'required_if:commission_api5_withdrawal_min_commission_type,==,2|numeric|gte:0',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $merchant = Merchant::where(['id' => $id])->first();

        if ($merchant) {
            $this->updateMerchantProperty($request, $merchant);
            $this->updateMerchantCommissionJson($request, $merchant);
            $this->updateMerchantCommissionMin($request, $merchant);
            $this->updateMerchantCommissionSum($request, $merchant);
            return response()->json(['alert-type' => 'success', 'message' => __('The Merchant Updated')]);
        }

        return response()->json(['alert-type' => 'error', 'message' => __('The Merchant Update Error')]);
    }

    public function updateLimit(Request $request, $id)
    {
        $messages = [];

        $rules = [
            'provider_limit' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $merchant = Merchant::where(['id' => $id])->first();

        if ($merchant) {
            $this->updateMerchantLimit($request, $merchant);
            return response()->json(['alert-type' => 'success', 'message' => __('The Merchant Updated')]);
        }

        return response()->json(['alert-type' => 'error', 'message' => __('The Merchant Update Error')]);
    }

    protected function createMerchant($request)
    {
        $merchant = Merchant::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'agent_id' => $request->input('agent_id') ? $request->input('agent_id') : NULL,
            'status' => $request->input('status') ? $request->input('status') : 0,
            'enable_2fa' => $request->input('enable_2fa') ? true : false,
            'api3_key' => $request->input('api3_key'),
            'api5_key' => $request->input('api5_key'),
            'payment_key' => $request->input('payment_key'),
            'token' => $request->input('token'),
            'type' => $request->input('type') ? $request->input('type') : 0,
            'email_verified_at' => now()
        ]);

        $available_providers_ids = json_decode($request->available_providers_ids, 1);
        $merchant->available_providers()->sync($available_providers_ids);

        return $merchant;
    }

    protected function updateMerchant($request, $merchant)
    {
        $merchant->name = $request->input('name');
        $merchant->email = $request->input('email');
        $merchant->agent_id = $request->input('agent_id') ? $request->input('agent_id') : NULL;
        $merchant->status = $request->input('status');
        $merchant->enable_2fa = $request->input('enable_2fa');
        $merchant->api3_key = $request->input('api3_key');
        $merchant->api5_key = $request->input('api5_key');
        $merchant->payment_key = $request->input('payment_key');
        $merchant->token = $request->input('token');
        $merchant->type = $request->input('type');
        if ($request->input('password')) {
            $merchant->password = Hash::make($request->input('password'));
        }

        $merchant->save();

        $available_providers_ids = json_decode($request->available_providers_ids, 1);
        $merchant->available_providers()->sync($available_providers_ids);
    }

    protected function updateMerchantProperty($request, $merchant)
    {
        $pvalue = $request->input('property-currency2currency');
        if ($request->has('property-currency2currency')) {
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'currency2currency'
                ],
                [
                    'value' => $pvalue,
                ]
            );
            $property->value = $pvalue;
            $property->save();
        }

        $pvalue = $request->input('property-excluded_currencies');
        if ($request->has('property-excluded_currencies')) {
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'excluded_currencies'
                ],
                [
                    'value' => json_encode(array_filter(explode(',', $pvalue))),
                ]
            );
            $property->value = json_encode(array_filter(explode(',', $pvalue)));
            $property->save();
        }

        if ($request->has('property-permitted_ip_list')) {
            $pvalue = $request->input('property-permitted_ip_list');
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'permitted_ip_list'
                ],
                [
                    'value' => $pvalue,
                ]
            );
            $property->value = $pvalue;
            $property->save();
        }

        if ($request->has('property-enable_ip_list')) {
            $pvalue = $request->input('property-enable_ip_list');
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'enable_ip_list'
                ],
                [
                    'value' => $pvalue,
                ]
            );
            $property->value = $pvalue;
            $property->save();
        }

        if ($request->has('property-disable_show_name')) {
            $pvalue = $request->input('property-disable_show_name');
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'disable_show_name'
                ],
                [
                    'value' => $pvalue,
                ]
            );
            $property->value = $pvalue;
            $property->save();
        }

        if ($request->has('property-exact_currency')) {
            $pvalue = $request->input('property-exact_currency');
            $property = MerchantProperty::firstOrCreate(
                [
                    'merchant_id' => $merchant->id,
                    'property' => 'exact_currency'
                ],
                [
                    'value' => $pvalue,
                ]
            );
            $property->value = $pvalue;
            $property->save();
        }

        if ($request->input('service_provider_id')) {
            if ($request->has('property-min_payment_invoice_amount')) {
                $property = MerchantProperty::firstOrCreate(
                    [
                        'merchant_id' => $merchant->id,
                        'service_provider_id' => $request->input('service_provider_id'),
                        'property' => 'min_payment_invoice_amount'
                    ],
                    [
                        'value' => $request->input('property-min_payment_invoice_amount'),
                        'service_provider_id' => $request->input('service_provider_id')
                    ]
                );
                $property->value = $request->input('property-min_payment_invoice_amount');
                $property->service_provider_id = $request->input('service_provider_id');
                $property->save();
            }

            if ($request->has('property-available_coins')) {
                $property = MerchantProperty::firstOrCreate(
                    [
                        'merchant_id' => $merchant->id,
                        'service_provider_id' => $request->input('service_provider_id'),
                        'property' => 'available_coins'
                    ],
                    [
                        'value' => json_encode($request->input('property-available_coins')),
                        'service_provider_id' => $request->input('service_provider_id')
                    ]
                );
                $property->value = json_encode($request->input('property-available_coins'));
                $property->service_provider_id = $request->input('service_provider_id');
                $property->save();
            }

            if ($request->has('property-min_merchant_withdrawal_amount')) {
                $property = MerchantProperty::firstOrCreate(
                    [
                        'merchant_id' => $merchant->id,
                        'service_provider_id' => $request->input('service_provider_id'),
                        'property' => 'min_merchant_withdrawal_amount'
                    ],
                    [
                        'value' => $request->input('property-min_merchant_withdrawal_amount'),
                        'service_provider_id' => $request->input('service_provider_id')
                    ]
                );
                $property->value = $request->input('property-min_merchant_withdrawal_amount');
                $property->service_provider_id = $request->input('service_provider_id');
                $property->save();
            }
        }
    }

    protected function updateMerchantLimit($request, $merchant)
    {

        $value = $request->input('provider_limit');

        $toJson = $this->arrayRemoveEmpty(json_decode($value, true));

        $result = json_encode($toJson,
            JSON_FORCE_OBJECT
        );
        $property = MerchantProperty::firstOrCreate(
            [
                'merchant_id' => $merchant->id,
                'property' => 'provider_limit'
            ],
            [
                'value' => $result,
            ]
        );
        $property->value = $result;
        $property->save();
    }

    protected function createMerchantCommissionJson($request, $merchant)
    {

        $invoiceGrowCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('grow-invoice-commission-type'),
                    'value' => floatval($request->input('grow-invoice-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        $withdrawalGrowCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('grow-withdrawal-commission-type'),
                    'value' => floatval($request->input('grow-withdrawal-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        $invoiceAgentCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('agent-invoice-commission-type'),
                    'value' => floatval($request->input('agent-invoice-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        $withdrawalAgentCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('agent-withdrawal-commission-type'),
                    'value' => floatval($request->input('agent-withdrawal-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        $invoiceServiceCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('service-invoice-commission-type'),
                    'value' => floatval($request->input('service-invoice-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        $withdrawalServiceCommission = json_encode(
            [
                '0' => [
                    'type' => $request->input('service-withdrawal-commission-type'),
                    'value' => floatval($request->input('service-withdrawal-commission-value')),
                ]
            ],
            JSON_FORCE_OBJECT
        );

        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => $invoiceGrowCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow',
            'value' => $withdrawalGrowCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow',
            'value' => $invoiceGrowCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow',
            'value' => $withdrawalGrowCommission
        ]);

        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => $invoiceServiceCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service',
            'value' => $withdrawalServiceCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service',
            'value' => $invoiceServiceCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service',
            'value' => $withdrawalServiceCommission
        ]);

        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => $invoiceAgentCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent',
            'value' => $withdrawalAgentCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent',
            'value' => $invoiceAgentCommission
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent',
            'value' => $withdrawalAgentCommission
        ]);
    }

    protected function updateMerchantCommissionJson($request, $merchant)
    {

        $invoiceApi3GrowCommission = json_encode(
            $this->getCommissionJson('grow-api3-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi3GrowCommission = json_encode(
            $this->getCommissionJson('grow-api3-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $invoiceApi5GrowCommission = json_encode(
            $this->getCommissionJson('grow-api5-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi5GrowCommission = json_encode(
            $this->getCommissionJson('grow-api5-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $invoiceApi3AgentCommission = json_encode(
            $this->getCommissionJson('agent-api3-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi3AgentCommission = json_encode(
            $this->getCommissionJson('agent-api3-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $invoiceApi5AgentCommission = json_encode(
            $this->getCommissionJson('agent-api5-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi5AgentCommission = json_encode(
            $this->getCommissionJson('agent-api5-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $invoiceApi3ServiceCommission = json_encode(
            $this->getCommissionJson('service_api3-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi3ServiceCommission = json_encode(
            $this->getCommissionJson('service_api3-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $invoiceApi5ServiceCommission = json_encode(
            $this->getCommissionJson('service_api5-invoice-commission', $request),
            JSON_FORCE_OBJECT
        );

        $withdrawalApi5ServiceCommission = json_encode(
            $this->getCommissionJson('service_api5-withdrawal-commission', $request),
            JSON_FORCE_OBJECT
        );

        $commission = MerchantCommission::firstOrCreate(
            [
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'grow'
            ],
            ['value' => $invoiceApi3GrowCommission]
        );
        $commission->value = $invoiceApi3GrowCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate(
            [
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'grow'
            ],
            [
                'value' => $withdrawalApi3GrowCommission
            ]
        );
        $commission->value = $withdrawalApi3GrowCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'grow'
        ], [
            'value' => $invoiceApi5GrowCommission
        ]);
        $commission->value = $invoiceApi5GrowCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'grow'
        ], [
            'value' => $withdrawalApi5GrowCommission
        ]);
        $commission->value = $withdrawalApi5GrowCommission;
        $commission->save();

        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service'
        ], [
            'value' => $invoiceApi3ServiceCommission
        ]);
        $commission->value = $invoiceApi3ServiceCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service'
        ], [
            'value' => $withdrawalApi3ServiceCommission
        ]);
        $commission->value = $withdrawalApi3ServiceCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'service'
        ], [
            'value' => $invoiceApi5ServiceCommission
        ]);
        $commission->value = $invoiceApi5ServiceCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'service'
        ], [
            'value' => $withdrawalApi5ServiceCommission
        ]);
        $commission->value = $withdrawalApi5ServiceCommission;
        $commission->save();

        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent'
        ], [
            'value' => $invoiceApi3AgentCommission
        ]);
        $commission->value = $invoiceApi3AgentCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent'
        ], [
            'value' => $withdrawalApi3AgentCommission
        ]);
        $commission->value = $withdrawalApi3AgentCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'agent'
        ], [
            'value' => $invoiceApi5AgentCommission
        ]);
        $commission->value = $invoiceApi5AgentCommission;
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'agent'
        ], [
            'value' => $withdrawalApi5AgentCommission
        ]);
        $commission->value = $withdrawalApi5AgentCommission;
        $commission->save();
    }

    protected function createMerchantCommissionMin($request, $merchant)
    {
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission_type',
            'value' => $request->input('commission-min_commission_type')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission_type',
            'value' => $request->input('commission-min_commission_type')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission_type',
            'value' => $request->input('commission-min_commission_type')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission_type',
            'value' => $request->input('commission-min_commission_type')
        ]);

        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission',
            'value' => $request->input('commission-api_min_commission')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission',
            'value' => $request->input('commission-api_type2_min_commission')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission',
            'value' => $request->input('commission-api_min_commission')
        ]);
        MerchantCommission::create([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission',
            'value' => $request->input('commission-api_type2_min_commission')
        ]);
    }

    protected function updateMerchantCommissionMin($request, $merchant)
    {
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission_type'
        ], [
            'value' => $request->input('commission_api3_invoice_min_commission_type')
        ]);
        $commission->value = $request->input('commission_api3_invoice_min_commission_type');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission_type'
        ], [
            'value' => $request->input('commission_api3_withdrawal_min_commission_type')
        ]);
        $commission->value = $request->input('commission_api3_withdrawal_min_commission_type');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission_type'
        ], [
            'value' => $request->input('commission_api5_invoice_min_commission_type')
        ]);
        $commission->value = $request->input('commission_api5_invoice_min_commission_type');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission_type'
        ], [
            'value' => $request->input('commission_api5_withdrawal_min_commission_type')
        ]);
        $commission->value = $request->input('commission_api5_withdrawal_min_commission_type');
        $commission->save();

        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission'
        ], [
            'value' => $request->input('commission-api3_min_commission')
        ]);
        $commission->value = $request->input('commission-api3_min_commission');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API3,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission'
        ], [
            'value' => $request->input('commission-api3_type2_min_commission')
        ]);
        $commission->value = $request->input('commission-api3_type2_min_commission');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
            'property' => 'min_commission'
        ], [
            'value' => $request->input('commission-api5_min_commission')
        ]);
        $commission->value = $request->input('commission-api5_min_commission');
        $commission->save();
        $commission = MerchantCommission::firstOrCreate([
            'merchant_id' => $merchant->id,
            'service_provider_id' => $request->input('service_provider_id'),
            'api_type' => MerchantCommission::API_TYPE_API5,
            'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
            'property' => 'min_commission'
        ], [
            'value' => $request->input('commission-api5_type2_min_commission')
        ]);
        $commission->value = $request->input('commission-api5_type2_min_commission');
        $commission->save();
    }

    protected function createMerchantCommissionSum($request, $merchant)
    {
        if (($request->input('commission-min_commission_type') == '1') || ($request->input('commission-min_commission_type') == '2')) {
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_agent',
                'value' => $request->input('commission-invoice_min_sum_agent')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_agent',
                'value' => $request->input('commission-withdrawal_min_sum_agent')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_agent',
                'value' => $request->input('commission-invoice_min_sum_agent')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_agent',
                'value' => $request->input('commission-withdrawal_min_sum_agent')
            ]);

            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_service',
                'value' => $request->input('commission-invoice_min_sum_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_service',
                'value' => $request->input('commission-withdrawal_min_sum_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_service',
                'value' => $request->input('commission-invoice_min_sum_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_service',
                'value' => $request->input('commission-withdrawal_min_sum_service')
            ]);

            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_grow',
                'value' => $request->input('commission-invoice_min_sum_grow')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_grow',
                'value' => $request->input('commission-withdrawal_min_sum_grow')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_grow',
                'value' => $request->input('commission-invoice_min_sum_grow')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_grow',
                'value' => $request->input('commission-withdrawal_min_sum_grow')
            ]);
        }

        if ($request->input('commission-min_commission_type') == '2') {
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_commission_service',
                'value' => $request->input('commission-invoice_min_commission_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_commission_service',
                'value' => $request->input('commission-withdrawal_commission_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_commission_service',
                'value' => $request->input('commission-invoice_min_commission_service')
            ]);
            MerchantCommission::create([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_commission_service',
                'value' => $request->input('commission-withdrawal_commission_service')
            ]);
        }
    }

    protected function updateMerchantCommissionSum($request, $merchant)
    {
        if (($request->input('commission_api3_invoice_min_commission_type') == '1') || ($request->input('commission_api3_invoice_min_commission_type') == '2')) {
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_agent'
            ], [
                'value' => $request->input('commission_invoice_api3_min_sum_agent')
            ]);
            $commission->value = $request->input('commission_invoice_api3_min_sum_agent');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_service'
            ], [
                'value' => $request->input('commission_invoice_api3_min_sum_service')
            ]);
            $commission->value = $request->input('commission_invoice_api3_min_sum_service');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_grow'
            ], [
                'value' => $request->input('commission_invoice_api3_min_sum_grow')
            ]);
            $commission->value = $request->input('commission_invoice_api3_min_sum_grow');
            $commission->save();
        }
        if ($request->input('commission_api3_invoice_min_commission_type') == '2') {
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_commission_service'
            ], [
                'value' => $request->input('commission_invoice_api3_min_commission_service')
            ]);
            $commission->value = $request->input('commission_invoice_api3_min_commission_service');
            $commission->save();
        }

        if (($request->input('commission_api5_invoice_min_commission_type') == '1') || ($request->input('commission_api5_invoice_min_commission_type') == '2')) {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_agent'
            ], [
                'value' => $request->input('commission_invoice_api5_min_sum_agent')
            ]);
            $commission->value = $request->input('commission_invoice_api5_min_sum_agent');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_service'
            ], [
                'value' => $request->input('commission_invoice_api5_min_sum_service')
            ]);
            $commission->value = $request->input('commission_invoice_api5_min_sum_service');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_sum_grow'
            ], [
                'value' => $request->input('commission_invoice_api5_min_sum_grow')
            ]);
            $commission->value = $request->input('commission_invoice_api5_min_sum_grow');
            $commission->save();
        }
        if ($request->input('commission_api5_invoice_min_commission_type') == '2') {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_COMMISSION,
                'property' => 'min_commission_service'
            ], [
                'value' => $request->input('commission_invoice_api5_min_commission_service')
            ]);
            $commission->value = $request->input('commission_invoice_api5_min_commission_service');
            $commission->save();
        }

        if (($request->input('commission_api3_withdrawal_min_commission_type') == '1') || ($request->input('commission_api3_withdrawal_min_commission_type') == '2')) {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_agent'
            ], [
                'value' => $request->input('commission_withdrawal_api3_min_sum_agent')
            ]);
            $commission->value = $request->input('commission_withdrawal_api3_min_sum_agent');
            $commission->save();

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_service'
            ], [
                'value' => $request->input('commission_withdrawal_api3_min_sum_service')
            ]);

            $commission->value = $request->input('commission_withdrawal_api3_min_sum_service');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_grow'
            ], [
                'value' => $request->input('commission_withdrawal_api3_min_sum_grow')
            ]);
            $commission->value = $request->input('commission_withdrawal_api3_min_sum_grow');
            $commission->save();
        }
        if ($request->input('commission_api3_withdrawal_min_commission_type') == '2') {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API3,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_commission_service'
            ], [
                'value' => $request->input('commission_withdrawal_api3_commission_service')
            ]);
            $commission->value = $request->input('commission_withdrawal_api3_commission_service');
            $commission->save();
        }

        if (($request->input('commission_api5_withdrawal_min_commission_type') == '1') || ($request->input('commission_api5_withdrawal_min_commission_type') == '2')) {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_agent'
            ], [
                'value' => $request->input('commission_withdrawal_api5_min_sum_agent')
            ]);
            $commission->value = $request->input('commission_withdrawal_api5_min_sum_agent');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_service'
            ], [
                'value' => $request->input('commission_withdrawal_api5_min_sum_service')
            ]);
            $commission->value = $request->input('commission_withdrawal_api5_min_sum_service');
            $commission->save();
            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_sum_grow'
            ], [
                'value' => $request->input('commission_withdrawal_api5_min_sum_grow')
            ]);
            $commission->value = $request->input('commission_withdrawal_api5_min_sum_grow');
            $commission->save();
        }
        if ($request->input('commission_api5_withdrawal_min_commission_type') == '2') {

            $commission = MerchantCommission::firstOrCreate([
                'merchant_id' => $merchant->id,
                'service_provider_id' => $request->input('service_provider_id'),
                'api_type' => MerchantCommission::API_TYPE_API5,
                'property_type' => MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION,
                'property' => 'min_commission_service'
            ], [
                'value' => $request->input('commission_withdrawal_api5_commission_service')
            ]);
            $commission->value = $request->input('commission_withdrawal_api5_commission_service');
            $commission->save();
        }
    }

    protected function getCommissionJson($name, $request)
    {
        $obj = array();
        foreach ($request->input($name) as $key => $value) {
            $obj[$value] = [
                'type' => $request->input($name . '-type')[$key],
                'value' => floatval($request->input($name . '-value')[$key]),
            ];
        }
        return $obj;
    }

    protected function getAvalableCurrencies()
    {
        $available_currency = json_decode(Property::getProperty('currency', 0, 'available_currency', '[]'), true);
        return $available_currency;
    }

    protected function arrayRemoveEmpty($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->arrayRemoveEmpty($haystack[$key]);
            }

            if (empty($haystack[$key]) && ($haystack[$key] !== '0')) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }
}
