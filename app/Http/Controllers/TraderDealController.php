<?php

namespace App\Http\Controllers;

use App\Models\Trader;
use App\Models\TraderAd;
use App\Models\TraderCurrency;
use App\Models\TraderDeal;
use App\Models\TraderDealHistory;
use App\Models\TraderDealMessage;
use App\Models\TraderFiat;
use App\Models\TraderPaymentSystem;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Libs\Api\Trader as TraderClient;

class TraderDealController extends Controller
{

    private $traderClient;

    public function __construct(
        TraderClient $traderClient
    ) {
        $this->traderClient = $traderClient;
    }

    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date', null);
        $to_date = $request->input('to_date', null);
        $currencyId = $request->input('currency_id', null);
        $fiatId = $request->input('fiat_id', null);
        $status = $request->input('status', null);
        $paymentSystemId = $request->input('payment_system_id', null);
        $traderId = $request->input('trader_id', null);
        $type = $request->input('type', null);

        $messages = [];
        $rules = [
            'currency_id' => 'nullable|numeric',
            'fiat_id' => 'nullable|numeric',
            'status' => 'nullable|numeric',
            'payment_system_id' => 'nullable|numeric',
            'trader_id' => 'nullable|numeric',
            'type' => 'nullable|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $dealListQuery = TraderDeal::orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $dealListQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $dealListQuery->where('created_at', '<=', $to_date);
        }

        if (!is_null($status)) {
            $dealListQuery->where('status', '=', $status);
        }

        if (!is_null($currencyId)) {
            $dealListQuery->where('currency_id', '=', $currencyId);
        }

        if (!is_null($fiatId)) {
            $dealListQuery->where('fiat_id', '=', $fiatId);
        }

        if (!is_null($paymentSystemId)) {
            $dealListQuery->where('payment_system_id', '=', $paymentSystemId);
        }

        if (!is_null($traderId)) {
            $dealListQuery->where('trader_id', '=', $traderId);
        }

        if (!is_null($type)) {
            $dealListQuery->where('type', '=', $type);
        }

        $currencies = TraderCurrency::all()->pluck('name', 'id');
        $fiats = TraderFiat::all()->pluck('name', 'id');
        $paymentSystems = TraderPaymentSystem::all()->pluck('name', 'id');
        $traders = Trader::all()->pluck('name', 'id');

        $types = TraderAd::getTypes();
        $statuses = $this->traderClient->getTraderDealStatus();

        $dealList = $dealListQuery->paginate($limit)->withQueryString();
        return view('trader.deals', [
            'dealList' => $dealList,
            'fiats' => $fiats,
            'paymentSystems' => $paymentSystems,
            'traders' => $traders,
            'types' => $types,
            'statuses' => $statuses,
            'currencies' => $currencies,

            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'currency' => $currencyId,
            'fiat' => $fiatId,
            'status' => $status,
            'paymentSystem' => $paymentSystemId,
            'trader' => $traderId,
            'type' => $type,

        ]);
    }

    public function history(Request $request, $id)
    {
        $limit = config('app.default.pagination_limit');
        $dealHistoryQuery = TraderDealHistory::where('deal_id', '=', $id)->orderBy('created_at', 'desc');
        $statuses = $this->traderClient->getTraderDealStatus();
        $statusesAvalable = $this->traderClient->getTraderDealAvalableStatus();
        $customerType = $this->traderClient->getCustomerType();
        $status = $dealHistoryQuery->first()->status ?? null;
        $dealHistory = $dealHistoryQuery->paginate($limit)->withQueryString();
        foreach ($dealHistory as $history) {
            if ($history->customer_type == TraderDeal::SYSTEM) {
                $history->customer_name = $customerType[TraderDeal::SYSTEM]['label'];
            } elseif ($history->customer_type) {
                $instance = app($customerType[$history->customer_type]['instance']);
                $history->customer_name = $instance::where('id', '=', $history->customer_id)->first()->name ?? '';
            }
        }
        return view('trader.history', [
            'dealHistory' => $dealHistory,
            'statuses' => $statuses,
            'status' => $status,
            'customerType' => $customerType,
            'statusesAvalable' => $statusesAvalable[$status][TraderDeal::SD_USER] ?? [],
            'id' => $id
        ]);
    }

    public function status(Request $request, $id)
    {

        $description = $request->input('description', null);
        $status = $request->input('status', null);

        $messages = [];
        $rules = [
            'description' => 'nullable|string',
            'status' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $result = $this->traderClient->setTraderDealStatus(['status' => $status, 'deal_id' => $id, 'description' => $description, 'customer_id' => $request->user()->id, 'customer_type' => TraderDeal::SD_USER]);
        if (!$result) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Deal Status Change Error')]);
        }
        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('The Deal Status Change')]);
    }

    public function changeAmount(Request $request, $id){
        $deal = TraderDeal::where(['id' => $id])->firstOrFail();

        $amount_currency = $request->input('amount_currency');
        $amount_fiat = $request->input('amount_fiat');

        $messages = [
            'amount_fiat.required' => __('One of the amounts is required'),
            'amount_currency.required' => __('One of the amounts is required'),
        ];
        $rules = [
            'comment' => 'nullable|string',
        ];

        if (!$amount_currency) {
            $rules['amount_fiat'] = 'required|numeric';
        }
        if (!$amount_fiat) {
            $rules['amount_currency'] = 'required|numeric';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
                'message' => __('Amount has not been changed')
            ]);
        }

        if ($amount_currency > 0) {
            $deal->amount_currency = $amount_currency;
        }
        if ($amount_fiat > 0) {
            $deal->amount_fiat = $amount_fiat;
        }
        $deal->comment = $request->input('comment');
        $deal->save();

        return response()->json([
            'status' => true,
            'message' => __('Amount has been changed')
        ]);
    }

    public function detail(Request $request, $id)
    {
        $limit = config('app.default.pagination_limit');

        $deal = TraderDeal::where(['id' => $id])->firstOrFail();

        $dealHistoryQuery = TraderDealHistory::where('deal_id', '=', $id)->orderBy('created_at', 'desc');
        $statuses = $this->traderClient->getTraderDealStatus();
        $statusesAvalable = $this->traderClient->getTraderDealAvalableStatus();
        $customerType = $this->traderClient->getCustomerType();
        $status = $dealHistoryQuery->first()->status ?? null;
        $dealHistory = $dealHistoryQuery->paginate($limit)->withQueryString();

        foreach ($dealHistory as $history) {
            if ($history->customer_type == TraderDeal::SYSTEM) {
                $history->customer_name = $customerType[TraderDeal::SYSTEM]['label'];
            } elseif ($history->customer_type) {
                $instance = app($customerType[$history->customer_type]['instance']);
                $history->customer_name = $instance::where('id', '=', $history->customer_id)->first()->name ?? '';
            }
        }

        $dealMessageQuery = TraderDealMessage::where('deal_id', '=', $id)->orderBy('created_at', 'desc');

        $senderType = $this->traderClient->getCustomerType();

        $dealMessage = $dealMessageQuery->paginate($limit)->withQueryString();

        foreach ($dealMessage as $message) {
            if ($message->sender_type == TraderDeal::SYSTEM) {
                $message->sender_name = $senderType[TraderDeal::SYSTEM]['label'];
            } else {
                $instance = app($senderType[$message->sender_type]['instance']);
                $message->sender_name = $instance::where('id', '=', $message->sender_id)->first()->name ?? '';
            }
            if ($message->attachment_type) {
                $content = $this->traderClient->getFile(['name' => $message->msg]);
                $fh = fopen('php://memory', 'w+b');
                fwrite($fh, $content);
                $contentType = mime_content_type($fh);
                fclose($fh);
                $message->file = 'data: ' . $contentType . ';base64,' . base64_encode($content);
            }
        }

        $statuses = $this->traderClient->getTraderDealStatus();

        return view('trader.deal-detail', [
            'id' => $id,
            'deal' => $deal,
            'statuses' => $statuses,
            'status' => $status,
            'statusesAvalable' => $statusesAvalable[$status][TraderDeal::TRADER_USER] ?? [],

            'dealMessage' => $dealMessage,
            'senderType' => $senderType,
            'dealHistory' => $dealHistory,
            'customerType' => $customerType,
        ]);
    }

    public function messageSend(Request $request, $id)
    {
        $message = $request->input('message', null);

        $messages = [];
        $rules = [
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif',
            'message' => 'nullable|string',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($message) {
            TraderDealMessage::create([
                'attachment_type' => 0,
                'deal_id' => $id,
                'msg' => $message,
                'sender_id' => $request->user()->id,
                'sender_type' => TraderDeal::SD_USER,
            ]);
        }

        if ($request->hasFile('document')) {
            $file = $request->file('document');

            if ($file) {
                $filePath = $this->traderClient->saveFile($file, $file->getClientOriginalName());
            }

            if (isset($filePath) && $filePath) {
                TraderDealMessage::create([
                    'attachment_type' => 1,
                    'deal_id' => $id,
                    'msg' => $filePath,
                    'sender_id' => $request->user()->id,
                    'sender_type' => TraderDeal::SD_USER,
                ]);
            } else {
                return redirect()->back()->with(['alert-type' => 'alarm', 'message' => __('The Deal Message Send Fail')]);
            }
        }
        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('The Deal Message Send Success')]);
    }

    public function attachment(Request $request, $id)
    {
        $message = TraderDealMessage::where(['id' => $id])->where('attachment_type', '>', 0)->firstOrFail();

        $content = $this->traderClient->getFile(['name' => $message->msg]);
        $fh = fopen('php://memory', 'w+b');
        fwrite($fh, $content);
        $contentType = mime_content_type($fh);
        fclose($fh);

        return response($content, 200)->header('Content-type', $contentType);
    }

}
