<?php

namespace App\Exports;

use App\Http\Libs\Api\User as UserClient;
use App\Models\GrowTokenInvoice;
use App\Repositories\UserWithdrawalRepository;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserWithdrawalExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    private UserWithdrawalRepository $userWithdrawalRepository;
    private array $conditions;
    private $partner;
    private UserClient $userClient;

    public function __construct(
        UserWithdrawalRepository $userWithdrawalRepository,
        UserClient $userClient,
        array $conditions = [])
    {
        $this->userClient = $userClient;
        $this->userWithdrawalRepository = $userWithdrawalRepository;
        $this->conditions = $conditions;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $queue = $this->userWithdrawalRepository->invoices($this->conditions)
            ->orderBy('created_at', 'desc');

        $collection = $queue->get();
        foreach ($collection as $key=>$invoice){
            $result = $this->userClient->setStatusPayed(['id' => $invoice->id]);
            if(!$result){
                $collection->forget($key);
            }
        }
        return $collection;
    }

    public function map($invoice): array
    {
        return [
            $invoice->id,
            $invoice->user->email,
            $invoice->user->name,
            $invoice->address,
            price_format($invoice->amount2pay, 6),
            'GRT',
            $invoice->created_at,
        ];
    }

    public function headings(): array
    {
        return ["id", "email", "name", "address", "amount", "currency", "created_at"];
    }
}
