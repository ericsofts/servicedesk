<?php

namespace App\Exports;

use App\Models\Partner;
use App\Repositories\TransactionRepository;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionsExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    private TransactionRepository $transactionRepository;
    private array $conditions;
    private $partner;

    public function __construct(TransactionRepository $transactionRepository, array $conditions = [])
    {

        $this->transactionRepository = $transactionRepository;
        $this->conditions = $conditions;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->transactionRepository->transactions($this->conditions)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function map($invoice): array
    {
        return [
            $invoice->id,
            invoice_type($invoice->invoice_type),
            $invoice->created_at,
            price_format($invoice->amount),
            $invoice->invoice_type == 1 ? -price_format($invoice->amount) : $invoice->amount,
            $invoice->invoice_type == 1 ? -price_format($invoice->amount2pay) : $invoice->amount2pay,
            price_format($invoice->amount2grow),
            price_format($invoice->amount2service),
            price_format($invoice->amount2agent),
            !empty($invoice->merchant->agent) ? $invoice->merchant->agent->name : '',
            !empty($invoice->merchant) ? $invoice->merchant->name : '',
            !empty($invoice->merchant->agent->partner) ? $invoice->merchant->agent->partner->name : '',
            invoice_status($invoice->status),
            strtoupper($invoice->fiat_currency),
            !empty($invoice->service_provider_id) ? $invoice->service_provider->name : '',
        ];
    }

    public function headings(): array
    {
        return ["invoice_id", "type", "date", "volume", "amount", "amount2pay", "amount2grow", "amount2service", "amount2agent", "agent", "merchant","partner", "status", "fiat", "provider"];
    }
}
