<?php

namespace App\Exports;

use App\Lib\Chatex\ChatexApi;
use App\Models\Partner;
use App\Repositories\TransactionRepository;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionsChatexExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    private TransactionRepository $transactionRepository;
    private array $conditions;
    private $partner;

    public function __construct(TransactionRepository $transactionRepository, array $conditions = [])
    {

        $this->transactionRepository = $transactionRepository;
        $this->conditions = $conditions;
        $this->partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->transactionRepository->chatex_transactions($this->conditions)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function map($invoice): array
    {
        return [
            $invoice->payment_id,
            invoice_type($invoice->invoice_type),
            $invoice->created_at,
            price_format($invoice->amount),
            $invoice->invoice_type == 1 ? -price_format($invoice->amount) : $invoice->amount,
            $this->show_value($invoice) ? $invoice->invoice_type == 1 ? -price_format($invoice->amount2pay) : $invoice->amount2pay : '',
            $this->show_value($invoice) ? price_format($invoice->amount2grow) : '',
            price_format($invoice->amount2service),
            $this->show_value($invoice) ? price_format($invoice->amount2agent) : '',
            $this->show_value($invoice) && !empty($invoice->merchant->agent) ? $invoice->merchant->agent->name : '',
            $this->show_value($invoice) && !empty($invoice->merchant) ? $invoice->merchant->name : '',
            $invoice->fiat_currency,
            invoice_status($invoice->status)
        ];
    }

    public function headings(): array
    {
        return ["contact_id", "type", "date", "volume", "amount", "amount2pay", "amount2grow", "amount2service", "amount2agent", "agent", "merchant", "fiat", "status"];
    }

    private function show_value($invoice)
    {
        return !empty($invoice->merchant->agent->partner->id) && $invoice->merchant->agent->partner->id == $this->partner->id;
    }
}
