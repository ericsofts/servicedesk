<?php

namespace App\Observers;

use App\Models\LogEntityModel;
use App\Models\PaymentInvoice;
use Illuminate\Support\Facades\Auth;

class PaymentInvoiceObserver
{

    public function updating(PaymentInvoice $invoice)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($invoice->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $invoice->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($invoice->getDirty(), JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $invoice::class
        ]);
    }
}
