<?php

namespace App\Observers;


use App\Models\LogEntityModel;
use App\Models\Merchant;
use Illuminate\Support\Facades\Auth;


class MerchantObserver
{
    public function updating(Merchant $merchant)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($merchant->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $merchant->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($merchant->getDirty(),JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $merchant::class
        ]);
    }

    public function creating(Merchant $merchant)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => '',
            'after' => $merchant->toJson(JSON_FORCE_OBJECT),
            'different' => $merchant->toJson(JSON_FORCE_OBJECT),
            'type' => 'create',
            'model_instance' => $merchant::class
        ]);
    }
}
