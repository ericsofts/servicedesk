<?php

namespace App\Observers;

use App\Models\LogEntityModel;
use App\Models\MerchantProperty;
use Illuminate\Support\Facades\Auth;


class MerchantPropertyObserver
{
    public function updating(MerchantProperty $property)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($property->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $property->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($property->getDirty(),JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $property::class
        ]);
    }

    public function creating(MerchantProperty $property)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => '',
            'after' => $property->toJson(JSON_FORCE_OBJECT),
            'different' => $property->toJson(JSON_FORCE_OBJECT),
            'type' => 'create',
            'model_instance' => $property::class
        ]);
    }

}
