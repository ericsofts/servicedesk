<?php

namespace App\Observers;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\MerchantBalance;
use App\Models\MerchantInputInvoice;

class MerchantInputInvoiceObserver
{
    public function created(MerchantInputInvoice $invoice)
    {
        if($invoice->isDirty('status')
            && $invoice->status == MerchantInputInvoice::STATUS_PAYED
            && $invoice->payment_system == MerchantInputInvoice::PAYMENT_SYSTEM_MANUALLY
        ){
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_MERCHANT,
                $invoice->merchant_id,
                $invoice->amount,
                AccountingEntrie::TYPE_CREDIT,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_INPUT_INVOICE
            );
        }
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type)
    {

        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_MERCHANT:
                $merchantBalance = MerchantBalance::firstOrCreate(['merchant_id' => $invoice->merchant_id]);
                $result = $merchantBalance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
