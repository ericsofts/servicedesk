<?php

namespace App\Observers;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\LogEntityModel;
use App\Models\MerchantBalance;
use App\Models\MerchantWithdrawalInvoice;
use Illuminate\Support\Facades\Auth;

class MerchantWithdrawalInvoiceObserver
{
    public function created(MerchantWithdrawalInvoice $invoice)
    {
        if($invoice->isDirty('status')
            && $invoice->status == MerchantWithdrawalInvoice::STATUS_PAYED
            && $invoice->payment_system == MerchantWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY
        ){
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_MERCHANT,
                $invoice->merchant_id,
                $invoice->amount,
                AccountingEntrie::TYPE_DEBIT,
                AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL
            );
        }
    }

    public function updating(MerchantWithdrawalInvoice $invoice)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($invoice->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $invoice->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($invoice->getDirty(),JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $invoice::class
        ]);
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type)
    {
        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_MERCHANT:
                $balance = MerchantBalance::firstOrCreate(['merchant_id' => $invoice->merchant_id]);
                $result = $balance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
