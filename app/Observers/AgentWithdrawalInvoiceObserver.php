<?php

namespace App\Observers;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\AgentBalance;
use App\Models\AgentWithdrawalInvoice;

class AgentWithdrawalInvoiceObserver
{
    public function created(AgentWithdrawalInvoice $invoice)
    {
        if($invoice->isDirty('status')
            && $invoice->status == AgentWithdrawalInvoice::STATUS_PAYED
            && $invoice->payment_system == AgentWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY
        ){
            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_AGENT,
                $invoice->agent_id,
                $invoice->amount,
                AccountingEntrie::TYPE_DEBIT,
                AccountingEntrie::INVOICE_TYPE_AGENT_WITHDRAWAL
            );
        }
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type)
    {
        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_AGENT:
                $balance = AgentBalance::firstOrCreate(['agent_id' => $invoice->agent_id]);
                $result = $balance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
