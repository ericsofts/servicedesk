<?php

namespace App\Observers;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\SystemBalance;
use App\Models\SystemBalanceWithdrawalInvoice;

class SystemBalanceWithdrawalInvoiceObserver
{
    public function created(SystemBalanceWithdrawalInvoice $invoice)
    {
        if($invoice->isDirty('status')
            && $invoice->status == SystemBalanceWithdrawalInvoice::STATUS_PAYED
            && $invoice->payment_system == SystemBalanceWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY
        ){
            $system_balance = SystemBalance::where(['id' => $invoice->system_balance_id])->first();
            if(!$system_balance){
                return;
            }

            if($system_balance->name == SystemBalance::NAME_SERVICE){
                $user_type = AccountingEntrie::USER_TYPE_SERVICE;
            }else{
                $user_type = AccountingEntrie::USER_TYPE_GROW;
            }
            $this->addAccountingQueue(
                $invoice,
                $user_type,
                $invoice->agent_id,
                $invoice->amount,
                AccountingEntrie::TYPE_DEBIT,
                AccountingEntrie::INVOICE_TYPE_SYSTEM_BALANCE_WITHDRAWAL
            );
        }
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($user_type, $invoice, $amount, $type),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($user_type, $invoice, $amount, $type)
    {
        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        switch ($user_type) {
            case AccountingEntrie::USER_TYPE_SERVICE:
                $balance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_SERVICE]);
                $result = $balance->amount + $amount;
                break;
            case AccountingEntrie::USER_TYPE_GROW:
                $balance = SystemBalance::firstOrCreate(['name' => SystemBalance::NAME_GROW]);
                $result = $balance->amount + $amount;
                break;
            default:
                $result = 0;
        }
        return $result;
    }
}
