<?php

namespace App\Observers;

use App\Models\AccountingEntrie;
use App\Models\AccountingQueue;
use App\Models\ServiceProvidersBalance;
use App\Models\ServiceProvidersWithdrawalInvoice;

class ServiceProvidersWithdrawalInvoiceObserver
{
    public function created(ServiceProvidersWithdrawalInvoice $invoice)
    {
        if($invoice->isDirty('status')
            && $invoice->status == ServiceProvidersWithdrawalInvoice::STATUS_PAYED
            && $invoice->payment_system == ServiceProvidersWithdrawalInvoice::PAYMENT_SYSTEM_MANUALLY
        ){
            $service_provider = ServiceProvidersBalance::where(['provider_id' => $invoice->service_provider_id])->first();
            if(!$service_provider){
                return;
            }

            $this->addAccountingQueue(
                $invoice,
                AccountingEntrie::USER_TYPE_SERVICE,
                $invoice->service_provider_id,
                $invoice->amount,
                AccountingEntrie::TYPE_DEBIT,
                AccountingEntrie::INVOICE_TYPE_SERVICE_BALANCE_WITHDRAWAL
            );
        }
    }

    private function addAccountingQueue($invoice, $user_type, $user_id, $amount, $type, $invoice_type)
    {
        AccountingQueue::create([
            'type' => $type,
            'user_type' => $user_type,
            'user_id' => $user_id,
            'amount' => $amount,
            'balance_after' => $this->getBalanceAfter($amount, $type, $user_id),
            'invoice_id' => $invoice->id,
            'invoice_type' => $invoice_type,
            'status' => AccountingEntrie::STATUS_CREATE,
            'created_at' => $invoice->created_at,
            'updated_at' => $invoice->updated_at
        ]);
    }

    private function getBalanceAfter($amount, $type, $user_id)
    {
        if ($type == AccountingEntrie::TYPE_DEBIT) {
            $amount = $amount * (-1);
        }

        $balance = ServiceProvidersBalance::firstOrCreate(['provider_id' => $user_id]);
        $result = $balance->amount + $amount;

        return $result;
    }
}
