<?php

namespace App\Observers;


use App\Models\Agent;
use App\Models\LogEntityModel;
use Illuminate\Support\Facades\Auth;


class AgentObserver
{
    public function updating(Agent $agent)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($agent->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $agent->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($agent->getDirty(),JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $agent::class
        ]);
    }

    public function creating(Agent $agent)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => '',
            'after' => $agent->toJson(JSON_FORCE_OBJECT),
            'different' => $agent->toJson(JSON_FORCE_OBJECT),
            'type' => 'create',
            'model_instance' => $agent::class
        ]);
    }

}
