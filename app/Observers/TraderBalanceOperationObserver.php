<?php

namespace App\Observers;

use App\Models\LogEntityModel;
use App\Models\TraderBalanceOperation;
use App\Models\TraderBalanceQueue;
use App\Models\TraderBalance;
use Illuminate\Support\Facades\Auth;


class TraderBalanceOperationObserver
{

    public function created(TraderBalanceOperation $invoice)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($invoice->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $invoice->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($invoice->getDirty(), JSON_FORCE_OBJECT),
            'type' => 'create',
            'model_instance' => $invoice::class
        ]);

        $balance = TraderBalance::find($invoice->balance_id);

        $this->addAccountingQueue(
            $invoice->id, $invoice->trader_id, $balance, $invoice->amount, $invoice->type, $invoice->comment
        );
    }
    private function addAccountingQueue($invoice_id, $user_id, $balance, $amount, $type, $comment = '')
    {
        TraderBalanceQueue::create([
            'amount' => $amount,
            'balance' => $balance->amount,
            'balance_id' => $balance->id,
            'balance_type' => $balance->type,
            'currency' => $balance->currency,
            'currency_id' => $balance->currency_id,
            'description' => $comment,
            'invoice_id' => $invoice_id,
            'invoice_type' => TraderBalanceQueue::TRADER_OPERATION,
            'status' => TraderBalanceQueue::STATUS_WAITING,
            'trader_id' => $user_id,
            'type' => $type,
        ]);
    }
}
