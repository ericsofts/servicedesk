<?php

namespace App\Observers;

use App\Models\LogEntityModel;
use App\Models\MerchantCommission;
use Illuminate\Support\Facades\Auth;


class MerchantCommissionObserver
{
    public function updating(MerchantCommission $commission)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => json_encode($commission->getOriginal(), JSON_FORCE_OBJECT),
            'after' => $commission->toJson(JSON_FORCE_OBJECT),
            'different' => json_encode($commission->getDirty(),JSON_FORCE_OBJECT),
            'type' => 'update',
            'model_instance' => $commission::class
        ]);
    }

    public function creating(MerchantCommission $commission)
    {
        LogEntityModel::create([
            'sd_user_id' => Auth::user()->id,
            'before' => '',
            'after' => $commission->toJson(JSON_FORCE_OBJECT),
            'different' => $commission->toJson(JSON_FORCE_OBJECT),
            'type' => 'create',
            'model_instance' => $commission::class
        ]);
    }

}
