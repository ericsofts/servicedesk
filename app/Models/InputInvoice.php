<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputInvoice extends Model
{
    use HasFactory;

    const PAYMENT_SYSTEM_CHATEX = 1;
    const PAYMENT_SYSTEM_CHATEX_LBC = 2;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_USER_SELECTED = 5;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_CANCELED = 99;

    protected $fillable = [
        'user_id', 'amount', 'payment_system', 'payment_id', 'addition_info', 'status', 'payed', 'checked_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
