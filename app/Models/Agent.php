<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Agent extends Model
{
    use HasFactory;


    protected $fillable = [
        'name', 'email', 'password', 'status', 'partner_id'
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $balance = AgentBalance::firstOrNew(
                ['agent_id' =>  $model->id]
            );
            $balance->amount = 0;
            $balance->save();
        });
    }

    public function merchants()
    {
        return $this->hasMany(Merchant::class, 'id');
    }

    public function partner()
    {
        return $this->belongsTo(Partner::class, 'partner_id', 'id');
    }

    public function balance()
    {
        return $this->hasOne(AgentBalance::class, 'agent_id');
    }

    public function balance_total(): float
    {
        $balance = $this->balance->amount ?? 0;
        $totalWithdrawal = AgentWithdrawalInvoice::where([
            'agent_id' => $this->id,
        ])
            ->whereNotIn('status', [
                AgentWithdrawalInvoice::STATUS_PAYED,
                AgentWithdrawalInvoice::STATUS_CANCELED,
                AgentWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT
            ])
            ->sum('amount');
        // sum debit queue accounting
        $totalAccounting = AccountingQueue::where(
            [
                'invoice_type' => AccountingEntrie::INVOICE_TYPE_AGENT_WITHDRAWAL,
                'status' => AccountingEntrie::STATUS_CREATE,
                'user_type' => AccountingEntrie::USER_TYPE_AGENT,
                'user_id' => $this->id,
            ]
        )->whereIn('status', [AccountingQueue::STATUS_PROCESSING, AccountingQueue::STATUS_WAITING])
            ->sum('amount');
        return round($balance - $totalWithdrawal - $totalAccounting, 6);
    }

    public static function generatePassword(): string
    {
        $key = Str::random(8);
        return $key;
    }

}
