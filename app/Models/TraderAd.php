<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderAd extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const TYPE_BUY = 1;
    const TYPE_SELL = 0;
    const RATE_FIXED = 0;
    const RATE_PERCENTAGE = 1;

    protected $fillable = [
        'card_number',
        'currency_id',
        'fiat_id',
        'max_amount',
        'min_amount',
        'payment_system_id',
        'rate',
        'rate_id',
        'rate_type',
        'status',
        'trader_id',
        'type',
    ];

    public static function getTypes(): array
    {
        return [
            self::TYPE_BUY => 'Buy',
            self::TYPE_SELL => 'Sell',
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE => 'Enable',
            self::STATUS_INACTIVE => 'Disable',
        ];
    }

    public static function getRates(): array
    {
        return [
            self::RATE_FIXED => 'Fixed',
            self::RATE_PERCENTAGE => 'Percentage',
        ];
    }

}
