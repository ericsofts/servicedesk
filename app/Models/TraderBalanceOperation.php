<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderBalanceOperation extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 1;
    const TYPE_DEBIT = 0;

    protected $fillable = [
        'trader_id', 'balance_id', 'amount', 'sd_user_id', 'comment', 'type'
    ];

    public function sd_user()
    {
        return $this->hasOne(SdUser::class, 'id', 'sd_user_id');
    }

    public function trader()
    {
        return $this->hasOne(Trader::class, 'id', 'trader_id');
    }

}
