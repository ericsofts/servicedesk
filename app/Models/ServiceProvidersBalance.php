<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProvidersBalance extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'provider_id', 'amount'
    ];

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class, 'id', 'provider_id');
    }

}
