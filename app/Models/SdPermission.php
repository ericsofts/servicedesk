<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SdPermission extends Model
{
    use HasFactory;

    public function roles() {

        return $this->belongsToMany(SdRole::class,'sd_roles_permissions');

    }

    public function sd_users() {

        return $this->belongsToMany(SdUser::class,'sd_users_permissions');

    }
}
