<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderDealMessage extends Model
{
    use HasFactory;



    protected $fillable = [
        'attachment_type',
        'deal_id',
        'msg',
        'sender_id',
        'sender_type'
    ];

}
