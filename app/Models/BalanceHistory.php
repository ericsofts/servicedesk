<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceHistory extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    const CURRENCY_USD = 0;
    const CURRENCY_GROW_TOKEN = 1;

    const BALANCE_TYPE_NORMAL = 0;
    const BALANCE_TYPE_GROW_TOKEN = 1;

    protected $fillable = [
        'transaction_id', 'user_id', 'merchant_id', 'amount', 'type', 'status', 'done', 'description', 'input_invoice_id', 'payment_invoice_id',
        'currency', 'balance_type'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function input_invoice()
    {
        return $this->hasOne(InputInvoice::class, 'id', 'input_invoice_id');
    }

    public function payment_invoice()
    {
        return $this->hasOne(PaymentInvoice::class, 'id', 'payment_invoice_id');
    }
}
