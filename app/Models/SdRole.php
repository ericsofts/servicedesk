<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SdRole extends Model
{
    use HasFactory;

    public function permissions() {

        return $this->belongsToMany(SdPermission::class,'sd_roles_permissions');

    }

    public function sd_users() {

        return $this->belongsToMany(SdUser::class,'sd_users_roles');

    }
}
