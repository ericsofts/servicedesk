<?php

namespace App\Models;

use App\Lib\Chatex\ChatexApiLBC;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class MerchantWithdrawalInvoiceAttachment extends Model
{
    use HasFactory;


    protected $fillable = [
        'invoice_id', 'type', 'url'
    ];

    public function invoice()
    {
        return $this->hasOne(MerchantWithdrawalInvoice::class, 'id', 'invoice_id');
    }

    static public function addAttachments($invoice)
    {
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $chatexLBC = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $messages = $chatexLBC->contactMessages($invoice['payment_id']);
        $new_attachments = new Collection();
        if (!empty($messages['data']['message_list'])) {
            $attachments = self::where(['invoice_id' => $invoice->id])->get()->pluck('url');
            foreach ($messages['data']['message_list'] as $message) {
                if (!empty($message['attachment_url']) && str_contains($message['attachment_type'], 'image') && !$attachments->contains($message['attachment_url'])) {
                    $new_attachments->add(MerchantWithdrawalInvoiceAttachment::firstOrCreate([
                        'invoice_id' => $invoice->id,
                        'type' => $message['attachment_type'],
                        'url' => $message['attachment_url']
                    ]));
                }
            }
        }
        return $new_attachments;
    }

}
