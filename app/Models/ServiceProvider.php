<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Trader;
use App\Lib\Chatex\ChatexApi;

class ServiceProvider extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TRADER_PROVIDER = 'trader';
    const CHATEX_PROVIDER = 'chatex';
    const CRYPTO_PROVIDER = 'crypto';

    protected $fillable = [
        'name', 'type', 'status'
    ];

    public static function getStatuses()
    {
        return [
            self::STATUS_INACTIVE => 'InActive',
            self::STATUS_ACTIVE => 'Active'
        ];
    }

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $balance = ServiceProvidersBalance::firstOrNew(
                ['provider_id' =>  $model->id]
            );
            $balance->name = $model->name;
            $balance->amount = 0;
            $balance->save();
        });
    }

    public static function types()
    {
        return [
            ChatexApi::OBJECT_NAME,
            Trader::OBJECT_NAME
        ];
    }

    public static function allowCreateTypes()
    {
        return [
            Trader::OBJECT_NAME
        ];
    }

    public function balance()
    {
        return $this->hasOne(ServiceProvidersBalance::class, 'provider_id', 'id');
    }

}
