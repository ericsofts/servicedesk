<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoicesHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id', 'invoice_type', 'event_id', 'event', 'status', 'source_request', 'source_response','additional_info','instance' ,'comment'
    ];

    /**
     * @return \string[][]
     */
    public static function getEvents(): array
    {
        return [
            0 =>  [
                'event' => 'create',
                'label' => 'Create Invoice'
            ],
            1 =>  [
                'event' => 'select',
                'label' => 'Select Trader'
            ],
            2 =>  [
                'event' => 'apply',
                'label' => 'Apply Invoice'
            ],
            3 =>  [
                'event' => 'complete',
                'label' => 'Complete Invoice'
            ],
            4 =>  [
                'event' => 'cancel',
                'label' => 'Cancel Invoice'
            ],
            5 =>  [
                'event' => 'arbitrage',
                'label' => 'Arbitrage Invoice'
            ],
            6 =>  [
                'event' => 'confirm',
                'label' => 'Confirm Invoice'
            ],
            7 =>  [
                'event' => 'attachment',
                'label' => 'Add Attachment Invoice'
            ],
            8 =>  [
                'event' => 'check',
                'label' => 'Check Invoice'
            ],
            9 =>  [
                'event' => 'approve',
                'label' => 'Approve Invoice'
            ],

        ];
    }

    /**
     * @return \string[][]
     */
    public static function getStatuses(): array
    {
        return [
            0 =>  [
                'status' => 'fail',
                'label' => 'Fail'
            ],
            1 =>  [
                'status' => 'success',
                'label' => 'Success'
            ],
        ];
    }

    /**
     * @return \string[][]
     */
    public static function getSources(): array
    {
        return [
            0 =>  [
                'source' => 'user',
                'label' => 'By User'
            ],
            1 =>  [
                'source' => 'time',
                'label' => 'By Timeout'
            ],
            2 =>  [
                'source' => 'chatex',
                'label' => 'By Chatex'
            ],
            3 =>  [
                'source' => 'system',
                'label' => 'By System'
            ],
            4 =>  [
                'source' => 'merchant',
                'label' => 'By Merchant'
            ],
            5 =>  [
                'source' => 'admin',
                'label' => 'By Admin'
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     */
    public static function getStatus($id): string
    {
        return self::getStatuses()[$id]['status'];
    }

    /**
     * @param $id
     * @return string
     */
    public static function getSource($id): string
    {
        return self::getSources()[$id]['source'];
    }

    /**
     * @param $id
     * @return string
     */
    public static function getEvent($id)
    {
        return self::getEvents()[$id]['event'];
    }

    /**
     * @param $status
     * @return string
     */
    public static function getStatusRender($status): string
    {
        foreach (self::getStatuses() as $item){
            if($item['status'] == $status){
                return $item['label'];
            }
        }
        return '';
    }

    /**
     * @param $status
     * @return string
     */
    public static function getSourceRender($status): string
    {
        foreach (self::getSources() as $item){
            if($item['source'] == $status){
                return $item['label'];
            }
        }
        return '';
    }

    /**
     * @param $status
     * @return string
     */
    public static function getEventRender($status): string
    {
        foreach (self::getEvents() as $item){
            if($item['event'] == $status){
                return $item['label'];
            }
        }
        return '';
    }

    /**
     * @param $instance
     * @param $invoiceId
     * @param $invoiceType
     * @param $eventId
     * @param null $status
     * @param null $source_request
     * @param null $source_responce
     * @param null $additionalInfo
     * @param null $comment
     */
    public static function addHistoryRecord($instance, $invoiceId, $invoiceType, $eventId, $status = NULL, $source_request = NULL, $source_responce = NULL, $additionalInfo = NULL, $comment = NULL){
        self::create([
            'instance' => $instance,
            'invoice_id' => $invoiceId,
            'invoice_type' => $invoiceType,
            'event_id' => $eventId,
            'event' => self::getEvent($eventId),
            'status' => $status,
            'source_request' => $source_request,
            'source_response' => $source_responce,
            'additional_info'  => $additionalInfo,
            'comment'  => $comment,
        ]);
    }
}
