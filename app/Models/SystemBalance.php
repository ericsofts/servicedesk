<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemBalance extends Model
{
    use HasFactory;

    const NAME_GROW = 'grow';
    const NAME_SERVICE = 'service';

    protected $fillable = [
        'name', 'amount'
    ];

    public function balance_total(): float
    {
        $balance = $this->amount ?? 0;
        $totalWithdrawal = SystemBalanceWithdrawalInvoice::where([
            'system_balance_id' => $this->id,
        ])
            ->whereNotIn('status', [SystemBalanceWithdrawalInvoice::STATUS_PAYED, SystemBalanceWithdrawalInvoice::STATUS_CANCELED])
            ->sum('amount');
        // sum debit queue accounting

        if($this->name == self::NAME_SERVICE){
            $user_type = AccountingEntrie::USER_TYPE_SERVICE;
        }else{
            $user_type = AccountingEntrie::USER_TYPE_GROW;
        }

        $totalAccounting = AccountingQueue::where(
            [
                'invoice_type' => AccountingEntrie::INVOICE_TYPE_SYSTEM_BALANCE_WITHDRAWAL,
                'status' => AccountingEntrie::STATUS_CREATE,
                'user_type' => $user_type,
            ]
        )->whereIn('status', [AccountingQueue::STATUS_PROCESSING, AccountingQueue::STATUS_WAITING])
            ->sum('amount');

        return round($balance - $totalWithdrawal - $totalAccounting, 6);
    }
}
