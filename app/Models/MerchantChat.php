<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantChat  extends Model
{

    use HasFactory;

    const searchLikeItems = ['chat_name', 'chat_id'];

    protected $fillable = [
        'merchant_id', 'chat_name', 'chat_id'
    ];

}
