<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderDeal extends Model
{
    use HasFactory;

    const STATUS_CREATE = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_TRADER_FUNDED = 2;
    const STATUS_PAYMENT_COMPLETED_USER = 3;
    const STATUS_PAYMENT_COMPLETED_TRADER = 4;
    const STATUS_DISPUTED = 5;
    const STATUS_CANCELED_BY_REPLY_TIMEOUT = 94;
    const STATUS_CANCELED_TIMEOUT = 95;
    const STATUS_CANCELED_SD = 96;
    const STATUS_CANCELED_TRADER = 97;
    const STATUS_CANCELED_USER = 98;
    const STATUS_CANCELED = 99;

    const MERCHANT_USER = 0;
    const SD_USER = 1;
    const TRADER_USER = 2;
    const PS_USER = 3;
    const SYSTEM = 4;

    protected $fillable = [
        'ad_id',
        'amount',
        'amount_currency',
        'card_number',
        'currency_id',
        'payment_system_id',
        'fiat_id',
        'rate',
        'status',
        'type',
        'trader_id',
        'comment'
    ];

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'fiat_id');
    }

    public function payment_system()
    {
        return $this->hasOne(TraderPaymentSystem::class, 'id', 'payment_system_id');
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [
            self::STATUS_CANCELED_BY_REPLY_TIMEOUT,
            self::STATUS_CANCELED_TIMEOUT,
            self::STATUS_CANCELED_SD,
            self::STATUS_CANCELED_TRADER,
            self::STATUS_CANCELED_USER,
            self::STATUS_CANCELED,
        ]);
    }

}
