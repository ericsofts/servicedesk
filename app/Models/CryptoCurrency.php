<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CryptoCurrency extends Model
{
    use HasFactory;

    const STATUS_ACITVE = 1;
    const TYPE_CRYPTO = 0;
    const TYPE_FIAT = 1;

    protected $fillable = [
        'asset', 'name', 'type', 'precision', 'crypto_asset', 'kraken_asset', 'status', 'scan_url'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACITVE);
    }

    public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }

}
