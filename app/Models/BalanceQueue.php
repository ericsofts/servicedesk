<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BalanceQueue extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    const STATUS_WAITING = 0; //waiting
    const STATUS_DONE = 1; //done
    const STATUS_PROCESSING = 2; //processing
    const STATUS_CONFIRMATION = 3; //confirmation
    const STATUS_FAILED = 99; //failed

    protected $fillable = [
        'transaction_id', 'user_id', 'merchant_id', 'amount', 'type', 'status', 'done', 'description', 'input_invoice_id', 'payment_invoice_id'
    ];
}
