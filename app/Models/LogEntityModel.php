<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogEntityModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'sd_user_id', 'model_instance', 'before', 'after', 'different', 'type'
    ];
}
