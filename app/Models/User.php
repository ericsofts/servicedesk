<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'account_id',
        'secret_2fa',
        'enable_2fa'
    ];

    public function balance()
    {
        return $this->hasOne(Balance::class, 'user_id');
    }

    public function balance_histories()
    {
        return $this->hasMany(BalanceHistory::class, 'user_id');
    }
}
