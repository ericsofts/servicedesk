<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Merchant extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    const searchLikeItems = ['name', 'email'];


    protected $fillable = [
        'name', 'email', 'password', 'token', 'status', 'payment_key', 'api3_key', 'type', 'agent_id', 'api5_key', 'enable_2fa', 'email_verified_at'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $balance = MerchantBalance::firstOrNew(
                ['merchant_id' =>  $model->id]
            );
            $balance->amount = 0;
            $balance->save();
        });
    }

    public function balance()
    {
        return $this->hasOne(MerchantBalance::class, 'merchant_id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    public function withdrawal_invoices()
    {
        return $this->hasMany(MerchantWithdrawalInvoice::class, 'merchant_id');
    }

    public function merchant_property()
    {
        return $this->hasMany(MerchantProperty::class, 'merchant_id');
    }

    public function chat()
    {
        return $this->hasOne(MerchantChat::class, 'merchant_id');
    }

    public function commissions()
    {
        return $this->hasMany(MerchantCommission::class, 'merchant_id');
    }

    public function balance_total(): float
    {
        $balance = $this->balance->amount ?? 0;
        $totalWithdrawal = MerchantWithdrawalInvoice::where([
            'merchant_id' => $this->id,
        ])
            ->whereNotIn('status', [
                MerchantWithdrawalInvoice::STATUS_PAYED,
                MerchantWithdrawalInvoice::STATUS_CANCELED,
                MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT
            ])
            ->sum('amount');
        // sum debit queue accounting
        $totalAccounting = AccountingQueue::where(
            [
                'invoice_type' => AccountingEntrie::INVOICE_TYPE_MERCHANT_WITHDRAWAL,
                'status' => AccountingEntrie::STATUS_CREATE,
                'user_type' => AccountingEntrie::USER_TYPE_MERCHANT,
                'user_id' => $this->id,
            ]
        )->whereIn('status', [AccountingQueue::STATUS_PROCESSING, AccountingQueue::STATUS_WAITING])
            ->sum('amount');
        return round($balance - $totalWithdrawal - $totalAccounting, 6);
    }

    public static function generatePassword(): string
    {
        $key = Str::random(8);
        return $key;
    }

    public static function generateApi3Key(): string
    {
        $key = Str::random(128);
        if (self::where('api3_key', '=', $key)->exists()) {
            return self::generateApi3Key();
        }

        return $key;
    }

    public static function generateApi5Key(): string
    {
        $key = Str::random(128);
        if (self::where('api5_key', '=', $key)->exists()) {
            return self::generateApi5Key();
        }

        return $key;
    }

    public static function generatePaymentKey(): string
    {
        $key = Str::random(64);
        if (self::where('payment_key', '=', $key)->exists()) {
            return self::generatePaymentKey();
        }

        return $key;
    }

    public function available_providers()
    {
        return $this->belongsToMany(ServiceProvider::class, 'merchant_service_providers', 'merchant_id', 'service_provider_id');
    }

    public function getFeeCommission($merchant_id,
                                     $amount,
                                     $api_type = MerchantCommission::API_TYPE_DEFAULT,
                                     $type_commission = MerchantCommission::PROPERTY_TYPE_COMMISSION,
                                     $service_provider_id = null)
    {
        $commissions = $this->getAllCommissions($merchant_id, $api_type, $type_commission, $service_provider_id);

        $merchant = self::where(['id' => $merchant_id])->first();
        if (!empty($merchant) && $merchant->agent_id) {
            $agent_commission = $this->getCommission($commissions, 'agent', $amount);
            $agent_percentage = $this->getCommissionSumPercentage($agent_commission, $amount);
        } else {
            $agent_commission = ['value' => 0, 'type' => 'percentage'];
            $agent_percentage = 0;
        }

        $grow_commission = $this->getCommission($commissions, 'grow', $amount);
        $service_commission = $this->getCommission($commissions, 'service', $amount);
        $grow_percentage = $this->getCommissionSumPercentage($grow_commission, $amount);
        $service_percentage = $this->getCommissionSumPercentage($service_commission, $amount);

        $min_commission_type = $this->getMinCommissionType($commissions);

        if ($min_commission_type == MerchantCommission::MIN_COMMISSION_TYPE_1) {
            $min_sum_agent = $this->getSumMinCommission($commissions, 'min_sum_agent');
            $min_sum_grow = $this->getSumMinCommission($commissions, 'min_sum_grow');
            $min_sum_service = $this->getSumMinCommission($commissions, 'min_sum_service');
            $min_commission = $min_sum_agent + $min_sum_grow + $min_sum_service;
        } else {
            $min_commission = $this->getMinProperty($commissions, 'min_commission');
        }

        $fee['total_min'] = $min_commission;
        $fee['total_percentage'] = $agent_percentage + $grow_percentage + $service_percentage;

        $use_min_value = true;
        if ($amount / 100 * $fee['total_percentage'] > $fee['total_min']) {
            $use_min_value = false;
        }

        if ($min_commission_type == MerchantCommission::MIN_COMMISSION_TYPE_1) {
            $fee['agent'] = $this->getCommissionAmount($agent_commission, $amount, $min_sum_agent, $use_min_value);
            $fee['service'] = $this->getCommissionAmount($service_commission, $amount, $min_sum_service, $use_min_value);
            $fee['grow'] = $this->getCommissionAmount($grow_commission, $amount, $min_sum_grow, $use_min_value);
        } else if ($min_commission_type == MerchantCommission::MIN_COMMISSION_TYPE_2 && $use_min_value) {
            $fee['agent'] = $this->getCommissionAmount($agent_commission, $amount);
            $amount2 = $min_commission - $fee['agent']['amount'];
            if ($amount2 < 0) {
                $amount2 = 0;
            }
            $service_commission2 = $this->getMinProperty($commissions, 'min_commission_service');
            $fee['service'] = $this->getCommissionAmount(['value' => $service_commission2, 'type' => 'percentage'], $amount2);
            $min_sum_grow = $amount2 - $fee['service']['amount'];
        } else {
            $fee['agent'] = $this->getCommissionAmount($agent_commission, $amount);
            $fee['service'] = $this->getCommissionAmount($service_commission, $amount);
            $min_sum_grow = $min_commission - $fee['agent']['amount'] - $fee['service']['amount'];
        }

        if ($min_sum_grow < 0) {
            $min_sum_grow = 0;
        }

        $fee['grow'] = $this->getCommissionAmount($grow_commission, $amount, $min_sum_grow, $use_min_value);
        if ($use_min_value) {
            $fee['total'] = $fee['total_min'];
        } else {
            $fee['total'] = $fee['agent']['amount'] + $fee['service']['amount'] + $fee['grow']['amount'];
        }
        return $fee;
    }

    private function getCommissionSumPercentage($commission, $amount)
    {
        $percentage = 0;
        if ($commission['type'] == 'percentage') {
            $percentage = $commission['value'];
        } elseif ($commission['type'] == 'fixed' && $commission['value'] > 0) {
            $percentage = $commission['value'] * 100 / $amount;
        }

        return $percentage;
    }

    private function getCommissionAmount($commission, $amount, $min_commission = 0, $use_min_value = false)
    {
        $result['amount'] = 0;
        $result['commission'] = 0;
        $value = $commission['value'] ?? 0;
        if ($use_min_value) {
            $result['amount'] = $min_commission;
            $result['commission'] = 'min';
        } else {
            if ($commission['type'] == 'percentage') {
                $result['amount'] = round(($value / 100) * $amount, 2);
                $result['commission'] = $value;
            } elseif ($commission['type'] == 'fixed') {
                $result['amount'] = $value;
                $result['commission'] = 'fixed';
            }
        }
        return $result;
    }

    private function getMinProperty($commissions, $name)
    {
        if (isset($commissions[$name])) {
            $property = $commissions[$name];
        } else {
            $property = config('app.commission.' . $name);
        }
        return floatval($property);
    }

    private function getMinCommissionType($commissions)
    {
        return $commissions['min_commission_type'] ?? 0;
    }

    private function getSumMinCommission($commissions, $name)
    {
        return $commissions[$name] ?? 0;
    }

    private function getCommission($commissions, $name, $amount)
    {

        if (isset($commissions[$name])) {
            $property = $commissions[$name];
        } else {
            $property = config('app.commission.' . $name);
        }

        if(!is_array($property)){
            $result = json_decode(
                $property,
                true
            );
        }else{
            $result = $property;
        }
        krsort($result);
        foreach ($result as $key => $value) {
            if ($amount >= $key) {
                return $value;
            }
        }
        return false;
    }

    private function getAllCommissions($merchant_id, $api_type, $type_commission, $service_provider_id = null)
    {
        return MerchantCommission::getProperties($merchant_id, $api_type, $type_commission, $service_provider_id);
    }

    public function getRevertFeeCommission($merchant_id, $amount,
                                           $api_type = MerchantCommission::API_TYPE_DEFAULT,
                                           $type_commission = MerchantCommission::PROPERTY_TYPE_COMMISSION,
                                           $service_provider_id = null)
    {
        $low = MerchantProperty::getProperty($merchant_id, 'min_payment_invoice_amount', config('app.min_payment_invoice_amount'), $service_provider_id);
        $high = 1000000;
        for (; ;) {
            $iteral = $this->getAmount($merchant_id, $low, $api_type, $type_commission, $service_provider_id);

            if ($iteral[0] && ($iteral[0] > floatval($amount))) {
                $high = $low;
                $low = $high / 2;
            } elseif ($iteral[0] && ($iteral[0] < floatval($amount))) {
                $low = $low + ($high - $low) / 2;
            } else {
                $value = $iteral;
                break;
            }
            if (($high - $low) < 0.0001) {
                $value = $iteral;
                break;
            }
        }
        return $value[1];
    }

    private function getAmount($merchant_id, $amount, $api_type, $type_commission, $service_provider_id = null)
    {
        $fee = $this->getFeeCommission($merchant_id, $amount, $api_type, $type_commission, $service_provider_id);
        $amount2pay = $amount - $fee['total'];
        return [$amount2pay, $fee];
    }

}
