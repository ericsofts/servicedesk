<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantProperty extends Model
{
    use HasFactory;

    protected $fillable = [
        'merchant_id', 'property', 'value',
    ];

    public static function getProperty($merchant_id, $property, $default = null, $service_provider_id = null)
    {
        $where = ['merchant_id' => $merchant_id, 'property' => $property];
        if ($service_provider_id) {
            $where['service_provider_id'] = $service_provider_id;
        }
        $item = self::where($where)->first();
        return $item->value ?? $default;
    }

    /**
     * @param $merchant_id
     * @return array
     */
    public static function getProperties($merchant_id, $service_provider_id = null)
    {
        $where = ['merchant_id' => $merchant_id];
        if ($service_provider_id) {
            $where['service_provider_id'] = $service_provider_id;
        }

        $properties = [];
        foreach (self::where($where)->get() as $property) {
            $properties[$property->property] = $property->value;
        }
        return $properties;
    }
}
