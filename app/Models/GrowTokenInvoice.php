<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GrowTokenInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_IN_PROCESS = 2;
    const STATUS_CANCELED = 99;

    protected $fillable = [
        'user_id', 'amount', 'address', 'addition_info', 'status', 'payed', 'amount2commission', 'commission', 'amount2pay'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
