<?php

use App\Lib\LuhnAlgorithm;
use App\Models\BalanceHistory;
use Illuminate\Support\Carbon;

if (!function_exists('price_format')) {
    function price_format($price, $decimals = 2)
    {
        return number_format($price, $decimals, '.', '');
    }
}

if (!function_exists('user_status')) {
    function user_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('InActive');
                break;
            case 1:
                $status_name = __('Active');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('input_invoice_status')) {
    function input_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
            case 5:
                $status_name = __('User Selected');
                break;
            case 3:
                $status_name = __('Trader accepted');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('payment_invoice_status')) {
    function payment_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 98:
                $status_name = __('Canceled by timeout');
                break;
            case 97:
                $status_name = __('Canceled by revert');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
            case 3:
                $status_name = __('Trader accepted');
                break;
            case 5:
                $status_name = __('User selected');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('crypto_payment_invoice_status')) {
    function crypto_payment_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('crypto_payment_invoice_cancel')) {
    function crypto_payment_invoice_cancel($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('CANCELED_BY_USER');
                break;
            case 1:
                $status_name = __('CANCELED_BY_EXPIRED');
                break;
            case 3:
                $status_name = __('CANCELED_BY_MERCHANT');
                break;
            case 4:
                $status_name = __('CANCELED_BY_SD');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('input_invoice_status')) {
    function input_invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
            case 5:
                $status_name = __('User Selected');
                break;
            case 3:
                $status_name = __('Trader accepted');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('datetimeFormat')) {
    function datetimeFormat($datetime = null, $format = null)
    {
        if (!$datetime) {
            return;
        }
        if (!$format) {
            $format = 'd.m.Y H:i:s';
        }
        try {
            return Carbon::parse($datetime)->setTimezone(config('app.default.timezone'))->format($format);
        } catch (Throwable $t) {
            return $datetime;
        }
    }
}

if (!function_exists('payment_system')) {
    function payment_system($ps)
    {
        switch ($ps) {
            case 1:
                $name = __('Chatex');
                break;
            case 2:
                $name = __('Chatex LBC');
                break;
            case 3:
                $name = __('USDT ERC20');
                break;
            default:
                $name = $ps;
        }
        return $name;
    }
}

if (!function_exists('balance_currency')) {
    function balance_currency($id): string
    {
        switch ($id) {
            case BalanceHistory::CURRENCY_USD:
                $name = 'USD';
                break;
            case BalanceHistory::CURRENCY_GROW_TOKEN:
                $name = 'GROW';
                break;
            default:
                $name = '';
        }
        return $name;
    }
}

if (!function_exists('price_format_currency')) {
    function price_format_currency($amount, $currency = 0, $fmt = null): string
    {
        if ($currency == BalanceHistory::CURRENCY_GROW_TOKEN) {
            return balance_currency($currency) . ' ' . price_format($amount, 8);
        }else if(!$fmt){
            return balance_currency($currency) . ' ' . price_format($amount, 2);
        } else {
            return $fmt->formatCurrency(price_format($amount), balance_currency($currency));
        }
    }
}

if (!function_exists('balance_history_type')) {
    function balance_history_type($id): string
    {
        switch ($id) {
            case BalanceHistory::TYPE_DEBIT:
                $name = __('Debit');
                break;
            case BalanceHistory::TYPE_CREDIT:
                $name = __('Credit');
                break;
            default:
                $name = '';
        }
        return $name;
    }
}

if (!function_exists('invoice_type')) {
    function invoice_type($type)
    {
        $name = '';
        switch ($type) {
            case 0:
                $name = __('Payment invoice');
                break;
            case 1:
                $name = __('Merchant withdrawal');
                break;
            case 2:
                $name = __('Merchant input');
                break;
            case 3:
                $name = __('User input');
                break;
        }
        return $name;
    }
}

if (!function_exists('withdrawal_type')) {
    function withdrawal_type($type)
    {
        $name = '';
        switch ($type) {
            case 0:
                $name = __('Manually');
                break;
            case 1:
                $name = __('Chatex');
                break;
        }
        return $name;
    }
}

if (!function_exists('invoice_status')) {
    function invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 98:
                $status_name = __('Canceled by timeout');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 3:
                $status_name = __('Trader accepted');
                break;
            case 4:
                $status_name = __('In process');
                break;
            case 5:
                $status_name = __('User Selected');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('show_merchant_property')) {
    function show_merchant_property($property, $default=null)
    {
        if($default && empty($property)){
            $property = $default;
        }
        if($property){
            $value = json_decode($property, true);
            if(is_array($value)){
                return implode(', ', $value);
            }
        }

        return $property;
    }
}

if (!function_exists('mask_credit_card')) {
    function mask_credit_card($string, $replacement = "*")
    {
        $regex = '/(?:\d[ \t-]*?){13,19}/m';
        $matches = [];
        preg_match_all($regex, $string, $matches);

        if (!isset($matches[0]) || empty($matches[0])) {
            return $string;
        }
        $luhn = new LuhnAlgorithm();
        foreach ($matches as $matchGroup) {
            foreach ($matchGroup as $match) {
                $strippedMatch = preg_replace('/[^\d]/', '', $match);
                if (false === $luhn->isValid($strippedMatch)) {
                    continue;
                }
                $cardLength = strlen($strippedMatch);
                $replacement_string = substr($strippedMatch, 0, 6) . str_pad('', $cardLength - 10, $replacement) . substr($strippedMatch, -4);
                $string = str_replace($match, $replacement_string, $string);
            }
        }
        return $string;
    }
}
