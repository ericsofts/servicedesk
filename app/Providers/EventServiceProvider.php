<?php

namespace App\Providers;

use App\Models\Agent;
use App\Models\AgentWithdrawalInvoice;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantInputInvoice;
use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvidersWithdrawalInvoice;
use App\Models\SystemBalanceWithdrawalInvoice;
use App\Models\TraderBalanceOperation;
use App\Observers\AgentObserver;
use App\Observers\AgentWithdrawalInvoiceObserver;
use App\Observers\MerchantCommissionObserver;
use App\Observers\MerchantInputInvoiceObserver;
use App\Observers\MerchantObserver;
use App\Observers\MerchantPropertyObserver;
use App\Observers\MerchantWithdrawalInvoiceObserver;
use App\Observers\PaymentInvoiceObserver;
use App\Observers\ServiceProvidersWithdrawalInvoiceObserver;
use App\Observers\SystemBalanceWithdrawalInvoiceObserver;
use App\Observers\TraderBalanceOperationObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        MerchantInputInvoice::observe(MerchantInputInvoiceObserver::class);
        MerchantWithdrawalInvoice::observe(MerchantWithdrawalInvoiceObserver::class);
        PaymentInvoice::observe(PaymentInvoiceObserver::class);
        AgentWithdrawalInvoice::observe(AgentWithdrawalInvoiceObserver::class);
        SystemBalanceWithdrawalInvoice::observe(SystemBalanceWithdrawalInvoiceObserver::class);
        ServiceProvidersWithdrawalInvoice::observe(ServiceProvidersWithdrawalInvoiceObserver::class);
        Agent::observe(AgentObserver::class);
        Merchant::observe(MerchantObserver::class);
        MerchantProperty::observe(MerchantPropertyObserver::class);
        MerchantCommission::observe(MerchantCommissionObserver::class);
        TraderBalanceOperation::observe(TraderBalanceOperationObserver::class);
    }
}
