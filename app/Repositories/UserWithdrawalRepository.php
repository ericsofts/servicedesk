<?php

namespace App\Repositories;

use App\Models\GrowTokenInvoice;

class UserWithdrawalRepository
{

    public function invoices($conditions = [])
    {
        $status = $conditions['status'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;

        $invoiceQuery = GrowTokenInvoice::with(['user']);

        if ($from_date) {
            $invoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $invoiceQuery->where('created_at', '<=', $to_date);
        }

        if (!is_null($status)) {
            $invoiceQuery->where('status', '=', $status);
        }

        return $invoiceQuery;
    }

}
