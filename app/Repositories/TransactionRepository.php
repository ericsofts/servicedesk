<?php


namespace App\Repositories;


use App\Lib\Chatex\ChatexApi;
use App\Models\InputInvoice;
use App\Models\Merchant;
use App\Models\MerchantInputInvoice;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\Partner;
use App\Models\PaymentInvoice;
use Illuminate\Support\Facades\DB;

class TransactionRepository
{

//    private PaymentInvoice $paymentInvoice;
//    private MerchantWithdrawalInvoice $merchantWithdrawalInvoice;
//    private MerchantInputInvoice $inputInvoice;
//
//    public function __construct(
//        PaymentInvoice $paymentInvoice,
//        MerchantWithdrawalInvoice $merchantWithdrawalInvoice,
//        MerchantInputInvoice $inputInvoice
//    )
//    {
//        $this->paymentInvoice = $paymentInvoice;
//        $this->merchantWithdrawalInvoice = $merchantWithdrawalInvoice;
//        $this->inputInvoice = $inputInvoice;
//    }

    public function transactions($conditions = [])
    {
        $invoice_types = $conditions['invoice_types'] ?? null;
        $withdrawal_types = $conditions['withdrawal_types'] ?? null;
        $status = $conditions['status'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_selected = $conditions['merchant_selected'] ?? null;
        $service_provider_id = $conditions['service_provider_id'] ?? null;
        $provider_selected = $conditions['provider_selected'] ?? [];
        $fiats = $conditions['fiats'] ?? null;
        $isMerchantFiltered = $conditions['isMerchantFiltered'] ?? null;

        $paymentInvoiceQuery = PaymentInvoice::select([
            'id',
            'created_at',
            'amount',
            'amount2pay',
            'amount2grow',
            'amount2service',
            'amount2agent',
            'merchant_id',
            'api_type',
            'status',
            'fiat_currency',
            DB::raw("0 AS invoice_type"),
            'payment_id',
            'service_provider_id'
        ]);

        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::select([
            'id',
            'created_at',
            'amount',
            DB::raw("amount as amount2pay"),
            'amount2grow',
            'amount2service',
            'amount2agent',
            'merchant_id',
            'api_type',
            'status',
            'fiat_currency',
            DB::raw("1 as invoice_type"),
            'payment_id',
            'service_provider_id'
        ]);

        $merchantInputInvoiceQuery = MerchantInputInvoice::select([
            'id',
            'created_at',
            'amount',
            DB::raw("amount as amount2pay"),
            DB::raw("0 as amount2grow"),
            DB::raw("0 as amount2service"),
            DB::raw("0 as amount2agent"),
            'merchant_id',
            DB::raw("NULL as api_type"),
            'status',
            DB::raw("NULL as fiat_currency"),
            DB::raw("2 as invoice_type"),
            DB::raw("NULL as payment_id"),
            DB::raw("NULL as service_provider_id"),
        ]);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
        }
        //$paymentInvoiceQuery->whereNotIn('api_type', [PaymentInvoice::API_TYPE_2]);
        $paymentInvoiceQuery->whereNotNull('merchant_id');

        if($withdrawal_types){
            $merchantWithdrawalInvoiceQuery->whereIn('payment_system', $withdrawal_types);
        }

        if ($merchant_selected || $isMerchantFiltered) {
            $paymentInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantWithdrawalInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantInputInvoiceQuery->whereIn('merchant_id', $merchant_selected);
        }

        if ($service_provider_id) {
            $paymentInvoiceQuery->where('service_provider_id', $service_provider_id);
            $merchantWithdrawalInvoiceQuery->where('service_provider_id', $service_provider_id);
        }

        if (count($provider_selected)) {
            $paymentInvoiceQuery->whereIn('service_provider_id', $provider_selected);
            $merchantWithdrawalInvoiceQuery->whereIn('service_provider_id', $provider_selected);
        }

        if (count($fiats)) {
            $paymentInvoiceQuery->whereIn('fiat_currency', $fiats);
            $merchantWithdrawalInvoiceQuery->whereIn('fiat_currency', $fiats);
            //$merchantInputInvoiceQuery->whereIn('fiat_currency', $fiats);
        }

        if (!is_null($status)) {
            $paymentInvoiceQuery->where('status', '=', $status);
            $merchantWithdrawalInvoiceQuery->where('status', '=', $status);
            $merchantInputInvoiceQuery->where('status', '=', $status);
        }

        if (empty($invoice_types)) {
            $paymentInvoiceQuery->union($merchantWithdrawalInvoiceQuery);
            if(!count($fiats) || !count($provider_selected)){
                $paymentInvoiceQuery->union($merchantInputInvoiceQuery);
            }
            return $paymentInvoiceQuery;
        }

        $union = null;
        if (in_array(0, $invoice_types)) {
            $union = $paymentInvoiceQuery;
        }
        if (in_array(1, $invoice_types)) {
            if ($union) {
                $union->union($merchantWithdrawalInvoiceQuery);
            } else {
                $union = $merchantWithdrawalInvoiceQuery;
            }
        }

        if (in_array(2, $invoice_types) && (!count($fiats) && !count($provider_selected))) {
            if ($union) {
                $union->union($merchantInputInvoiceQuery);
            } else {
                $union = $merchantInputInvoiceQuery;
            }
        }
        return $union;
    }

    public function transactions_total($conditions = [])
    {
        $invoice_types = $conditions['invoice_types'] ?? null;
        $withdrawal_types = $conditions['withdrawal_types'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_selected = $conditions['merchant_selected'] ?? null;
        $service_provider_id = $conditions['service_provider_id'] ?? null;
        $provider_selected = $conditions['provider_selected'] ?? [];
        $fiats = $conditions['fiats'] ?? null;
        $isMerchantFiltered = $conditions['isMerchantFiltered'] ?? null;

        $paymentInvoiceQuery = PaymentInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantInputInvoiceQuery = MerchantInputInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
        }

        $paymentInvoiceQuery->whereNotNull('merchant_id');

        if($withdrawal_types){
            $merchantWithdrawalInvoiceQuery->whereIn('payment_system', $withdrawal_types);
        }

        if ($merchant_selected || $isMerchantFiltered) {
            $paymentInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantWithdrawalInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantInputInvoiceQuery->whereIn('merchant_id', $merchant_selected);
        }

        if ($service_provider_id) {
            $paymentInvoiceQuery->where('service_provider_id', $service_provider_id);
            $merchantWithdrawalInvoiceQuery->where('service_provider_id', $service_provider_id);
        }

        if (count($provider_selected)) {
            $paymentInvoiceQuery->whereIn('service_provider_id', $provider_selected);
            $merchantWithdrawalInvoiceQuery->whereIn('service_provider_id', $provider_selected);
        }

        if (count($fiats)) {
            $paymentInvoiceQuery->whereIn('fiat_currency', $fiats);
            $merchantWithdrawalInvoiceQuery->whereIn('fiat_currency', $fiats);
//            $merchantInputInvoiceQuery->whereIn('fiat_currency', $fiats);
        }

        $amount = $volume = $amount2pay = $amount2grow = $amount2service = $amount2agent = 0;
        if (!$invoice_types || in_array(0, $invoice_types)) {
            $totalPaymentInvoice = $paymentInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount2pay) as total_amount2pay'),
                DB::raw('SUM(amount2service) as total_amount2service'),
                DB::raw('SUM(amount2grow) as total_amount2grow'),
                DB::raw('SUM(amount2agent) as total_amount2agent'),
            )
                ->first();
            $amount += $totalPaymentInvoice->total_amount;
            $volume += $totalPaymentInvoice->total_amount;
            $amount2pay += $totalPaymentInvoice->total_amount2pay;
            $amount2grow += $totalPaymentInvoice->total_amount2grow;
            $amount2service += $totalPaymentInvoice->total_amount2service;
            $amount2agent += $totalPaymentInvoice->total_amount2agent;
        }
        if (!$invoice_types || in_array(1, $invoice_types)) {
            $totalMerchantWithdrawalInvoice = $merchantWithdrawalInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount) as total_amount2pay'),
                DB::raw('SUM(amount2service) as total_amount2service'),
                DB::raw('SUM(amount2grow) as total_amount2grow'),
                DB::raw('SUM(amount2agent) as total_amount2agent'),
            )
                ->first();
            $amount -= $totalMerchantWithdrawalInvoice->total_amount;
            $volume += $totalMerchantWithdrawalInvoice->total_amount;
            $amount2pay -= $totalMerchantWithdrawalInvoice->total_amount2pay;
            $amount2grow += $totalMerchantWithdrawalInvoice->total_amount2grow;
            $amount2service += $totalMerchantWithdrawalInvoice->total_amount2service;
            $amount2agent += $totalMerchantWithdrawalInvoice->total_amount2agent;
        }

        if ((!$invoice_types || in_array(2, $invoice_types)) && (!count($fiats) && !count($provider_selected))) {
            $totalPMerchantInputInvoice = $merchantInputInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount) as total_amount2pay'),
                DB::raw('0 as total_amount2service'),
                DB::raw('0 as total_amount2grow'),
                DB::raw('0 as total_amount2agent'),
            )
                ->first();
            $amount += $totalPMerchantInputInvoice->total_amount;
            $volume += $totalPMerchantInputInvoice->total_amount;
            $amount2pay += $totalPMerchantInputInvoice->total_amount2pay;
            $amount2grow += $totalPMerchantInputInvoice->total_amount2grow;
            $amount2service += $totalPMerchantInputInvoice->total_amount2service;
            $amount2agent += $totalPMerchantInputInvoice->total_amount2agent;
        }

        return [
            'amount' => $amount,
            'volume' => $volume,
            'amount2pay' => $amount2pay,
            'amount2grow' => $amount2grow,
            'amount2service' => $amount2service,
            'amount2agent' => $amount2agent,
        ];
    }

    public function chatex_transactions($conditions = [])
    {
        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $merchants = Merchant::whereHas('agent', function ($query) use ($partner) {
            $query->where('partner_id', $partner->id);
        })->get();

        $status = $conditions['status'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_selected = $conditions['merchant_selected'] ?? null;
        $service_provider_id = $conditions['service_provider_id'] ?? null;
        $chatex_merchant_selected = $not_chatex_merchant_selected = [];

        foreach ($merchant_selected as $k => $v) {
            if (!$merchants->get($v)) {
                $not_chatex_merchant_selected[] = $v;
            } else {
                $chatex_merchant_selected[] = $v;
            }
        }

        $paymentInvoiceQuery = PaymentInvoice::select([
            'id',
            'created_at',
            'amount',
            'amount2pay',
            'amount2grow',
            'amount2service',
            'amount2agent',
            'merchant_id',
            'api_type',
            'status',
            DB::raw("0 AS invoice_type"),
            'payment_id',
            'fiat_currency'
        ]);

        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::select([
            'id',
            'created_at',
            'amount',
            DB::raw("amount as amount2pay"),
            'amount2grow',
            'amount2service',
            'amount2agent',
            'merchant_id',
            'api_type',
            'status',
            DB::raw("1 as invoice_type"),
            'payment_id',
            'fiat_currency'
        ]);

        $merchantInputInvoiceQuery = MerchantInputInvoice::select([
            'id',
            'created_at',
            'amount',
            DB::raw("amount as amount2pay"),
            DB::raw("0 as amount2grow"),
            DB::raw("0 as amount2service"),
            DB::raw("0 as amount2agent"),
            'merchant_id',
            DB::raw("NULL as api_type"),
            'status',
            DB::raw("2 as invoice_type"),
            DB::raw("NULL as payment_id"),
            DB::raw("NULL as fiat_currency"),
        ]);

        $userSdInputInvoiceQuery = InputInvoice::select([
            'id',
            'created_at',
            'amount',
            'amount2pay',
            'amount2grow',
            'amount2service',
            DB::raw("0 as amount2agent"),
            DB::raw("NULL as merchant_id"),
            DB::raw("NULL as api_type"),
            'status',
            DB::raw("3 as invoice_type"),
            'payment_id',
            'fiat_currency'
        ])->whereIn('payment_system', [InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_CHATEX]);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
            $userSdInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
            $userSdInputInvoiceQuery->where('created_at', '<=', $to_date);
        }
        $paymentInvoiceQuery->whereNotNull('merchant_id');
        $paymentInvoiceQuery->whereNotIn('api_type', [PaymentInvoice::API_TYPE_2]);

        if ($service_provider_id) {
            $paymentInvoiceQuery->where('service_provider_id', $service_provider_id);
            $merchantWithdrawalInvoiceQuery->where('service_provider_id', $service_provider_id);
            $userSdInputInvoiceQuery->where('service_provider_id', $service_provider_id);
        }

        if ($merchant_selected) {
            $paymentInvoiceQuery->where(function ($query) use ($chatex_merchant_selected, $not_chatex_merchant_selected) {
                $query->whereIn('merchant_id', $chatex_merchant_selected)
                    ->orWhere(function ($query) use ($not_chatex_merchant_selected) {
                        $query->whereNotNull('amount2service')
                            ->whereIn('merchant_id', $not_chatex_merchant_selected);
                    });
            });
            $merchantWithdrawalInvoiceQuery->where(function ($query) use ($chatex_merchant_selected, $not_chatex_merchant_selected) {
                $query->whereIn('merchant_id', $chatex_merchant_selected)
                    ->orWhere(function ($query) use ($not_chatex_merchant_selected) {
                        $query->whereNotNull('amount2service')
                            ->whereIn('merchant_id', $not_chatex_merchant_selected);
                    });
            });
            $merchantInputInvoiceQuery->whereIn('merchant_id', $chatex_merchant_selected);
            $userSdInputInvoiceQuery->whereIn('merchant_id', $chatex_merchant_selected);
        } else {
            $paymentInvoiceQuery->where(function ($query) use ($merchants) {
                $query->whereIn('merchant_id', $merchants->pluck('id')->toArray())
                    ->orWhereNotNull('amount2service');
            });

            $merchantWithdrawalInvoiceQuery->where(function ($query) use ($merchants) {
                $query->whereIn('merchant_id', $merchants->pluck('id')->toArray())
                    ->orWhereNotNull('amount2service');
            });

            $merchantInputInvoiceQuery->whereIn('merchant_id', $merchants->pluck('id')->toArray());
        }

        if (!is_null($status)) {
            $paymentInvoiceQuery->where('status', '=', $status);
            $merchantWithdrawalInvoiceQuery->where('status', '=', $status);
            $merchantInputInvoiceQuery->where('status', '=', $status);
            $userSdInputInvoiceQuery->where('status', '=', $status);
        }

        $paymentInvoiceQuery->union($merchantWithdrawalInvoiceQuery);
        $paymentInvoiceQuery->union($merchantInputInvoiceQuery);
        $paymentInvoiceQuery->union($userSdInputInvoiceQuery);

        return $paymentInvoiceQuery;
    }

    public function chatex_transactions_total($conditions = [])
    {
        $partner = Partner::where(['name' => ChatexApi::OBJECT_NAME])->first();
        $merchants = Merchant::whereHas('agent', function ($query) use ($partner) {
            $query->where('partner_id', $partner->id);
        })->get();

        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_selected = $conditions['merchant_selected'] ?? null;
        $service_provider_id = $conditions['service_provider_id'] ?? null;

        $paymentInvoiceQuery = PaymentInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantInputInvoiceQuery = MerchantInputInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $userSdInputInvoiceQuery = InputInvoice::where('status', '=', InputInvoice::STATUS_PAYED)->whereIn('payment_system', [InputInvoice::PAYMENT_SYSTEM_CHATEX_LBC, InputInvoice::PAYMENT_SYSTEM_CHATEX]);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
            $userSdInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
            $userSdInputInvoiceQuery->where('created_at', '<=', $to_date);
        }
        $paymentInvoiceQuery->whereNotIn('api_type', [PaymentInvoice::API_TYPE_2]);
        $paymentInvoiceQuery->whereNotNull('merchant_id');


        $paymentInvoiceQueryTotalC = clone $paymentInvoiceQuery;
        $merchantWithdrawalInvoiceQueryTotalC = clone $merchantWithdrawalInvoiceQuery;
        $merchantInputInvoiceQueryTotalC = clone $merchantInputInvoiceQuery;
        $userSdInputInvoiceQueryTotalC = clone $userSdInputInvoiceQuery;

        if ($merchant_selected) {
            $paymentInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantWithdrawalInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $merchantInputInvoiceQuery->whereIn('merchant_id', $merchant_selected);
            $userSdInputInvoiceQuery->where('id', '<',0);
        }

        if ($service_provider_id) {
            $paymentInvoiceQuery->where('service_provider_id', $service_provider_id);
            $merchantWithdrawalInvoiceQuery->where('service_provider_id', $service_provider_id);
            $userSdInputInvoiceQuery->where('service_provider_id', $service_provider_id);
        }

        $totalPaymentInvoice = $paymentInvoiceQuery->select(
            DB::raw('SUM(amount) as total_amount'),
            DB::raw('SUM(amount2service) as total_amount2service')
        )
            ->first();

        $totalPaymentInvoiceC = $paymentInvoiceQueryTotalC->select(
            DB::raw('SUM(amount2pay) as total_amount2pay'),
            DB::raw('SUM(amount2grow) as total_amount2grow'),
            DB::raw('SUM(amount2agent) as total_amount2agent'),
        )
            ->whereIn('merchant_id', $merchants->pluck('id')->toArray())
            ->first();

        $totalMerchantWithdrawalInvoice = $merchantWithdrawalInvoiceQuery->select(
            DB::raw('SUM(amount) as total_amount'),
            DB::raw('SUM(amount2service) as total_amount2service'),
        )
            ->whereNotNull('amount2service')
            ->first();

        $totalMerchantWithdrawalInvoiceC = $merchantWithdrawalInvoiceQueryTotalC->select(
            DB::raw('SUM(amount) as total_amount2pay'),
            DB::raw('SUM(amount2grow) as total_amount2grow'),
            DB::raw('SUM(amount2agent) as total_amount2agent'),
        )
            ->whereIn('merchant_id', $merchants->pluck('id')->toArray())
            ->whereNotNull('amount2service')
            ->first();

        $totalUserSdInputInvoice = $userSdInputInvoiceQuery->select(
            DB::raw('SUM(amount) as total_amount'),
            DB::raw('SUM(amount2service) as total_amount2service'),
        )
            ->whereNotNull('amount2service')
            ->first();

        $totalUserSdInputInvoiceC = $userSdInputInvoiceQueryTotalC->select(
            DB::raw('SUM(amount) as total_amount2pay'),
            DB::raw('SUM(amount2grow) as total_amount2grow'),
        );
        $totalUserSdInputInvoiceC = count($merchants->pluck('id')->toArray()) ? $totalUserSdInputInvoiceC->where('id', '<',0) : $totalUserSdInputInvoiceC;
        $totalUserSdInputInvoiceC = $totalUserSdInputInvoiceC->whereNotNull('amount2service')->first();

//        $totalPMerchantInputInvoice = $merchantInputInvoiceQuery->select(
//            DB::raw('SUM(amount) as total_amount'),
//            DB::raw('0 as total_amount2service'),
//        )
//            ->first();

//        $totalPMerchantInputInvoiceC = $merchantInputInvoiceQueryTotalC->select(
//            DB::raw('SUM(amount) as total_amount2pay'),
//            DB::raw('0 as total_amount2grow'),
//            DB::raw('0 as total_amount2agent'),
//        )
//            ->whereIn('merchant_id', $merchants->pluck('id')->toArray())
//            ->first();

        return [
            'amount' => $totalPaymentInvoice->total_amount - $totalMerchantWithdrawalInvoice->total_amount + $totalUserSdInputInvoice->total_amount,
            'volume' => $totalPaymentInvoice->total_amount + $totalMerchantWithdrawalInvoice->total_amount + $totalUserSdInputInvoice->total_amount,
            'amount2pay' => $totalPaymentInvoiceC->total_amount2pay - $totalMerchantWithdrawalInvoiceC->total_amount2pay + $totalUserSdInputInvoiceC->total_amount2pay,
            'amount2grow' => $totalPaymentInvoiceC->total_amount2grow + $totalMerchantWithdrawalInvoiceC->total_amount2grow + $totalUserSdInputInvoiceC->total_amount2grow,
            'amount2service' => $totalPaymentInvoice->total_amount2service + $totalMerchantWithdrawalInvoice->total_amount2service + $totalUserSdInputInvoice->total_amount2service,
            'amount2agent' => $totalPaymentInvoiceC->total_amount2agent + $totalMerchantWithdrawalInvoiceC->total_amount2agent,
        ];
    }


}
