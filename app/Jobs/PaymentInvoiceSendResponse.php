<?php

namespace App\Jobs;

use App\Models\PaymentInvoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PaymentInvoiceSendResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 20;

    protected $paymentInvoice;
    protected $response2server;

    /**
     * Create a new job instance.
     *
     * @param PaymentInvoice $paymentInvoice
     * @param $response2server
     */
    public function __construct(PaymentInvoice $paymentInvoice, $response2server)
    {
        $this->paymentInvoice = $paymentInvoice;
        $this->response2server = $response2server;
    }

    public function backoff()
    {
        return [10, 30, 30, 60];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if($this->paymentInvoice->merchant_server_url){
            $validator = Validator::make(['url' => $this->paymentInvoice->merchant_server_url], [
                'url' => 'required|url'
            ]);
            if (!$validator->fails()) {
                try{
                    $response = Http::withoutVerifying()->asForm()->post($this->paymentInvoice->merchant_server_url, $this->response2server);
                    Log::channel('payment_invoice_server_response')->info('PAYMENT_INVOICE_ID|' . $this->paymentInvoice->id.'|STATUS|' . $response->status(),
                        ['response' => $response->json(), 'response2server' => $this->response2server, 'attempt' => $this->attempts()]);
                    if($response->ok()){
                        $this->paymentInvoice->merchant_response_at = now();
                        $this->paymentInvoice->save();
                    }else{
                        $this->release($this->backoff()[$this->attempts()] ?? 60);
                    }
                }catch (\Exception $e){
                    Log::channel('payment_invoice_server_response')->info('PAYMENT_INVOICE_ID|' . $this->paymentInvoice->id.'|EXECPTION|STATUS|' .$e->getCode(),
                        ['response' => $e->getMessage(), 'response2server' => $this->response2server, 'attempt' => $this->attempts()]);
                    $this->release($this->backoff()[$this->attempts()] ?? 60);
                }

            }else{
                Log::channel('payment_invoice_server_response')->info('PAYMENT_INVOICE_ID|' . $this->paymentInvoice->id.'|ERROR|',
                ['errors' => $validator->errors()->toArray(), 'response2server' => $this->response2server, 'attempt' => $this->attempts()]);
                $this->fail();
            }
        }
    }
}
